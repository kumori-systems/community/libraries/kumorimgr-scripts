#!/bin/bash

###############################################################################
##  THIS SCRIPT IS TO BE RUN INSIDE A DOCKER CONTAINER IN THE SAME DOCKER    ##
##  NETWORK AS THE KIND CONTAINERS.                                          ##
##                                                                           ##
##  IT WILL TYPICALLY BE RUN WITH THE FOLLOWING COMMAND:                     ##
##                                                                           ##
##  docker run -it --rm \                                                    ##
##    --network=kind \                                                       ##
##    -v <runtime_config_dir>:/kumoridev/runtime \                           ##
##    <kumori_addon_installer_image>                                         ##
##                                                                           ##
##  NOTE: The above command assumes the container entrypoint will run this   ##
##        script.                                                            ##
##                                                                           ##
###############################################################################

# Installation files directories (inside Docker image)
KUMORI_DIR="/kumori"
ADDONS_DIR="${KUMORI_DIR}/addons-installer"
PROVISION_DIR="${ADDONS_DIR}/provision"

# Directory mounted from the Host with runtime configuration:
# - cluster kubeConfig file
# - cluster configuration variables file (variables.sh)
# - cluster ref domain certificates (certs/wildcard.<ref_domain>.crt and .key)
RUNTIME_CONFIG_DIR="${KUMORI_DIR}/runtime"


# Colors definitions for logging
source ${ADDONS_DIR}/defs-colors.sh

echo
echo -e $COL_BLUE"* Preparing configuration files..."$COL_DEFAULT
echo

# Prepare kubeConfig file for cluster access (from runtime cofiguration dir)
KUBECONFIG_HOST_FILE="${RUNTIME_CONFIG_DIR}/kubeConfig"
KUBECONFIG_LOCAL_DIR="/root/.kube"
KUBECONFIG_LOCAL_FILE="${KUBECONFIG_LOCAL_DIR}/config"

# Check required kubeConfig file exists
if [ ! -f "${KUBECONFIG_HOST_FILE}" ]; then
  echo -e $COL_RED"Couldn't find kubeConfig file in ${KUBECONFIG_HOST_FILE}. This file is mounted at runtime."$COL_DEFAULT
  echo -e $COL_RED"Aborting installation."$COL_DEFAULT
  echo
  exit 1
else
  echo "Found cluster kubeConfig file."
  mkdir -p ${KUBECONFIG_LOCAL_DIR}
  cp ${KUBECONFIG_HOST_FILE} ${KUBECONFIG_LOCAL_FILE}
fi


# Prepare configuration variables file (from runtime cofiguration dir)
CLUSTER_VARIABLES_HOST_FILE="${RUNTIME_CONFIG_DIR}/variables.sh"
CLUSTER_VARIABLES_LOCAL_FILE="${ADDONS_DIR}/variables.sh"

# Check required variables file exists
if [ ! -f "${CLUSTER_VARIABLES_HOST_FILE}" ]; then
  echo -e $COL_RED"Couldn't find configuration variables file in ${CLUSTER_VARIABLES_HOST_FILE}. This file is mounted at runtime."$COL_DEFAULT
  echo -e $COL_RED"Aborting installation."$COL_DEFAULT
  echo
  exit 1
else
  echo "Found cluster configuration variables file."
  cp ${CLUSTER_VARIABLES_HOST_FILE} ${CLUSTER_VARIABLES_LOCAL_FILE}
  source ${CLUSTER_VARIABLES_LOCAL_FILE}
fi

# Prepare reference domain certificates (from runtime configuration dir)
CLUSTER_CERTS_DIR_HOST="${RUNTIME_CONFIG_DIR}/clusterCerts"
CLUSTER_CERTS_DIR_LOCAL="${ADDONS_DIR}/clusterCerts"

# Check required variables file exists
if [ ! -f "${CLUSTER_CERTS_DIR_HOST}/wildcard.${REFERENCE_DOMAIN}.crt" ] || \
   [ ! -f "${CLUSTER_CERTS_DIR_HOST}/wildcard.${REFERENCE_DOMAIN}.key" ]
then
  echo "Couldn't find reference domain certificate files in directory ${CLUSTER_CERTS_DIR_HOST}."
  echo "Expected to find wildcard.${REFERENCE_DOMAIN}.crt and wildcard.${REFERENCE_DOMAIN}.key."
  echo "Aborting installation."
  exit 1
else
  echo "Found reference domain certificate files."
  mkdir -p ${CLUSTER_CERTS_DIR_LOCAL}
  cp ${CLUSTER_CERTS_DIR_HOST}/* ${CLUSTER_CERTS_DIR_LOCAL}/
fi

if [ "${ADMISSION_AUTHENTICATION_TYPE}" == "clientcertificate" ]; then

  # Prepare Admission certificates (from runtime configuration dir)
  ADMISSION_CERTS_DIR_HOST="${RUNTIME_CONFIG_DIR}/kumoriPki/admission"
  ADMISSION_CERTS_DIR_LOCAL="${ADDONS_DIR}/kumoriPki/admission"

  # Check required variables file exists
  if [ ! -f "${ADMISSION_CERTS_DIR_HOST}/cert.crt" ] || \
    [ ! -f "${ADMISSION_CERTS_DIR_HOST}/cert.key" ]
  then
    echo "Couldn't find Admission certificate files in directory ${ADMISSION_CERTS_DIR_HOST}."
    echo "Expected to find cert.crt and cert.key."
    echo "Aborting installation."
    exit 1
  else
    echo "Found Admission certificate files."
    mkdir -p ${ADMISSION_CERTS_DIR_LOCAL}
    cp ${ADMISSION_CERTS_DIR_HOST}/* ${ADMISSION_CERTS_DIR_LOCAL}/
  fi
fi

if [ "${CLUSTER_PKI_ROOT_CA_FILE}" != "" ]; then
  # Prepare ROOTCA (from runtime configuration dir)
  ROOTCA_DIR_HOST="${RUNTIME_CONFIG_DIR}/kumoriPki/rootCA"
  ROOTCA_DIR_LOCAL="${ADDONS_DIR}/kumoriPki/rootCA"

  ROOTCA_FILENAME=$(basename $CLUSTER_PKI_ROOT_CA_FILE)

  # Check required variables file exists
  if [ ! -f "${ROOTCA_DIR_HOST}/${ROOTCA_FILENAME}" ]
  then
    echo "Couldn't find ROOTCA certificate file in directory ${ROOTCA_DIR_HOST}."
    echo "Expected to find ca.crt."
    echo "Aborting installation."
    exit 1
  else
    echo "Found ROOTCA certificate file."
    mkdir -p ${ROOTCA_DIR_LOCAL}
    cp ${ROOTCA_DIR_HOST}/* ${ROOTCA_DIR_LOCAL}/
  fi
fi


if [ "${INSTALL_OPENSTACK_CLOUD_CONTROLLER}" = "true" ]; then
  if [ "${OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_FILE}" != "" ]; then

    OPENSTACK_CRED_DIR_HOST="${RUNTIME_CONFIG_DIR}/openstack-cloud-credentials"
    OPENSTACK_CRED_DIR_LOCAL="${ADDONS_DIR}/openstack-cloud-credentials"

    OPENSTACK_CRED_FILENAME=$(basename $OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_FILE)

    # Check required variables file exists
    if [ ! -f "${OPENSTACK_CRED_DIR_HOST}/${OPENSTACK_CRED_FILENAME}" ]
    then
      echo "Couldn't find Openstack credentials file ${OPENSTACK_CRED_DIR_HOST}/${OPENSTACK_CRED_FILENAME}."
      echo "Aborting installation."
      exit 1
    else
      echo "Found Openstack credentials file."
      mkdir -p ${OPENSTACK_CRED_DIR_LOCAL}
      cp ${OPENSTACK_CRED_DIR_HOST}/* ${OPENSTACK_CRED_DIR_LOCAL}/
    fi
  fi
fi

echo
echo -e $COL_BLUE"* Configuration prepared!"$COL_DEFAULT
echo

echo
echo -e $COL_BLUE"* Applying Kumori CoreDNS custom configuration..."$COL_DEFAULT
echo
kubectl apply -f ${PROVISION_DIR}/coredns/coredns-rbac.yaml
kubectl apply -f ${PROVISION_DIR}/coredns/coredns-configmap.yaml
echo
echo -e $COL_BLUE"* CoreDNS configuration patched!"$COL_DEFAULT
echo


echo
echo -e $COL_BLUE"* Starting Kumori Platform addon installation..."$COL_DEFAULT
echo
cd ${ADDONS_DIR}
./primary-master_2_install-addons.sh
cd ${SCRIPT_DIR}
echo
echo -e $COL_BLUE"* Kumori Platform addons installed."$COL_DEFAULT
echo
