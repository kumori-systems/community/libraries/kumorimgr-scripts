package main

import (
	"fmt"
	"hash/fnv"
	"os"
)

// Hash returns a hashed version of a given string
func Hash(source string) string {
	hf := fnv.New32()
	hf.Write([]byte(source))
	return string(fmt.Sprintf("%08x", hf.Sum32()))
	// return rand.SafeEncodeString(fmt.Sprint(hf.Sum32()))
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Missing name")
		os.Exit(1)
	}
	value := os.Args[1]
	hashed := Hash(value)
	fmt.Printf("%s\n", hashed)
}
