#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2021 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"

TIMESTAMP=$(date +"%Y%m%d_%H%M%S")


################################################################################
##  CALCULATE ENVOY CONFIGURATION FOR INGRESS
##
##  IMPORTANT: The generated configuration is only valid for Kumori Cluster v1.0.1
##
################################################################################
GENERATED_BASE_DIR="${SCRIPT_DIR}/result"
GENERATED_DIR="${GENERATED_BASE_DIR}/${TIMESTAMP}"

mkdir -p ${GENERATED_DIR}


# Prepare a basic Envoy configuration file with HTTPS configuration
HOSTNAME="$(hostname)"
sed \
  -e "s?{{HOSTNAME}}?${HOSTNAME}?g" \
  -e "s?{{CLUSTER_NAME}}?${RELEASE_NAME}?g" \
  -e "s?{{INGRESS_EXTERNAL_PORT}}?${INGRESS_EXTERNAL_PORT}?g" \
  -e "s?{{NODE_IP}}?${NODE_IP}?g" \
  ${SCRIPT_DIR}/provision/envoy/envoy-config-workers.yaml_${CLUSTER_VERSION}.template \
  > ${GENERATED_DIR}/envoy-kumori-config.yaml

# Add custom TCP ports configuration
if [ -n "${INGRESS_TCP_PORTS}" ]; then
  # Work is done converting to JSON for easier data handling
  ENVOY_CFG_JSON_FILE="${GENERATED_DIR}/envoy-config.json"
  ENVOY_CFG_JSON_FILE_TMP="${GENERATED_DIR}/envoy-config_tmp.json"

  cat ${GENERATED_DIR}/envoy-kumori-config.yaml | yq r -j - > ${ENVOY_CFG_JSON_FILE}

  # For each TCP port, add  new listener and a new cluster definition

  # Circuit breaker config slug, for readability
  CIRCUIT_BREAKER_CFG_SLUG="\"circuit_breakers\": { \"thresholds\": [ \
    { \"priority\": \"DEFAULT\", \"max_connections\": 1000000000, \"max_pending_requests\": 1000000000, \"max_requests\": 1000000000, \"max_retries\": 1000000000 }, \
    { \"priority\": \"HIGH\", \"max_connections\": 1000000000, \"max_pending_requests\": 1000000000, \"max_requests\": 1000000000, \"max_retries\": 1000000000 } \
  ]}"

  ACCESS_LOG_CFG_SLUG='"access_log": [ { "name": "envoy.access_loggers.file", "config": { "format": "[%START_TIME%] \"%REQ(:METHOD)% %REQ(X-ENVOY-ORIGINAL-PATH?:PATH)% %PROTOCOL%\" %RESPONSE_CODE% %RESPONSE_FLAGS% %BYTES_RECEIVED% %BYTES_SENT% %DURATION% %RESP(X-ENVOY-UPSTREAM-SERVICE-TIME)% \"%DOWNSTREAM_REMOTE_ADDRESS%\" \"%REQ(USER-AGENT)%\" \"%REQ(X-REQUEST-ID)%\" \"%REQ(:AUTHORITY)%\" \"%UPSTREAM_HOST%\"\n", "path": "/var/log/kumori-ingress-envoy-lb.log" } } ]'

  # for PORT in ${INGRESS_TCP_PORTS[@]}; do
  for i in "${!INGRESS_TCP_PORTS[@]}"; do
    PORT="${INGRESS_TCP_PORTS[$i]}"
    cat ${ENVOY_CFG_JSON_FILE} \
      | jq ".static_resources.listeners += [ {address:{socket_address:{address:\"0.0.0.0\",port_value:${PORT}}},filter_chains:[{filters:[{config:{cluster:\"tcp${PORT}\",stat_prefix:\"ingress_tcp\",idle_timeout:\"0s\"},name:\"envoy.tcp_proxy\"}]}],${ACCESS_LOG_CFG_SLUG},name:\"tcp${PORT}\"} ]" \
      | jq ".static_resources.clusters += [ {connect_timeout:\"0.25s\",${CIRCUIT_BREAKER_CFG_SLUG},eds_cluster_config:{eds_config:{path:\"/etc/envoy/eds_tcp${PORT}.conf\"},service_name:\"tcp${PORT}\"},health_checks:[{healthy_threshold:1,tcp_health_check:{},interval:\"5s\",timeout:\"1s\",unhealthy_threshold:1}],lb_policy:\"round_robin\",common_lb_config:{healthy_panic_threshold:{value:0.0}},name:\"tcp${PORT}\",type:\"EDS\"} ]" \
      > ${ENVOY_CFG_JSON_FILE_TMP}
    mv ${ENVOY_CFG_JSON_FILE_TMP} ${ENVOY_CFG_JSON_FILE}
  done

  cat ${ENVOY_CFG_JSON_FILE} \
    | yq r --prettyPrint - \
    > ${GENERATED_DIR}/envoy-kumori-config.yaml
  rm ${ENVOY_CFG_JSON_FILE}
fi

echo
echo "Envoy configuration file:"
echo
cat ${GENERATED_DIR}/envoy-kumori-config.yaml
echo



# Prepare Envoy EDS (Endpoint Discovery Service) configuration file
echo
echo "Preparing Envoy EDS files..."
echo

EDS_CONF_TEMPLATE_FILE="${SCRIPT_DIR}/provision/envoy/eds.conf.template"
EDS_CONF_TMP_FILE="${GENERATED_DIR}/eds.conf.tmp"
EDS_CONF_FILE="${GENERATED_DIR}/eds.conf"

# Create a base configuration file based on the EDS config template
cp ${EDS_CONF_TEMPLATE_FILE} ${EDS_CONF_FILE}

# Add a new cluster for HTTPS configuration
cat ${EDS_CONF_FILE} \
  | jq ".resources += [ {\"@type\":\"type.googleapis.com/envoy.api.v2.ClusterLoadAssignment\",cluster_name:\"ingressHTTPS\",endpoints:[{lb_endpoints:[]}]} ]" \
  > ${EDS_CONF_TMP_FILE}
mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}


# For each IP add a new "endpoint" element to the configuration (JSON).
# An endpoint has the following structure:
#   {
#     "endpoint": {
#       "address": {
#         "socket_address": {
#           "address": "10.0.18.56",
#           "port_value": 8443
#         }
#       },
#       "health_check_config": {     <==== ONLY NEEDED IF HEALTHCHECK PORT IS DIFFERENT
#         "port_value": 8080
#       }
#     }
#   }
# Add HTTPS endpoints to ingressHTTPS cluster
for ENDPOINT_IP in ${INGRESS_NODES_IPS[@]}; do
  echo "IP: ${ENDPOINT_IP}"
  # FOR REFERENCE: if healthcheck is different, use this replacement instead:
  #   | jq ".resources[0].endpoints[0].lb_endpoints += [ { endpoint: { address: { socket_address: { address: \"${ENDPOINT_IP}\", port_value: ${INGRESS_INTERNAL_PORT} } }, health_check_config: { port_value: 8080 } } } ]"  \
  cat ${EDS_CONF_FILE} \
    | jq ".resources[0].endpoints[0].lb_endpoints += [ { endpoint: { address: { socket_address: { address: \"${ENDPOINT_IP}\", port_value: ${INGRESS_INTERNAL_PORT} } } } } ]"  \
    > ${EDS_CONF_TMP_FILE}
  mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}
done

mv ${EDS_CONF_FILE} ${GENERATED_DIR}/eds_ingressHTTPS.conf
echo
echo "Envoy EDS file for ingressHTTPS: "
echo
cat ${GENERATED_DIR}/eds_ingressHTTPS.conf
echo

if [ -n "${INGRESS_TCP_PORTS}" ]; then
  # For each custom TCP port, create a new EDS configuration file and populate it
  # Loop array indices
  for i in "${!INGRESS_TCP_PORTS[@]}"; do
    INTERNAL_PORT="${INGRESS_TCP_INTERNAL_PORTS[$i]}"
    EXTERNAL_PORT="${INGRESS_TCP_PORTS[$i]}"

    PORT_EDS_CONFIG_FILE="${GENERATED_DIR}/eds_tcp${EXTERNAL_PORT}.conf"

    # Create a base configuration file based on the EDS config template
    cp ${EDS_CONF_TEMPLATE_FILE} ${EDS_CONF_FILE}

    cat ${EDS_CONF_FILE} \
      | jq ".resources += [ {\"@type\":\"type.googleapis.com/envoy.api.v2.ClusterLoadAssignment\",cluster_name:\"tcp${EXTERNAL_PORT}\",endpoints:[{lb_endpoints:[]}]} ]" \
      > ${EDS_CONF_TMP_FILE}
    mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}

    for ENDPOINT_IP in ${INGRESS_NODES_IPS[@]}; do
      cat ${EDS_CONF_FILE} \
        | jq ".resources[0].endpoints[0].lb_endpoints += [ { endpoint: { address: { socket_address: { address: \"${ENDPOINT_IP}\", port_value: ${INTERNAL_PORT} } } } } ]"  \
        > ${EDS_CONF_TMP_FILE}
      mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}
    done

    mv ${EDS_CONF_FILE} ${PORT_EDS_CONFIG_FILE}
    echo
    echo "Envoy EDS file for TCP ${EXTERNAL_PORT}: "
    echo
    cat ${PORT_EDS_CONFIG_FILE}
    echo
  done
fi

################################################################################
##  APPLY NEW ENVOY CONFIGURATION FILES
################################################################################

# Create a backup of the original configuration files
BACKUP_DIR="/etc/envoy/config-backup-${TIMESTAMP}"
sudo mkdir -p ${BACKUP_DIR}

sudo cp /etc/envoy/*.conf ${BACKUP_DIR}
sudo cp /etc/envoy/envoy-kumori-config.yaml ${BACKUP_DIR}

# Delete the original configuration files
sudo rm -f /etc/envoy/*.conf /etc/envoy/envoy-kumori-config.yaml

# Copy the new configuration files to /etc/envoy
sudo cp ${GENERATED_DIR}/*.conf ${GENERATED_DIR}/envoy-kumori-config.yaml /etc/envoy

# Restart Envoy to reload the main config
sudo systemctl restart envoy

