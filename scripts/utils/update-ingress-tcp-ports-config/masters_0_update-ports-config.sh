#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2021 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"

################################################################################
##  RECONFIGURE AMBASSADOR PORTS                                              ##
################################################################################
AMBASSADOR_DAEMONSET_ORIGINAL="${SCRIPT_DIR}/daemonset_original.yaml"
AMBASSADOR_DAEMONSET_CUSTOM="${SCRIPT_DIR}/daemonset_with_ports.yaml"
AMBASSADOR_DAEMONSET_TMP="${SCRIPT_DIR}/daemonset_original_TMP.yaml"

# Save current Ambassador DaemonSet definition to a work file
kubectl -n kumori get daemonset ambassador -oyaml > ${AMBASSADOR_DAEMONSET_ORIGINAL}

cp ${AMBASSADOR_DAEMONSET_ORIGINAL} ${AMBASSADOR_DAEMONSET_CUSTOM}
# NOTE: "Q" means remove till the last line
sed -i \
  -e '/^\s*deprecated.daemonset.template.generation:.*/d' \
  -e '/^\s*kubectl.kubernetes.io\/last-applied-configuration:.*/d' \
  -e '/^\s*{\"apiVersion\":".*/d' \
  -e '/^\s*creationTimestamp:.*/d' \
  -e '/^\s*generation:.*/d' \
  -e '/^\s*resourceVersion:.*/d' \
  -e '/^\s*selfLink:.*/d' \
  -e '/^\s*uid:.*/d' \
  -e '/^\s*revisionHistoryLimit:.*/d' \
  -e '/^\s*status:.*/Q' \
  ${AMBASSADOR_DAEMONSET_CUSTOM}


# Delete all ports and add HTTP, HTTPS and admin ports
HTTP_PORT="{\"containerPort\": 8080, \"hostPort\": 80, \"name\": \"http\", \"protocol\": \"TCP\"}"
HTTPS_PORT="{\"containerPort\": 8443, \"hostPort\": ${INGRESS_INTERNAL_PORT}, \"name\": \"https\", \"protocol\": \"TCP\"}"
ADMIN_PORT="{\"containerPort\": 8877, \"hostPort\": 8080, \"name\": \"admin\", \"protocol\": \"TCP\"}"

cat ${AMBASSADOR_DAEMONSET_CUSTOM} \
  | yq r -j - \
  | jq ".spec.template.spec.containers[0].ports = []" \
  | jq ".spec.template.spec.containers[0].ports += [ ${HTTP_PORT} ]" \
  | jq ".spec.template.spec.containers[0].ports += [ ${HTTPS_PORT} ]" \
  | jq ".spec.template.spec.containers[0].ports += [ ${ADMIN_PORT} ]" \
  | yq r --prettyPrint - \
  > ${AMBASSADOR_DAEMONSET_TMP}
mv ${AMBASSADOR_DAEMONSET_TMP} ${AMBASSADOR_DAEMONSET_CUSTOM}

# Add TCP ports
for i in "${!INGRESS_TCP_PORTS[@]}"; do
  INTERNAL_PORT="${INGRESS_TCP_INTERNAL_PORTS[$i]}"
  EXTERNAL_PORT="${INGRESS_TCP_PORTS[$i]}"
  cat ${AMBASSADOR_DAEMONSET_CUSTOM} \
    | yq r -j - \
    | jq ".spec.template.spec.containers[0].ports += [ { name: \"tcp${EXTERNAL_PORT}\", containerPort: ${INTERNAL_PORT} , hostPort: ${INTERNAL_PORT}, protocol: \"TCP\" } ]" \
    | yq r --prettyPrint - \
    > ${AMBASSADOR_DAEMONSET_TMP}
  mv ${AMBASSADOR_DAEMONSET_TMP} ${AMBASSADOR_DAEMONSET_CUSTOM}
done

# Apply updated Ambassador DaemonSet manifest
kubectl apply -f ${AMBASSADOR_DAEMONSET_CUSTOM}


################################################################################
##  RECONFIGURE ADMISSION PORTS                                               ##
################################################################################

ADMISSION_CONFIG_SECRET="admission-config-secret"

# Convert the INGRESS_TCP_PORT and INGRESS_TCP_INTERNAL_PORT arrays into comma-separated strings
INGRESS_TCP_PORTS_STR=""
INGRESS_TCP_INTERNAL_PORTS_STR=""
SEP=""
# Loop array indices
for i in "${!INGRESS_TCP_PORTS[@]}"; do
  INGRESS_TCP_PORTS_STR="${INGRESS_TCP_PORTS_STR}${SEP}${INGRESS_TCP_PORTS[$i]}"
  INGRESS_TCP_INTERNAL_PORTS_STR="${INGRESS_TCP_INTERNAL_PORTS_STR}${SEP}${INGRESS_TCP_INTERNAL_PORTS[$i]}"
  SEP=","
done

# Get Admission config secret, decode it, replace ports and reencode it
ADMISSION_NEW_CONFIG=$(
  kubectl -n kumori get secret ${ADMISSION_CONFIG_SECRET} -ojson \
  | jq -r '.data["admission-server-config.json"]' \
  | base64 --decode \
  | jq ".admission.tcpports = [ ${INGRESS_TCP_PORTS_STR} ]" \
  | jq ".admission.tcpInternalPorts = [ ${INGRESS_TCP_INTERNAL_PORTS_STR} ]"
)
ENCODED_ADMISSION_NEW_CONFIG=$(echo "${ADMISSION_NEW_CONFIG}" | base64 -w0)

echo "ADMISSION NEW CONFIG:"
echo ${ADMISSION_NEW_CONFIG}
echo

# Apply new Admission configuration by patching the config secret
kubectl -n kumori patch secret ${ADMISSION_CONFIG_SECRET} \
  -p "{\"data\":{\"admission-server-config.json\":\"${ENCODED_ADMISSION_NEW_CONFIG}\"}}"

# Restart Admission so the new configuration is applied
kubectl -n kumori rollout restart deployment admission

