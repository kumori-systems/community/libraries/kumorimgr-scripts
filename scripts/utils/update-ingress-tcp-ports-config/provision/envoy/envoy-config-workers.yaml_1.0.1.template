node:
  id: {{HOSTNAME}}
  cluster: {{CLUSTER_NAME}}

static_resources:
  listeners:
  - name: main
    address:
      socket_address:
        address: 0.0.0.0
        port_value: {{INGRESS_EXTERNAL_PORT}}
    # # Configure custom logging (currently disabled for HTTP listener)
    # access_log:
    #   - name: envoy.access_loggers.file
    #     config:
    #       # Standard log format but setting DOWNSTREAM_REMOTE_ADDRESS instead of REQ(X-FORWARDED-FOR)
    #       format: "[%START_TIME%] \"%REQ(:METHOD)% %REQ(X-ENVOY-ORIGINAL-PATH?:PATH)% %PROTOCOL%\" %RESPONSE_CODE% %RESPONSE_FLAGS% %BYTES_RECEIVED% %BYTES_SENT% %DURATION% %RESP(X-ENVOY-UPSTREAM-SERVICE-TIME)% \"%DOWNSTREAM_REMOTE_ADDRESS%\" \"%REQ(USER-AGENT)%\" \"%REQ(X-REQUEST-ID)%\" \"%REQ(:AUTHORITY)%\" \"%UPSTREAM_HOST%\"\n"
    #       path: "/var/log/kumori-ingress-envoy-lb.log"
    filter_chains:
    - filters:
      - name: envoy.tcp_proxy
        config:
          stat_prefix: ingress_tcp
          cluster: ingressHTTPS
          idle_timeout: 0s
  clusters:
  - name: ingressHTTPS
    connect_timeout: 0.25s
    circuit_breakers:
      thresholds:
        - priority: DEFAULT
          max_connections: 1000000000
          max_pending_requests: 1000000000
          max_requests: 1000000000
          max_retries: 1000000000
        - priority: HIGH
          max_connections: 1000000000
          max_pending_requests: 1000000000
          max_requests: 1000000000
          max_retries: 1000000000
    type: EDS
    eds_cluster_config:
      service_name: ingressHTTPS
      eds_config:
        path: '/etc/envoy/eds_ingressHTTPS.conf'
    lb_policy: round_robin
    common_lb_config:
      healthy_panic_threshold:
        value: 0.0
    drain_connections_on_host_removal: true
    close_connections_on_host_health_failure: true
    # Enable Proxy-Protocol for HTTP/HTTPS connections
    transport_socket:
      name: envoy.transport_sockets.upstream_proxy_protocol
      typed_config:
        "@type": type.googleapis.com/envoy.extensions.transport_sockets.proxy_protocol.v3.ProxyProtocolUpstreamTransport
        config:
          version: V1
        transport_socket:
          name: envoy.transport_sockets.raw_buffer
    health_checks:
    # Check Ambassador health via TCP because HTTP check is not workign when using Proxy-Protocol
    - timeout: 1s
      interval: 5s
      unhealthy_threshold: 1
      healthy_threshold: 1
      tcp_health_check: {}
    # FOR REFERENCE: Better health-check mechanism if Ambasador admin port is reachable and working.
    #                Use Ambassador 'check_ready' admin API method.
    # health_checks:
    # - timeout: 1s
    #   interval: 5s
    #   unhealthy_threshold: 1
    #   healthy_threshold: 1
    #   # Health check verifies that Ambassador is ready via its admin API.
    #   # Warning: special port for checks must be configured in EDS.
    #   http_health_check:
    #     path: "/ambassador/v0/check_ready"
    #
    # FOR REFERENCE: Alternative health-check if Ambasador admin port is not reachable.
    #                In that case, checked port should be the same as the endpoints.
    # health_checks:
    # - timeout: 1s
    #   interval: 5s
    #   unhealthy_threshold: 1
    #   healthy_threshold: 1
    #   tcp_health_check: {}
admin:
  access_log_path: "/dev/null"
  address:
    socket_address:
      address: {{NODE_IP}}
      port_value: 8001
