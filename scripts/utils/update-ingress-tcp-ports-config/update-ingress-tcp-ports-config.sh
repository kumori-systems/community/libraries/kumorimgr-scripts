#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2021 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"


# Load install configuration variables from file
source "${LOCAL_WORKDIR}/defs-colors.sh"
source "${LOCAL_WORKDIR}/variables.sh"


# Timestamp label to use in files, traces, etc.
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

SSH_COMMAND="ssh -o ServerAliveInterval=15 -o ServerAliveCountMax=4"

REMOTE_WORKDIR=".kumori/Kumori_envoy_updater_${TIMESTAMP}"


################################################################################
##  VALIDATE TOPOLOGY CONFIGURATION                                           ##
################################################################################
# Validate that a list of MASTER nodes has been provided
if [[ "${#MASTERS_IPS[@]}" -eq 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - MASTERS_IPS can't be empty."$COL_DEFAULT
  echo
  exit -1
fi

# Validate that a list of WORKER nodes has been provided
# NOTE: We don't consider this an error anymore, only a warning
if [[ "${#WORKERS_IPS[@]}" -eq 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - WORKERS_IPS can't be empty."$COL_DEFAULT
  echo
  exit -1
fi

# Validate that a list of INGRESS nodes has been provided
if [[ "${#INGRESS_NODES_IPS[@]}" -eq 0 ]]; then
  # Only error if list of workers is not empty
  if [[ ! "${#WORKERS_IPS[@]}" -eq 0 ]]; then
    echo
    echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
    echo -e $COL_RED_BOLD" - INGRESS_NODES_IPS can't be empty."$COL_DEFAULT
    echo
    exit -1
  fi
fi

# Validate that all INGRESS nodes are also WORKER nodes
ERRORS=()
for INGRESS_NODE_IP in ${INGRESS_NODES_IPS[@]}; do
  if [[ ! " ${WORKERS_IPS[@]} " =~ " ${INGRESS_NODE_IP} " ]]; then
    ERRORS+=(${INGRESS_NODE_IP})
  fi
done
if [[ "${#ERRORS[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - Some INGRESS nodes are not in WORKER list: ${ERRORS[*]}"$COL_DEFAULT
  echo
  exit -1
fi


# Pick first master on the list as "super-master"
SUPER_MASTER_IP=${MASTERS_IPS[0]}

################################################################################
##  PREPARE ASSOCIATION OF EACH MACHINE IP WITH A COLOR FOR CLEARER LOGGING   ##
################################################################################
declare -A IP_COLORS
###  IP_COLORS[$SUPER_MASTER_IP]=${BACKGROUND_COLORS[0]}
###  i=1
###  for ADDED_IP in ${ADD_MASTERS_IPS[@]}; do
###    IP_COLORS[$ADDED_IP]=${BACKGROUND_COLORS[$i]}
###    ((i++))
###  done
###  for ADDED_IP in ${ADD_WORKERS_IPS[@]}; do
###    IP_COLORS[$ADDED_IP]=${BACKGROUND_COLORS[$i]}
###    ((i++))
###  done
###  i=1
###  for REMOVED_IP in ${REMOVE_MASTERS_IPS[@]}; do
###    IP_COLORS[$REMOVED_IP]=${BACKGROUND_COLORS[$i]}
###    ((i++))
###  done
###  for REMOVED_IP in ${REMOVE_WORKERS_IPS[@]}; do
###    IP_COLORS[$REMOVED_IP]=${BACKGROUND_COLORS[$i]}
###    ((i++))
###  done

# Supermaster is one of the normal nodes,
IP_COLORS[${SUPER_MASTER_IP}]=${BACKGROUND_COLORS[0]}
i=1
for WORKERS_IP in ${WORKERS_IPS[@]}; do
  IP_COLORS[$WORKERS_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done


echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                           CLUSTER UPDATER                                  ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER NAME    : ${RELEASE_NAME}                                            "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER VERSION : ${CLUSTER_VERSION}                                         "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## RECONFIGURING INGRESS NODES:                                                 "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress Nodes IPs   : ${INGRESS_NODES_IPS[*]}                             "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress domain      : ${INGRESS_DOMAIN}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - HTTPS internal port : ${INGRESS_INTERNAL_PORT}                            "$COL_DEFAULT
echo -e $COL_GREEN"##  - HTTPS external port : ${INGRESS_EXTERNAL_PORT}                            "$COL_DEFAULT
echo -e $COL_GREEN"##  - TCP internal port   : ${INGRESS_TCP_INTERNAL_PORTS[*]}                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - TCP external port   : ${INGRESS_TCP_PORTS[*]}                             "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER CONFIGURATION (unchanged):                                           "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Release Name:           ${RELEASE_NAME}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo
read -rsp $'--> Press any key to begin cluster update...\n' -n1 key
echo


# Prepare directory for storing remote installations output
LOGS_DIR="${LOCAL_WORKDIR}/logs_envoy_updater_${TIMESTAMP}"
mkdir -p "${LOGS_DIR}"


################################################################################
##                PRE-FLIGHT CHECKS: SSH ACCESS TARGET HOSTS                  ##
################################################################################
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" *  Pre-Flight check: SSH access to all machines...     "$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT

ALL_IPS=( ${SUPER_MASTER_IP} "${INGRESS_NODES_IPS[@]}" )

echo  -e $COL_BLUE"* Remove hosts fingerprints from known hosts list"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ssh-keygen -R ${IP} >/dev/null 2>&1
done
echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo


echo  -e $COL_BLUE"* Check SSH connectivity and refresh hosts fingerprints"$COL_DEFAULT

UNREACHABLE=()
for IP in ${ALL_IPS[@]}; do
  ssh-keyscan ${IP} 2>&1 | grep -vE "^$|route|getaddrinfo" > /dev/null
  [ $? != 0 ] && UNREACHABLE+=(${IP})
done
if [[ "${#UNREACHABLE[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: SSH connectivity problem with the following machines:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${UNREACHABLE[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


echo  -e $COL_BLUE"* Add hosts SSH fingerprints"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ssh-keyscan -H ${IP} >> ${HOME}/.ssh/known_hosts 2>/dev/null
done
echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo


echo  -e $COL_BLUE"* Check user can connect via SSH"$COL_DEFAULT

FAILCONNECT=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
for IP in ${ALL_IPS[@]}; do
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} uptime 2> /dev/null)"
  [ "${RES}" == "" ] && FAILCONNECT+=(${IP})
done
if [[ "${#FAILCONNECT[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: Couldn't connect via SSH to the folowing machines:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAILCONNECT[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi



################################################################################
##  RECONFIGURE AMBASSADOR AND ADMISSION                                      ##
################################################################################
IP=${SUPER_MASTER_IP}
OUTPUT_FILE="${LOGS_DIR}/master_${IP}.log"
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" * Updating ports in Ambassador and Admission configuration from ${IP}..."$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
. ${LOCAL_WORKDIR}/control-masters-update-ports-config.sh \
  |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
  |& tee -a ${OUTPUT_FILE} \
  |& sed "s/^/$(printf "[${IP_COLORS[$IP]}MASTER ${IP}$COL_DEFAULT]") /" &

# Make sure Ambassador and Admission configurations are updated first
wait


################################################################################
##  UPDATE INTERNAL LOAD-BALANCERS CONFIGURATION                              ##
################################################################################
for WORKER_IP in ${INGRESS_NODES_IPS[@]}; do
  if [[ " ${MAINTENANCE_WORKERS_IPS[@]} " =~ " ${WORKER_IP} " ]]; then
    # Node is in maintenance, skip it
    # ":" is bash's no-command (do nothing)
    :
  else
    IP="${WORKER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/worker_${IP}.log"
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Updating internal load-balancer of WORKER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-workers-update-envoy-config.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}WORKER ${IP}$COL_DEFAULT]") /" &
  fi
done

wait


echo
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                      CLUSTER UPDATE COMPLETED                              ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## INGRESS NODES RECONFIGURED:                                                  "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress Nodes IPs   : ${INGRESS_NODES_IPS[*]}                             "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress domain      : ${INGRESS_DOMAIN}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - HTTPS internal port : ${INGRESS_INTERNAL_PORT}                            "$COL_DEFAULT
echo -e $COL_GREEN"##  - HTTPS external port : ${INGRESS_EXTERNAL_PORT}                            "$COL_DEFAULT
echo -e $COL_GREEN"##  - TCP internal port   : ${INGRESS_TCP_INTERNAL_PORTS[*]}                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - TCP external port   : ${INGRESS_TCP_PORTS[*]}                             "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo
