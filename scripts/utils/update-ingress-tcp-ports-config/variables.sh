#!/bin/bash

# CLUSTER_VERSION="0.2.4"
# CLUSTER_VERSION="0.2.6"
# CLUSTER_VERSION="1.0.0"
# CLUSTER_VERSION="1.0.1"
# CLUSTER_VERSION="1.1.1"
# CLUSTER_VERSION="1.3.0"
# CLUSTER_VERSION="1.0.2"   <=== NOT SUPPORTED! (not tested)
# CLUSTER_VERSION="1.1.0"   <=== NOT SUPPORTED! (not tested)
# CLUSTER_VERSION="1.2.0"   <=== NOT SUPPORTED! (not tested)
CLUSTER_VERSION="1.0.1"

# Cluster name
RELEASE_NAME="clustername"

# MASTERS (AT LEAST ONE IS NECESSARY)
MASTERS_IPS=(
	"10.0.18.53"
)

# WORKERS
WORKERS_IPS=(
	"10.0.18.56"
	"10.0.18.58"
	"10.0.18.57"
)

# Nodes that will be Ingress entrypoints
INGRESS_NODES_IPS=(
	"10.0.18.56"
	"10.0.18.58"
	"10.0.18.57"
)


SSH_USER="ubuntu"
SSH_KEY="${HOME}/.ssh/mykey"

INGRESS_INTERNAL_BALANCING="true"

# HTTPS Ingress ports
if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  INGRESS_EXTERNAL_PORT="443"
  INGRESS_INTERNAL_PORT="8443"
else
  INGRESS_EXTERNAL_PORT="443"
  INGRESS_INTERNAL_PORT="443"
fi


# List of ports that ingress will listen to for TCP connections.
# Between 9000 and 9999
INGRESS_TCP_PORTS=( 
  9200 9201 9202 9203 9204 9205 9206 9207 9208 9209
  9210 9211 9212 9213 9214 9215 9216 9217 9218 9219
  9220 9221 9222 9223 9224 9225 9226 9227 9228 9229
  9230 9231 9232 9233 9234 9235 9236 9237 9238 9239
  9240 9241 9242 9243 9244 9245 9246 9247 9248 9249
  9250 9251 9252 9253 9254 9255 9256 9257 9258 9259
  9260 9261 9262 9263 9264 9265 9266 9267 9268 9269
  9270 9271 9272 9273 9274 9275 9276 9277 9278 9279
  9280 9281 9282 9283 9284 9285 9286 9287 9288 9289
  9290 9291 9292 9293 9294 9295 9296 9297 9298 9299
)


###############################################################################
## DO NOT EDIT BELOW THIS LINE  (AUTOMATIC CALCULATION OF INTERNAL PORTS)
###############################################################################
# Assign an internal port to each TCP port. If internal load balancing is disabled, the internal
# and external ports will be the same.
INGRESS_TCP_INTERNAL_PORTS=()
if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  for PORT in ${INGRESS_TCP_PORTS[@]}; do
    # Currently allowed TCP ports are in the range 9000-9999, so we will just add
    # 10000 to convert them to internal: 19000-19999
    INGRESS_TCP_INTERNAL_PORTS+=("1${PORT}")
  done
else
  # Simply copy the original TCP port list
  INGRESS_TCP_INTERNAL_PORTS=( "${INGRESS_TCP_PORTS[@]}" )
fi
