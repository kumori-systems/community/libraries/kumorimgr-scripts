#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2021 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${LOCAL_WORKDIR}/defs-colors.sh"
source "${LOCAL_WORKDIR}/variables.sh"

# Timestamp label to use in files, traces, etc.
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

SSH_COMMAND="ssh -o ServerAliveInterval=15 -o ServerAliveCountMax=4"

REMOTE_WORKDIR=".kumori/Kumori_update_lbenvoy_${TIMESTAMP}"

################################################################################
##  VALIDATE TOPOLOGY CONFIGURATION                                           ##
################################################################################

# If internal balancing is not enabled, there is no work to do
if [[ "${INGRESS_INTERNAL_BALANCING}" != "true" ]]; then
  echo
  echo -e $COL_RED_BOLD"INTERNAL BALANCING DISABLED:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - No work to do."$COL_DEFAULT
  echo
  exit -1
fi

# Validate that a list of ingress nodes has been provided
if [[ "${#INGRESS_NODES_IPS[@]}" -eq 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - INGRESS_NODES_IPS can't be empty."$COL_DEFAULT
  echo
  exit -1
fi

# Validate that a list of nodes to be updated has been provided
if [[ "${#UPDATE_NODES_IPS[@]}" -eq 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - UPDATE_NODES_IPS can't be empty."$COL_DEFAULT
  echo
  exit -1
fi

# Validate that all nodes to be updated are ingress nodes
ERRORS=()
for UPDATE_NODES_IP in ${UPDATE_NODES_IPS[@]}; do
  if [[ ! " ${INGRESS_NODES_IPS[@]} " =~ " ${UPDATE_NODES_IP} " ]]; then
    ERRORS+=(${UPDATE_NODES_IP})
  fi
done
if [[ "${#ERRORS[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - Some nodes to be updated are not in INGRESS list: ${ERRORS[*]}"$COL_DEFAULT
  echo
  exit -1
fi

################################################################################
##  PREPARE ASSOCIATION OF EACH MACHINE IP WITH A COLOR FOR CLEARER LOGGING   ##
################################################################################

# Just selecting a color for each node
declare -A IP_COLORS
i=1
for UPDATE_NODES_IP in ${UPDATE_NODES_IPS[@]}; do
  IP_COLORS[$UPDATE_NODES_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done

echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                           CLUSTER UPDATER                                  ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER NAME : ${RELEASE_NAME}                                               "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## RECONFIGURING INGRESS NODES:                                                 "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Nodes to be updated IPs : ${UPDATE_NODES_IPS[*]}                          "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress Nodes IPs       : ${INGRESS_NODES_IPS[*]}                         "$COL_DEFAULT
echo -e $COL_GREEN"##  - HTTPS internal port     : ${INGRESS_INTERNAL_PORT}                        "$COL_DEFAULT
echo -e $COL_GREEN"##  - HTTPS external port     : ${INGRESS_EXTERNAL_PORT}                        "$COL_DEFAULT
echo -e $COL_GREEN"##  - TCP internal port       : ${INGRESS_TCP_INTERNAL_PORTS[*]}                "$COL_DEFAULT
echo -e $COL_GREEN"##  - TCP external port       : ${INGRESS_TCP_PORTS[*]}                         "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER CONFIGURATION (unchanged):                                           "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Release Name (cluster names: ${RELEASE_NAME}                              "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo
read -rsp $'--> Press any key to begin cluster update...\n' -n1 key
echo

# Prepare directory for storing remote installations output
LOGS_DIR="${LOCAL_WORKDIR}/logs_envoy_updater_${TIMESTAMP}"
mkdir -p "${LOGS_DIR}"

################################################################################
##                PRE-FLIGHT CHECKS: SSH ACCESS TARGET HOSTS                  ##
################################################################################
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" *  Pre-Flight check: SSH access to all machines...     "$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT

echo  -e $COL_BLUE"* Remove hosts fingerprints from known hosts list"$COL_DEFAULT
for IP in ${UPDATE_NODES_IPS[@]}; do
  ssh-keygen -R ${IP} >/dev/null 2>&1
done
echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo


echo  -e $COL_BLUE"* Check SSH connectivity and refresh hosts fingerprints"$COL_DEFAULT
UNREACHABLE=()
for IP in ${UPDATE_NODES_IPS[@]}; do
  ssh-keyscan ${IP} 2>&1 | grep -vE "^$|route|getaddrinfo" > /dev/null
  [ $? != 0 ] && UNREACHABLE+=(${IP})
done
if [[ "${#UNREACHABLE[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: SSH connectivity problem with the following machines:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${UNREACHABLE[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


echo  -e $COL_BLUE"* Add hosts SSH fingerprints"$COL_DEFAULT
for IP in ${UPDATE_NODES_IPS[@]}; do
  ssh-keyscan -H ${IP} >> ${HOME}/.ssh/known_hosts 2>/dev/null
done
echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo


echo  -e $COL_BLUE"* Check user can connect via SSH"$COL_DEFAULT
FAILCONNECT=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
for IP in ${UPDATE_NODES_IPS[@]}; do
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} uptime 2> /dev/null)"
  [ "${RES}" == "" ] && FAILCONNECT+=(${IP})
done
if [[ "${#FAILCONNECT[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: Couldn't connect via SSH to the following machines:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAILCONNECT[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi

################################################################################
##  UPDATE INTERNAL LOAD-BALANCERS CONFIGURATION                              ##
################################################################################
for NODE_IP in ${UPDATE_NODES_IPS[@]}; do
  IP="${NODE_IP}"
  OUTPUT_FILE="${LOGS_DIR}/worker_${IP}.log"
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" * Updating internal load-balancer of WORKER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-workers-update-envoy-and-config.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}WORKER ${IP}$COL_DEFAULT]") /" &echo -e $COL_GREEN"##  - Release Name (cluster names: ${RELEASE_NAME}                              "$COL_DEFAULT
done

wait

echo
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                      CLUSTER UPDATE COMPLETED                              ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## INGRESS NODES RECONFIGURED:                                                  "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Nodes to be updated IPs : ${UPDATE_NODES_IPS[*]}                          "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress Nodes IPs       : ${INGRESS_NODES_IPS[*]}                         "$COL_DEFAULT
echo -e $COL_GREEN"##  - HTTPS internal port     : ${INGRESS_INTERNAL_PORT}                        "$COL_DEFAULT
echo -e $COL_GREEN"##  - HTTPS external port     : ${INGRESS_EXTERNAL_PORT}                        "$COL_DEFAULT
echo -e $COL_GREEN"##  - TCP internal port       : ${INGRESS_TCP_INTERNAL_PORTS[*]}                "$COL_DEFAULT
echo -e $COL_GREEN"##  - TCP external port       : ${INGRESS_TCP_PORTS[*]}                         "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo
