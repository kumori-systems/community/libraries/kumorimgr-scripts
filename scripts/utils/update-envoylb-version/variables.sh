#!/bin/bash

# Cluster name
RELEASE_NAME="kqa2"

SSH_USER="ubuntu"
SSH_KEY="${HOME}/.ssh/ecloud_deployment_key"

ENVOY_VERSION="1.25.3"

# List of ingress nodes to use in configuration (MUST be a complete list)
INGRESS_NODES_IPS=(
  "100.100.100.250"
  "100.100.100.251"
  "100.100.100.253"
)

# List of ingress nodes to be updated
# TYpical use:
# - in a first execution: update all nodes except the node owning the floating ip
# - in a second execution: update the node owning the floating ip
# UPDATE_NODES_IPS=(
#   "100.100.100.250"
# )
UPDATE_NODES_IPS=(
  "100.100.100.251"
  "100.100.100.252"
)

# Internal balancing configuration
INGRESS_INTERNAL_BALANCING="true"
INGRESS_EXTERNAL_PORT="443"
INGRESS_INTERNAL_PORT="8443"

# List of ports that ingress will listen to for TCP connections.
INGRESS_TCP_PORTS=(
  9000 9001 9002 9003 9004 9005 9006 9007 9008 9009
  9010 9011 9012 9013 9014 9015 9016 9017 9018 9019
  9020 9021 9022 9023 9024 9025 9026 9027 9028 9029
  9030 9031 9032 9033 9034 9035 9036 9037 9038 9039
  9040 9041 9042 9043 9044 9045 9046 9047 9048 9049
  9050 9051 9052 9053 9054 9055 9056 9057 9058 9059
  9060 9061 9062 9063 9064 9065 9066 9067 9068 9069
  9070 9071 9072 9073 9074 9075 9076 9077 9078 9079
  9080 9081 9082 9083 9084 9085 9086 9087 9088 9089
  9090 9091 9092 9093 9094 9095 9096 9097 9098 9099
)

###############################################################################
## DO NOT EDIT BELOW THIS LINE  (AUTOMATIC CALCULATION OF INTERNAL PORTS)
###############################################################################
# Assign an internal port to each TCP port. If internal load balancing is disabled, the internal
# and external ports will be the same.
INGRESS_TCP_INTERNAL_PORTS=()
if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  for PORT in ${INGRESS_TCP_PORTS[@]}; do
    # Currently allowed TCP ports are in the range 9000-9999, so we will just add
    # 10000 to convert them to internal: 19000-19999
    INGRESS_TCP_INTERNAL_PORTS+=("1${PORT}")
  done
else
  # Simply copy the original TCP port list
  INGRESS_TCP_INTERNAL_PORTS=( "${INGRESS_TCP_PORTS[@]}" )
fi
