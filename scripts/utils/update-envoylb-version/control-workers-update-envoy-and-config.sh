#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2021 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

echo
echo -e $COL_BLUE"* Uploading files to remote machine ${IP}..."$COL_DEFAULT
scp -i ${SSH_KEY} -r ${LOCAL_WORKDIR} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}
echo -e $COL_BLUE"* Upload finished."$COL_DEFAULT
echo


echo
echo -e $COL_BLUE"* Executing remote envoy config updater..."$COL_DEFAULT
${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} << EOF
 cd ${REMOTE_WORKDIR}
 echo >> variables.sh
 echo "NODE_IP=\"$IP\"" >> variables.sh
 chmod +x *.sh
 ./workers_0_update-envoy-and-config.sh
EOF
echo -e $COL_BLUE"* Envoy config updater finished."$COL_DEFAULT
echo
