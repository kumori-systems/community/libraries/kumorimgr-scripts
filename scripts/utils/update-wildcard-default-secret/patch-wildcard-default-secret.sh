#/bin/bash

SECRET_NAME="wildcard-default"

CRT_FILE="$(pwd)/wildcard.test.kumori.cloud.crt"
KEY_FILE="$(pwd)/wildcard.test.kumori.cloud.key"

CRT_64=$( cat ${CRT_FILE} | base64 -w0 )
KEY_64=$( cat ${KEY_FILE} | base64 -w0 )

kubectl -n kumori patch secret ${SECRET_NAME} -p "{\"data\":{\"tls.crt\":\"${CRT_64}\", \"tls.key\":\"${KEY_64}\"}}"