#!/bin/bash

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"

PROVISION_DIR="${LOCAL_WORKDIR}/provision"

source "${LOCAL_WORKDIR}/variables.sh"
source "${PROVISION_DIR}/defs-colors.sh"



if [[ -z "${PROMETHEUS_REMOTE_WRITE_URL}" ]]; then
  echo
  echo -e $COL_BLUE"Skipped Prometheus remote_write configuration (disabled by configuration)."$COL_DEFAULT
  echo
else
  echo
  echo -e $COL_BLUE"Configuring Prometheus remote_write..."$COL_DEFAULT
  echo

  # Prepare Prometheus object manifest with remote write
  PROM_RW_PARTIAL_YAML_TEMPLATE="${PROVISION_DIR}/prometheus-prometheus-remotewrite.yaml.part_template"
  PROM_RW_PARTIAL_YAML_FILE="${PROVISION_DIR}/prometheus-prometheus-remotewrite.yaml.part"

  CURRENT_PROMETHEUS_YAML_FILE="${PROVISION_DIR}/prometheus-k8s-current.yaml"
  NEW_PROMETHEUS_YAML_FILE="${PROVISION_DIR}/prometheus-k8s-new.yaml"


  sed \
    -e "s?{{CLUSTER_NAME}}?${CLUSTER_NAME}?g" \
    -e "s?{{PROMETHEUS_REMOTE_WRITE_URL}}?${PROMETHEUS_REMOTE_WRITE_URL}?g" \
    "${PROM_RW_PARTIAL_YAML_TEMPLATE}" \
    > "${PROM_RW_PARTIAL_YAML_FILE}"

  kubectl -n monitoring get prometheus k8s -oyaml > "${CURRENT_PROMETHEUS_YAML_FILE}"

  # Get current configuration
  cat "${CURRENT_PROMETHEUS_YAML_FILE}" > "${NEW_PROMETHEUS_YAML_FILE}"
  # Remove 'externalLabels' property from configuration
  yq d -i "${NEW_PROMETHEUS_YAML_FILE}" 'spec.externalLabels'
  # Add remote_write configuration partial file at the end
  cat "${PROM_RW_PARTIAL_YAML_FILE}" >> "${NEW_PROMETHEUS_YAML_FILE}"

  # Apply Prometheus object manifest with remote write
  # NOTE: Prometheus auto-detects configuration changes, so no Pod restarts are required
  kubectl apply -f "${NEW_PROMETHEUS_YAML_FILE}"
fi


if [[ -z "${GRAFANA_REMOTE_DATASOURCE_NAME}" ]]; then
  echo
  echo -e $COL_BLUE"Skipped Grafana datasource installation (disabled by configuration)."$COL_DEFAULT
  echo
else
  echo
  echo -e $COL_BLUE"Installing Grafana datasource installation (disabled by configuration)."$COL_DEFAULT
  echo
  # Prepare Grafana Datasource configuration secret with remote read
  GRAF_DATASOURCE_SECRET_YAML_TEMPLATE="${PROVISION_DIR}/datasource_with_persistence.yaml_template"
  GRAF_DATASOURCE_SECRET_YAML_FILE="${PROVISION_DIR}/datasource_with_persistence.yaml"

  sed \
    -e "s?__PROMETHEUS_REMOTE_READ_URL__?${PROMETHEUS_REMOTE_READ_URL}?g" \
    -e "s?__GRAFANA_REMOTE_DATASOURCE_NAME__?${GRAFANA_REMOTE_DATASOURCE_NAME}?g" \
    "${GRAF_DATASOURCE_SECRET_YAML_TEMPLATE}" \
    > "${GRAF_DATASOURCE_SECRET_YAML_FILE}"

  # Apply Grafana Datasource configuration secret
  kubectl apply -f "${GRAF_DATASOURCE_SECRET_YAML_FILE}"

  # Restart grafana Pods
  kubectl -n monitoring rollout restart deployment grafana
fi


