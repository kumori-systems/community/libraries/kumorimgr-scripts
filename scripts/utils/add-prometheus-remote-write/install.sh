#!/bin/bash

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"

PROVISION_DIR="${LOCAL_WORKDIR}/provision"

source "${LOCAL_WORKDIR}/variables.sh"
source "${PROVISION_DIR}/defs-colors.sh"


# Timestamp label to use in files, traces, etc.
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

SSH_COMMAND="ssh -o ServerAliveInterval=15 -o ServerAliveCountMax=4"

REMOTE_WORKDIR=".kumori/Kumori_Prometheus_Remote_Write_installer_${TIMESTAMP}"




echo
echo -e $COL_GREEN"----------------------------------------------"$COL_DEFAULT
echo -e $COL_GREEN"Running installer in machine ${MASTER_IP}"$COL_DEFAULT
echo -e $COL_GREEN"----------------------------------------------"$COL_DEFAULT
echo

echo  -e $COL_BLUE"* Creating work directory in remote machine ${MASTER_IP}"$COL_DEFAULT
${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${MASTER_IP} mkdir -p ${REMOTE_WORKDIR}

echo  -e $COL_BLUE"* Uploading files to remote machine ${MASTER_IP}"$COL_DEFAULT
scp -i ${SSH_KEY} -r ${LOCAL_WORKDIR}/* ${SSH_USER}@${MASTER_IP}:${REMOTE_WORKDIR}

echo -e $COL_BLUE"* Executing remote installer in remote machine ${MASTER_IP}..."$COL_DEFAULT
${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${MASTER_IP} << EOF
cd ${REMOTE_WORKDIR}
chmod +x *.sh
./master-add-prometheus-remote-write.sh
EOF

echo
echo -e $COL_GREEN"----------------------------------------------"$COL_DEFAULT
echo -e $COL_GREEN"Installation process finished."$COL_DEFAULT
echo -e $COL_GREEN"----------------------------------------------"$COL_DEFAULT
echo
