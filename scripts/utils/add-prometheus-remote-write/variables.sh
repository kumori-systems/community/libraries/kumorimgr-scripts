#!/bin/bash

# IP of a Master node
MASTER_IP="100.100.100.58"

# SSH connection details
SSH_USER="ubuntu"
SSH_KEY="${HOME}/.ssh/my-ssh-key"

# Name of the cluster
CLUSTER_NAME= "mycluster"

# Prometheus remote persistence backend URLs. The remote read URL is only required if
# installing the optional Grafana datasource.
#
# Example values for Victoria Metrics:
#   PROMETHEUS_REMOTE_WRITE_URL="http://victoriametrics-mycluster.test.kumori.cloud:8428/api/v1/write"
#   PROMETHEUS_REMOTE_READ_URL="http://victoriametrics-mycluster.test.kumori.cloud:8428"
PROMETHEUS_REMOTE_WRITE_URL="<persistence-backend-write-url>"
PROMETHEUS_REMOTE_READ_URL="<persistence-backend-read-url>"

# Name of the remote Grafana datasource. To disable datasource installation, set to ""
GRAFANA_REMOTE_DATASOURCE_NAME="Victoria Metrics"