#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2021 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"



###############################################################################
##                        SCRIPT CONFIGURATION                               ##
###############################################################################

# New Admission version.
# - For cluster v0.2.4, use 'v0.2.20-patch-image-check'
# - For cluster v0.2.6, use 'v0.2.23-patch-image-check'
# - For cluster v1.0.0, use 'dev839' (temporarily)
KUMORI_ADMISSION_VERSION="v0.2.20-patch-image-check"

# Docker Registry mirror
DOCKER_REGISTRY_MIRROR="docker-mirror.iti.upv.es"

# DockerHub credentials
DOCKERHUB_USERNAME="username"
DOCKERHUB_PASSWORD="password"
#                                                                            ##
###############################################################################




###############################################################################
## 1. Update Admission configuration secret                                  ##
###############################################################################

ADMISSION_CONFIG_SECRET="admission-config-secret"

echo
echo "Calculating new Admission configuration..."
echo

# Get current Admission configuration, patch it and encode it using Base64.
# Steps:
# - Read Admission configuration secret in JSON format
# - Extract the 'data."admission-server-config.json"' property using JQ (base64 encoded)
# - Base64-decode the configuration
# - Patch the configuration using JQ
#   - set 'admission.dockerRegistry.username' property
#   - set 'admission.dockerRegistry.password' property
#   - set 'admission.dockerRegistry.mirror' property
# - Base64-encode the resulting configuration

NEW_ENCODED_CONFIGURATION=$(kubectl -n kumori get secret admission-config-secret -ojson \
  | jq -r '.data."admission-server-config.json"' \
  | base64 --decode \
  | jq ".admission.dockerRegistry.username=\"$DOCKERHUB_USERNAME\"" \
  | jq ".admission.dockerRegistry.password=\"$DOCKERHUB_PASSWORD\"" \
  | jq ".admission.dockerRegistry.mirror=\"$DOCKER_REGISTRY_MIRROR\"" \
  | base64 -w0)

UPDATED_ADMISSION_CONFIG_JSON=$(echo ${NEW_ENCODED_CONFIGURATION} | base64 --decode)
echo
echo "New Admission configuration to be applied:"
echo "------------------------------------------------------------------------------"
echo "${UPDATED_ADMISSION_CONFIG_JSON}"
echo "------------------------------------------------------------------------------"
echo

echo
echo "Applying new Admission configuration..."
echo

# Patch the Admission configuration secret using kubectl
echo "kubectl -n kumori patch secret ${ADMISSION_CONFIG_SECRET} -p" "{\"data\":{\"admission-server-config.json\":\"${NEW_ENCODED_CONFIGURATION}\"}}"
kubectl -n kumori patch secret ${ADMISSION_CONFIG_SECRET} \
  -p "{\"data\":{\"admission-server-config.json\":\"${NEW_ENCODED_CONFIGURATION}\"}}"

echo
echo "New Admission configuration applied."
echo


###############################################################################
## 2. Update Admission image version                                         ##
###############################################################################

# Determine Kumori image registry in use in the cluster
if [ -z "${KUMORI_IMAGES_REGISTRY}" ]; then
  echo
  echo "Determining Kumori image registry in use in the cluster..."
  echo
  KUMORI_IMAGES_REGISTRY=$(kubectl -n kumori get deployment admission -oyaml | grep -E 'image:\s*.*/admission:' | sed -e 's|.*image:\s*\(.*\)/admission:[-a-z.0-9]*.*$|\1|g')
fi
echo "Using Kumori image registry: ${KUMORI_IMAGES_REGISTRY}"
echo

echo
echo "Updating Admission controller version to ${KUMORI_ADMISSION_VERSION}..."
echo

echo "kubectl -n kumori set image deployment/admission admission=${KUMORI_IMAGES_REGISTRY}/admission:${KUMORI_ADMISSION_VERSION}"
kubectl -n kumori set image deployment/admission admission=${KUMORI_IMAGES_REGISTRY}/admission:${KUMORI_ADMISSION_VERSION}

echo
echo "Admission controller updated to version ${KUMORI_ADMISSION_VERSION}."
echo
