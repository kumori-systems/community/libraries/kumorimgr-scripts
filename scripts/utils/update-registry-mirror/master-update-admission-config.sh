#!/bin/bash

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"

source "${LOCAL_WORKDIR}/variables.sh"
source "${LOCAL_WORKDIR}/defs-colors.sh"


###############################################################################
## 1. Update Admission configuration secret                                  ##
###############################################################################

ADMISSION_CONFIG_SECRET="admission-config-secret"

echo
echo "Calculating new Admission configuration..."
echo

# Get current Admission configuration, patch it and encode it using Base64.
# Steps:
# - Read Admission configuration secret in JSON format
# - Extract the 'data."admission-server-config.json"' property using JQ (base64 encoded)
# - Base64-decode the configuration
# - Patch the configuration using JQ
#   - set 'admission.dockerRegistry.mirror' property
# - Base64-encode the resulting configuration

NEW_ENCODED_CONFIGURATION=$(kubectl -n kumori get secret admission-config-secret -ojson \
  | jq -r '.data."admission-server-config.json"' \
  | base64 --decode \
  | jq ".admission.dockerRegistry.mirror=\"$DOCKER_REGISTRY_MIRROR\"" \
  | base64 -w0)

UPDATED_ADMISSION_CONFIG_JSON=$(echo ${NEW_ENCODED_CONFIGURATION} | base64 --decode)
echo
echo "New Admission configuration to be applied:"
echo "------------------------------------------------------------------------------"
echo "${UPDATED_ADMISSION_CONFIG_JSON}"
echo "------------------------------------------------------------------------------"
echo

echo
echo "Applying new Admission configuration..."
echo

# Patch the Admission configuration secret using kubectl
echo "kubectl -n kumori patch secret ${ADMISSION_CONFIG_SECRET} -p" "{\"data\":{\"admission-server-config.json\":\"${NEW_ENCODED_CONFIGURATION}\"}}"
kubectl -n kumori patch secret ${ADMISSION_CONFIG_SECRET} \
  -p "{\"data\":{\"admission-server-config.json\":\"${NEW_ENCODED_CONFIGURATION}\"}}"

echo
echo "New Admission configuration applied."
echo


###############################################################################
## 2. Restart Admission                                                      ##
###############################################################################

echo
echo "Restarting Admission..."
echo

kubectl -n kumori rollout restart deployment admission

echo
echo "Admission restarted."
echo
