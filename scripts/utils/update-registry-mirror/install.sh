#!/bin/bash

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"

PROVISION_DIR="${LOCAL_WORKDIR}/provision"

source "${LOCAL_WORKDIR}/variables.sh"
source "${LOCAL_WORKDIR}/defs-colors.sh"


# Timestamp label to use in files, traces, etc.
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

SSH_COMMAND="ssh -o ServerAliveInterval=15 -o ServerAliveCountMax=4"
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"

REMOTE_WORKDIR=".kumori/Kumori_Docker_Registry_Updater_${TIMESTAMP}"



# Fetch cluster node IP and hostname lists from a Master
echo
echo "Retrieving cluster nodes information..."
echo

# Sudo command that has access to the user $HOME for accessing $HOME/.kube/config
# (Ubuntu 19.04+ new behaviour)
REMOTE_SUDO="sudo --preserve-env=HOME"
# Get Master nodes IPs (from a Master node)
COMMAND="kubectl get nodes -o jsonpath='{.items[*].status.addresses[?(@.type==\"InternalIP\")].address}'"
ENCODED=$(echo "${COMMAND}" | base64 -w0)
NODES_IPS=(
  $(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${MASTER_IP} "echo ${ENCODED} | base64 -d | ${REMOTE_SUDO} bash")
)
# Get Master nodes hostnames (from a Master node)
COMMAND="kubectl get nodes -o jsonpath={.items[*].metadata.name}"
ENCODED=$(echo "${COMMAND}" | base64 -w0)
NODES_HOSTNAMES=(
  $(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${MASTER_IP} "echo ${ENCODED} | base64 -d | ${REMOTE_SUDO} bash")
)

echo
echo -e $COL_GREEN"This patch will update the Docker registry mirror in cluster ${CLUSTER_NAME}"$COL_DEFAULT
echo
echo
echo -e $COL_GREEN"Detected cluster nodes:                                                         "$COL_DEFAULT
for i in "${!NODES_IPS[@]}"; do
  echo -e $COL_GREEN" - ${NODES_HOSTNAMES[$i]}  -  ${NODES_IPS[$i]}                                "$COL_DEFAULT
done
echo
echo -e $COL_GREEN"New registry mirror: ${DOCKER_REGISTRY_MIRROR}"$COL_DEFAULT
echo
read -rsp $'--> Press any key to proceed...\n' -n1 key
echo



# In each node
# - patch /etc/docker/daemon.json
# - sudo systemctl reload docker
for i in "${!NODES_IPS[@]}"; do
  NODE_IP="${NODES_IPS[$i]}"
  NODE_NAME="${NODES_HOSTNAMES[$i]}"

  echo
  echo -e $COL_GREEN"----------------------------------------------"$COL_DEFAULT
  echo -e $COL_GREEN"Updating Docker daemon config in node ${NODE_NAME} (${NODE_IP})"$COL_DEFAULT
  echo -e $COL_GREEN"----------------------------------------------"$COL_DEFAULT
  echo
  
  echo  -e $COL_BLUE"* Creating work directory in remote machine ${NODE_NAME}"$COL_DEFAULT
  ${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${NODE_IP} mkdir -p ${REMOTE_WORKDIR}
  
  echo  -e $COL_BLUE"* Uploading files to remote machine ${NODE_NAME}"$COL_DEFAULT
  scp -i ${SSH_KEY} -r ${LOCAL_WORKDIR}/* ${SSH_USER}@${NODE_IP}:${REMOTE_WORKDIR}
  
  echo -e $COL_BLUE"* Executing remote installer in remote machine ${NODE_NAME}..."$COL_DEFAULT
${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${NODE_IP} << EOF
cd ${REMOTE_WORKDIR}
chmod +x *.sh
./all-update-docker-daemon-config.sh
EOF
  
  echo
  echo -e $COL_GREEN"----------------------------------------------"$COL_DEFAULT
  echo -e $COL_GREEN"Updated Docker daemon config in node ${NODE_NAME} (${NODE_IP})"$COL_DEFAULT
  echo -e $COL_GREEN"----------------------------------------------"$COL_DEFAULT
  echo
done



# In a Master node
# - update Admission config
# - restart Admission
echo
echo -e $COL_GREEN"----------------------------------------------"$COL_DEFAULT
echo -e $COL_GREEN"Updating Admission configuration from machine ${MASTER_IP}"$COL_DEFAULT
echo -e $COL_GREEN"----------------------------------------------"$COL_DEFAULT
echo

### echo  -e $COL_BLUE"* Creating work directory in remote machine ${MASTER_IP}"$COL_DEFAULT
### ${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${MASTER_IP} mkdir -p ${REMOTE_WORKDIR}
### 
### echo  -e $COL_BLUE"* Uploading files to remote machine ${MASTER_IP}"$COL_DEFAULT
### scp -i ${SSH_KEY} -r ${LOCAL_WORKDIR}/* ${SSH_USER}@${MASTER_IP}:${REMOTE_WORKDIR}

echo -e $COL_BLUE"* Executing remote installer in remote machine ${MASTER_IP}..."$COL_DEFAULT
${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${MASTER_IP} << EOF
cd ${REMOTE_WORKDIR}
chmod +x *.sh
./master-update-admission-config.sh
EOF

echo
echo -e $COL_GREEN"----------------------------------------------"$COL_DEFAULT
echo -e $COL_GREEN"Admission configuration updated.."$COL_DEFAULT
echo -e $COL_GREEN"----------------------------------------------"$COL_DEFAULT
echo
