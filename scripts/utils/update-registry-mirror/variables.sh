#!/bin/bash

# IP of a Master node
MASTER_IP="100.100.100.58"

# SSH connection details
SSH_USER="ubuntu"
SSH_KEY="${HOME}/.ssh/my-ssh-key"

# Name of the cluster
CLUSTER_NAME="mycluster"

# Docker Registry mirror
# IMPORTANT: include the protocol (http:// or https://)
DOCKER_REGISTRY_MIRROR="https://docker-mirror.mycompany.es"
