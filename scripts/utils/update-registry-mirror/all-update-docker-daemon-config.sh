#!/bin/bash

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"

source "${LOCAL_WORKDIR}/variables.sh"
source "${LOCAL_WORKDIR}/defs-colors.sh"


# Initialize temporary dir for manipulating manifest files
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

# Update Docker daemon config file
sudo cp /etc/docker/daemon.json /etc/docker/daemon.json_BAK_${TIMESTAMP}
sudo cat /etc/docker/daemon.json \
  | jq ".\"registry-mirrors\"[0]=\"${DOCKER_REGISTRY_MIRROR}\"" \
  > new-daemon.json
echo "New Docker daemon configuration:"
cat new-daemon.json
echo
sudo mv new-daemon.json /etc/docker/daemon.json

# Reload Docker daemon config
echo "Applying new Docker daemon configuration..."
sudo systemctl reload docker
echo "Done."