#!/bin/bash

echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##               LOCAL DEVELOPMENT CLUSTER INSTALLER                          ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER NAME : ${RELEASE_NAME}                                               "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## NODE TOPOLOGY (fixed) :  1 Master + 2 Workers                                "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## INGRESS ACCESS:                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress domain      : ${INGRESS_DOMAIN}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress HTTPS port  : ${INGRESS_INTERNAL_PORT}                            "$COL_DEFAULT
if [ -n "${INGRESS_TCP_PORTS}" ]; then
  echo -e $COL_GREEN"##  - Ingress TCP Ports   : ${INGRESS_TCP_PORTS[*]}                             "$COL_DEFAULT
fi
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## ADDONS:                                                                      "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
if [ "${INSTALL_KEYCLOAK}" == "true" ]; then
  echo -e $COL_GREEN"##  - Install Keycloak:         ${INSTALL_KEYCLOAK}                             "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Keycloak username:        ${KEYCLOAK_ADMIN_USERNAME}                      "$COL_DEFAULT
  if [ -n "${KEYCLOAK_ADMIN_PASSWORD}" ]; then
    echo -e $COL_GREEN"##  - Keycloak password:        yes <hidden>                                    "$COL_DEFAULT
  else
    echo -e $COL_GREEN"##  - Keycloak password:        "$COL_RED_BOLD"NO                               "$COL_DEFAULT
  fi
  echo -e $COL_GREEN"##  - Keycloack CPU:            Request: ${KEYCLOAK_CPU_REQUEST} / Limit: ${KEYCLOAK_CPU_LIMIT} "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Keycloack Memory:         Request: ${KEYCLOAK_MEM_REQUEST} / Limit: ${KEYCLOAK_MEM_LIMIT} "$COL_DEFAULT
else
  echo -e $COL_GREEN"##  - Install Keycloak:         false (Admission won't require authentication)"$COL_DEFAULT
fi
echo -e $COL_GREEN"##  - Install Kube-Prometheus:  ${INSTALL_KUBE_PROMETHEUS}                      "$COL_DEFAULT
if [ "${INSTALL_KUBE_PROMETHEUS}" == "true" ]; then
  echo -e $COL_GREEN"##  - Prometheus CPU:           Request: ${PROMETHEUS_CPU_REQUEST} / Limit: ${PROMETHEUS_CPU_LIMIT} "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Prometheus Memory:        Request: ${PROMETHEUS_MEM_REQUEST} / Limit: ${PROMETHEUS_MEM_LIMIT} "$COL_DEFAULT
  echo -e $COL_GREEN"##  - AlertManager CPU:         Request: ${ALERTMANAGER_CPU_REQUEST} / Limit: ${ALERTMANAGER_CPU_LIMIT} "$COL_DEFAULT
  echo -e $COL_GREEN"##  - AlertManager Memory:      Request: ${ALERTMANAGER_MEM_REQUEST} / Limit: ${ALERTMANAGER_MEM_LIMIT} "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Grafana username:         ${GRAFANA_ADMIN_USERNAME}             "$COL_DEFAULT
  if [ -n "${GRAFANA_ADMIN_PASSWORD}" ]; then
    echo -e $COL_GREEN"##  - Grafana password:         yes <hidden>                          "$COL_DEFAULT
  else
    echo -e $COL_GREEN"##  - Grafana password:         "$COL_RED_BOLD"NO                     "$COL_DEFAULT
  fi
  echo -e $COL_GREEN"##  - Grafana CPU:              Request: ${GRAFANA_CPU_REQUEST} / Limit: ${GRAFANA_CPU_LIMIT} "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Grafana Memory:           Request: ${GRAFANA_MEM_REQUEST} / Limit: ${GRAFANA_MEM_LIMIT} "$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
fi

if [ -n "${DOCKERHUB_USERNAME}" ]; then
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"## DOCKER HUB ACCOUNT                                                           "$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"##  - DockerHub username:  ${DOCKERHUB_USERNAME}                                "$COL_DEFAULT
  if [ -n "${DOCKERHUB_PASSWORD}" ]; then
    echo -e $COL_GREEN"##  - DockerHub password:  yes <hidden>                                          "$COL_DEFAULT
  else
    echo -e $COL_GREEN"##  - DockerHub password:  "$COL_RED_BOLD"NO (credentials won't be used!)        "$COL_DEFAULT
  fi
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
fi

if [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ]; then
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"## KUMORI IMAGE REGISTRY                                                        "$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Registry :  ${KUMORI_IMAGES_REGISTRY}                                     "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Username :  ${KUMORI_IMAGES_REGISTRY_USERNAME}                            "$COL_DEFAULT
  if [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
    echo -e $COL_GREEN"##  - Password :  yes <hidden>                                                  "$COL_DEFAULT
  else
    echo -e $COL_GREEN"##  - Password :  "$COL_RED_BOLD"NO (credentials won't be used!)                "$COL_DEFAULT
  fi
fi
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo

echo
read -rsp $'--> Press any key to begin installation...\n' -n1 key
echo

