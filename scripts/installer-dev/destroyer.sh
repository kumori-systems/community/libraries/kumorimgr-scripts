#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

COMMON_DIR="${SCRIPT_DIR}/../common"

# Load install configuration variables from file
source "${COMMON_DIR}/defs-colors.sh"

# Directories containing the necessary material for the installation
CERTS_WORKDIR="${SCRIPT_DIR}/certs-creation"
RUNTIME_CONFIG_DIR="${SCRIPT_DIR}/runtime-configuration"
CLUSTER_VARIABLES_FILE="${SCRIPT_DIR}/variables.sh"
CLUSTER_CERTS_DIR="${RUNTIME_CONFIG_DIR}/clusterCerts"

# Validate cluster configuration file exists
if [ ! -f "${CLUSTER_VARIABLES_FILE}" ]; then
  echo -e $COL_RED"Couldn't find cluster configuration variables file in ${CLUSTER_VARIABLES_FILE}."$COL_DEFAULT
  echo -e $COL_RED"Make sure you are in the correct directory."$COL_DEFAULT
  echo -e $COL_RED"Exiting."$COL_DEFAULT
  exit 1
fi

# Load install configuration variables from file
source "${CLUSTER_VARIABLES_FILE}"

CLUSTER_NAME="${RELEASE_NAME}"

echo
echo -e $COL_BLUE"* Destroying Kubernetes cluster \"${CLUSTER_NAME}\"..."$COL_DEFAULT
echo
kind delete cluster --name ${CLUSTER_NAME}
rm -f ${RUNTIME_CONFIG_DIR}/kubeConfig
echo
echo -e $COL_BLUE"* Kubernetes cluster has been destroyed."$COL_DEFAULT
echo


# Delete variables.sh file from runtime-configuration directory
rm -f ${RUNTIME_CONFIG_DIR}/variables.sh


# If self-signed certificates were created, uninstall them
if [ -d "${CERTS_WORKDIR}" ]; then

  # Delete 'mkcert' temporary certificates from the runtime-configuration dir
  rm ${CLUSTER_CERTS_DIR}/wildcard.${REFERENCE_DOMAIN}.crt
  rm ${CLUSTER_CERTS_DIR}/wildcard.${REFERENCE_DOMAIN}.key

  # Remove the mkcert CA as trusted in the local machine
  echo
  echo -e $COL_BLUE"* Removing temporary Certification Authority (CA)..."$COL_DEFAULT
  echo
  cd ${CERTS_WORKDIR}
  CAROOT="${CERTS_WORKDIR}" bin/mkcert -uninstall
  sudo /usr/sbin/update-ca-certificates --fresh > /dev/null
  cd ${SCRIPT_DIR}
  echo
  echo -e $COL_BLUE"* Temporary Certification Authority (CA) removed."$COL_DEFAULT
  echo

  # Delete the certificates working directory
  rm -rf ${CERTS_WORKDIR}
fi
