#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

# This script generates self-signed certificates trusted by the local machine.
# It uses 'mkcert' for creating a temporary CA (Certification Authority) and
# configuring it in the local machine as trusted CA.

MKCERT_VERSION="v1.4.3"

CERTS_WORKDIR="${SCRIPT_DIR}/certs-creation"

mkdir -p ${CERTS_WORKDIR}

cd ${CERTS_WORKDIR}

# Install a copy of mkcert in a local directory
mkdir -p bin
wget --quiet -O mkcert https://github.com/FiloSottile/mkcert/releases/download/${MKCERT_VERSION}/mkcert-${MKCERT_VERSION}-linux-amd64
chmod +x mkcert
mv mkcert bin/

# Create a CA and install it as trusted
CAROOT="${CERTS_WORKDIR}" bin/mkcert -install

# Create a wildcard certificate for the cluster reference domain
CAROOT="${CERTS_WORKDIR}" bin/mkcert *.${REFERENCE_DOMAIN} 2> /dev/null
if [ "$?" == "0" ]; then
  echo "Created certificates for domain *.${REFERENCE_DOMAIN}."
else
  # Re-run the command showing the output (to see the error)
  CAROOT="${CERTS_WORKDIR}" bin/mkcert *.${REFERENCE_DOMAIN}
  if [ "$?" == "0" ]; then
    echo "Created certificates for domain *.${REFERENCE_DOMAIN}."
  else
    echo
    echo -e $COL_RED"Failed to create certificates for domain *.${REFERENCE_DOMAIN}."$COL_DEFAULT
    echo
    exit 1
  fi
fi

# Copy the new certificate to the correct directory to be used by the installer
mkdir -p ${CLUSTER_CERTS_DIR}
mv _wildcard.${REFERENCE_DOMAIN}.pem ${CLUSTER_CERTS_DIR}/wildcard.${REFERENCE_DOMAIN}.crt
mv _wildcard.${REFERENCE_DOMAIN}-key.pem ${CLUSTER_CERTS_DIR}/wildcard.${REFERENCE_DOMAIN}.key

# Return to previous directory
cd ${SCRIPT_DIR}