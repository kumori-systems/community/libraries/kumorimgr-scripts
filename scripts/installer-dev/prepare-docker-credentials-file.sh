#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################


DOCKER_CREDENTIALS_BASE_FILE="${PROVISION_DIR}/docker-credentials/docker-credentials-base.json"
DOCKER_CREDENTIALS_FILE="${PROVISION_DIR}/docker-credentials/docker-credentials.json"
DOCKER_CREDENTIALS_FILE_TMP="${PROVISION_DIR}/docker-credentials/docker-credentials_TMP.json"

# Create a "blank" docker credentials file
cp "${DOCKER_CREDENTIALS_BASE_FILE}" "${DOCKER_CREDENTIALS_FILE}"

# DockerHub credentials
if [ -n "${DOCKERHUB_USERNAME}" ] && [ -n "${DOCKERHUB_PASSWORD}" ]; then
  echo
  echo "Adding DockerHub credentials..."
  echo
  # Base64-encode username and password
  ENCODED_AUTH=$(echo -n "${DOCKERHUB_USERNAME}:${DOCKERHUB_PASSWORD}" | base64)

  cat "${DOCKER_CREDENTIALS_FILE}" \
    | jq ".auths += { \"https://index.docker.io/v1/\": { auth: \"${ENCODED_AUTH}\" } }" \
    > ${DOCKER_CREDENTIALS_FILE_TMP}

  cp ${DOCKER_CREDENTIALS_FILE_TMP} ${DOCKER_CREDENTIALS_FILE}
fi

