#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

COMMON_DIR="${SCRIPT_DIR}/../common"

# Load install configuration variables from file
source "${COMMON_DIR}/defs-colors.sh"

# Directories containing the necessary material for the installation
PROVISION_DIR="${COMMON_DIR}/provision"
KIND_FILES_DIR="${SCRIPT_DIR}/kind-files"
ADDONS_DIR="${SCRIPT_DIR}/addons"
RUNTIME_CONFIG_DIR="${SCRIPT_DIR}/runtime-configuration"


# Check all required software is installed in the local machine
# TODO: this could be improved to also check for specific versions.
REQUIRED_COMMANDS=( "docker" "kind" "kubectl" "jq" "yq" "sed" "nc" )
# REQUIRED_COMMANDS=( "docker" "kind" "kubectl" "jq" "yq" "sed")
ERRORS=()
for REQ_COM in ${REQUIRED_COMMANDS[@]}; do
  if ! command -v ${REQ_COM} &> /dev/null; then
    ERRORS+=(${REQ_COM})
  fi
done
if [[ "${#ERRORS[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"The following software is required but couldn't be found:"$COL_DEFAULT
  echo
  echo -e $COL_RED_BOLD"${ERRORS[*]}"$COL_DEFAULT
  echo
  echo -e $COL_RED"Please refer to the documentation for more details."$COL_DEFAULT
  echo
  exit -1
fi


# This script requires a parameter being passed.
# The addon installer image must be provided as a script parameter.
if [ -z "$1" ]; then
  echo -e $COL_RED"A mandatory parameter is missing (addon installer image version)."$COL_DEFAULT
  echo -e $COL_RED"Aborting installation."$COL_DEFAULT
  exit 1
else
  # Kumori addons installer docker image to use
  KUMORI_ADDON_INSTALLER_IMAGE="$1"
fi

# Create directory where runtime configuration files will be placed
mkdir -p ${RUNTIME_CONFIG_DIR}

# Validate cluster configuration file exists
CLUSTER_VARIABLES_FILE="${SCRIPT_DIR}/variables.sh"
if [ ! -f "${CLUSTER_VARIABLES_FILE}" ]; then
  echo -e $COL_RED"Couldn't find Host configuration variables file in ${CLUSTER_VARIABLES_FILE}."$COL_DEFAULT
  echo -e $COL_RED"Aborting installation."$COL_DEFAULT
  exit 1
else
  cp ${CLUSTER_VARIABLES_FILE} ${RUNTIME_CONFIG_DIR}/variables.sh
fi

# Load install configuration variables from file
source "${CLUSTER_VARIABLES_FILE}"

# Cluster name is called RELEASE_NAME in config vars
CLUSTER_NAME="${RELEASE_NAME}"


# Get time for measuring script duration
START_TIME=$SECONDS


# Show pre-installation information screen and ask for confirmation
source ${SCRIPT_DIR}/pre-install-info-screen.sh


# Pre-flight check: check that all configured ports are available in the local machine
ERRORS=()
# Check Ingress HTTPS port
if ( nc -z localhost ${INGRESS_EXTERNAL_PORT} 2>&1 >/dev/null ); then
  # If port accepts the connection, then it is in use
  ERRORS+=(${INGRESS_EXTERNAL_PORT})
fi
# Check Ingress TCP ports
for TCP_PORT in "${INGRESS_TCP_PORTS[@]}"; do
  if ( nc -z localhost ${TCP_PORT} 2>&1 >/dev/null ); then
    ERRORS+=(${TCP_PORT})
  fi
done
if [[ "${#ERRORS[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"The following required cluster ports are in use in the local machine:"$COL_DEFAULT
  echo
  echo -e $COL_RED_BOLD"${ERRORS[*]}"$COL_DEFAULT
  echo
  echo -e $COL_RED"Please make sure they are not in use or configure different ports for the cluster."$COL_DEFAULT
  echo
  exit -1
fi


###############################################################################
##  PREPARE CLUSTER CERTIFICATES                                             ##
###############################################################################
echo
echo -e $COL_BLUE"* Preparing cluster certificates and CAs..."$COL_DEFAULT
echo

# Prepare reference domain certificates (from runtime configuration dir)
CLUSTER_CERTS_DIR="${RUNTIME_CONFIG_DIR}/clusterCerts"

# Check required variables file exists
if [ ! -f "${CLUSTER_CERTS_DIR}/wildcard.${REFERENCE_DOMAIN}.crt" ] || \
   [ ! -f "${CLUSTER_CERTS_DIR}/wildcard.${REFERENCE_DOMAIN}.key" ]
then
  echo "Generating self-signed cluster certificates..."
  echo
  source ${SCRIPT_DIR}/generate-cluster-certificates.sh
  echo
  echo "Self-signed cluster certificates created!"
  echo
else
  echo "Found reference domain certificate files."
fi

mkdir -p $RUNTIME_CONFIG_DIR/kumoriPki

# Copy Admission certificate to the runtime configuration directory
if [ "${ADMISSION_AUTHENTICATION_TYPE}" == "clientcertificate" ]; then
  echo "Copying Admission certificate to the runtime configuration directory"
  mkdir -p $RUNTIME_CONFIG_DIR/kumoriPki/admission
  cp -r $ADMISSION_SERVER_CERT_DIR/* $RUNTIME_CONFIG_DIR/kumoriPki/admission
fi

# Copy the ROOTCA to the runtime configuration directory
if [ "${CLUSTER_PKI_ROOT_CA_FILE}" != "" ]; then
  echo "Copying the ROOTCA to the runtime configuration directory"
  mkdir -p $RUNTIME_CONFIG_DIR/kumoriPki/rootCA
  cp -r $CLUSTER_PKI_ROOT_CA_FILE $RUNTIME_CONFIG_DIR/kumoriPki/rootCA
fi

# Copy the Monitoring CA to the runtime configuration directory
if [ "${MONITORING_CLIENTCERT_TRUSTED_CA_FILE}" != "" ]; then
  echo "Copying the Monitoring CA to the runtime configuration directory"
  mkdir -p $RUNTIME_CONFIG_DIR/kumoriPki/monitoringCA
  cp -r $MONITORING_CLIENTCERT_TRUSTED_CA_FILE $RUNTIME_CONFIG_DIR/kumoriPki/monitoringCA
fi

echo "Current kumoriPki tree:"
tree $RUNTIME_CONFIG_DIR/kumoriPki

echo
echo -e $COL_BLUE"* Cluster certificates and CAs ready!"$COL_DEFAULT
echo

###############################################################################
##  PREPARE CLOUD CREDENTIALS                                                ##
###############################################################################
echo
echo -e $COL_BLUE"* Preparing cloud credentials (if any)..."$COL_DEFAULT
echo

# Copy the Openstack credentials to the runtime configuration directory
if [ "${INSTALL_OPENSTACK_CLOUD_CONTROLLER}" = "true" ]; then
  if [ "${OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_FILE}" != "" ]; then
    echo "Copying Openstack credentials to the runtime configuration directory"
    mkdir -p $RUNTIME_CONFIG_DIR/openstack-cloud-credentials
    cp -r $OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_FILE $RUNTIME_CONFIG_DIR/openstack-cloud-credentials
  fi
fi

echo
echo -e $COL_BLUE"* Cloud credentials ready!"$COL_DEFAULT
echo

###############################################################################
##  PREPARE KIND CLUSTER CONFIGURATION                                       ##
###############################################################################
echo
echo -e $COL_BLUE"* Preparing Kind configuration file for cluster \"${CLUSTER_NAME}\"..."$COL_DEFAULT
echo

KIND_CLUSTER_FILE_TEMPLATE="${KIND_FILES_DIR}/kind-kumoridns.yaml_template"
KIND_CLUSTER_FILE="${KIND_FILES_DIR}/kind-kumoridns.yaml"

# Create a new config file from the original template
cp ${KIND_CLUSTER_FILE_TEMPLATE} ${KIND_CLUSTER_FILE}

# Configure docker credentials file location in Kind cluster configuration
DOCKER_CREDENTIALS_FILE="${PROVISION_DIR}/docker-credentials/docker-credentials.json"
source ${SCRIPT_DIR}/prepare-docker-credentials-file.sh

sed -i \
  -e "s?{{DOCKER_CREDENTIALS_FILE}}?${DOCKER_CREDENTIALS_FILE}?g" \
  -e "s?{{KUMORI_COREDNS_VERSION}}?${KUMORI_COREDNS_VERSION}?g" \
  ${KIND_CLUSTER_FILE}

# Configure Ingress TCP ports (forward each TCP port from localhost to Ingress node container)
if [ -n "${INGRESS_TCP_PORTS}" ]; then
  # Add custom extra ports to listen to in Ambassador configuration
  TMP_FILE="${KIND_CLUSTER_FILE}_TMP"

  echo "Configuring TCP ports forwarding..."

  # for PORT in ${INGRESS_TCP_PORTS[@]}; do
  for TCP_PORT in "${INGRESS_TCP_PORTS[@]}"; do
    cat ${KIND_CLUSTER_FILE} \
      | yq r -j - \
      | jq ".nodes[1].extraPortMappings += [ { containerPort: ${TCP_PORT}, hostPort: ${TCP_PORT}, protocol: \"TCP\" } ]" \
      | yq r --prettyPrint - \
      > ${TMP_FILE}
    mv ${TMP_FILE} ${KIND_CLUSTER_FILE}
  done
  echo "Done."
fi

echo
echo -e $COL_BLUE"* Cluster configuration file created!"$COL_DEFAULT
echo


###############################################################################
##  CREATE KIND CLUSTER                                                      ##
###############################################################################
echo
echo -e $COL_BLUE"* Creating Kubernetes cluster \"${CLUSTER_NAME}\" from config file: ${KIND_CLUSTER_FILE}..."$COL_DEFAULT
echo
kind create cluster --name ${CLUSTER_NAME} --config ${KIND_CLUSTER_FILE}
EXIT_CODE="$?"
if [ "${EXIT_CODE}" != "0" ]; then
  echo
  echo -e $COL_RED"Error creating cluster \"${CLUSTER_NAME}\". Exiting."
  echo
  exit 1
else
  echo
  echo -e $COL_BLUE"* Kubernetes cluster created!"$COL_DEFAULT
  echo
fi

echo
echo -e $COL_BLUE"* Saving cluster kubeConfig file..."$COL_DEFAULT
echo
# Retrieve the cluster kubeConfig for using it in the addon installer container.
# The '--internal' flag will point ApiServer directly to the Master container instead
# of the Host redirected port.
kind get kubeconfig --internal --name ${CLUSTER_NAME} > ${RUNTIME_CONFIG_DIR}/kubeConfig
echo
echo -e $COL_BLUE"* Cluster kubeConfig file saved!"$COL_DEFAULT
echo

echo
echo -e $COL_BLUE"* Waiting for Kubernetes cluster to be up & running..."$COL_DEFAULT
echo
kubectl wait --for=condition=Ready pods --all --all-namespaces
echo
echo -e $COL_BLUE"* Kubernetes cluster is ready for use!"$COL_DEFAULT
echo

# Get time for measuring script duration
CLUSTER_ELAPSED_TIME=$(($SECONDS - $START_TIME))

# Create monitoring namespace
echo
echo -e $COL_BLUE"* Creating monitoring cluster"$COL_DEFAULT
echo
kubectl create namespace monitoring


###############################################################################
##  INSTALL KUMORI CONTROLLERS AND ADDONS                                    ##
###############################################################################
echo
echo -e $COL_BLUE"* Starting Kumori Platform addon installation..."$COL_DEFAULT
echo
docker run --rm \
  --network=kind \
  -v ${RUNTIME_CONFIG_DIR}:/kumori/runtime \
  ${KUMORI_ADDON_INSTALLER_IMAGE}
echo
echo -e $COL_BLUE"* Kumori Platform addons installed."$COL_DEFAULT
echo

# Get time for measuring script duration
ADDONS_ELAPSED_TIME=$(($SECONDS - $START_TIME - $CLUSTER_ELAPSED_TIME))

echo
echo -e $COL_BLUE"* Waiting for all cluster elements to be up and running..."$COL_DEFAULT
echo
# Wait for all cluster Pods to be ready
kubectl wait --for=condition=Ready pods --all --all-namespaces
echo
echo -e $COL_BLUE"* Kumori Platform Dev cluster is ready for use!"$COL_DEFAULT
echo

FULL_ELAPSED_TIME=$(($SECONDS - $START_TIME))



echo
echo
echo -e $COL_GREEN"*****************************************************************************************************"$COL_DEFAULT
echo
echo -e $COL_GREEN"Your Kumori cluster ${RELEASE_NAME} is ready to use."$COL_DEFAULT
echo -e $COL_GREEN"Configure your kumori CLI tools to point to Admission service at:  admission-${RELEASE_NAME}.${REFERENCE_DOMAIN} "$COL_DEFAULT
echo
echo
echo -e $COL_GREEN"Installation time:"$COL_DEFAULT
echo
echo -e $COL_GREEN"- Kind Kubernetes cluster installation:         $(($CLUSTER_ELAPSED_TIME/60)) min $(($CLUSTER_ELAPSED_TIME%60)) sec."$COL_DEFAULT
echo -e $COL_GREEN"- Kumori Platform Addons installation:          $(($ADDONS_ELAPSED_TIME/60)) min $(($ADDONS_ELAPSED_TIME%60)) sec."$COL_DEFAULT
echo -e $COL_GREEN"- Full cluster installation (all Pods running): $(($FULL_ELAPSED_TIME/60)) min $(($FULL_ELAPSED_TIME%60)) sec."$COL_DEFAULT
echo -e $COL_GREEN"*****************************************************************************************************"$COL_DEFAULT
echo
echo
