#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"

COMMON_DIR="${LOCAL_WORKDIR}/../common"

# Load install configuration variables from file
source "${COMMON_DIR}/defs-colors.sh"
source "${LOCAL_WORKDIR}/variables.sh"

################################################################
##         COMMUNITY-SPECIFIC CONFIGURATION CHECK             ##
################################################################
source "${COMMON_DIR}/preflight-checks/community-specific.sh"

# Timestamp label to use in files, traces, etc.
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

SSH_COMMAND="ssh -o ServerAliveInterval=15 -o ServerAliveCountMax=4"

REMOTE_WORKDIR=".kumori/Kumori_updater_${TIMESTAMP}"


################################################################################
##  VALIDATE TOPOLOGY CONFIGURATION                                           ##
################################################################################
# Validate that a list of MASTER nodes has been provided
if [[ "${#MASTERS_IPS[@]}" -eq 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - MASTERS_IPS can't be empty."$COL_DEFAULT
  echo
  exit -1
fi

# Validate that a list of WORKER nodes has been provided
# NOTE: We don't consider this an error anymore, only a warning
if [[ "${#WORKERS_IPS[@]}" -eq 0 ]]; then
  echo
  echo -e $COL_YELLOW"WARNING: No WORKER nodes provided. Cluster won't be fully functional."$COL_DEFAULT
  echo
fi

# Validate that a list of INGRESS nodes has been provided
if [[ "${#INGRESS_NODES_IPS[@]}" -eq 0 ]]; then
  # Only error if list of workers is not empty
  if [[ ! "${#WORKERS_IPS[@]}" -eq 0 ]]; then
    echo
    echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
    echo -e $COL_RED_BOLD" - INGRESS_NODES_IPS can't be empty."$COL_DEFAULT
    echo
    exit -1
  fi
fi

# Validate that all INGRESS nodes are also WORKER nodes
ERRORS=()
for INGRESS_NODE_IP in ${INGRESS_NODES_IPS[@]}; do
  if [[ ! " ${WORKERS_IPS[@]} " =~ " ${INGRESS_NODE_IP} " ]]; then
    ERRORS+=(${INGRESS_NODE_IP})
  fi
done
if [[ "${#ERRORS[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - Some INGRESS nodes are not in WORKER list: ${ERRORS[*]}"$COL_DEFAULT
  echo
  exit -1
fi


# Maintenance operations must be exclusive (no add or remove operations)
if [ -n "${ADD_MAINTENANCE_MASTERS_IPS}" ] || \
   [ -n "${REMOVE_MAINTENANCE_MASTERS_IPS}" ] || \
   [ -n "${ADD_MAINTENANCE_WORKERS_IPS}" ] || \
   [ -n "${REMOVE_MAINTENANCE_WORKERS_IPS}" ]; then
  if [ -n "${ADD_MASTERS_IPS}" ] || [ -n "${ADD_WORKERS_IPS}" ] || [ -n "${REMOVE_MASTERS_IPS}" ] || [ -n "${REMOVE_WORKERS_IPS}" ]; then
    echo
    echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
    echo -e $COL_RED_BOLD" - Maintenance operations can't be combined with Node additions or removals."$COL_DEFAULT
    echo
    exit -1
  fi
fi


# New nodes can't be in Maintenance mode
ERRORS=()
echo "MAINTENANCE_MASTERS_IPS: ${MAINTENANCE_MASTERS_IPS[*]}"
for MASTER_IP in ${ADD_MASTERS_IPS[@]}; do
  echo "Checking master: ${MASTER_IP}"
  if [[ " ${MAINTENANCE_MASTERS_IPS[@]} " =~ " ${MASTER_IP} " ]]; then
    ERRORS+=(${MASTER_IP})
  fi
done
echo "MAINTENANCE_WORKERS_IPS: ${MAINTENANCE_WORKERS_IPS[*]}"
for WORKER_IP in ${ADD_WORKERS_IPS[@]}; do
  echo "Checking worker: ${WORKER_IP}"
  if [[ " ${MAINTENANCE_WORKERS_IPS[@]} " =~ " ${WORKER_IP} " ]]; then
    ERRORS+=(${WORKER_IP})
  fi
done
if [[ "${#ERRORS[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - New Nodes can't be in Maintenance mode: ${ERRORS[*]}"$COL_DEFAULT
  echo
  exit -1
fi


# Verify if Etcd cluster is healthy or not
NUM_MASTERS=${#MASTERS_IPS[@]}
NUM_REMOVED_MASTERS=${#REMOVE_MASTERS_IPS[@]}
NUM_MAINTENANCE_MASTERS=${#MAINTENANCE_MASTERS_IPS[@]}
NUM_INITIAL_MASTERS=$(($NUM_MASTERS + $NUM_REMOVED_MASTERS))
NUM_UP_MASTERS=$(($NUM_MASTERS - $NUM_MAINTENANCE_MASTERS))
NUM_QUORUM=$(( ($NUM_INITIAL_MASTERS / 2) + 1 ))
if (( ${NUM_UP_MASTERS} < ${NUM_QUORUM} )); then
  echo
  echo -e $COL_YELLOW_BOLD"[WARNING] After this operation, Etcd cluster would have too many offline members."$COL_DEFAULT
  echo -e $COL_YELLOW_BOLD"          If you proceed, the cluster control plane High Availability will not be guaranteed."$COL_DEFAULT
  echo
  read -rsp $'--> Press any key to accept the risk and continue...\n' -n1 key
  echo
fi


################################################################################
##  SELECT A MASTER NODE TO ACT AS PRIMARY MASTER DURING INSTALLATION         ##
################################################################################
# Create a copy of the original Masters list (to print it at the end)
INITIAL_MASTERS_IPS=( "${MASTERS_IPS[@]}" )
if [[ -z "${SUPER_MASTER_IP}" ]]; then
  # No supermaster was provided, use the first master of the list that is not
  # new as supermaster.

  for MASTER_IP in ${MASTERS_IPS[@]}; do
    # If it's a new master to add, ignore it
    if [[ " ${ADD_MASTERS_IPS[@]} " =~ " ${MASTER_IP} " ]]; then
      continue
    fi
    # If it's a master to be removed, ignore it
    if [[ " ${REMOVE_MASTERS_IPS[@]} " =~ " ${MASTER_IP} " ]]; then
      continue
    fi
    # If we get here, select this node and end loop
    SUPER_MASTER_IP="${MASTER_IP}"
    break
  done
fi


################################################################################
##  PREPARE ASSOCIATION OF EACH MACHINE IP WITH A COLOR FOR CLEARER LOGGING   ##
################################################################################
declare -A IP_COLORS
###  IP_COLORS[$SUPER_MASTER_IP]=${BACKGROUND_COLORS[0]}
###  i=1
###  for ADDED_IP in ${ADD_MASTERS_IPS[@]}; do
###    IP_COLORS[$ADDED_IP]=${BACKGROUND_COLORS[$i]}
###    ((i++))
###  done
###  for ADDED_IP in ${ADD_WORKERS_IPS[@]}; do
###    IP_COLORS[$ADDED_IP]=${BACKGROUND_COLORS[$i]}
###    ((i++))
###  done
###  i=1
###  for REMOVED_IP in ${REMOVE_MASTERS_IPS[@]}; do
###    IP_COLORS[$REMOVED_IP]=${BACKGROUND_COLORS[$i]}
###    ((i++))
###  done
###  for REMOVED_IP in ${REMOVE_WORKERS_IPS[@]}; do
###    IP_COLORS[$REMOVED_IP]=${BACKGROUND_COLORS[$i]}
###    ((i++))
###  done

# Supermaster is one of the normal nodes,
IP_COLORS[$SUPER_MASTER_IP]=${BACKGROUND_COLORS[0]}
i=1
for MASTER_IP in ${MASTERS_IPS[@]}; do
  if [[ " ${IP_COLORS[@]} " =~ " ${MASTER_IP} " ]]; then
    # in case supermaster was already added
    continue
  fi
  IP_COLORS[$MASTER_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done
i=1
for WORKERS_IP in ${WORKERS_IPS[@]}; do
  IP_COLORS[$WORKERS_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done
for REMOVED_IP in ${REMOVE_MASTERS_IPS[@]}; do
  IP_COLORS[$REMOVED_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done
for REMOVED_IP in ${REMOVE_WORKERS_IPS[@]}; do
  IP_COLORS[$REMOVED_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done



echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                           CLUSTER UPDATER                                  ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER NAME : ${RELEASE_NAME}                                               "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## NODE TOPOLOGY CHANGES:                                                       "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Adding Masters IPs   : ${ADD_MASTERS_IPS[*]}                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Adding Workers IPs   : ${ADD_WORKERS_IPS[*]}                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Removing Masters IPs : ${REMOVE_MASTERS_IPS[*]}                           "$COL_DEFAULT
echo -e $COL_GREEN"##  - Removing Workers IPs : ${REMOVE_WORKERS_IPS[*]}                           "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## NODES IN MAINTENANCE:                                                        "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Master nodes:                                                             "$COL_DEFAULT
echo -e $COL_GREEN"##    - Put into maintenance  : ${ADD_MAINTENANCE_MASTERS_IPS[*]}               "$COL_DEFAULT
echo -e $COL_GREEN"##    - Back from maintenance : ${REMOVE_MAINTENANCE_MASTERS_IPS[*]}            "$COL_DEFAULT
echo -e $COL_GREEN"##    - Maintenance nodes     : ${MAINTENANCE_MASTERS_IPS[*]}                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - Worker nodes:                                                             "$COL_DEFAULT
echo -e $COL_GREEN"##    - Put into maintenance  : ${ADD_MAINTENANCE_WORKERS_IPS[*]}               "$COL_DEFAULT
echo -e $COL_GREEN"##    - Back from maintenance : ${REMOVE_MAINTENANCE_WORKERS_IPS[*]}            "$COL_DEFAULT
echo -e $COL_GREEN"##    - Maintenance nodes     : ${MAINTENANCE_WORKERS_IPS[*]}                   "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## NEW NODE TOPOLOGY:                                                           "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Masters IPs       : ${MASTERS_IPS[*]}                                     "$COL_DEFAULT
echo -e $COL_GREEN"##    Acting as primary : ${SUPER_MASTER_IP}                                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - Workers IPs       : ${WORKERS_IPS[*]}                                     "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress nodes IPs : ${INGRESS_NODES_IPS[*]}                               "$COL_DEFAULT
echo -e $COL_GREEN"##  - Storage nodes IPs : ${STORAGE_NODES_IPS[*]}                               "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
if [ -n "${MASTER_MIN_MEMORY_KB}" ]; then
  echo -e $COL_GREEN"##  - Master Nodes required Memory: ${MASTER_MIN_MEMORY_KB} KB                  "$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
fi
echo -e $COL_GREEN"## SOFTWARE VERSIONS (unchanged):                                               "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Kubernetes:      ${KUBERNETES_VERSION}                                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - Docker:          ${DOCKER_VERSION}                                        "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER CONFIGURATION (unchanged):                                           "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Release Name:           ${RELEASE_NAME}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - K8s APIServer Endpoint: ${APISERVER_URL}                                  "$COL_DEFAULT
echo -e $COL_GREEN"##  - K8s Pods Network CIDR:  ${PODS_NETWORK_CIDR}                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - K8s Service CIDR:       ${SERVICE_CIDR}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - Docker bridge CIDR:     ${DOCKER_BRIDGE_CIDR}                             "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Custom installations from: ${CUSTOM_DIR}                                  "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo
read -rsp $'--> Press any key to begin cluster update...\n' -n1 key
echo


## ################################################################################
## ##            INSTALL 'MOREUTILS' PACKAGE ON LOCAL MACHINE                    ##
## ################################################################################
## echo  -e $COL_GREEN" ************************************************************"$COL_DEFAULT
## echo  -e $COL_GREEN" *  Install 'moreutils' package for log tracing timestamps..."$COL_DEFAULT
## echo  -e $COL_GREEN" ************************************************************"$COL_DEFAULT
## sudo apt-get update -q && sudo apt-get install moreutils -y -q

# Prepare directory for storing remote installations output
LOGS_DIR="${LOCAL_WORKDIR}/logs_updater_${TIMESTAMP}"
mkdir -p "${LOGS_DIR}"


################################################################################
##          PRE-FLIGHT CHECKS: ALL NECESSARY FILE ARE ACCESSIBLE              ##
################################################################################
echo  -e $COL_GREEN" *******************************************************·"$COL_DEFAULT
echo  -e $COL_GREEN" *  Pre-Flight check: Access to all installation files..."$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************·"$COL_DEFAULT

NOT_ACCESSIBLE="$(find ${LOCAL_WORKDIR} \( -type d -not -executable \) -o \( -type f -not -readable \) 2> /dev/null)"
if [ "${NOT_ACCESSIBLE}" != "" ]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: Some required files or directories are not accessible. Check their permissions (recommended) or run the script as root."$COL_DEFAULT
  echo
  echo -e $COL_RED_BOLD"${NOT_ACCESSIBLE}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi

################################################################################
##                PRE-FLIGHT CHECKS: SSH ACCESS TARGET HOSTS                  ##
################################################################################
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" *  Pre-Flight check: SSH access to all machines...     "$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT

ALL_IPS=("${SUPER_MASTER_IP}" "${ADD_MASTERS_IPS[@]}" "${REMOVE_MASTERS_IPS[@]}" \
         "${ADD_WORKERS_IPS[@]}" "${REMOVE_WORKERS_IPS[@]}" \
         "${ADD_MAINTENANCE_MASTERS_IPS[@]}" "${REMOVE_MAINTENANCE_MASTERS_IPS[@]}" \
         "${ADD_MAINTENANCE_WORKERS_IPS[@]}" "${REMOVE_MAINTENANCE_WORKERS_IPS[@]}" \
        )


echo  -e $COL_BLUE"* Remove hosts fingerprints from known hosts list"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ssh-keygen -R ${IP} >/dev/null 2>&1
done
echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo


echo  -e $COL_BLUE"* Check SSH connectivity and refresh hosts fingerprints"$COL_DEFAULT

UNREACHABLE=()
for IP in ${ALL_IPS[@]}; do
  ssh-keyscan ${IP} 2>&1 | grep -vE "^$|route|getaddrinfo" > /dev/null
  [ $? != 0 ] && UNREACHABLE+=(${IP})
done
if [[ "${#UNREACHABLE[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: SSH connectivity problem with the following machines:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${UNREACHABLE[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


echo  -e $COL_BLUE"* Add hosts SSH fingerprints"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ssh-keyscan -H ${IP} >> ${HOME}/.ssh/known_hosts 2>/dev/null
done
echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo


echo  -e $COL_BLUE"* Check user can connect via SSH"$COL_DEFAULT

FAILCONNECT=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
for IP in ${ALL_IPS[@]}; do
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} uptime 2> /dev/null)"
  [ "${RES}" == "" ] && FAILCONNECT+=(${IP})
done
if [[ "${#FAILCONNECT[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: Couldn't connect via SSH to the folowing machines:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAILCONNECT[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


################################################################################
##        PRE-FLIGHT CHECKS: HOSTS USE A SUPPORTED OS VERSION                 ##
################################################################################
echo  -e $COL_BLUE"* Check node OS version"$COL_DEFAULT

# Currently only one OS version is supported per distribution
SUPPORTED_OS="${OS_FLAVOUR} ${OS_VERSION}"
echo
echo "Supported OS: ${SUPPORTED_OS}"
echo

FAIL_OS=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
COMMAND_GET_OS="echo \"\$(lsb_release -is) \$(lsb_release -rs)\""
for IP in ${ALL_IPS[@]}; do
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \(${COMMAND_GET_OS}\))"
  # echo "RES= ${RES} --> ${RES,,}   (expected: ${SUPPORTED_OS,,})"

  # Lowercase comparison --> lowercase conversion in bash: ${variable,,}
  [ "${RES,,}" != "${SUPPORTED_OS,,}" ] && FAIL_OS+=("${IP}:${RES}")
done
if [[ "${#FAIL_OS[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: The following machines failed OS version (${SUPPORTED_OS}) check:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAIL_OS[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


################################################################################
##      PRE-FLIGHT CHECKS: HOSTS USE A SUPPORTED KERNEL VERSION               ##
################################################################################
echo  -e $COL_BLUE"* Check node kernel version"$COL_DEFAULT

# Currently only one kernel version is supported per distribution (if set)
echo
if [ -n "${KERNEL_VERSION}" ]; then
  echo "Supported kernel version: ${KERNEL_VERSION}"

  FAIL_KERNEL=()
  CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
  COMMAND_GET_KERNEL="uname -r"
  for IP in ${ALL_IPS[@]}; do
    RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \(${COMMAND_GET_KERNEL}\))"
    [[ "${RES}" =~ ^${KERNEL_VERSION}.* ]] || FAIL_KERNEL+=("${IP} (${RES})  ")
  done
  if [[ "${#FAIL_KERNEL[@]}" -ne 0 ]]; then
    # Allow operator to continue even if the check fails
    # echo
    # echo -e $COL_RED_BOLD"Pre-flight error: The following machines failed kernel version (${KERNEL_VERSION}) check:"$COL_DEFAULT
    # echo -e $COL_RED_BOLD"                  ${FAIL_KERNEL[*]}"$COL_DEFAULT
    # echo
    # exit -1
    echo
    echo -e $COL_YELLOW_BOLD"Pre-flight warning: The following machines failed kernel version (${KERNEL_VERSION}) check:"$COL_DEFAULT
    echo -e $COL_YELLOW_BOLD"                  ${FAIL_KERNEL[*]}"$COL_DEFAULT
    echo
    echo -e $COL_YELLOW_BOLD"You can proceed with installation with a different kernel than the recommended at your own risk."$COL_DEFAULT
    read -rsp $'--> Press any key to accept the risk and continue, or press CTRL-C to exit...\n' -n1 key
    echo
  else
    echo
    echo -e $COL_GREEN"Passed."$COL_DEFAULT
    echo
  fi
else
  echo "No specific kernel version required. Skipped kernel version check."
fi



################################################################################
##        PRE-FLIGHT CHECKS: HOSTS DON'T USE SWAP                             ##
################################################################################
echo  -e $COL_BLUE"* Check node swap is disabled"$COL_DEFAULT

FAIL_SWAP=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
COMMAND_GET_SWAP="sudo swapon --show"
for IP in ${ALL_IPS[@]}; do
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \(${COMMAND_GET_SWAP}\))"
  [ "${RES}" != "" ] && FAIL_SWAP+=("${IP}")
done
if [[ "${#FAIL_SWAP[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: The following machines have swap enabled:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAIL_SWAP[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


################################################################################
##        PRE-FLIGHT CHECKS: MASTER NODES HAVE ENOUGH RAM                     ##
################################################################################
if [ -n "${MASTER_MIN_MEMORY_KB}" ]; then
  echo  -e $COL_BLUE"* Check Master Nodes have enough memory (more than ${MASTER_MIN_MEMORY_KB} KB)"$COL_DEFAULT

  FAIL_MEM=()
  CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
  COMMAND_GET_MEM="free -m | awk '/^Mem:/{print \$2}'"
  for IP in ${INITIAL_MASTERS_IPS[@]}; do
    RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \(${COMMAND_GET_MEM}\))"
    [ "${RES}" -gt "${MASTER_MIN_MEMORY_KB}" ] || FAIL_MEM+=("${IP}")
  done
  if [[ "${#FAIL_MEM[@]}" -ne 0 ]]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: The following machines don't meet the memory requirements:"$COL_DEFAULT
    echo -e $COL_RED_BOLD"                  ${FAIL_MEM[*]}"$COL_DEFAULT
    echo
    exit -1
  else
    echo
    echo -e $COL_GREEN"Passed."$COL_DEFAULT
    echo
  fi
fi


################################################################################
##        PRE-FLIGHT CHECKS: HOSTS INTERNET ACCESS AND DNS RESOLUTION         ##
################################################################################
echo  -e $COL_BLUE"* Check node internet access and DNS"$COL_DEFAULT

FAIL_INTERNET=()
FAIL_DNS=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
TEST_TARGET_IP="8.8.8.8"
TEST_TARGET_DOMAIN="google.com"
COMMAND_TEST_IP="if nc -zw2 ${TEST_TARGET_IP} 443; then echo OK; else echo ERROR; fi"
COMMAND_TEST_DOMAIN="if nc -zw2 ${TEST_TARGET_DOMAIN} 443; then echo OK; else echo ERROR; fi"
for IP in ${ALL_IPS[@]}; do
  # RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \( hostname \))"
  # echo "RES= ${RES}"
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \(${COMMAND_TEST_IP}\))"
  # echo "RES= ${RES}"
  [ "${RES}" != "OK" ] && FAIL_INTERNET+=(${IP})
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \(${COMMAND_TEST_DOMAIN}\))"
  # echo "RES= ${RES}"
  [ "${RES}" != "OK" ] && FAIL_DNS+=(${IP})
done
if [[ "${#FAIL_INTERNET[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: The following machines failed Internet access check:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAIL_INTERNET[*]}"$COL_DEFAULT
  echo
fi
if [[ "${#FAIL_DNS[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: The following machines failed DNS resolution check:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAIL_DNS[*]}"$COL_DEFAULT
  echo
fi
if [[ "${#FAIL_DNS[@]}" -ne 0 || "${#FAIL_INTERNET[@]}" -ne 0 ]]; then
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


# Create .kumori directory in all remote machines
echo  -e $COL_BLUE"* Creating .kumori directory in remote machines"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} mkdir -p .kumori &
done
wait

# Deactivate login welcome info
echo  -e $COL_BLUE"* Deactivating login welcome info in remote machines"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} touch .hushlogin &
done
wait


################################################################################
##                          START CLUSTER UPDATES                             ##
################################################################################

################################################################################
##  UPDATE INTERNAL BALANCERS SERVICE-MONITORS CONFIGURATION                  ##
################################################################################
# Update internal load-balancer ServiceMonitors endpoints (if balancing is enabled)
if  [ "${APISERVER_INTERNAL_BALANCING}" = "true" ] || [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  IP="${SUPER_MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" *  Update load-balancer ServiceMonitors endpoints in PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-primary-update-servicemonitors.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &

  wait
fi


################################################################################
##  PREPARE JOIN SCRIPTS AND KUBECONFIG IF NECESSARY                          ##
################################################################################
# If there is any new node, generate join scripts on Super-Master machine
if [ "${#ADD_MASTERS_IPS[@]}" != 0 ] || [ "${#ADD_WORKERS_IPS[@]}" != 0 ]; then
  IP="${SUPER_MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" *  Generating join commands in PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-primary-get-join-scripts.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &

  wait
fi


################################################################################
##  PUT MASTER NODES BACK FROM MAINTENANCE MODE                               ##
################################################################################
# Master maintenance: (parallelized)
# 1. from a Master node, uncordon the nodes
if [ "${#REMOVE_MAINTENANCE_MASTERS_IPS[@]}" != 0 ]; then
  if [ -f "${LOCAL_WORKDIR}/control-primary-maintenance-off-masters.sh" ]; then
    IP="${SUPER_MASTER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
    echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Disabling Maintenance mode for nodes ${REMOVE_MAINTENANCE_MASTERS_IPS[*]} from MASTER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-primary-maintenance-off-masters.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &
  else
    echo  -e $COL_GREEN" *  Skipped Maintenance node operation (not supported)."$COL_DEFAULT
  fi
  wait
fi


################################################################################
##  ADD NEW MASTER NODES                                                      ##
################################################################################
# Run software installation in all new Masters (in parallel)
for MASTER_IP in ${ADD_MASTERS_IPS[@]}; do
  IP="${MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/add_master_${IP}.log"
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" * Processing new MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-install-master-secondary.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}+MASTER ${IP}$COL_DEFAULT]") /" &
done

wait

# Master cluster joins are not parallelized to avoid Etcd overload
FIRST="true"
for MASTER_IP in ${ADD_MASTERS_IPS[@]}; do
  if [[ "${FIRST}" == "true" ]]; then
    FIRST="false"
  else
    echo
    echo  -e $COL_BLUE"* Waiting one minute for previous master join to stabilize..."$COL_DEFAULT
    echo
    sleep 60s
  fi
  IP="${MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/master_${IP}.log"
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" * Joining MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-join-master-secondary.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}MASTER ${IP}$COL_DEFAULT]") /"
done


echo
echo  -e $COL_GREEN" *****************************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" * Secondary masters installation finished. Prepare workers..."$COL_DEFAULT
echo  -e $COL_GREEN" *****************************************************************"$COL_DEFAULT
echo



################################################################################
##  PUT WORKER NODES BACK FROM MAINTENANCE MODE                               ##
################################################################################
# Worker maintenance: (parallelized)
# 1. from a Master node, uncordon the node
if [ "${#REMOVE_MAINTENANCE_WORKERS_IPS[@]}" != 0 ]; then
  if [ -f "${LOCAL_WORKDIR}/control-primary-maintenance-off-workers.sh" ]; then
    IP="${SUPER_MASTER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
    echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Disabling Maintenance mode for nodes ${REMOVE_MAINTENANCE_WORKERS_IPS[*]} from MASTER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-primary-maintenance-off-workers.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &
  else
    echo  -e $COL_GREEN" *  Skipped Maintenance node operation (not supported)."$COL_DEFAULT
  fi

  wait
fi

################################################################################
##  ADD NEW WORKER NODES                                                      ##
################################################################################
# Run software installation in all new Workers and join them to the cluster (in parallel)
for WORKER_IP in ${ADD_WORKERS_IPS[@]}; do
  IP="${WORKER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/add_worker_${IP}.log"
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" * Processing new WORKER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-install-worker.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}+WORKER ${IP}$COL_DEFAULT]") /" &
done

wait


###################################################################################
##  DISABLE KEEPALIVED IN OLD OR MAINTENANCE NODES (BEFORE UPDATING BALANCERS)   ##
###################################################################################
if [ "${APISERVER_HANDLE_FLOATING_IP}" = "true" ]; then
  # Disable in Master nodes being removed
  for MASTER_IP in ${REMOVE_MASTERS_IPS[@]}; do
    IP="${MASTER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/remove_master_${IP}.log"
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Disabling Keepalived in MASTER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-master-keepalived-disable.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}+MASTER ${IP}$COL_DEFAULT]") /" &
  done
  # Disable in Master nodes going to maintenance
  for MASTER_IP in ${ADD_MAINTENANCE_MASTERS_IPS[@]}; do
    IP="${MASTER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/remove_master_${IP}.log"
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Disabling Keepalived in MASTER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-master-keepalived-disable.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}+MASTER ${IP}$COL_DEFAULT]") /" &
  done
fi

wait

if [ "${INGRESS_HANDLE_FLOATING_IP}" = "true" ]; then
  # Disable in Worker nodes being removed
  for WORKER_IP in ${REMOVE_WORKERS_IPS[@]}; do
    IP="${WORKER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/remove_worker_${IP}.log"
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Disabling Keepalived in WORKER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-worker-keepalived-disable.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}+WORKER ${IP}$COL_DEFAULT]") /" &
  done
  # Disable in Workers nodes going to maintenance
  for WORKER_IP in ${ADD_MAINTENANCE_WORKERS_IPS[@]}; do
    IP="${WORKER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/remove_worker_${IP}.log"
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Disabling Keepalived in WORKER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-worker-keepalived-disable.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}+WORKER ${IP}$COL_DEFAULT]") /" &
  done
fi

wait

# Give some time for Keepalived of removed nodes to go into FAULT state
sleep 10

################################################################################
##  UPDATE INTERNAL LOAD-BALANCERS CONFIGURATION IF NECESSARY                 ##
################################################################################
# If any Master is being added or removed:
# UPDATE ALL MASTERS INTERNAL BALANCERS TARGETS (if internal balancing is on)
# We do it after adding the new nodes and before removing nodes, in case all
# nodes are replaced (rare untested scenario).
if [ "${APISERVER_INTERNAL_BALANCING}" = "true" ]; then
  if [ "${#ADD_MASTERS_IPS[@]}" != 0 ] || \
    [ "${#REMOVE_MASTERS_IPS[@]}" != 0 ] || \
    [ "${#ADD_MAINTENANCE_MASTERS_IPS[@]}" != 0 ] || \
    [ "${#REMOVE_MAINTENANCE_MASTERS_IPS[@]}" != 0 ]; then
    # Create a list of all masters (could be optimized removing the ones just added)
    ENDPOINTS=()
    # ENDPOINTS+=(${SUPER_MASTER_IP})
    for MASTER_IP in ${MASTERS_IPS[@]}; do
      # If master node is not in maintenance, it's an edpoint
      if [[ " ${MAINTENANCE_MASTERS_IPS[@]} " =~ " ${MASTER_IP} " ]]; then
        # Master is in maintenance, skip it
        # ":" is bash's no-command (do nothing)
        :
      else
        ENDPOINTS+=(${MASTER_IP})
      fi
    done

    for MASTER_IP in ${ENDPOINTS[@]}; do
      IP="${MASTER_IP}"
      OUTPUT_FILE="${LOGS_DIR}/master_${IP}.log"
      echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
      echo  -e $COL_GREEN" * Updating internal load-balancer of MASTER node in ${IP}..."$COL_DEFAULT
      echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
      . ${LOCAL_WORKDIR}/control-update-master-balancing.sh \
        |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
        |& tee -a ${OUTPUT_FILE} \
        |& sed "s/^/$(printf "[${IP_COLORS[$IP]}MASTER ${IP}$COL_DEFAULT]") /" &
    done
  fi
fi

# If any Worker is being added or removed (could be improved to "if any Ingress node"):
# UPDATE ALL INGRESS WORKERS INTERNAL BALANCERS TARGETS (if internal balancing is on)
# We do it after adding the new nodes and before removing nodes, in case all
# nodes are replaced (rare untested scenario).
if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  if [ "${#ADD_WORKERS_IPS[@]}" != 0 ] || \
    [ "${#REMOVE_WORKERS_IPS[@]}" != 0 ] || \
    [ "${#ADD_MAINTENANCE_WORKERS_IPS[@]}" != 0 ] || \
    [ "${#REMOVE_MAINTENANCE_WORKERS_IPS[@]}" != 0 ]; then
    for WORKER_IP in ${INGRESS_NODES_IPS[@]}; do
      if [[ " ${MAINTENANCE_WORKERS_IPS[@]} " =~ " ${WORKER_IP} " ]]; then
        # Node is in maintenance, skip it
        # ":" is bash's no-command (do nothing)
        :
      else
        IP="${WORKER_IP}"
        OUTPUT_FILE="${LOGS_DIR}/worker_${IP}.log"
        echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
        echo  -e $COL_GREEN" * Updating internal load-balancer of WORKER node in ${IP}..."$COL_DEFAULT
        echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
        . ${LOCAL_WORKDIR}/control-update-worker-balancing.sh \
          |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
          |& tee -a ${OUTPUT_FILE} \
          |& sed "s/^/$(printf "[${IP_COLORS[$IP]}WORKER ${IP}$COL_DEFAULT]") /" &
      fi
    done
  fi
fi

wait


#########################################################################################
##  ENABLE KEEPALIVED IN NEW OR BACK FROM MAINTENANCE NODES (AFTER UPDATING BALANCERS) ##
#########################################################################################
if [ "${APISERVER_HANDLE_FLOATING_IP}" = "true" ]; then
  # Enable in Master nodes being removed
  for MASTER_IP in ${ADD_MASTERS_IPS[@]}; do
    IP="${MASTER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/add_master_${IP}.log"
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Enabling Keepalived in new MASTER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-master-keepalived-enable.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}+MASTER ${IP}$COL_DEFAULT]") /" &
  done
  # Enable in Master nodes going to maintenance
  for MASTER_IP in ${REMOVE_MAINTENANCE_MASTERS_IPS[@]}; do
    IP="${MASTER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/add_master_${IP}.log"
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Re-enabling Keepalived in MASTER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-master-keepalived-enable.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}+MASTER ${IP}$COL_DEFAULT]") /" &
  done
fi

wait

if [ "${INGRESS_HANDLE_FLOATING_IP}" = "true" ]; then
  # Enable in Worker nodes being removed
  for WORKER_IP in ${ADD_WORKERS_IPS[@]}; do
    IP="${WORKER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/add_worker_${IP}.log"
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Enabling Keepalived in new WORKER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-worker-keepalived-enable.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}+WORKER ${IP}$COL_DEFAULT]") /" &
  done
  # Enable in Worker nodes going to maintenance
  for MASTER_IP in ${REMOVE_MAINTENANCE_WORKERS_IPS[@]}; do
    IP="${WORKER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/add_worker_${IP}.log"
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Re-enabling Keepalived in WORKER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-worker-keepalived-enable.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}+WORKER ${IP}$COL_DEFAULT]") /" &
  done
fi

wait


################################################################################
##  PUT MASTER NODES IN MAINTENANCE MODE                                      ##
################################################################################
# Master maintenance: (not parallelized to avoid Etcd overload)
# 1. from a Master node, drain the node to be removed
if [ "${#ADD_MAINTENANCE_MASTERS_IPS[@]}" != 0 ]; then
  if [ -f "${LOCAL_WORKDIR}/control-primary-maintenance-on-masters.sh" ]; then
    IP="${SUPER_MASTER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
    echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Enabling Maintenance mode for nodes ${ADD_MAINTENANCE_MASTERS_IPS[*]} from MASTER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-primary-maintenance-on-masters.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &
  else
    echo  -e $COL_GREEN" *  Skipped Maintenance node operation (not supported)."$COL_DEFAULT
  fi

  wait
fi


################################################################################
##  PUT WORKER NODES IN MAINTENANCE MODE                                      ##
################################################################################
# Worker maintenance: (parallelized)
# 1. from a Master node, drain the node to be removed
if [ "${#ADD_MAINTENANCE_WORKERS_IPS[@]}" != 0 ]; then
  if [ -f "${LOCAL_WORKDIR}/control-primary-maintenance-on-workers.sh" ]; then
    IP="${SUPER_MASTER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
    echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Enabling Maintenance mode for nodes ${ADD_MAINTENANCE_WORKERS_IPS[*]} from MASTER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-primary-maintenance-on-workers.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &
  else
    echo  -e $COL_GREEN" *  Skipped Maintenance node operation (not supported)."$COL_DEFAULT
  fi

  wait
fi


################################################################################
##  REMOVE MASTER NODES                                                       ##
################################################################################
# Master removals: (not parallelized to avoid Etcd overload)
# 1. from a Master node, drain the node to be removed
# 2. from each Master node to be removed, run a kubeadm reset
# 3. from a Master node, delete the nodes to be removed from Etcd state

if [ "${#REMOVE_MASTERS_IPS[@]}" != 0 ]; then
  # 1. from a Master node, drain the nodes to be removed
  IP="${SUPER_MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" *  Draining Master nodes from PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-primary-drain-masters.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &

  wait


  # 2. from each Master node to be removed, run a kubeadm reset giving some time to
  #    correctly reconfigure Etcd cluster
  FIRST="true"
  for MASTER_IP in ${REMOVE_MASTERS_IPS[@]}; do
    if [[ "${FIRST}" == "true" ]]; then
      FIRST="false"
    else
      echo
      echo  -e $COL_BLUE"* Waiting one minute for previous master join to stabilize..."$COL_DEFAULT
      echo
      sleep 60s
    fi
    IP="${MASTER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/remove_master_${IP}.log"
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Processing reset of MASTER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-reset-master.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}-MASTER ${IP}$COL_DEFAULT]") /"
  done


  # 3. from a Master node, delete the nodes to be removed from Etcd state
  IP="${SUPER_MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" *  Deleting Master nodes from PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-primary-delete-masters.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &
fi

wait


################################################################################
##  REMOVE WORKER NODES                                                       ##
################################################################################
# Worker removal
# 1. from a Master node, drain the node to be removed
# 2. from each Worker node to be removed, run a kubeadm reset
# 3. from a Master node, delete the nodes to be removed from Etcd state

if [ "${#REMOVE_WORKERS_IPS[@]}" != 0 ]; then
  # 1. from a Master node, drain the node to be removed
  IP="${SUPER_MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" *  Draining Worker nodes from PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-primary-drain-workers.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &

  wait

  # 2. from each Worker node to be removed, run a kubeadm reset
  for WORKER_IP in ${REMOVE_WORKERS_IPS[@]}; do
    IP="${WORKER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/remove_worker_${IP}.log"
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Processing reset of WORKER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-reset-worker.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}-WORKER ${IP}$COL_DEFAULT]") /" &
  done

  wait

  # 3. from a Master node, delete the nodes to be removed from Etcd state
  IP="${SUPER_MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" *  Deleting Worker nodes from PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-primary-delete-workers.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &
fi

wait


################################################################################
##  RECONFIGURE STORAGE POOLS                                                 ##
################################################################################
# Storage pool reconfiguration:
# 1. from a Master node, reconfigure storage pools

if [ "${#ADD_STORAGE_IPS[@]}" != 0 ] || [ "${#REMOVE_STORAGE_IPS[@]}" != 0 ]; then
  if [ -f "${LOCAL_WORKDIR}/control-primary-reconfigure-storage.sh" ]; then
    # 1. from a Master node, reconfigure storage pools
    IP="${SUPER_MASTER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
    echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" *  Reconfiguring storage pools from PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-primary-reconfigure-storage.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &
  else
    echo  -e $COL_GREEN" *  Skipped OpenEBS StoragePool reconfiguration (not supported)."$COL_DEFAULT
  fi
fi

wait





# Reactivate login welcome info
# echo  -e $COL_BLUE"* Reactivating login welcome info in remote machines"$COL_DEFAULT
# for IP in ${ALL_IPS[@]}; do
#   ${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} rm -f .hushlogin &
# done
# wait

echo
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                      CLUSTER UPDATE COMPLETED                              ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER TOPOLOGY CHANGES:                                                    "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Added Masters IPs    : ${ADD_MASTERS_IPS[*]}                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Added Workers IPs    : ${ADD_WORKERS_IPS[*]}                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Removed Masters IPs  : ${REMOVE_MASTERS_IPS[*]}                           "$COL_DEFAULT
echo -e $COL_GREEN"##  - Removed Workers IPs  : ${REMOVE_WORKERS_IPS[*]}                           "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## NEW CLUSTER TOPOLOGY:                                                        "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Masters IPs        : ${INITIAL_MASTERS_IPS[*]}                            "$COL_DEFAULT
echo -e $COL_GREEN"##  - Workers IPs        : ${WORKERS_IPS[*]}                                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress nodes IPs  : ${INGRESS_NODES_IPS[*]}                              "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo
