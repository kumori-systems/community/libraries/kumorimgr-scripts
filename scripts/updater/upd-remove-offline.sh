#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"

COMMON_DIR="${LOCAL_WORKDIR}/../common"

# Load install configuration variables from file
source "${COMMON_DIR}/defs-colors.sh"
source "${LOCAL_WORKDIR}/variables.sh"

################################################################
##         COMMUNITY-SPECIFIC CONFIGURATION CHECK             ##
################################################################
source "${COMMON_DIR}/preflight-checks/community-specific.sh"

# Timestamp label to use in files, traces, etc.
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

SSH_COMMAND="ssh -o ServerAliveInterval=15 -o ServerAliveCountMax=4"

REMOTE_WORKDIR=".kumori/Kumori_updater_${TIMESTAMP}"


################################################################################
##  VALIDATE TOPOLOGY CONFIGURATION                                           ##
################################################################################
# Validate that a list of MASTER nodes has been provided
if [[ "${#MASTERS_IPS[@]}" -eq 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - MASTERS_IPS can't be empty."$COL_DEFAULT
  echo
  exit -1
fi

# Validate that a list of WORKER nodes has been provided
# NOTE: We don't consider this an error anymore, only a warning
if [[ "${#WORKERS_IPS[@]}" -eq 0 ]]; then
  echo
  echo -e $COL_YELLOW"WARNING: No WORKER nodes provided. Cluster won't be fully functional."$COL_DEFAULT
  echo
fi

# Validate that a list of INGRESS nodes has been provided
if [[ "${#INGRESS_NODES_IPS[@]}" -eq 0 ]]; then
  # Only error if list of workers is not empty
  if [[ ! "${#WORKERS_IPS[@]}" -eq 0 ]]; then
    echo
    echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
    echo -e $COL_RED_BOLD" - INGRESS_NODES_IPS can't be empty."$COL_DEFAULT
    echo
    exit -1
  fi
fi

# Validate that all INGRESS nodes are also WORKER nodes
ERRORS=()
for INGRESS_NODE_IP in ${INGRESS_NODES_IPS[@]}; do
  if [[ ! " ${WORKERS_IPS[@]} " =~ " ${INGRESS_NODE_IP} " ]]; then
    ERRORS+=(${INGRESS_NODE_IP})
  fi
done
if [[ "${#ERRORS[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - Some INGRESS nodes are not in WORKER list: ${ERRORS[*]}"$COL_DEFAULT
  exit -1
fi

# Validate that no new Nodes have been provided
if [ -n "${ADD_MASTERS_IPS}" ] || [ -n "${ADD_WORKERS_IPS}" ]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - This command is only for removing nodes, but some new nodes were provided."$COL_DEFAULT
  if [ -n "${ADD_MASTERS_IPS}" ]; then
    echo -e $COL_RED_BOLD"   New Masters: ${ADD_MASTERS_IPS[*]}"$COL_DEFAULT
  fi
  if [ -n "${ADD_WORKERS_IPS}" ]; then
    echo -e $COL_RED_BOLD"   New Workers: ${ADD_WORKERS_IPS[*]}"$COL_DEFAULT
  fi
  echo
  exit -1
fi


# Verify if Etcd cluster is healthy or not
NUM_MASTERS=${#MASTERS_IPS[@]}
NUM_REMOVED_MASTERS=${#REMOVE_MASTERS_IPS[@]}
NUM_INITIAL_MASTERS=$(($NUM_MASTERS + $NUM_REMOVED_MASTERS))
NUM_QUORUM=$(( ($NUM_INITIAL_MASTERS / 2) + 1 ))
if (( ${NUM_MASTERS} < ${NUM_QUORUM} )); then
  echo
  echo -e $COL_YELLOW_BOLD"[WARNING] The Etcd cluster has too many offline members."$COL_DEFAULT
  echo -e $COL_YELLOW"          After this script removes the offline nodes, you should perform the necessary"$COL_DEFAULT
  echo -e $COL_YELLOW"          operations for recovering the Etcd cluster before adding new nodes."$COL_DEFAULT
  echo
fi


################################################################################
##  SELECT A MASTER NODE TO ACT AS PRIMARY MASTER DURING INSTALLATION         ##
################################################################################
# Create a copy of the original Masters list (to print it at the end)
INITIAL_MASTERS_IPS=( "${MASTERS_IPS[@]}" )
if [[ -z "${SUPER_MASTER_IP}" ]]; then
  # No supermaster was provided, use the first master of the list that is not
  # new as supermaster.

  for MASTER_IP in ${MASTERS_IPS[@]}; do
    # If it's a new master to add, ignore it
    if [[ " ${ADD_MASTERS_IPS[@]} " =~ " ${MASTER_IP} " ]]; then
      continue
    fi
    # If it's a master to be removed, ignore it
    if [[ " ${REMOVE_MASTERS_IPS[@]} " =~ " ${MASTER_IP} " ]]; then
      continue
    fi
    # If we get here, select this node and end loop
    SUPER_MASTER_IP="${MASTER_IP}"
    break
  done
fi


################################################################################
##  PREPARE ASSOCIATION OF EACH MACHINE IP WITH A COLOR FOR CLEARER LOGGING   ##
################################################################################
declare -A IP_COLORS
###  IP_COLORS[$SUPER_MASTER_IP]=${BACKGROUND_COLORS[0]}
###  i=1
###  for ADDED_IP in ${ADD_MASTERS_IPS[@]}; do
###    IP_COLORS[$ADDED_IP]=${BACKGROUND_COLORS[$i]}
###    ((i++))
###  done
###  for ADDED_IP in ${ADD_WORKERS_IPS[@]}; do
###    IP_COLORS[$ADDED_IP]=${BACKGROUND_COLORS[$i]}
###    ((i++))
###  done
###  i=1
###  for REMOVED_IP in ${REMOVE_MASTERS_IPS[@]}; do
###    IP_COLORS[$REMOVED_IP]=${BACKGROUND_COLORS[$i]}
###    ((i++))
###  done
###  for REMOVED_IP in ${REMOVE_WORKERS_IPS[@]}; do
###    IP_COLORS[$REMOVED_IP]=${BACKGROUND_COLORS[$i]}
###    ((i++))
###  done

# Supermaster is one of the normal nodes,
IP_COLORS[$SUPER_MASTER_IP]=${BACKGROUND_COLORS[0]}
i=1
for MASTER_IP in ${MASTERS_IPS[@]}; do
  if [[ " ${IP_COLORS[@]} " =~ " ${MASTER_IP} " ]]; then
    # in case supermaster was already added
    continue
  fi
  IP_COLORS[$MASTER_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done
i=1
for WORKERS_IP in ${WORKERS_IPS[@]}; do
  IP_COLORS[$WORKERS_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done
for REMOVED_IP in ${REMOVE_MASTERS_IPS[@]}; do
  IP_COLORS[$REMOVED_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done
for REMOVED_IP in ${REMOVE_WORKERS_IPS[@]}; do
  IP_COLORS[$REMOVED_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done



echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##         CLUSTER UPDATER - FORCED REMOVAL OF OFFLINE NODES                  ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER NAME : ${RELEASE_NAME}                                               "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## NODE TOPOLOGY CHANGES:                                                       "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Removing Masters IPs : ${REMOVE_MASTERS_IPS[*]}                           "$COL_DEFAULT
echo -e $COL_GREEN"##  - Removing Workers IPs : ${REMOVE_WORKERS_IPS[*]}                           "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## NEW NODE TOPOLOGY:                                                           "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Masters IPs       : ${MASTERS_IPS[*]}                                     "$COL_DEFAULT
echo -e $COL_GREEN"##    Acting as primary : ${SUPER_MASTER_IP}                                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - Workers IPs       : ${WORKERS_IPS[*]}                                     "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress nodes IPs : ${INGRESS_NODES_IPS[*]}                               "$COL_DEFAULT
echo -e $COL_GREEN"##  - Storage nodes IPs : ${STORAGE_NODES_IPS[*]}                               "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo
echo
echo -e $COL_RED_BOLD"IMPORTANT: The nodes to be removed must be offline before proceeding."$COL_DEFAULT
echo
read -rsp $'--> Press any key to begin cluster update...\n' -n1 key
echo


# Prepare directory for storing remote installations output
LOGS_DIR="${LOCAL_WORKDIR}/logs_updater_${TIMESTAMP}"
mkdir -p "${LOGS_DIR}"


################################################################################
##          PRE-FLIGHT CHECKS: ALL NECESSARY FILES ARE ACCESSIBLE             ##
################################################################################
echo  -e $COL_GREEN" *******************************************************·"$COL_DEFAULT
echo  -e $COL_GREEN" *  Pre-Flight check: Access to all installation files..."$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************·"$COL_DEFAULT

NOT_ACCESSIBLE="$(find ${LOCAL_WORKDIR} \( -type d -not -executable \) -o \( -type f -not -readable \) 2> /dev/null)"
if [ "${NOT_ACCESSIBLE}" != "" ]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: Some required files or directories are not accessible. Check their permissions (recommended) or run the script as root."$COL_DEFAULT
  echo
  echo -e $COL_RED_BOLD"${NOT_ACCESSIBLE}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi

################################################################################
##                PRE-FLIGHT CHECKS: SSH ACCESS TARGET HOSTS                  ##
################################################################################
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" *  Pre-Flight check: SSH access to a Master machine... "$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT

ALL_IPS=( "${MASTERS_IPS[@]}" "${WORKERS_IPS[@]}" )

echo  -e $COL_BLUE"* Remove hosts fingerprints from known hosts list"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ssh-keygen -R ${IP} >/dev/null 2>&1
done
echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo


echo  -e $COL_BLUE"* Check SSH connectivity and refresh hosts fingerprints"$COL_DEFAULT

UNREACHABLE=()
for IP in ${ALL_IPS[@]}; do
  ssh-keyscan ${IP} 2>&1 | grep -vE "^$|route|getaddrinfo" > /dev/null
  [ $? != 0 ] && UNREACHABLE+=(${IP})
done
if [[ "${#UNREACHABLE[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: SSH connectivity problem with the following machines:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${UNREACHABLE[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


echo  -e $COL_BLUE"* Add hosts SSH fingerprints"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ssh-keyscan -H ${IP} >> ${HOME}/.ssh/known_hosts 2>/dev/null
done
echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo


echo  -e $COL_BLUE"* Check user can connect via SSH"$COL_DEFAULT

FAILCONNECT=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
for IP in ${ALL_IPS[@]}; do
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} uptime 2> /dev/null)"
  [ "${RES}" == "" ] && FAILCONNECT+=(${IP})
done
if [[ "${#FAILCONNECT[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: Couldn't connect via SSH to the folowing machines:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAILCONNECT[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


################################################################################
##           PRE-FLIGHT CHECKS: MACHINES TO REMOVED ARE OFFLINE               ##
################################################################################
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" *  Pre-Flight check: Machines to remove are offline... "$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT

REMOVE_NODES_IPS=( "${REMOVE_MASTERS_IPS[@]}" "${REMOVE_WORKERS_IPS[@]}" )

REACHABLE=()
for IP in ${REMOVE_NODES_IPS[@]}; do
  ssh-keyscan ${IP} 2>&1 | grep -vE "^$|route|getaddrinfo" > /dev/null
  [ $? == 0 ] && REACHABLE+=(${IP})
done
if [[ "${#REACHABLE[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: The following machines are reachable via SSH:"
  echo -e $COL_RED_BOLD"                  ${REACHABLE[*]}"$COL_DEFAULT
  echo
  echo -e $COL_RED_BOLD"                  Please make sure they are shut down, then run the command again."$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


# Deactivate login welcome info
echo  -e $COL_BLUE"* Deactivating login welcome info in remote machines"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} touch .hushlogin &
done
wait


################################################################################
##                          START CLUSTER UPDATES                             ##
################################################################################

################################################################################
##  UPDATE INTERNAL BALANCERS SERVICE-MONITORS CONFIGURATION                  ##
################################################################################
# Update internal load-balancer ServiceMonitors endpoints (if balancing is enabled)
if  [ "${APISERVER_INTERNAL_BALANCING}" = "true" ] || [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  IP="${SUPER_MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" *  Update load-balancer ServiceMonitors endpoints in PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-primary-update-servicemonitors.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &

  wait
fi


################################################################################
##  UPDATE INTERNAL LOAD-BALANCERS CONFIGURATION IF NECESSARY                 ##
################################################################################
# If any Master is being removed:
# UPDATE ALL MASTERS INTERNAL BALANCERS TARGETS (if internal balancing is on)
if [ "${APISERVER_INTERNAL_BALANCING}" = "true" ] && [ "${#REMOVE_MASTERS_IPS[@]}" != 0 ]; then
  # Create a list of all masters (could be optimized removing the ones just added)
  ENDPOINTS=()
  # ENDPOINTS+=(${SUPER_MASTER_IP})
  for MASTER_IP in ${MASTERS_IPS[@]}; do
    # If master node is not in maintenance, it's an edpoint
    if [[ " ${MAINTENANCE_MASTERS_IPS[@]} " =~ " ${MASTER_IP} " ]]; then
      # Master is in maintenance, skip it
      # ":" is bash's no-command (do nothing)
      :
    else
      ENDPOINTS+=(${MASTER_IP})
    fi
  done

  for MASTER_IP in ${ENDPOINTS[@]}; do
    IP="${MASTER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/master_${IP}.log"
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" * Updating internal load-balancer of MASTER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-update-master-balancing.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}MASTER ${IP}$COL_DEFAULT]") /" &
  done
fi

# If any Worker is being added or removed (could be improved to "if any Ingress node"):
# UPDATE ALL INGRESS WORKERS INTERNAL BALANCERS TARGETS (if internal balancing is on)
# We do it after adding the new nodes and before removing nodes, in case all
# nodes are replaced (rare untested scenario).
if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ] && [ "${#REMOVE_WORKERS_IPS[@]}" != 0 ]; then
  for WORKER_IP in ${INGRESS_NODES_IPS[@]}; do
    if [[ " ${MAINTENANCE_WORKERS_IPS[@]} " =~ " ${WORKER_IP} " ]]; then
      # Node is in maintenance, skip it
      # ":" is bash's no-command (do nothing)
      :
    else
      IP="${WORKER_IP}"
      OUTPUT_FILE="${LOGS_DIR}/worker_${IP}.log"
      echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
      echo  -e $COL_GREEN" * Updating internal load-balancer of WORKER node in ${IP}..."$COL_DEFAULT
      echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
      . ${LOCAL_WORKDIR}/control-update-worker-balancing.sh \
        |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
        |& tee -a ${OUTPUT_FILE} \
        |& sed "s/^/$(printf "[${IP_COLORS[$IP]}WORKER ${IP}$COL_DEFAULT]") /" &
    fi
  done
fi

wait


################################################################################
##  REMOVE MASTER NODES                                                       ##
################################################################################
# Master removals: (not parallelized to avoid Etcd overload)
# 1. from a Master node, delete the nodes to be removed from Etcd state

if [ "${#REMOVE_MASTERS_IPS[@]}" != 0 ]; then

  # 1. from a Master node, delete the nodes to be removed from Etcd state
  IP="${SUPER_MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" *  Deleting Master nodes from PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-primary-delete-masters.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &
fi

wait



################################################################################
##  REMOVE WORKER NODES                                                       ##
################################################################################
# Worker removal
# 1. from a Master node, delete the nodes to be removed from Etcd state

if [ "${#REMOVE_WORKERS_IPS[@]}" != 0 ]; then

  # 1. from a Master node, delete the nodes to be removed from Etcd state
  IP="${SUPER_MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" *  Deleting Worker nodes from PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-primary-delete-workers.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &
fi

wait


################################################################################
##  RECONFIGURE STORAGE POOLS                                                 ##
################################################################################
# Storage pool reconfiguration:
# 1. from a Master node, reconfigure storage pools

if [ "${#ADD_STORAGE_IPS[@]}" != 0 ] || [ "${#REMOVE_STORAGE_IPS[@]}" != 0 ]; then
  if [ -f "${LOCAL_WORKDIR}/control-primary-reconfigure-storage.sh" ]; then
    # 1. from a Master node, reconfigure storage pools
    IP="${SUPER_MASTER_IP}"
    OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
    echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" *  Reconfiguring storage pools from PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
    echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-primary-reconfigure-storage.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &
  else
    echo  -e $COL_GREEN" *  Skipped OpenEBS StoragePool reconfiguration (not supported)."$COL_DEFAULT
  fi
fi

wait



# Reactivate login welcome info
# echo  -e $COL_BLUE"* Reactivating login welcome info in remote machines"$COL_DEFAULT
# for IP in ${ALL_IPS[@]}; do
#   ${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} rm -f .hushlogin &
# done
# wait

echo
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                      CLUSTER UPDATE COMPLETED                              ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER TOPOLOGY CHANGES:                                                    "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Removed Masters IPs  : ${REMOVE_MASTERS_IPS[*]}                           "$COL_DEFAULT
echo -e $COL_GREEN"##  - Removed Workers IPs  : ${REMOVE_WORKERS_IPS[*]}                           "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## NEW CLUSTER TOPOLOGY:                                                        "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Masters IPs        : ${INITIAL_MASTERS_IPS[*]}                            "$COL_DEFAULT
echo -e $COL_GREEN"##  - Workers IPs        : ${WORKERS_IPS[*]}                                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress nodes IPs  : ${INGRESS_NODES_IPS[*]}                              "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo
