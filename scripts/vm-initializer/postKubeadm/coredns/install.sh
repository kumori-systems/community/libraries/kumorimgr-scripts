#!/bin/bash

KUBECONFIG="/home/${MAIN_USER}/.kube/config"

echo
echo "Apply Kumori custom configuration to CoreDNS..."
echo

# Modify CoreDNS configuration ConfigMap (with retries)
echo "Wait for CoreDNS ConfigMap to exist and update it (some errors may be printed)..."
echo
while true; do
  kubectl --kubeconfig ${KUBECONFIG} apply -f coredns-configmap.yaml && break
  sleep 5
  echo "Retrying to update CoreDNS ConfigMap..."
done
echo
echo "CoreDNS ConfigMap updated."
echo

# Modify CoreDNS rbac (with retries)
echo "Wait for CoreDNS ClusterRole to exist and update it (some errors may be printed)..."
echo
while true; do
  kubectl --kubeconfig ${KUBECONFIG} apply -f coredns-rbac.yaml && break
  sleep 5
  echo "Retrying to update CoreDNS ClusterRole..."
done
echo
echo "CoreDNS ClusterRole updated."
echo


## # Patch CoreDNS deployment to set Kumori custom image and credentials
##
## THIS IS NOT NECESSSARY IF KUMORI COREDNS IMAGE IS PUBLIC (AS IS THE CASE).
## IF THE IMAGE IS PUBLIC, IT CAN BE SET IN THE KubeadmConfig OBJECT.
##
##
## COREDNS_IMAGE="${KUMORI_COREDNS_IMAGE_REGISTRY}/coredns:${KUBERNETES_COREDNS_VERSION}_v${KUMORI_COREDNS_VERSION}"
## echo "Patching CoreDNS Deployment to set Kumori custom image ${COREDNS_IMAGE}..."
## echo
## kubectl --kubeconfig ${KUBECONFIG} -n kube-system set image deployment/coredns coredns=${COREDNS_IMAGE}
## echo
## echo "CoreDNS deployment patched."
## echo
##
## # If Enterprise registry credentials have been provided, create a secret for the credentials
## DOCKERHUB_KUMORI_CREDENTIALS_SECRET="kumori-enterprise-registry-credentials"
## if [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ] && [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
##   # Create Secret for CoreDNS registry credentials
##   echo "Creating Secret for CoreDNS registry credentials \"${DOCKERHUB_KUMORI_CREDENTIALS_SECRET}\" in namespace kube-system..."
##     kubectl --kubeconfig ${KUBECONFIG} -n kube-system create secret docker-registry ${DOCKERHUB_KUMORI_CREDENTIALS_SECRET} \
##       --docker-server=${KUMORI_IMAGES_REGISTRY} \
##       --docker-username=${KUMORI_IMAGES_REGISTRY_USERNAME} \
##       --docker-password=${KUMORI_IMAGES_REGISTRY_PASSWORD}
## 
##   # Add PullSecret to CoreDNS deployment
##   echo "Adding an ImagePullSecret to CoreDNS deployment..."
##   echo
##   ORIGINAL_MANIFEST_FILE="coredns-deployment-manifest-original.yaml"
##   UPDATED_MANIFEST_FILE="coredns-deployment-manifest-updated.yaml"
## 
##   kubectl --kubeconfig ${KUBECONFIG} -n kube-system get deployment coredns -oyaml > ${ORIGINAL_MANIFEST_FILE}
##
##   cat ${ORIGINAL_MANIFEST_FILE} \
##     | yq r -j - \
##     | jq ".spec.template.spec.imagePullSecrets = [{\"name\": \"${DOCKERHUB_KUMORI_CREDENTIALS_SECRET}\"}]" \
##     | yq r --prettyPrint - \
##     > ${UPDATED_MANIFEST_FILE}
##
##   kubectl --kubeconfig ${KUBECONFIG} apply -f ${UPDATED_MANIFEST_FILE}
##
## else
##   echo -e $COL_BLUE"No Enterprise registry credentials provided, skipping secret and PullSecret creation."$COL_DEFAULT
## fi


# THIS WAS REMOVED IN PREVIOUS VERSIONS
## # Patch kube-dns service to add custom Kumori port (with retries)
## echo "Wait for CoreDNS Service to exist and patch it (some errors may be printed)..."
## echo
## while true; do
##   kubectl --kubeconfig ${KUBECONFIG} get service kube-dns -n kube-system && break
##   sleep 5
##   echo "Retrying to patch CoreDNS service..."
## done
## echo
## echo "CoreDNS service patched."
## echo

# Patch CoreDNS deployment to set custom resource requests and limits if necessary (with retries)
if [ -n "${COREDNS_CPU_LIMIT}" ]; then
  RESOURCE_PARAMETERS="--requests=cpu=${COREDNS_CPU_REQUEST},memory=${COREDNS_MEM_REQUEST} --limits=cpu=${COREDNS_CPU_LIMIT},memory=${COREDNS_MEM_LIMIT}"
else
  RESOURCE_PARAMETERS="--requests=cpu=${COREDNS_CPU_REQUEST},memory=${COREDNS_MEM_REQUEST} --limits=memory=${COREDNS_MEM_LIMIT}"
fi  

echo "Wait for CoreDNS Deployment to exist and patch it (some errors may be printed)..."
echo
while true; do
  kubectl -n kube-system set resources deployment coredns -c=coredns ${RESOURCE_PARAMETERS} && break
  sleep 5
  echo "Retrying to patch CoreDNS deployment..."
done
echo
echo "CoreDNS deployment patched."
echo

