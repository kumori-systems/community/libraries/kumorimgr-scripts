#!/bin/bash

KUBECONFIG="/home/${MAIN_USER}/.kube/config"

# If ApiServer automatic Load-Balancer is used (ClusterAPI), extract the LB
# public IP register it in DNS.
if [ "${APISERVER_AUTO_BALANCING}" = "true" ]; then

  echo
  echo "Extracting ApiServer LB public IP from kubeconfig..."
  echo

  # The kubeConfig file looks like this
  #
  #    apiVersion: v1
  #    clusters:
  #    - name: kubernetes
  #      cluster:
  #        certificate-authority-data: LS0tLS...0tCg==
  #        server: 192.168.100.100:6443                    <=== THAT'S THE IP WE WANT TO EXTRACT !
  #
  KUBECONFIG_APISERVER_ENDPOINT=$(
    cat ${KUBECONFIG} \
    | yq r - "clusters[0].cluster.server" \
    | sed -e 's|https://||g' \
    | sed -e 's|http://||g' \
    | sed -e 's|:6443||g'
  )

  if [ "${KUMORI_IAAS}" = "openstack" ]; then
    APISERVER_LB_IP=${KUBECONFIG_APISERVER_ENDPOINT}
    APISERVER_DNS_TYPE="A"
    # APISERVER_DNS_TTL="300" -- setted in variables.sh
  elif [ "${KUMORI_IAAS}" = "aws" ]; then
    # It is mandatory to add a "dot" when a CNAME record is created
    APISERVER_LB_IP=${KUBECONFIG_APISERVER_ENDPOINT}.
    APISERVER_DNS_TYPE="CNAME"
    APISERVER_DNS_TTL="0"
  else
    echo
    echo "APISERVER LB IP: IAAS-NOT-SUPPORTED ${KUMORI_IAAS}"
    echo
  fi

  echo
  echo "APISERVER LB : ${APISERVER_DNS_TYPE} / ${APISERVER_LB_IP}"
  echo

  # Register the ApiServer LB IP in DNS
  if [ "${MANAGED_DNS}" = "true" ] && [ "${APISERVER_REGISTER_DOMAIN}" = "true" ]; then
    echo
    echo "Registering ApiServer DNS record..."
    echo

    if [ "${MANAGED_DNS_PROVIDER}" = "route53" ]; then
      AWS_CONFIG_DIR="${INSTALLER_FILES_DIR}/awsConfiguration"

      # ApiServer DNS record template (DO NOT TABULATE! - USES EOF)
DNS_ADD_APISERVER_DOMAIN=$(cat <<EOF
{
  "Comment": "Create ApiServer main record",
  "Changes": [
    {
      "Action": "UPSERT",
      "ResourceRecordSet": {
        "Name": "${APISERVER_DOMAIN}",
        "Type": "${APISERVER_DNS_TYPE}",
        "TTL": ${APISERVER_DNS_TTL},
        "ResourceRecords": [ { "Value": "${APISERVER_LB_IP}"} ]
      }
    }
  ]
}
EOF
)

      # Define AWS base command based on Docker image
      AWS_CLI="docker run --rm --env AWS_PAGER= -v ${AWS_CONFIG_DIR}:/root/.aws ${MANAGED_DNS_ROUTE53_CLI_IMAGE}"

      # Find out HostedZone ID from reference domain
      echo "Running command: ${AWS_CLI} route53 list-hosted-zones"
      RESULT=$(${AWS_CLI} route53 list-hosted-zones --output json)
      EXIT_CODE="$?"
      if [ "${EXIT_CODE}" != "0" ]; then
        echo
        echo -e $COL_RED_BOLD"Error getting DNS HostedZones. This will require manual corrective actions."
        echo
        exit 1
      fi

      echo
      echo "Current available HostedZones:"
      echo "${RESULT}"
      echo

      HOSTEDZONE_ID=$(echo ${RESULT} | jq -cr ".HostedZones[] | select(.Name==\"$REFERENCE_DOMAIN.\") | .Id")
      if [[ ${HOSTEDZONE_ID} != /hostedzone/* ]]; then
        echo
        echo -e $COL_RED_BOLD"Error getting HostedZone ID for ${REFERENCE_DOMAIN}. This will require manual corrective actions."
        echo
        exit 1
      fi

      echo "HostedZone domain: ${REFERENCE_DOMAIN}"
      echo "HostedZone ID: ${HOSTEDZONE_ID}"
      echo


      # Create a new ApiServer DNS record
      echo
      echo -e $COL_BLUE"* Creating DNS entry for ApiServer..."$COL_DEFAULT
      echo
      echo "Creating the following DNS record in AWS Route53:"
      echo
      echo
      echo " - Domain : ${APISERVER_DOMAIN}"
      echo "   Type   : ${APISERVER_DNS_TYPE}"
      echo "   Value  : ${APISERVER_LB_IP}"
      echo "   TTL    : ${APISERVER_DNS_TTL}"

      # Run command to create new DNS record set
      echo "Running command: ${AWS_CLI} route53 change-resource-record-sets \
        --hosted-zone-id ${HOSTEDZONE_ID} \
        --change-batch ${DNS_ADD_APISERVER_DOMAIN}"

      ${AWS_CLI} route53 change-resource-record-sets \
        --hosted-zone-id "${HOSTEDZONE_ID}" \
        --change-batch "${DNS_ADD_APISERVER_DOMAIN}"

      EXIT_CODE="$?"
      if [ "${EXIT_CODE}" == "0" ]; then
        echo
        echo "Domain created successfully."
        echo
      else
        echo
        echo -e $COL_RED_BOLD"An error ocurred when creating ApiServer domain. This will require manual corrective actions."$COL_DEFAULT
        echo
        exit 1
      fi
    elif [ "${MANAGED_DNS_PROVIDER}" = "ovh" ]; then
      # Extract OVH credentials from credentials file
      echo "Extracting credentials from OVH configuration file..."
      OVH_CREDENTIALS_FILE="${INSTALLER_FILES_DIR}/ovhConfiguration/ovh-dns-credentials.ini"

      OVH_ENDPOINT="$(grep "dns_ovh_endpoint" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
      OVH_APPLICATION_KEY="$(grep "dns_ovh_application_key" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
      OVH_APPLICATION_SECRET="$(grep "dns_ovh_application_secret" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
      OVH_CONSUMER_KEY="$(grep "dns_ovh_consumer_key" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"

      echo " - Extracted OVH_ENDPOINT           : ${OVH_ENDPOINT}"
      echo " - Extracted OVH_APPLICATION_KEY    : ${OVH_APPLICATION_KEY}"
      echo " - Extracted OVH_APPLICATION_SECRET : <hidden>  (length: ${#OVH_APPLICATION_SECRET})"
      echo " - Extracted OVH_CONSUMER_KEY       : <hidden>  (length: ${#OVH_CONSUMER_KEY})"

      # Define OVH API helper base command based on Docker image
      OVH_CLI="docker run --rm \
        --name kovhcli \
        --env OVH_ENDPOINT="${OVH_ENDPOINT}" \
        --env OVH_APPLICATION_KEY="${OVH_APPLICATION_KEY}" \
        --env OVH_APPLICATION_SECRET="${OVH_APPLICATION_SECRET}" \
        --env OVH_CONSUMER_KEY="${OVH_CONSUMER_KEY}" \
        ${MANAGED_DNS_OVH_CLI_IMAGE}"

      # Validate Reference Domain and DNS Zone
      RESULT=$(${OVH_CLI} check-refdomain ${REFERENCE_DOMAIN})
      EXIT_CODE="$?"
      if [ "${EXIT_CODE}" != "0" ]; then
        echo
        echo -e $COL_RED_BOLD"Error checking Reference Domain against DNS Zones. This will require manual corrective actions."
        echo
        exit 1
      fi

      echo
      echo -e $COL_BLUE"* Creating DNS entry for ApiServer..."$COL_DEFAULT
      echo
      echo "Creating the following DNS record in OVH:"
      echo
      echo
      echo " - Domain : ${APISERVER_DOMAIN}"
      echo "   Type   : ${APISERVER_DNS_TYPE}"
      echo "   Value  : ${APISERVER_LB_IP}"
      echo "   TTL    : ${APISERVER_DNS_TTL}"

      RESULT=$(${OVH_CLI} create-refdomain-record ${REFERENCE_DOMAIN} apiserver-${RELEASE_NAME} ${APISERVER_DNS_TYPE} ${APISERVER_LB_IP} ${APISERVER_DNS_TTL})
      EXIT_CODE="$?"
      if [ "${EXIT_CODE}" == "0" ]; then
        echo
        echo "Domain created successfully."
        echo
      else
        echo
        echo -e $COL_RED_BOLD"An error ocurred when creating ApiServer domain. This will require manual corrective actions."$COL_DEFAULT
        echo
        exit 1
      fi
    else
      echo
      echo -e $COL_YELLOW"DNS Provider ${MANAGED_DNS_PROVIDER} not supported. Skipping DNS configuration."$COL_DEFAULT
      echo
    fi
  else
    echo
    echo "Skipped ApiServer DNS registration."
    echo
  fi
fi
