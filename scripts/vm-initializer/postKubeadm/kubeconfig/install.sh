#!/bin/bash

KUBECONFIG_DIR="/home/${MAIN_USER}/.kube"
ROOT_KUBECONFIG_DIR="/root/.kube"

echo
echo "Copying Kubectl configuration to ${KUBECONFIG_DIR}..."
echo

# Copy kubectl configuration file to main user home directory
mkdir -p ${KUBECONFIG_DIR}
sudo cp -f /etc/kubernetes/admin.conf ${KUBECONFIG_DIR}/config
# sudo chown $(id -u):$(id -g) $HOME/.kube/config
sudo chown ${MAIN_USER}:${MAIN_USER} ${KUBECONFIG_DIR}/config

# Copy kubectl configuration file to root user home directory
mkdir -p ${ROOT_KUBECONFIG_DIR}
sudo cp -f /etc/kubernetes/admin.conf ${ROOT_KUBECONFIG_DIR}/config
# sudo chown $(id -u):$(id -g) $HOME/.kube/config
sudo chown root:root ${ROOT_KUBECONFIG_DIR}/config


echo
echo "Configuring Kubectl autocompletion..."
echo

# Enable kubectl autompletion
sudo mkdir -p /etc/bash_completion.d
sudo sh -c 'kubectl completion bash > /etc/bash_completion.d/kubectl'
