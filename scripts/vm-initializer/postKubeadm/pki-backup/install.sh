#!/bin/bash

COL_DEFAULT='\e[0;00m'
COL_BLUE='\e[0;96m'
COL_GREEN='\e[0;92m'
COL_RED='\e[0;91m'
COL_YELLOW='\e[0;93m'
COL_RED_BOLD='\e[1;91m'
COL_YELLOW_BOLD='\e[1;93m'

#####################################################################################
##  TEMPORARY PATCH TO ALLOW MULTIPLE CLUSTERS TO USE SAME S3 BUCKET (ticket334)   ##
#####################################################################################
if [ -z "${PKI_BACKUP_STORAGE_PREFIX}" ]; then
  PKI_BACKUP_STORAGE_PREFIX="${CLUSTER_NAME}-backups"
fi
if [ -z "${ETCD_BACKUP_STORAGE_PREFIX}" ]; then
  ETCD_BACKUP_STORAGE_PREFIX="${CLUSTER_NAME}-backups"
fi
#################################
##  END OF TEMPORARY PATCH     ##
#################################


################################################################################
##  ON PRIMARY MASTER NODE - BACKUP CLUSTER PKI CREDENTIALS                   ##
################################################################################

echo
echo "* Backing up Cluster PKI credentials..."
echo

if [ "${PKI_BACKUP}" = "false" ]; then
  echo
  echo -e $COL_BLUE"*** Skipped Backing up Cluster PKI credentials. ***"$COL_DEFAULT
  echo
  return
fi

echo "- Creating zip file..."
echo
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")
PKI_ZIP_DIR="/tmp/pki-backup"
PKI_ZIP_FILE="${PKI_ZIP_DIR}/${CLUSTER_NAME}_PKI_backup_${TIMESTAMP}.zip"
mkdir -p ${PKI_ZIP_DIR}

cd /etc/kubernetes/pki
sudo zip -r ${PKI_ZIP_FILE} *
cd -
sudo chown ${MAIN_USER}:${MAIN_USER} ${PKI_ZIP_FILE}

echo
echo "- Checking S3 endpoint connectivity..."
echo

MC_ENDPOINT_ALIAS="${CLUSTER_NAME}"

# Wait for S3 endpoint respond to curl
n=0
max=60
while [[ $n -lt $max ]]; do
  curl -sS "${PKI_BACKUP_S3_ENDPOINT}"
  EXIT_CODE="$?"
  [ "${EXIT_CODE}" == "0" ] && break
  n=$[$n+1]
  if [ $n -eq $max ]; then
    echo -e $COL_RED_BOLD"Unable to reach S3 endpoint by curl in time (${PKI_BACKUP_S3_ENDPOINT})."$COL_DEFAULT
  else
    echo -e $COL_YELLOW"S3 endpoint seems to be down (curl exit code ${EXIT_CODE}). Retrying in 10 seconds... (iteration $n)..."$COL_DEFAULT
    sleep 10
  fi
done

# Configure S3 endpoint as Minio CLI host
TMP_MC_CONFIG="$(pwd)/mc-config"
mkdir -p "${TMP_MC_CONFIG}"

# We use Minio MC client via a docker image
MINIO_MC_DOCKER_IMAGE="minio/mc:${MINIO_MC_VERSION}"
MINIO_MC_COMMAND="docker run --rm -v ${TMP_MC_CONFIG}:/root/.mc ${MINIO_MC_DOCKER_IMAGE}"

n=0
max=24
S3_UP="false"
while [[ $n -lt $max ]]; do
  # Configure access to S3 endpoint (will fail if S3 endpoint is down!)
  ${MINIO_MC_COMMAND} --config-dir="/root/.mc" config host add \
    "${MC_ENDPOINT_ALIAS}" \
    "${PKI_BACKUP_S3_ENDPOINT}" \
    "${PKI_BACKUP_S3_ACCESS_KEY}" \
    "${PKI_BACKUP_S3_SECRET_KEY}"
  [ "$?" == "0" ] && S3_UP="true" && break

  n=$[$n+1]
  if [ $n -eq $max ]; then
    echo -e $COL_RED_BOLD"Unable to reach S3 endpoint ${PKI_BACKUP_S3_ENDPOINT} in time."$COL_DEFAULT
  else
    echo -e $COL_YELLOW"S3 endpoint seems to be down. Retrying in 5 seconds... (iteration $n)..."$COL_DEFAULT
    sleep 5
  fi
done


echo
echo "- Preparing/verifying S3 bucket ${PKI_BACKUP_S3_BUCKET}..."
echo

# Verify the target bucket exists; otherwise create it
if [ "${S3_UP}" == "true" ]; then
  echo
  echo "S3 endpoint is up. Creating bucket ${PKI_BACKUP_S3_BUCKET} if it doesn't exist..."
  echo
  echo "Checking bucket existence..."
  ${MINIO_MC_COMMAND} --config-dir="/root/.mc" stat "${MC_ENDPOINT_ALIAS}/${PKI_BACKUP_S3_BUCKET}" > /dev/null 2>&1
  if [ "$?" == "0" ]; then
    echo "Bucket already exists."
  else
    echo "Bucket not found. Creating bucket..."
    # Add a trailing / (required in bucket creation for OVH S3)
    if [ -n "${PKI_BACKUP_S3_REGION}" ]; then
      ${MINIO_MC_COMMAND} --config-dir="/root/.mc" mb --region ${PKI_BACKUP_S3_REGION} -p "${MC_ENDPOINT_ALIAS}/${PKI_BACKUP_S3_BUCKET}/"
    else
      ${MINIO_MC_COMMAND} --config-dir="/root/.mc" mb -p "${MC_ENDPOINT_ALIAS}/${PKI_BACKUP_S3_BUCKET}/"
    fi
    echo "New bucket created."
  fi
fi


echo
echo "- Uploading PKI backup to S3..."
echo

MINIO_MC_COMMAND="docker run --rm -v ${TMP_MC_CONFIG}:/root/.mc -v ${PKI_ZIP_DIR}:${PKI_ZIP_DIR} ${MINIO_MC_DOCKER_IMAGE}"

if [ -n "${PKI_BACKUP_STORAGE_PREFIX}" ]; then
  echo "Copying PKI backup to ${PKI_BACKUP_S3_BUCKET}/${PKI_BACKUP_STORAGE_PREFIX}/ "
  ${MINIO_MC_COMMAND} \
    --config-dir="/root/.mc" \
    cp ${PKI_ZIP_FILE} "${MC_ENDPOINT_ALIAS}/${PKI_BACKUP_S3_BUCKET}/${PKI_BACKUP_STORAGE_PREFIX}/"
else
  echo "Copying PKI backup to ${PKI_BACKUP_S3_BUCKET}/ "
  ${MINIO_MC_COMMAND} \
    --config-dir="/root/.mc" \
    cp ${PKI_ZIP_FILE} "${MC_ENDPOINT_ALIAS}/${PKI_BACKUP_S3_BUCKET}/"
fi

echo
echo "- Cleaning up MinIO CLI configuration..."
echo

# Delete mc configuration dir
sudo rm -rf "${TMP_MC_CONFIG}"
echo
