#!/bin/bash

POSTKUBEADM_SCRIPT_DIR="$(dirname $(readlink -f $0))"
ORIGINAL_DIR=$(pwd)

# Load the Kumori configuration files
CONFIGURATION_DIR="/home/ubuntu/.kumori/installer-files"
CLUSTERAPI_CONFIGURATION_FILE="${CONFIGURATION_DIR}/clusterapi-configuration.sh"
CLUSTER_CONFIGURATION_FILE="${CONFIGURATION_DIR}/cluster-configuration.sh"
ADDONS_CONFIGURATION_FILE="${CONFIGURATION_DIR}/addons-configuration.sh"
NODE_CONFIGURATION_FILE="${CONFIGURATION_DIR}/node-configuration.sh"

echo
echo "Loading ClusterAPI configuration variables from file: ${CLUSTERAPI_CONFIGURATION_FILE}"
source ${CLUSTERAPI_CONFIGURATION_FILE}
echo "Loading Kumori cluster configuration variables from file: ${CLUSTER_CONFIGURATION_FILE}"
source ${CLUSTER_CONFIGURATION_FILE}
echo "Loading Kumori addons configuration variables from file: ${ADDONS_CONFIGURATION_FILE}"
source ${ADDONS_CONFIGURATION_FILE}
echo "Loading Kumori node configuration variables from file: ${NODE_CONFIGURATION_FILE}"
source ${NODE_CONFIGURATION_FILE}


cd ${POSTKUBEADM_SCRIPT_DIR}

echo
echo "Current environment variables:"
set | sort
echo
echo

####################################
##      ONLY FOR MASTER NODES     ##
####################################
if [ "${IS_MASTER}" = "true" ]; then

  echo
  echo "Master node detected."
  echo "Configuring Master node stuff..."
  echo

  # Setup kubeconfig file
  cd kubeconfig
  source install.sh
  cd ..

  ####################################
  ##      ONLY FOR FIRST MASTER     ##
  ####################################
  #
  # First Master node (where 'kubeadm init' is run) can be detected by checking
  # if the file /run/kubeadm/kubeadm.yaml exists. This file only exists in the
  # first Master node.
  # All the other nodes, a similar file will exist but with a different name:
  # /run/kubeadm/kubeadm-join-config.yaml
  if [ -f "/run/kubeadm/kubeadm.yaml" ]; then
    
    echo
    echo "Primary master detected."
    echo "Running primary master installation steps..."
    echo

    # Path to the kubeconfig file for accessing the cluster
    export KUBECONFIG="/home/ubuntu/.kube/config"

    # Cluster installer configuration and files path
    INSTALLER_FILES_DIR="/home/ubuntu/.kumori/installer-files/kumorimgr-scripts/tmp"

    # # Load cluster configuration variables (required for CodeDNS and addons)
    # source ${INSTALLER_FILES_DIR}/variables.sh

    # Configure CoreDNS with Kumori specific settings
    cd coredns
    source install.sh
    cd ..

    # Register ApiServer LB IP in DNS (if necessary)
    cd apiserver-dns-registration
    source install.sh
    cd ..

    # Install Cluster addons
    PREVIOUS_DIR=$(pwd)

    cd ${INSTALLER_FILES_DIR}
    source primary-master_2_install-addons.sh
    cd ${PREVIOUS_DIR}

    # Create a backup of Kubernetes PKI
    echo
    echo "Backing up cluster PKIs from primary master nodes..."
    echo
    cd pki-backup
    source install.sh
    cd ..

  else
    echo
    echo "Not the primary master, skipping addon installation."
    echo
  fi
else
  echo
  echo "Worker node detected."
  echo "Configuring Worker node stuff..."
  echo

fi

cd ${ORIGINAL_DIR}