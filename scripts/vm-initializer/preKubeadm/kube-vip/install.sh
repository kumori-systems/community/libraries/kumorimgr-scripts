#!/bin/bash

if [ "${IS_MASTER}" = "true" ]; then
  echo
  echo "Configuring KubeVIP network interface..."
  echo

  KUBEVIP_MANIFEST_FILE="/etc/kubernetes/manifests/kube-vip.yaml"

  # Find out default network interface (will be used for floating IP)
  DEFAULT_NETWORK_INTERFACE=$(route | grep '^default' | grep -o '[^ ]*$')

  echo
  echo "Configuring KubeVIP with interface: ${DEFAULT_NETWORK_INTERFACE}"
  echo

  sed -i \
    -e "s|__APISERVER_VIRTUAL_IP_INTERFACE__|${DEFAULT_NETWORK_INTERFACE}|g" \
    ${KUBEVIP_MANIFEST_FILE}
fi