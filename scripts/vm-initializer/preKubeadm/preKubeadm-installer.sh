#!/bin/bash

PREKUBEADM_SCRIPT_DIR="$(dirname $(readlink -f $0))"
ORIGINAL_DIR=$(pwd)

# Uncompress the Kumori configuration files and load the configuration
CONFIGURATION_DIR="/home/ubuntu/.kumori/installer-files"
CLUSTERAPI_CONFIGURATION_ZIP="${CONFIGURATION_DIR}/clusterapi-configuration.zip"
CLUSTERAPI_CONFIGURATION_FILE="${CONFIGURATION_DIR}/clusterapi-configuration.sh"
CLUSTER_CONFIGURATION_ZIP="${CONFIGURATION_DIR}/cluster-configuration.zip"
CLUSTER_CONFIGURATION_FILE="${CONFIGURATION_DIR}/cluster-configuration.sh"
ADDONS_CONFIGURATION_ZIP="${CONFIGURATION_DIR}/addons-configuration.zip"
ADDONS_CONFIGURATION_FILE="${CONFIGURATION_DIR}/addons-configuration.sh"
NODE_CONFIGURATION_FILE="${CONFIGURATION_DIR}/node-configuration.sh"

cat ${CLUSTERAPI_CONFIGURATION_ZIP} \
  | base64 -d \
  | gunzip \
  > ${CLUSTERAPI_CONFIGURATION_FILE}

cat ${CLUSTER_CONFIGURATION_ZIP} \
  | base64 -d \
  | gunzip \
  > ${CLUSTER_CONFIGURATION_FILE}

cat ${ADDONS_CONFIGURATION_ZIP} \
  | base64 -d \
  | gunzip \
  > ${ADDONS_CONFIGURATION_FILE}

echo
echo "Loading ClusterAPI configuration variables from file: ${CLUSTERAPI_CONFIGURATION_FILE}"
source ${CLUSTERAPI_CONFIGURATION_FILE}
echo "Loading Kumori cluster configuration variables from file: ${CLUSTER_CONFIGURATION_FILE}"
source ${CLUSTER_CONFIGURATION_FILE}
echo "Loading Kumori addons configuration variables from file: ${ADDONS_CONFIGURATION_FILE}"
source ${ADDONS_CONFIGURATION_FILE}
echo "Loading Kumori node configuration variables from file: ${NODE_CONFIGURATION_FILE}"
source ${NODE_CONFIGURATION_FILE}


cd ${PREKUBEADM_SCRIPT_DIR}

echo
echo "Current environment variables:"
set | sort
echo
echo

cd custom-dns-resolvers
source install.sh
cd ..

cd docker-configuration
source install.sh
cd ..

cd keepalived
source install.sh
cd ..

cd preload-coredns-image
source install.sh
cd ..

cd docker-credentials
source install.sh
cd ..

cd containerd
source install.sh
cd ..

cd logrotate
source install.sh
cd ..


if [ "${IS_MASTER}" = "true" ]; then
  ####################################
  ##      ONLY FOR MASTER NODES     ##
  ####################################

  # Configure KubeVIP network interface
  cd kube-vip
  source install.sh
  cd ..

  # Install util scripts
  cd util-scripts
  source install.sh
  cd ..

  ####################################
  ##      ONLY FOR FIRST MASTER     ##
  ####################################
  #
  # First Master node (where 'kubeadm init' is run) can be detected by checking
  # if the file /run/kubeadm/kubeadm.yaml exists. This file only exists in the
  # first Master node.
  # All the other nodes, a similar file will exist but with a different name:
  # /run/kubeadm/kubeadm-join-config.yaml
  if [ -f "/run/kubeadm/kubeadm.yaml" ]; then
    
    echo
    echo "Primary master detected."
    echo "Running primary master installation steps..."
    echo

    # # Load variable.sh configuration file
    # INSTALLER_FILES_DIR="/home/ubuntu/.kumori/installer-files/kumorimgr-scripts/tmp"
    # source ${INSTALLER_FILES_DIR}/variables.sh

    cd apiserver-dns-setup
    source install.sh
    cd ..
  else
    echo
    echo "Not the primary master, skipping primary-only steps."
    echo
  fi
fi

cd ${ORIGINAL_DIR}