#!/bin/bash

echo
echo "Configuring Docker dameon (mirror)..."
echo


# Configure Docker
DAEMON_CFG_FILE="/tmp/docker-daemon-cfg.json"

cat <<EOF > "${DAEMON_CFG_FILE}"
{
  "bip": "${DOCKER_BRIDGE_CIDR}",
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m",
    "max-file": "5",
    "compress": "true"
  },
  "storage-driver": "overlay2",
  "registry-mirrors": []
}
EOF

# Add Docker registry mirrors
if [ -n "${DOCKER_REGISTRY_MIRRORS}" ]; then
  ITI_MIRROR_DOMAIN="docker-mirror.iti.upv.es"
  ITI_MIRROR_IP="10.149.16.56"
  ITI_MIRROR_USED="false"

  DAEMON_CFG_TMP_FILE="/tmp/docker-daemon-cfg_TMP.json"
  for MIRROR in ${DOCKER_REGISTRY_MIRRORS[@]}; do
    # Patch: detect if ITI mirror is used
    [[ "${MIRROR}" =~ "${ITI_MIRROR_DOMAIN}" ]] && ITI_MIRROR_USED="true"
    # The jq '--arg key' stuff is for being able to use property names with dashes ('-')
    cat ${DAEMON_CFG_FILE} \
      | jq --arg key "registry-mirrors" '.[ $key ] += [ "'${MIRROR}'" ]' \
      > ${DAEMON_CFG_TMP_FILE}
    mv ${DAEMON_CFG_TMP_FILE} ${DAEMON_CFG_FILE}
  done

  # If ITI mirror is used, add its ITI-internal address to /etc/hosts.
  # The reason is that for ITI we override the default DNS resolvers.
  if [[ "${ITI_MIRROR_USED}" == "true" ]]; then
    echo -e "\n# ITI Docker registry mirror\n${ITI_MIRROR_IP} ${ITI_MIRROR_DOMAIN}" \
      | sudo tee -a /etc/hosts > /dev/null
    echo "Patched /etc/hosts with ITI docker mirror domain."
  fi
fi

echo
echo "Docker daemon configuration:"
echo
cat ${DAEMON_CFG_FILE}
echo

sudo mv ${DAEMON_CFG_FILE} /etc/docker/daemon.json

sudo mkdir -p /etc/systemd/system/docker.service.d
sudo systemctl daemon-reload
sudo systemctl restart docker