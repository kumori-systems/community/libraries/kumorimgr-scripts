#!/bin/bash

# Install Kumori Etcdctl, Kman and Kubectl-Kum scripts
sudo cp ketcdctl /usr/local/bin/
sudo cp kman /usr/local/bin/
sudo cp kubectl-kum /usr/local/bin/
sudo chmod +x /usr/local/bin/ketcdctl
sudo chmod +x /usr/local/bin/kman
sudo chmod +x /usr/local/bin/kubectl-kum

# Install Kubectl-Kum configuration files
sudo mkdir -p /home/${MAIN_USER}/.kumori
sudo cp -r kumori-custom-columns /home/${MAIN_USER}/.kumori
