#!/bin/sh

###############################################################################
# This custom script is used for temporarily enabling Keepalived by making    #
# it succeed the health checks, with no need to modify its configuration.     #
###############################################################################

exit 0
