#!/bin/sh

###############################################################################
# This custom script is used for temporarily disabling Keepalived by making   #
# it fail the health checks, with no need to modify its configuration.        #
###############################################################################

exit 1
