#!/bin/bash


if [ "${INGRESS_HANDLE_FLOATING_IP}" = "true" ] && [ "${IS_INGRESS_NODE}" = "true" ]; then

  echo
  echo "Configuring Ingress Keepalived..."
  echo

  # Find out default network interface (will be used for floating IP)
  DEFAULT_NETWORK_INTERFACE=$(route | grep '^default' | grep -o '[^ ]*$')

  # Find out the Node IP (on the main interface)
  NODE_IP=$(hostname -i)

  # Prepare Keepalived configuration file and health check script
  KEEPALIVED_CONF_FILE_TEMPLATE_FILE="keepalived-ingress.conf.template"
  KEEPALIVED_CONF_FILE="keepalived-ingress.conf"

  # Use VRID set in configuration or use default values (11 for ApiServer, 12 for Ingress)
  KEEPALIVED_VIRTUAL_ROUTER_ID="${INGRESS_VRID:-12}"

  # We create a specific password (max length 8) for this Keepalived cluster, with the following format:
  # <prefix><hash>  where <prefix> can be AS for ApiServer or IN for Ingress and <hash> is a simple checksum
  # of the Kumori cluster name (truncated to 6 chars).
  KEEPALIVED_PASSWORD="IN$(echo ${CLUSTER_NAME} | cksum | cut -f 1 -d ' ' | tail -c 6)"

  # Temporarily, only one Ingress node is supported
  IS_FIRST="true"

  KEEPALIVED_ROLE="BACKUP"
  KEEPALIVED_PRIORITY="254"
  if [ "${IS_FIRST}" = "true" ]; then
    KEEPALIVED_ROLE="MASTER"
    KEEPALIVED_PRIORITY="255"
  fi

  # TODO: THIS VARIABLE IS TEMPORARY, UNTIL BETTER CONFIGURATION IS POSSIBLE
  #       Checks to port 80 or 443 with no domain will fail.
  #       For now, check Ambassador admin endpoint on port 8080 (the official
  #       documented method).
  #       See: https://www.getambassador.io/docs/latest/topics/running/ambassador-with-gke/#redirecting-the-health-check
  INGRESS_CHECK_PORT="8080"

  sed \
    -e "s?{{CLUSTER_NAME}}?${CLUSTER_NAME}?g" \
    -e "s?{{INITIAL_ROLE}}?${KEEPALIVED_ROLE}?g" \
    -e "s?{{NETWORK_INTERFACE}}?${DEFAULT_NETWORK_INTERFACE}?g" \
    -e "s?{{VIRTUAL_ROUTER_ID}}?${KEEPALIVED_VIRTUAL_ROUTER_ID}?g" \
    -e "s?{{PRIORITY}}?${KEEPALIVED_PRIORITY}?g" \
    -e "s?{{PASSWORD}}?${KEEPALIVED_PASSWORD}?g" \
    -e "s?{{INGRESS_VIRTUAL_IP}}?${INGRESS_VIRTUAL_IP}?g" \
    -e "s?{{NODE_IP}}?${NODE_IP}?g" \
    ${KEEPALIVED_CONF_FILE_TEMPLATE_FILE} \
    > ${KEEPALIVED_CONF_FILE}


  # Temporarily, only one Ingress node is supported.
  #
  EMPTY_LIST="true"
  ### # Add a VRRP peer for each ingress node (except itself)
  ### for INGRESS_NODE_IP in ${INGRESS_NODES_IPS[@]}; do
  ###   echo "IP: ${INGRESS_NODE_IP}"
  ###   if [[ "${INGRESS_NODE_IP}" == "${NODE_IP}" ]]; then
  ###     echo "Skipping node (self)"
  ###   else
  ###     EMPTY_LIST="false"
  ###     # Duplicate the peer template line and replace the IP in one of the copies
  ###     sed -i -e "s/^\(\s*\){{PEER_NODE_IP}}\s*$/\1${INGRESS_NODE_IP}\n&/g" ${KEEPALIVED_CONF_FILE}
  ###   fi
  ### done

  # If peer list is empty, add 127.0.0.1 (localhost) to avoid Keepalived falling back to multicast
  if [[ "${EMPTY_LIST}" == "true" ]]; then
    DEFAULT_PEER_IP="127.0.0.1"
    sed -i -e "s/^\(\s*\){{PEER_NODE_IP}}\s*$/\1${DEFAULT_PEER_IP}\n&/g" ${KEEPALIVED_CONF_FILE}
  fi

  # Remove peer template line
  sed -i -e "/^.*{{PEER_NODE_IP}}.*$/d" ${KEEPALIVED_CONF_FILE}

  echo
  echo "Applying Keepalived configuration:"
  echo
  cat ${KEEPALIVED_CONF_FILE}
  echo

  # Prepare check and notify scripts
  sed \
    -e "s?{{INGRESS_VIRTUAL_IP}}?${INGRESS_VIRTUAL_IP}?g" \
    -e "s?{{INGRESS_EXTERNAL_PORT}}?${INGRESS_EXTERNAL_PORT}?g" \
    -e "s?{{INGRESS_INTERNAL_PORT}}?${INGRESS_INTERNAL_PORT}?g" \
    -e "s?{{INGRESS_INTERNAL_BALANCING}}?${INGRESS_INTERNAL_BALANCING}?g" \
    -e "s?{{NODE_IP}}?${NODE_IP}?g" \
    check_ingress.sh.template \
    > check_ingress.sh

  # Prepare default custom notify scripts
  CUSTOM_KEEPALIVED_SCRIPTS_DIR="/etc/keepalived/custom-notify-scripts"
  CUSTOM_KEEPALIVED_STATE_EVENTS_SCRIPT="${CUSTOM_KEEPALIVED_SCRIPTS_DIR}/custom_state_events_handler.sh"
  sudo mkdir -p ${CUSTOM_KEEPALIVED_SCRIPTS_DIR}
  sudo cp custom_state_events_handler.sh ${CUSTOM_KEEPALIVED_STATE_EVENTS_SCRIPT}

  sed \
    -e "s?{{CUSTOM_KEEPALIVED_STATE_EVENTS_SCRIPT}}?${CUSTOM_KEEPALIVED_STATE_EVENTS_SCRIPT}?g" \
    notify_fifo.sh.template \
    > notify_fifo.sh

  sudo mv check_ingress.sh /etc/keepalived/check_ingress_real.sh
  sudo mv check_force_failure.sh /etc/keepalived/check_force_failure.sh
  sudo mv check_force_success.sh /etc/keepalived/check_force_success.sh
  sudo mv notify_fifo.sh /etc/keepalived/notify_fifo.sh
  sudo mv ${KEEPALIVED_CONF_FILE} /etc/keepalived/keepalived.conf

  # Keepalived is strict about script security. Only root user must have acces to the script
  sudo chown -R root:root /etc/keepalived/*
  sudo chmod 700 /etc/keepalived/check_ingress_real.sh
  sudo chmod 700 /etc/keepalived/check_force_failure.sh
  sudo chmod 700 /etc/keepalived/check_force_success.sh
  sudo chmod 700 /etc/keepalived/notify_fifo.sh
  sudo chmod 700 /etc/keepalived/custom-notify-scripts/custom_state_events_handler.sh

  # Initially set a health-check script that forces failure (for Keepalived to not start properly
  # until the script is replaced by the real one).
  # sudo cp /etc/keepalived/check_force_failure.sh /etc/keepalived/check_ingress.sh
  #
  # TODO: THIS IS TEMPORARY UNTIL AMBASSADOR INSTALLATION IS AUTOMATED
  sudo cp /etc/keepalived/check_force_success.sh /etc/keepalived/check_ingress.sh

  # Configure Keepalived service to automatically restart on failures
  sudo sed -i '/\[Service\]/a RestartSec=5s' /lib/systemd/system/keepalived.service
  sudo sed -i '/\[Service\]/a Restart=on-failure' /lib/systemd/system/keepalived.service
  sudo systemctl daemon-reload
  sudo systemctl restart keepalived


  # Add a networkd-dispatcher to force failover when network is restarted
  sed -e "s?{{NETWORK_INTERFACE}}?${DEFAULT_NETWORK_INTERFACE}?g" \
    10-kumori-keepalived-restart.template \
    > 10-kumori-keepalived-restart

  sudo mkdir -p /usr/lib/networkd-dispatcher/routable.d
  sudo mv 10-kumori-keepalived-restart \
    /usr/lib/networkd-dispatcher/routable.d/10-kumori-keepalived-restart
  sudo chmod 755 /usr/lib/networkd-dispatcher/routable.d/10-kumori-keepalived-restart
  sudo chown root:root /usr/lib/networkd-dispatcher/routable.d/10-kumori-keepalived-restart
else
  echo
  echo "**** Skipped Keepalived configuration (conditions not met)."
  echo "  - INGRESS_HANDLE_FLOATING_IP = \"${INGRESS_HANDLE_FLOATING_IP}\""
  echo "  - IS_INGRESS_NODE =            \"${IS_INGRESS_NODE}\""
  echo
fi
