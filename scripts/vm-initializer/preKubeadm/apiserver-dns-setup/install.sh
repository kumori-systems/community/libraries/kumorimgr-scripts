#!/bin/bash

COL_DEFAULT='\e[0;00m'
COL_BLUE='\e[0;96m'
COL_GREEN='\e[0;92m'
COL_RED='\e[0;91m'
COL_YELLOW='\e[0;93m'
COL_RED_BOLD='\e[1;91m'
COL_YELLOW_BOLD='\e[1;93m'


# If DNS is managed by the platform:
# - clean DNS records associated to the Reference Domain managed by a cluster
#   of the same name
# - if platform is in charge of ApiServer load-balancing (Keepalived+Envoy),
#   create a DNS record for ApiServer floating IP
# - if platform is in charge of Ingress load-balancing (Keepalived+Envoy),
#   create a DNS record for Ingress floating IP




#
# ROUTE53 API REQUEST TEMPLATE FOR CREATING DNS RECORDS FOR ApiServer
#
# NOTE: 'UPSERT' operation creates (if not exists) or updates (if exists)
DNS_ADD_APISERVER_DOMAIN=$(cat <<EOF
{
  "Comment": "Create ApiServer main record",
  "Changes": [
    {
      "Action": "UPSERT",
      "ResourceRecordSet": {
        "Name": "${APISERVER_DOMAIN}",
        "Type": "A",
        "TTL": ${APISERVER_DNS_TTL},
        "ResourceRecords": [ { "Value": "${APISERVER_DNS_IP}"} ]
      }
    }
  ]
}
EOF
)

#
# ROUTE53 API REQUEST TEMPLATE FOR CREATING DNS RECORDS FOR Ingress
#
# NOTE: 'UPSERT' operation creates (if not exists) or updates (if exists)
DNS_ADD_INGRESS_DOMAIN=$(cat <<EOF
{
  "Comment": "Create Ingress main record",
  "Changes": [
    {
      "Action": "UPSERT",
      "ResourceRecordSet": {
        "Name": "${INGRESS_DOMAIN}",
        "Type": "A",
        "TTL": ${INGRESS_DNS_TTL},
        "ResourceRecords": [ { "Value": "${INGRESS_DNS_IP}"} ]
      }
    }
  ]
}
EOF
)

if [ "${MANAGED_DNS}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Checking DNS reference domain..."$COL_DEFAULT
  echo

  if [ "${MANAGED_DNS_PROVIDER}" = "route53" ]; then

    # AWS credentials path
    # AWS_CONFIG_DIR="/home/${MAIN_USER}/.kumori/installer-files/awsConfiguration"
    AWS_CONFIG_DIR="${MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR}"

    # Define AWS base command based on Docker image
    AWS_CLI="docker run --rm --env AWS_PAGER= -v ${AWS_CONFIG_DIR}:/root/.aws ${MANAGED_DNS_ROUTE53_CLI_IMAGE}"

    # Find out HostedZone ID from reference domain
    echo "Running command: ${AWS_CLI} route53 list-hosted-zones"
    RESULT=$(${AWS_CLI} route53 list-hosted-zones --output json)
    EXIT_CODE="$?"
    if [ "${EXIT_CODE}" != "0" ]; then
      echo
      echo -e $COL_RED_BOLD"Error getting DNS HostedZones. This will require manual corrective actions."
      echo
      exit 1
    fi

    echo
    echo "Current available HostedZones:"
    echo "${RESULT}"
    echo

    HOSTEDZONE_ID=$(echo ${RESULT} | jq -cr ".HostedZones[] | select(.Name==\"$REFERENCE_DOMAIN.\") | .Id")
    if [[ ${HOSTEDZONE_ID} != /hostedzone/* ]]; then
      echo
      echo -e $COL_RED_BOLD"Error getting HostedZone ID for ${REFERENCE_DOMAIN}. This will require manual corrective actions."
      echo
      exit 1
    fi

    echo "HostedZone domain: ${REFERENCE_DOMAIN}"
    echo "HostedZone ID: ${HOSTEDZONE_ID}"
    echo



    # Get a list of all the records in the HostedZone
    echo "Running command: ${AWS_CLI} route53 list-resource-record-sets starting with 'txt-'."

    # Look for all the DNS recods that start with 'txt-' in the HostedZone
    RESULT=$(
      ${AWS_CLI} route53 list-resource-record-sets \
      --query "ResourceRecordSets[?starts_with(Name, 'txt-')]" \
      --hosted-zone-id ${HOSTEDZONE_ID})
    EXIT_CODE="$?"
    if [ "${EXIT_CODE}" != "0" ]; then
      echo
      echo -e $COL_RED_BOLD"Error listing HostedZone records. This will require manual corrective actions."
      echo
      exit 1
    fi

    NUMBER_OF_RECORDS=$( echo "${RESULT}" | jq '. | length' )

    if [[ "${NUMBER_OF_RECORDS}" == "0" ]]; then
      echo
      echo -e $COL_GREEN"HostedZone does not contain any records matching: \"txt-\""$COL_DEFAULT
      echo
    else
      echo
      echo "HostedZone contains ${NUMBER_OF_RECORDS} records matching: \"txt-\""
      echo "Filtering the ones related to cluster \"${RELEASE_NAME}\" and managed by ExternalDNS..."

      # We need to filter the list of records to only take into account the ones
      # related to a cluster with the same name.
      # ExternalDNS puts a mark in the TXT records that allows us to identify which
      # cluster they belog to:
      #     external-dns/owner=kumori-cluster-<CLUSTER_NAME>
      EXPECTED_TXT_OWNER="kumori-cluster-${RELEASE_NAME}"
      DOMAINS_TO_DELETE=()

      # Put all TXT records domain names in a bash array
      readarray -t TXT_RECORDS < <(echo ${RESULT} | jq -r '.[].Name' -)

      # Loop over the array and for each domain:
      # - resolve it to get the TXT value
      # - extract the externalDNS owner label from the TXT value
      # - if it matches our cluster:
      #   - add it the list of domains to delete
      #   - add its "related" domain to the list of domains to delete
      for TXT_RECORD in "${TXT_RECORDS[@]}"; do

        # Get the owner label from the TXT record value
        RECORD_OWNER=$(
          dig -t txt +short ${TXT_RECORD} \
          | sed -e 's?^.*external-dns/owner=\(.*\),.*$?\1?g'
        )

        if [[ "${RECORD_OWNER}" == "${EXPECTED_TXT_OWNER}" ]]; then
          # It belongs to our cluster, add it to the list of domains to delete
          DOMAINS_TO_DELETE+=(${TXT_RECORD})

          # Calculate the actual domain the TXT record is related to and add it
          # to the list
          if [[ "${TXT_RECORD}" =~ ^txt-cname-.* ]]; then
            RELATED_DOMAIN=$( echo ${TXT_RECORD} | sed -e 's?^txt-cname-\(.*\)$?\1?g' )
            if [[ " ${DOMAINS_TO_DELETE[@]} " =~ " ${RELATED_DOMAIN} " ]]; then
              :  # Do nothing, domain is already in the list
            else
              # Add the related domain to the list
              DOMAINS_TO_DELETE+=(${RELATED_DOMAIN})
            fi
          elif [[ "${TXT_RECORD}" =~ ^txt-.* ]]; then
            RELATED_DOMAIN=$( echo ${TXT_RECORD} | sed -e 's?^txt-\(.*\)$?\1?g' )
            if [[ " ${DOMAINS_TO_DELETE[@]} " =~ " ${RELATED_DOMAIN} " ]]; then
              :  # Do nothing, domain is already in the list
            else
              # Add the related domain to the list
              DOMAINS_TO_DELETE+=(${RELATED_DOMAIN})
            fi
          else
            :  # Do nothing, domain this should never happen
          fi
        else
          :  # Do nothing, domain belongs to a different cluster
        fi
      done


      if [[ "${#DOMAINS_TO_DELETE[@]}" == "0" ]]; then
        echo
        echo -e $COL_GREEN"HostedZone does not contain any records managed by ExternalDNS in a previous ${RELEASE_NAME} cluster"$COL_DEFAULT
        echo
      else
        # Print the list of domains to be deleted (sorted alphabetically)
        echo
        IFS=$'\n' SORTED_DOMAINS_TO_DELETE=($(sort <<<"${DOMAINS_TO_DELETE[*]}"))
        unset IFS
        echo -e $COL_YELLOW_BOLD"The following ${#DOMAINS_TO_DELETE[@]} DNS records will be deleted:"$COL_DEFAULT
        for DOMAIN_TO_DELETE in "${SORTED_DOMAINS_TO_DELETE[@]}"; do
          echo "  - ${DOMAIN_TO_DELETE}"
        done
        echo
        # NOTE: removed confirmation to make installer easier to run
        # echo -e $COL_YELLOW_BOLD"Are you sure you want to continue?"$COL_DEFAULT
        # read -rsp $'--> Press any key to delete the records...\n' -n1 key

        # Build a filter to only obtain the information of the records to be deleted
        # from Route53.
        # Example of filter: "Name == 'admission-demo.test.kumori.cloud.' ||  Name == 'txt-admission-demo.test.kumori.cloud.' ||  Name == 'txt-cname-admission-demo.test.kumori.cloud.'"
        FILTER=""
        FILTER_SEPARATOR=""
        for DOMAIN_TO_DELETE in "${SORTED_DOMAINS_TO_DELETE[@]}"; do
          FILTER="${FILTER}${FILTER_SEPARATOR} Name == '${DOMAIN_TO_DELETE}'"
          FILTER_SEPARATOR=" || "
        done

        # Run the query with the calculated filter. This will return the necessary
        # information to buid the deletion update batch.
        RESULT=$(
          ${AWS_CLI} route53 list-resource-record-sets \
          --query "ResourceRecordSets[?${FILTER}]" \
          --hosted-zone-id ${HOSTEDZONE_ID}
        )

        # Build the list of DNS changes (deletions) and apply it
        JSON_DATA=$(echo ${RESULT} | jq '[ .[] | { Action: "DELETE", ResourceRecordSet: . } ] | { Comment: "DELETE all clusterrecords", Changes: . }')
        RESULT=$(
          ${AWS_CLI} route53 change-resource-record-sets \
            --hosted-zone-id ${HOSTEDZONE_ID} \
            --change-batch "${JSON_DATA}"
        )
        EXIT_CODE="$?"
        if [ "${EXIT_CODE}" != "0" ]; then
          echo
          echo -e $COL_RED_BOLD"Error deleting HostedZone records. This will require manual corrective actions."
          echo
          exit 1
        fi

        # If this wait becomes too long it could actually be skipped, since we
        # are only deleting domain records managed by ExternalDNS, and the addon
        # will not start until much later.
        CHANGE_ID=$( echo "${RESULT}" | jq -r '.ChangeInfo.Id' )
        echo "Waiting for change to be applied and synchronized by Route53 (ChangeId: ${CHANGE_ID})..."
        while true; do
          STATUS=$( ${AWS_CLI} route53 get-change --id ${CHANGE_ID} | jq -r '.ChangeInfo.Status' )
          if [[ "${STATUS}" == "PENDING" ]]; then
            echo "  Status: ${STATUS}. Waiting a little more..."
            sleep 5
          elif [[ "${STATUS}" == "INSYNC" ]]; then
            echo "  Status: ${STATUS}. Changes have been applied!"
            break
          else
            echo -e $COL_RED"  Status: ${STATUS}. Unexpected status!"$COL_DEFAULT
          fi
        done
      fi
    fi

    # Create a new ApiServer DNS record
    if [ "${APISERVER_REGISTER_DOMAIN}" = "true" ] && [ "${APISERVER_AUTO_BALANCING}" != "true" ]; then
      echo
      echo -e $COL_BLUE"* Creating DNS entry for ApiServer..."$COL_DEFAULT
      echo
      echo "Creating the following DNS record in AWS Route53:"
      echo
      echo
      echo " - Domain : ${APISERVER_DOMAIN}"
      echo "   Type   : A"
      echo "   Value  : ${APISERVER_DNS_IP}"
      echo "   TTL    : ${APISERVER_DNS_TTL}"

      # Run command to create new DNS record set
      echo "Running command: ${AWS_CLI} route53 change-resource-record-sets \
        --hosted-zone-id ${HOSTEDZONE_ID} \
        --change-batch ${DNS_ADD_APISERVER_DOMAIN}"

      ${AWS_CLI} route53 change-resource-record-sets \
        --hosted-zone-id "${HOSTEDZONE_ID}" \
        --change-batch "${DNS_ADD_APISERVER_DOMAIN}"

      EXIT_CODE="$?"
      if [ "${EXIT_CODE}" == "0" ]; then
        echo
        echo "Domain created successfully."
        echo
      else
        echo
        echo -e $COL_RED_BOLD"An error ocurred when creating ApiServer domain. This will require manual corrective actions."$COL_DEFAULT
        echo
        exit 1
      fi
    else
      echo "ApiServer domain DNS registration is disabled."
    fi

    # Create a new Ingress DNS record
    if [ "${INGRESS_REGISTER_DOMAIN}" = "true" ] && [ "${INGRESS_AUTO_BALANCING}" != "true" ]; then
      echo
      echo -e $COL_BLUE"* Creating DNS entry for Ingress..."$COL_DEFAULT
      echo
      echo "Creating the following DNS record in AWS Route53:"
      echo
      echo
      echo " - Domain : ${INGRESS_DOMAIN}"
      echo "   Type   : A"
      echo "   Value  : ${INGRESS_DNS_IP}"
      echo "   TTL    : ${INGRESS_DNS_TTL}"

      # Run command to create new DNS record set
      echo "Running command: ${AWS_CLI} route53 change-resource-record-sets \
        --hosted-zone-id ${HOSTEDZONE_ID} \
        --change-batch ${DNS_ADD_INGRESS_DOMAIN}"

      ${AWS_CLI} route53 change-resource-record-sets \
        --hosted-zone-id "${HOSTEDZONE_ID}" \
        --change-batch "${DNS_ADD_INGRESS_DOMAIN}"

      EXIT_CODE="$?"
      if [ "${EXIT_CODE}" == "0" ]; then
        echo
        echo "Domain created successfully."
        echo
      else
        echo
        echo -e $COL_RED_BOLD"An error ocurred when creating Ingress domain. This will require manual corrective actions."$COL_DEFAULT
        echo
        exit 1
      fi
    else
      echo "Ingress domain DNS registration is disabled."
    fi

  elif [ "${MANAGED_DNS_PROVIDER}" = "ovh" ]; then

    # OVH DNS credentials path
    # OVH_CREDENTIALS_FILE="/home/${MAIN_USER}/.kumori/installer-files/ovhCredentials/ovh-dns-credentials.ini"
    OVH_CREDENTIALS_FILE="${MANAGED_DNS_OVH_CONFIG_FILE}"

    # Extract OVH credentials from credentials file
    echo "Extracting credentials from OVH configuration file..."

    OVH_ENDPOINT="$(grep "dns_ovh_endpoint" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
    OVH_APPLICATION_KEY="$(grep "dns_ovh_application_key" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
    OVH_APPLICATION_SECRET="$(grep "dns_ovh_application_secret" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
    OVH_CONSUMER_KEY="$(grep "dns_ovh_consumer_key" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"

    echo " - Extracted OVH_ENDPOINT           : ${OVH_ENDPOINT}"
    echo " - Extracted OVH_APPLICATION_KEY    : ${OVH_APPLICATION_KEY}"
    echo " - Extracted OVH_APPLICATION_SECRET : <hidden>  (length: ${#OVH_APPLICATION_SECRET})"
    echo " - Extracted OVH_CONSUMER_KEY       : <hidden>  (length: ${#OVH_CONSUMER_KEY})"

    # Define OVH API helper base command based on Docker image
    OVH_CLI="docker run --rm \
      --name kovhcli \
      --env OVH_ENDPOINT="${OVH_ENDPOINT}" \
      --env OVH_APPLICATION_KEY="${OVH_APPLICATION_KEY}" \
      --env OVH_APPLICATION_SECRET="${OVH_APPLICATION_SECRET}" \
      --env OVH_CONSUMER_KEY="${OVH_CONSUMER_KEY}" \
      ${MANAGED_DNS_OVH_CLI_IMAGE}"

    # Validate Reference Domain and DNS Zone
    RESULT=$(${OVH_CLI} check-refdomain ${REFERENCE_DOMAIN})
    EXIT_CODE="$?"
    if [ "${EXIT_CODE}" != "0" ]; then
      echo
      echo -e $COL_RED_BOLD"Error checking Reference Domain against DNS Zones. This will require manual corrective actions."
      echo
      exit 1
    fi

    # Delete previous cluster managed DNS records
    RESULT=$(${OVH_CLI} delete-refdomain-records ${REFERENCE_DOMAIN} ${RELEASE_NAME} txt-)
    EXIT_CODE="$?"
    if [ "${EXIT_CODE}" != "0" ]; then
      echo
      echo -e $COL_RED_BOLD"Error checking Reference Domain against DNS Zones. This will require manual corrective actions."
      echo
      exit 1
    fi


    # Create a new ApiServer DNS record
    if [ "${APISERVER_REGISTER_DOMAIN}" = "true" ] && [ "${APISERVER_AUTO_BALANCING}" != "true" ]; then
      echo
      echo -e $COL_BLUE"* Creating DNS entry for ApiServer..."$COL_DEFAULT
      echo
      echo "Creating the following DNS record in AWS Route53:"
      echo
      echo
      echo " - Domain : ${APISERVER_DOMAIN}"
      echo "   Type   : A"
      echo "   Value  : ${APISERVER_DNS_IP}"
      echo "   TTL    : ${APISERVER_DNS_TTL}"

      RESULT=$(${OVH_CLI} create-refdomain-record ${REFERENCE_DOMAIN} apiserver-${RELEASE_NAME} A ${APISERVER_DNS_IP} ${APISERVER_DNS_TTL})
      EXIT_CODE="$?"
      if [ "${EXIT_CODE}" == "0" ]; then
        echo
        echo "Domain created successfully."
        echo
      else
        echo
        echo -e $COL_RED_BOLD"An error ocurred when creating ApiServer domain. This will require manual corrective actions."$COL_DEFAULT
        echo
        exit 1
      fi
    fi


    # Create a new Ingress DNS record
    if [ "${INGRESS_REGISTER_DOMAIN}" = "true" ] && [ "${INGRESS_AUTO_BALANCING}" != "true" ]; then
      echo
      echo -e $COL_BLUE"* Creating DNS entry for Ingress..."$COL_DEFAULT
      echo
      echo "Creating the following DNS record in AWS Route53:"
      echo
      echo
      echo " - Domain : ${INGRESS_DOMAIN}"
      echo "   Type   : A"
      echo "   Value  : ${INGRESS_DNS_IP}"
      echo "   TTL    : ${INGRESS_DNS_TTL}"

      RESULT=$(${OVH_CLI} create-refdomain-record ${REFERENCE_DOMAIN} ingress-${RELEASE_NAME} A ${INGRESS_DNS_IP} ${INGRESS_DNS_TTL})
      EXIT_CODE="$?"
      if [ "${EXIT_CODE}" == "0" ]; then
        echo
        echo "Domain created successfully."
        echo
      else
        echo
        echo -e $COL_RED_BOLD"An error ocurred when creating Ingress domain. This will require manual corrective actions."$COL_DEFAULT
        echo
        exit 1
      fi
    fi







  else
    echo
    echo -e $COL_YELLOW"DNS Provider ${MANAGED_DNS_PROVIDER} not supported. Skipping DNS configuration."$COL_DEFAULT
    echo
  fi
else
  echo
  echo -e $COL_YELLOW"Setting up cluster with manual DNS management. Skipping DNS configuration."$COL_DEFAULT
  echo
fi
