#!/bin/bash

################################################################################
## ON NON PRIMARY MASTER NODES - CONFIGURE DOCKER CREDENTIALS                 ##
################################################################################
# If Docker credentials are provided for DockerHub or Kumori images registry
# configure them at node level. Create a docker credentials file with all ncessary
# credentials.
DOCKER_CREDENTIALS_BASE_FILE="docker-credentials-base.json"
DOCKER_CREDENTIALS_FILE="docker-credentials.json"
DOCKER_CREDENTIALS_FILE_TMP="docker-credentials_TMP.json"

# Create a "blank" docker credentials file
cp "${DOCKER_CREDENTIALS_BASE_FILE}" "${DOCKER_CREDENTIALS_FILE}"


# DockerHub credentials
if [ -n "${DOCKERHUB_USERNAME}" ] && [ -n "${DOCKERHUB_PASSWORD}" ]; then
  echo
  echo "Adding DockerHub credentials..."
  echo
  # Base64-encode username and password
  ENCODED_AUTH=$(echo -n "${DOCKERHUB_USERNAME}:${DOCKERHUB_PASSWORD}" | base64)

  cat "${DOCKER_CREDENTIALS_FILE}" \
    | jq ".auths += { \"https://index.docker.io/v1/\": { auth: \"${ENCODED_AUTH}\" } }" \
    > ${DOCKER_CREDENTIALS_FILE_TMP}

  cp ${DOCKER_CREDENTIALS_FILE_TMP} ${DOCKER_CREDENTIALS_FILE}
fi


# Kumori private registry
if [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ] && [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
  echo
  echo "Adding Kumori registry docker credentials..."
  echo
  REGISTRY_BASE_URL=$(echo "${KUMORI_IMAGES_REGISTRY}" | awk -F[/:] '{print $1}')
  ENCODED_AUTH=$(echo -n "${KUMORI_IMAGES_REGISTRY_USERNAME}:${KUMORI_IMAGES_REGISTRY_PASSWORD}" | base64)

  cat "${DOCKER_CREDENTIALS_FILE}" \
    | jq ".auths += { \"${REGISTRY_BASE_URL}\": { auth: \"${ENCODED_AUTH}\" } }" \
    > ${DOCKER_CREDENTIALS_FILE_TMP}

  cp ${DOCKER_CREDENTIALS_FILE_TMP} ${DOCKER_CREDENTIALS_FILE}
fi


# Copy credentials file to home directory
mkdir -p /home/${MAIN_USER}/.docker
cp "${DOCKER_CREDENTIALS_FILE}" /home/${MAIN_USER}/.docker/config.json
chown -R ${MAIN_USER}:${MAIN_USER} /home/${MAIN_USER}/.docker

# Copy credentials file to root user home directory
sudo mkdir -p /root/.docker
sudo cp "${DOCKER_CREDENTIALS_FILE}" /root/.docker/config.json

# Copy credentials file to Kubelet path
sudo mkdir -p /var/lib/kubelet
sudo cp "${DOCKER_CREDENTIALS_FILE}" /var/lib/kubelet/config.json


