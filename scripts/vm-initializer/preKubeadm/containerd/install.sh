#!/bin/bash



echo
echo "Creating containerd group and adding user ${MAIN_USER} to the group..."
echo

# Add ubuntu user to containerd group (for some reason not working the 'normal way')
sudo addgroup containerd || true
sudo adduser ${MAIN_USER} containerd

# Configure conatinerd socker to avoid using sudo with crictl
chown root:containerd /var/run/containerd/containerd.sock


CHANGES="false"


echo
echo "Setting basic containerd configuration (allow mirrors)..."
echo
sudo cp config.toml /etc/containerd/config.toml
sudo mkdir -p /etc/containerd/certs.d


echo
echo "Configuring docker registry mirror..."
echo
if [ -z "${DOCKER_REGISTRY_MIRROR}" ]; then
  echo "No docker registry mirror configured. Skipping."
else
  CHANGES="true"
  echo "Configuring containerd to use docker registry mirror: ${DOCKER_REGISTRY_MIRROR}"

  sed \
    -e "s?{{DOCKER_REGISTRY_MIRROR}}?${DOCKER_REGISTRY_MIRROR}?g" \
    docker.io-mirror-template.toml \
    > docker.io-mirror-hosts.toml
  
  sudo mkdir -p /etc/containerd/certs.d/docker.io
  sudo mv docker.io-mirror-hosts.toml /etc/containerd/certs.d/docker.io/hosts.toml
fi


echo
echo "Configuring dockerhub registry credentials..."
echo

if [ -z "${DOCKERHUB_USERNAME}" ]; then
  echo "No dockerhub credential provided. Skipping."
else
  CHANGES="true"
  echo "Configuring dockerhub credentials (username: ${DOCKERHUB_USERNAME})..."

  sed \
    -e "s?{{DOCKERHUB_USERNAME}}?${DOCKERHUB_USERNAME}?g" \
    -e "s?{{DOCKERHUB_PASSWORD}}?${DOCKERHUB_PASSWORD}?g" \
    auth-config-addendum-template.toml \
    > auth-config-addendum.toml

  cat auth-config-addendum.toml | tee -a /etc/containerd/config.toml > /dev/null 2>&1

fi

if [ "${CHANGES}" == "true" ]; then
  # Restart containerd to reload configuration
  echo
  echo "Restarting containerd service to reload configuration changes..."
  echo
  sudo systemctl restart containerd
fi