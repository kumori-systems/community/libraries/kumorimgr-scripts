#!/bin/bash

echo
echo "Preloading Kumori custom CoreDNS image..."
echo


##########
##########
##########
# # Load variable.sh configuration file
# INSTALLER_FILES_DIR="/home/ubuntu/.kumori/installer-files/kumorimgr-scripts/tmp"
# source ${INSTALLER_FILES_DIR}/variables.sh

echo "KUMORI_COREDNS_IMAGE_REGISTRY : ${KUMORI_COREDNS_IMAGE_REGISTRY}"
echo "KUBERNETES_COREDNS_VERSION : ${KUBERNETES_COREDNS_VERSION}"
echo "KUMORI_COREDNS_VERSION : ${KUMORI_COREDNS_VERSION}"
##########
##########
##########
##########

# Determine the base registry URL to perform the login operation
REGISTRY_BASE_URL=$(echo "${KUMORI_COREDNS_IMAGE_REGISTRY}" | awk -F[/:] '{print $1}')

echo "Image:        coredns:${KUBERNETES_COREDNS_VERSION}_v${KUMORI_COREDNS_VERSION}"
echo "Registry:     ${KUMORI_COREDNS_IMAGE_REGISTRY}"
echo "Registry URL: ${REGISTRY_BASE_URL}"

# If Kumori private registry credentials are provided, log in to the registry
if [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ] && [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
  echo
  echo "Private registry requires credentials, logging in to ${REGISTRY_BASE_URL}..."
  echo "- Username: ${KUMORI_IMAGES_REGISTRY_USERNAME}"
  echo "- Password: **********"
  echo
  echo "Running:  docker login ${REGISTRY_BASE_URL} -u ${KUMORI_IMAGES_REGISTRY_USERNAME} -p ****************"
  docker login ${REGISTRY_BASE_URL} -u ${KUMORI_IMAGES_REGISTRY_USERNAME} -p ${KUMORI_IMAGES_REGISTRY_PASSWORD}
fi

echo
echo "Pulling image: ${KUMORI_COREDNS_IMAGE_REGISTRY}:${KUBERNETES_COREDNS_VERSION}_v${KUMORI_COREDNS_VERSION}"
docker pull ${KUMORI_COREDNS_IMAGE_REGISTRY}/coredns:${KUBERNETES_COREDNS_VERSION}_v${KUMORI_COREDNS_VERSION}

# If Kumori private registry credentials are provided, logout from the registry
if [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ] && [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
  echo
  echo "Log out from the private registry..."
  echo
  docker logout ${REGISTRY_BASE_URL}
fi



