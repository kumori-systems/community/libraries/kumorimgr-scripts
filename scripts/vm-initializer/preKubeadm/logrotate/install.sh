#!/bin/bash

if [ "${IS_MASTER}" = "true" ]; then
  echo
  echo "Configuring logrotate for Kumori logs for Master nodes..."
  echo

  sudo cp kumori-masters /etc/logrotate.d/kumori-masters
else
  echo
  echo "Configuring logrotate for Kumori logs for Worker nodes..."
  echo

  sudo cp kumori-masters /etc/logrotate.d/kumori-workers
fi

