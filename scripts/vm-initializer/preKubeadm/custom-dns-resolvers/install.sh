#!/bin/bash


################################################################################
##  CONFIGURE CUSTOM DNS RESOLVERS                                            ##
################################################################################
if [ -n "${DNS_RESOLVERS}" ]; then
  echo "Trying to set default DNS resolvers (${DNS_RESOLVERS})..."
  # echo "supersede domain-name-servers ${DNS_RESOLVERS};" | sudo tee -a /etc/dhcp/dhclient.conf

  # Detect what Netplan file is used:
  # - /etc/netplan/50-cloud-init.yaml  (works in Openstack)
  # - /etc/netplan/01-netcfg.yaml  (works in Virtualbox)
  if [ -f "/etc/netplan/50-cloud-init.yaml" ]; then
    NETPLAN_CONFIG_FILE="/etc/netplan/50-cloud-init.yaml"
  elif [ -f "/etc/netplan/01-netcfg.yaml" ]; then
    NETPLAN_CONFIG_FILE="/etc/netplan/01-netcfg.yaml"
  else
    NETPLAN_CONFIG_FILE=""
  fi

  if [ -n "${NETPLAN_CONFIG_FILE}" ] && [ -f "${NETPLAN_CONFIG_FILE}" ]; then
    echo "Found ${NETPLAN_CONFIG_FILE} file, patching it..."

    # Backup original file for restoring it on cluster destruction
    NETPLAN_CONFIG_BACKUP="${SCRIPT_DIR}/etc-netplan-50-cloud-init_BACKUP.yaml"
    sudo cp "${NETPLAN_CONFIG_FILE}" "${NETPLAN_CONFIG_BACKUP}"

    # Prepare DNS server list in the correct format ("xx","yy","zz")
    JOINED_DNS=$(printf ",\"%s\"" "${DNS_RESOLVERS[@]}")
    JOINED_DNS=${JOINED_DNS:1}  # remove comma from the beginning
    echo "DNS SERVERS:"${JOINED_DNS}

    # Add nameservers to every ethernet interface found
    SRC_YAML=$(sudo cat "${NETPLAN_CONFIG_FILE}")
    echo "Processing netplan configuration to add nameservers..."
    echo
    echo "Source:"
    echo
    echo "${SRC_YAML}"

    echo
    echo "Patched:"
    echo
    echo "${SRC_YAML}" \
      | yq r -j - \
      | jq ".network.ethernets[] += {nameservers: { addresses: [ ${JOINED_DNS} ] } }"  \
      | yq r --prettyPrint - \
      | sudo tee ${NETPLAN_CONFIG_FILE}
    echo
    echo "Applying netplan changes..."
    echo
    sudo netplan apply
  fi
fi


