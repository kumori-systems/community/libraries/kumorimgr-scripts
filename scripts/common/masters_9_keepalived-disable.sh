#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"


################################################################################
##  DISABLE LOCAL KEEPALIVED                                                  ##
################################################################################
if [ "${INGRESS_HANDLE_FLOATING_IP}" = "true" ]; then
  echo
  echo -e $COL_BLUE"Disabling ApiServer Keepalived..."$COL_DEFAULT
  echo

  sudo cp /etc/keepalived/check_force_failure.sh /etc/keepalived/check_apiserver.sh

  echo
  echo "Keepalived health-check script replaced for disabling it!"
  echo
  echo
  echo "Done."
  echo
fi

