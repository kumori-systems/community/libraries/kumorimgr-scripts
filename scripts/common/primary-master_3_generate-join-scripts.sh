#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"

################################################################################
##  ON PRIMARY MASTER NODE - CREATE JOIN SCRIPTS                              ##
################################################################################
echo
echo -e $COL_BLUE"Creating join scripts..."$COL_DEFAULT
echo

# Find out the worker zone
NODE_ZONE=$( hostname )
for i in "${!MASTERS_IPS[@]}"; do
  if [[ "${MASTERS_IPS[$i]}" == "$NODE_IP" ]]; then
    if [[ "${MASTERS_ZONES[$i]}" != "" ]]; then
      NODE_ZONE="${MASTERS_ZONES[$i]}"
    fi
    break
  fi
done

# Force re-upload of the certificates
#
# There is a problem:
# If the command to re-upload certificates is executed, an error occur:
# "Found multiple CRI endpoints on the host."
# In practice, nothing happens (or at least we have not detected anything): the
# node works correctly.
# To avoid the error, the "--cri-socket" should be used... but the command doesn't
# support the flag! So it must be provided via configuration file, so we need
# prepare the configuration file.
#
# Other people have detected the same problem:
# https://karneliuk.com/2022/09/kubernetes-002-scaling-out-cluster-and-turning-it-in-highly-available-one/
#
KUBEADM_INIT_CONFIG_FILE="$(pwd)/provision/kubeadm-config/kubeadm-init-config.yaml"
sed -i \
  -e "s?{{IP}}?${NODE_IP}?g" \
  -e "s?{{ZONE}}?${NODE_ZONE}?g" \
  -e "s?{{APISERVER_INTERNAL_PORT}}?${APISERVER_INTERNAL_PORT}?g" \
  -e "s?{{APISERVER_URL}}?${APISERVER_URL}?g" \
  -e "s?{{APISERVER_DOMAIN}}?${APISERVER_DOMAIN}?g" \
  -e "s?{{KUMORI_COREDNS_VERSION}}?${KUMORI_COREDNS_VERSION}?g" \
  -e "s?{{KUBERNETES_COREDNS_VERSION}}?${KUBERNETES_COREDNS_VERSION}?g" \
  -e "s?{{PODS_NETWORK_CIDR}}?${PODS_NETWORK_CIDR}?g" \
  -e "s?{{SERVICE_CIDR}}?${SERVICE_CIDR}?g" \
  -e "s?{{KUMORI_IMAGES_REGISTRY}}?${KUMORI_IMAGES_REGISTRY}?g" \
  -e "s?{{KUBERNETES_VERSION}}?${KUBERNETES_VERSION}?g" \
  -e "s?{{IS_INGRESS}}?${IS_INGRESS_NODE}?g" \
  -e "s?{{IS_STORAGE}}?${IS_STORAGE_NODE}?g" \
  "${KUBEADM_INIT_CONFIG_FILE}"
CERT_KEY=$(sudo kubeadm init phase upload-certs --upload-certs --config "${KUBEADM_INIT_CONFIG_FILE}"| tail -1)

# Create a new bootstrap token, generate join commands for workers and master
# and save them to script files
JOIN_COMMAND=$(sudo kubeadm token create --print-join-command | tail -1)
echo ${JOIN_COMMAND} > /tmp/join-workers.sh

JOIN_COMMAND=$(sudo kubeadm token create --print-join-command --certificate-key ${CERT_KEY} | tail -1)
echo ${JOIN_COMMAND} > /tmp/join-masters.sh

echo
echo -e $COL_BLUE"Done."$COL_DEFAULT
echo
