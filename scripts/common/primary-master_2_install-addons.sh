#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

# For VM image generation, the base directory should be the one set in the
# INSTALLER_FILES_DIR variables because scripts are sourced.
# If INSTALLER_FILES_DIR is not set, use this script location for backwards
# compatibility.
if [ -z "${INSTALLER_FILES_DIR}" ]; then
  SCRIPT_DIR="$(dirname $(readlink -f $0))"
  HELM_HOME="${SCRIPT_DIR}/.helm"
  export HELM_HOME
else
  SCRIPT_DIR="${INSTALLER_FILES_DIR}"
  HELM_HOME="${INSTALLER_FILES_DIR}/.helm"
  export HELM_HOME
fi

# Load install configuration variables and helper funcions from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"
source "${SCRIPT_DIR}/helper.sh"

#####################################################################################
##  TEMPORARY PATCH TO ALLOW MULTIPLE CLUSTERS TO USE SAME S3 BUCKET (ticket334)   ##
#####################################################################################
if [ -z "${PKI_BACKUP_STORAGE_PREFIX}" ]; then
  PKI_BACKUP_STORAGE_PREFIX="${RELEASE_NAME}-backups"
fi
if [ -z "${ETCD_BACKUP_STORAGE_PREFIX}" ]; then
  ETCD_BACKUP_STORAGE_PREFIX="${RELEASE_NAME}-backups"
fi
#################################
##  END OF TEMPORARY PATCH     ##
#################################



################################################################################
##                     SET ADDON FIXTURES PATHS                               ##
################################################################################
ADDON_FIXTURES_DIR="${SCRIPT_DIR}/addons"
ADDON_FIXTURES_DIR_CALICO="${ADDON_FIXTURES_DIR}/calico-${CALICO_VERSION}"
ADDON_FIXTURES_DIR_AMBASSADOR="${ADDON_FIXTURES_DIR}/ambassador-ingress-${AMBASSADOR_VERSION}"
ADDON_FIXTURES_DIR_ELASTIC="${ADDON_FIXTURES_DIR}/elastic-elk-${ELASTICSEARCH_VERSION}"
ADDON_FIXTURES_DIR_FILEBEAT="${ADDON_FIXTURES_DIR}/filebeat-${FILEBEAT_VERSION}"
ADDON_FIXTURES_DIR_KUBE_PROMETHEUS="${ADDON_FIXTURES_DIR}/kube-prometheus-${KUBE_PROMETHEUS_VERSION}"
ADDON_FIXTURES_DIR_HELM="${ADDON_FIXTURES_DIR}/helm"
ADDON_FIXTURES_DIR_K8S_DASHBOARD="${ADDON_FIXTURES_DIR}/kubernetes-dashboard-${K8S_DASHBOARD_VERSION}"
ADDON_FIXTURES_DIR_KEYCLOAK="${ADDON_FIXTURES_DIR}/keycloak-operator-${KEYCLOAK_VERSION}"
ADDON_FIXTURES_DIR_MINIO="${ADDON_FIXTURES_DIR}/minio"
ADDON_FIXTURES_DIR_ETCD_BACKUP="${ADDON_FIXTURES_DIR}/etcd-backup-${ETCD_BACKUP_VERSION}"
ADDON_FIXTURES_DIR_EXTERNALDNS="${ADDON_FIXTURES_DIR}/external-dns-${EXTERNALDNS_VERSION}"
ADDON_FIXTURES_DIR_EVENTEXPORTER="${ADDON_FIXTURES_DIR}/event-exporter-${EVENT_EXPORTER_VERSION}"
ADDON_FIXTURES_DIR_OPENEBS="${ADDON_FIXTURES_DIR}/openebs-${OPENEBS_VERSION}"
ADDON_FIXTURES_DIR_CSI_CINDER_PROVIDER="${ADDON_FIXTURES_DIR}/csi-cinder-${CSI_CINDER_PROVIDER_VERSION}"
ADDON_FIXTURES_DIR_CSI_NFS_PROVIDER="${ADDON_FIXTURES_DIR}/csi-nfs-${CSI_NFS_PROVIDER_VERSION}"
ADDON_FIXTURES_DIR_CSI_CEPH_RBD_PROVIDER="${ADDON_FIXTURES_DIR}/csi-ceph-rbd-${CSI_CEPH_RBD_PROVIDER_VERSION}"
ADDON_FIXTURES_DIR_KUMORI_CRD="${ADDON_FIXTURES_DIR}/kumori-crd"
ADDON_FIXTURES_DIR_KUMORI="${ADDON_FIXTURES_DIR}/kumori"
ADDON_FIXTURES_DIR_KUMORI_PRIORITY_CLASSES="${ADDON_FIXTURES_DIR}/kumori-priority-classes"
ADDON_FIXTURES_DIR_DESCHEDULER="${ADDON_FIXTURES_DIR}/descheduler-${DESCHEDULER_VERSION}"
ADDON_FIXTURES_DIR_CERT_MANAGER="${ADDON_FIXTURES_DIR}/cert-manager-${CERT_MANAGER_VERSION}"
ADDON_FIXTURES_DIR_CERT_MANAGER_WEBHOOK_OVH="${ADDON_FIXTURES_DIR}/cert-manager-webhook-ovh-${CERT_MANAGER_WEBHOOK_OVH_VERSION}"
ADDON_FIXTURES_DIR_CLUSTERAPI="${ADDON_FIXTURES_DIR}/clusterapi-${CLUSTERAPI_VERSION}"
ADDON_FIXTURES_DIR_OPENSTACK_CLOUD_CONTROLLER="${ADDON_FIXTURES_DIR}/openstack-cloud-controller-${OPENSTACK_CLOUD_CONTROLLER_VERSION}"
ADDON_FIXTURES_DIR_AWS_CLOUD_CONTROLLER="${ADDON_FIXTURES_DIR}/aws-cloud-controller-${AWS_CLOUD_CONTROLLER_VERSION}"

# Directory containing the cluster reference domain certificate files
ADDON_FIXTURES_DIR_CERTS="${SCRIPT_DIR}/clusterCerts"

# Directory containing the Admissino certificate files
ADDON_FIXTURES_KPKI_ADMISSION_SERVER_CERT_DIR="${SCRIPT_DIR}/kumoriPki/admission"

# File with the RootCA
ROOTCA_FILENAME=$(basename $CLUSTER_PKI_ROOT_CA_FILE)
ADDON_FIXTURES_KPKI_ROOTCA="${SCRIPT_DIR}/kumoriPki/rootCA/${ROOTCA_FILENAME}"

# File with the Monitoring CA
if [ -n "${MONITORING_CLIENTCERT_TRUSTED_CA_FILE}" ]; then
  MONITORING_CA_FILENAME=$(basename $MONITORING_CLIENTCERT_TRUSTED_CA_FILE)
  ADDON_FIXTURES_MONITORING_CA="${SCRIPT_DIR}/kumoriPki/monitoringCA/${MONITORING_CA_FILENAME}"
else
  MONITORING_CA_FILENAME=""
  ADDON_FIXTURES_MONITORING_CA=""
fi

# File with the Monitoring Client Certificate (for secure remote write/read)
if [ "${PROMETHEUS_REMOTE_AUTH_TYPE}" == "clientcertificate" ]; then
  MONITORING_CLIENTCERT_CERT_FILENAME=$(basename $PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_CERT_FILE)
  MONITORING_CLIENTCERT_KEY_FILENAME=$(basename $PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_KEY_FILE)
  ADDON_FIXTURES_MONITORING_CLIENTCERT_CERT="${SCRIPT_DIR}/kumoriPki/monitoringClientCert/${MONITORING_CLIENTCERT_CERT_FILENAME}"
  ADDON_FIXTURES_MONITORING_CLIENTCERT_KEY="${SCRIPT_DIR}/kumoriPki/monitoringClientCert/${MONITORING_CLIENTCERT_KEY_FILENAME}"
else
  MONITORING_CLIENTCERT_CERT_FILENAME=""
  MONITORING_CLIENTCERT_KEY_FILENAME=""
  ADDON_FIXTURES_MONITORING_CLIENTCERT_CERT=""
  ADDON_FIXTURES_MONITORING_CLIENTCERT_KEY=""
fi

# Directory where the user can drop custom addons
ADDON_FIXTURES_DIR_CUSTOM="${SCRIPT_DIR}/custom/addons"


################################################################################
##  ON ONE MASTER NODE OR FROM OUTSIDE - INSTALL ADDONS                       ##
################################################################################
echo
echo -e $COL_BLUE"Starting cluster addons installation..."$COL_DEFAULT
echo


################################################################################
### KUMORI PRIORITY CLASSES
################################################################################
echo
echo -e $COL_BLUE"* Installing addon: KUMORI PRIORITY CLASSES..."$COL_DEFAULT
echo

cd ${ADDON_FIXTURES_DIR_KUMORI_PRIORITY_CLASSES}

source install.sh

cd ${SCRIPT_DIR}


################################################################################
### CALICO
################################################################################
if [ "${INSTALL_CALICO}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: CALICO ${CALICO_VERSION}..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_CALICO}

  source install.sh

  cd ${SCRIPT_DIR}
else
  echo
  echo -e $COL_BLUE"*** Skipped Calico installation. ***"$COL_DEFAULT
  echo
fi


################################################################################
### OPENSTACK CLOUD CONTROLLER
################################################################################
if [ "${INSTALL_OPENSTACK_CLOUD_CONTROLLER}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: OPENSTACK CLOUD CONTROLLER v${OPENSTACK_CLOUD_CONTROLLER_VERSION}..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_OPENSTACK_CLOUD_CONTROLLER}

  source install.sh

  cd ${SCRIPT_DIR}
else
  echo
  echo -e $COL_BLUE"*** Skipped Openstack Cloud Controller installation. ***"$COL_DEFAULT
  echo
fi

################################################################################
### AWS CLOUD CONTROLLER
################################################################################
if [ "${INSTALL_AWS_CLOUD_CONTROLLER}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: AWS CLOUD CONTROLLER v${AWS_CLOUD_CONTROLLER_VERSION}..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_AWS_CLOUD_CONTROLLER}

  source install.sh

  cd ${SCRIPT_DIR}
else
  echo
  echo -e $COL_BLUE"*** Skipped AWS Cloud Controller installation. ***"$COL_DEFAULT
  echo
fi


################################################################################
### OPEN-EBS STORAGE PROVIDER
################################################################################
if [ "${INSTALL_OPENEBS_PROVIDER}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: OPEN-EBS ${OPENEBS_VERSION}..."$COL_DEFAULT
  echo

  if [ -d "${ADDON_FIXTURES_DIR_OPENEBS}" ]; then
    cd ${ADDON_FIXTURES_DIR_OPENEBS}
    source install.sh
    cd ${SCRIPT_DIR}
  else
    echo
    echo -e $COL_BLUE"*** Skipped OpenEBS installation (not supported). ***"$COL_DEFAULT
    echo
  fi
else
  echo
  echo -e $COL_BLUE"*** Skipped OpenEBS installation (disabled). ***"$COL_DEFAULT
  echo
fi


################################################################################
### CSI OPENSTACK CINDER PROVIDER
################################################################################
if [ "${INSTALL_CSI_CINDER_PROVIDER}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: Openstack Cinder CSI Provider ${CSI_CINDER_PROVIDER_VERSION}..."$COL_DEFAULT
  echo

  if [ -d "${ADDON_FIXTURES_DIR_CSI_CINDER_PROVIDER}" ]; then
    cd ${ADDON_FIXTURES_DIR_CSI_CINDER_PROVIDER}
    source install.sh
    cd ${SCRIPT_DIR}
  else
    echo
    echo -e $COL_BLUE"*** Skipped Openstack Cinder CSI Provider installation (not supported). ***"$COL_DEFAULT
    echo
  fi
else
  echo
  echo -e $COL_BLUE"*** Skipped Openstack Cinder CSI Provider installation (disabled). ***"$COL_DEFAULT
  echo
fi


################################################################################
### CSI NFS PROVIDER
################################################################################
if [ "${INSTALL_CSI_NFS_PROVIDER}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: CSI NFS Provider ${CSI_NFS_PROVIDER_VERSION}..."$COL_DEFAULT
  echo

  if [ -d "${ADDON_FIXTURES_DIR_CSI_NFS_PROVIDER}" ]; then
    cd ${ADDON_FIXTURES_DIR_CSI_NFS_PROVIDER}
    source install.sh
    cd ${SCRIPT_DIR}
  else
    echo
    echo -e $COL_BLUE"*** Skipped CSI NFS Provider installation (not supported). ***"$COL_DEFAULT
    echo
  fi
else
  echo
  echo -e $COL_BLUE"*** Skipped CSI NFS Provider installation (disabled). ***"$COL_DEFAULT
  echo
fi


################################################################################
### CSI CEPH RBD PROVIDER
################################################################################
if [ "${INSTALL_CSI_CEPH_RBD_PROVIDER}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: CSI Ceph RBD Provider ${CSI_CEPH_RBD_PROVIDER_VERSION}..."$COL_DEFAULT
  echo

  if [ -d "${ADDON_FIXTURES_DIR_CSI_CEPH_RBD_PROVIDER}" ]; then
    cd ${ADDON_FIXTURES_DIR_CSI_CEPH_RBD_PROVIDER}
    source install.sh
    cd ${SCRIPT_DIR}
  else
    echo
    echo -e $COL_BLUE"*** Skipped CSI Ceph RBD Provider installation (not supported). ***"$COL_DEFAULT
    echo
  fi
else
  echo
  echo -e $COL_BLUE"*** Skipped CSI Ceph RBD Provider installation (disabled). ***"$COL_DEFAULT
  echo
fi


################################################################################
### AMBASSADOR-INGRESS
################################################################################
echo
echo -e $COL_BLUE"* Installing addon: AMBASSADOR ${AMBASSADOR_VERSION}..."$COL_DEFAULT
echo

cd ${ADDON_FIXTURES_DIR_AMBASSADOR}

source install.sh

cd ${SCRIPT_DIR}


################################################################################
### EXTERNAL DNS
################################################################################
if [ "${INSTALL_EXTERNALDNS}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: External-DNS ${EXTERNALDNS_VERSION}..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_EXTERNALDNS}

  source install.sh

  cd ${SCRIPT_DIR}
else
  echo
  echo -e $COL_BLUE"*** Skipped External-DNS installation. ***"$COL_DEFAULT
  echo
fi


################################################################################
### KUMORI CRD
################################################################################
# NOTE: Kumori CRDs are installed early since other addon installers may create
#       KukuObjects. For example, depending on KubePrometheus configuration, it
#       can be necessary to create a KukuCA.
################################################################################
echo
echo -e $COL_BLUE"* Installing addon: KUMORI CRDs..."$COL_DEFAULT
echo

cd ${ADDON_FIXTURES_DIR_KUMORI_CRD}

source install.sh

cd ${SCRIPT_DIR}


################################################################################
### KUBE-PROMETHEUS
################################################################################
if [ "${INSTALL_KUBE_PROMETHEUS}" == "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: KUBE-PROMETHEUS ${KUBE_PROMETHEUS_VERSION}..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_KUBE_PROMETHEUS}

  source install.sh

  cd ${SCRIPT_DIR}
else
  echo
  echo -e $COL_BLUE"*** Skipped Kube-Prometheus installation. ***"$COL_DEFAULT
  echo
fi


################################################################################
### KUMORI
################################################################################
# NOTE: Kumori addon has been moved up in the installation order because it
#       installs the wildcard SSL certificate required for any web domain to
#       work, and some installation scripts (Etcd-backup) required access to
#       deployed domains (Minio S3 endpoint)
################################################################################
echo
echo -e $COL_BLUE"* Installing addon: KUMORI..."$COL_DEFAULT
echo

cd ${ADDON_FIXTURES_DIR_KUMORI}

source install.sh

cd ${SCRIPT_DIR}


################################################################################
### MINIO
################################################################################
if [ "${INSTALL_MINIO}" == "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: MINIO STORAGE ${MINIO_VERSION}..."$COL_DEFAULT
  echo

  if [ -d "${ADDON_FIXTURES_DIR_MINIO}" ]; then
    cd ${ADDON_FIXTURES_DIR_MINIO}
    source install.sh
    cd ${SCRIPT_DIR}
  else
    echo
    echo -e $COL_BLUE"*** Skipped MinIO Storage installation (not supported). ***"$COL_DEFAULT
    echo
  fi
else
  echo
  echo -e $COL_BLUE"*** Skipped MinIO Storage installation. ***"$COL_DEFAULT
  echo
fi

################################################################################
### HELM
################################################################################
if [ "${INSTALL_HELM}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: HELM TILLER..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_HELM}

  kubectl create -f rbac-config.yaml
  helm init --service-account tiller --wait
  sleep 10

  cd ${SCRIPT_DIR}
else
  echo
  echo -e $COL_BLUE"*** Skipped Helm Tiller installation. ***"$COL_DEFAULT
  echo
fi


################################################################################
### ELASTICSEARCH-KIBANA
################################################################################
if [ "${INSTALL_ELASTICSEARCH}" = "true" ] || \
   [ "${INSTALL_KIBANA}" = "true" ]
then
  echo
  echo -e $COL_BLUE"* Installing addon: ELASTIC EFK ${ELASTICSEARCH_VERSION}..."$COL_DEFAULT
  echo

  if [ -d "${ADDON_FIXTURES_DIR_ELASTIC}" ]; then
    cd ${ADDON_FIXTURES_DIR_ELASTIC}
    source install.sh
    cd ${SCRIPT_DIR}
  else
    echo
    echo -e $COL_BLUE"*** Skipped Elastic ELK installation (not supported). ***"$COL_DEFAULT
    echo
  fi
else
  echo
  echo -e $COL_BLUE"*** Skipped Elastic ELK installation. ***"$COL_DEFAULT
  echo
fi


################################################################################
### FILEBEAT
################################################################################
if [ "${INSTALL_FILEBEAT}" = "true" ]
then
  echo
  echo -e $COL_BLUE"* Installing addon: FILEBEAT ${FILEBEAT_VERSION}..."$COL_DEFAULT
  echo

  if [ -d "${ADDON_FIXTURES_DIR_FILEBEAT}" ]; then
    cd ${ADDON_FIXTURES_DIR_FILEBEAT}
    source install.sh
    cd ${SCRIPT_DIR}
  else
    echo
    echo -e $COL_BLUE"*** Skipped Filebeat installation (not supported). ***"$COL_DEFAULT
    echo
  fi
else
  echo
  echo -e $COL_BLUE"*** Skipped Filebeat installation. ***"$COL_DEFAULT
  echo
fi

################################################################################
### KUBERNETES-DASHBOARD
################################################################################
if [ "${INSTALL_KUBERNETES_DASHBOARD}" == "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: KUBERNETES-DASHBOARD ${K8S_DASHBOARD_VERSION}..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_K8S_DASHBOARD}

  source install.sh

  cd ${SCRIPT_DIR}
else
  echo
  echo -e $COL_BLUE"*** Skipped Kubernetes Dashboard installation. ***"$COL_DEFAULT
  echo
fi

################################################################################
### KEYCLOAK
################################################################################
if [ "${INSTALL_KEYCLOAK}" == "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: KEYCLOAK ${KEYCLOAK_VERSION}..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_KEYCLOAK}

  source install.sh

  cd ${SCRIPT_DIR}
else
  echo
  echo -e $COL_BLUE"*** Skipped Keycloak installation. ***"$COL_DEFAULT
  echo
fi


################################################################################
### EVENT-EXPORTER
################################################################################
if [ "${EVENTS_STORE_TYPE}" == "elasticsearch" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: Event-Exporter ${EVENT_EXPORTER_VERSION}..."$COL_DEFAULT
  echo

  if [ -d "${ADDON_FIXTURES_DIR_EVENTEXPORTER}" ]; then
    cd ${ADDON_FIXTURES_DIR_EVENTEXPORTER}
    source install.sh
    cd ${SCRIPT_DIR}
  else
    echo
    echo -e $COL_BLUE"*** Skipped Event-Exporter installation (not supported). ***"$COL_DEFAULT
    echo
  fi
else
  echo
  echo -e $COL_BLUE"*** Skipped Event-Exporter installation. ***"$COL_DEFAULT
  echo
fi

################################################################################
### ETCD-BACKUP
################################################################################
if [ "${INSTALL_ETCD_BACKUP}" == "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: Etcd-Backup ${ETCD_BACKUP_VERSION}..."$COL_DEFAULT
  echo

  if [ -d "${ADDON_FIXTURES_DIR_ETCD_BACKUP}" ]; then
    cd ${ADDON_FIXTURES_DIR_ETCD_BACKUP}
    source install.sh
    cd ${SCRIPT_DIR}
  else
    echo
    echo -e $COL_BLUE"*** Skipped Etcd-Backup installation (not supported). ***"$COL_DEFAULT
    echo
  fi
else
  echo
  echo -e $COL_BLUE"*** Skipped Etcd-Backup installation. ***"$COL_DEFAULT
  echo
fi

################################################################################
### DESCHEDULER
################################################################################
if [ "${INSTALL_DESCHEDULER}" == "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: DESCHEDULER ${DESCHEDULER_VERSION}..."$COL_DEFAULT
  echo

  if [ -d "${ADDON_FIXTURES_DIR_DESCHEDULER}" ]; then
    cd ${ADDON_FIXTURES_DIR_DESCHEDULER}
    source install.sh
    cd ${SCRIPT_DIR}
  else
    echo
    echo -e $COL_BLUE"*** Skipped Descheduler installation (not supported). ***"$COL_DEFAULT
    echo
  fi
else
  echo
  echo -e $COL_BLUE"*** Skipped Descheduler installation. ***"$COL_DEFAULT
  echo
fi


################################################################################
### CERT-MANAGER
################################################################################
if [ "${INSTALL_CERT_MANAGER}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: CertManager v${CERT_MANAGER_VERSION}..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_CERT_MANAGER}

  source install.sh

  cd ${SCRIPT_DIR}
else
  echo
  echo -e $COL_BLUE"*** Skipped CertManager installation. ***"$COL_DEFAULT
  echo
fi


################################################################################
### CERT-MANAGER WEBHOOK OVH (only if CertManager is also installed)
################################################################################
if [ "${INSTALL_CERT_MANAGER}" = "true" ] && [ "${INSTALL_CERT_MANAGER_WEBHOOK_OVH}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: CertManager Webhook OVH v${CERT_MANAGER_WEBHOOK_OVH_VERSION}..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_CERT_MANAGER_WEBHOOK_OVH}

  source install.sh

  cd ${SCRIPT_DIR}
else
  echo
  echo -e $COL_BLUE"*** Skipped CertManager Webhook OVH installation. ***"$COL_DEFAULT
  echo
fi


################################################################################
### CLUSTERAPI
################################################################################
if [ "${INSTALL_CLUSTERAPI}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Installing addon: ClusterAPI v${CLUSTERAPI_VERSION}..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_CLUSTERAPI}

  source install.sh

  cd ${SCRIPT_DIR}
else
  echo
  echo -e $COL_BLUE"*** Skipped ClusterApi installation. ***"$COL_DEFAULT
  echo
fi


################################################################################
### USER CUSTOM SCRIPTS
################################################################################
if [ -f "${ADDON_FIXTURES_DIR_CUSTOM}/install.sh" ]; then
  echo
  echo -e $COL_BLUE"* Running user custom scripts..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_CUSTOM}
  source ${ADDON_FIXTURES_DIR_CUSTOM}/install.sh

  cd ${SCRIPT_DIR}
fi


################################################################################
### HELM-RESET
################################################################################
if [ "${INSTALL_HELM}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Removing addon: HELM TILLER..."$COL_DEFAULT
  echo

  cd ${ADDON_FIXTURES_DIR_HELM}

  helm reset -f
  kubectl delete -f rbac-config.yaml

  cd ${SCRIPT_DIR}
else
  echo
  echo -e $COL_BLUE"*** Skipped Helm Tiller removal. ***"$COL_DEFAULT
  echo
fi



echo
echo -e $COL_BLUE"Finished cluster addons installation."$COL_DEFAULT
echo
