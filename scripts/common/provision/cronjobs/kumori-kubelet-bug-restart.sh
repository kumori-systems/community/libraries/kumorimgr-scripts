#!/bin/bash

# Look for the well-known Kubelet errors related to Go HTTP2 issue #39750
OUTPUT=$(journalctl -u kubelet -n 1 | grep "use of closed network connection")

if [[ $? != 0 ]]; then
  echo "Error not found in logs"
elif [[ ${OUTPUT} ]]; then
  echo "Restart kubelet"
  systemctl restart kubelet
fi
