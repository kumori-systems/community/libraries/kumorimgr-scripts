#!/bin/bash

# This script will be called every time the local Keepalived state changes.
# It will be called with the following parameter:    <VRRP_INSTANCE_ID> <NEW_STATE>
# Examples:
#   KA_installertest_INGRESS MASTER
#   KA_installertest_INGRESS BACKUP
#   KA_installertest_INGRESS FAULT
#   KA_installertest_INGRESS MASTER_RX_LOWER_PRI
#
# MASTER_RX_LOWER_PRI: indicates a MASTER has received an advert from a lower priority node

LOG_FILE="/tmp/custom_keepalived_state_event_handler.log"

echo "$(date +"%a %b %e %X %Y") - Received state change. New state is $2" >> ${LOG_FILE}

exit 0
