#!/bin/sh

# This custom script is used by Keepalived to determine if the node should be
# considered healthy or faulty.
#
# The following checks must be successful in order for the node to be Healthy:
#
# Scenario 1: with ApiServer internal balancing
# - Check that local Envoy load-balancer is up and responding (`/health`)
# - Check that local Envoy is responding on the ApiServer external port (8443)
# - If the node is holding the virtual IP, check that the ApiServer external port (8443) of virtual IP is responding
#
# Scenario 2: without ApiServer internal balancing
# - Check that local ApiServer is responding on the ApiServer internal port (6443)
# - If the node is holding the virtual IP, check that the ApiServer external port (6443) of virtual IP is responding
#
###################################################################################################
# IMPLEMENTATION DETAILS
#
# Checking TCP ports: 'nc' for checking if the port is open. Exit code is 0 on success, 1 on failure.
#   -v : verbose, prints "Connection to 127.0.0.1 443 port [tcp/https] succeeded!"
#   -z : only scan port
#   -w : connection timeout in seconds
# Example: nc -z -w5 <host> <port>
###################################################################################################


# Function for exiting on check errors
errorExit() {
    echo "*** $*" 1>&2
    exit 1
}


APISERVER_INTERNAL_BALANCING="{{APISERVER_INTERNAL_BALANCING}}"

APISERVER_VIRTUAL_IP="{{APISERVER_VIRTUAL_IP}}"
APISERVER_INTERNAL_PORT="{{APISERVER_INTERNAL_PORT}}"
APISERVER_EXTERNAL_PORT="{{APISERVER_EXTERNAL_PORT}}"

ENVOY_ADMIN_PORT="8001"
ENVOY_ADMIN_ADDRESS="{{NODE_IP}}"
ENVOY_ADMIN_PROTOCOL="http"

LOAD_BALANCER_CHECK_URL="${ENVOY_ADMIN_PROTOCOL}://${ENVOY_ADMIN_ADDRESS}:${ENVOY_ADMIN_PORT}/ready"

# LOG_FILE="/temp/keepalived-check-script.log"

errorExit() {
    echo "*** $*" 1>&2
    exit 1
}


if [ "${APISERVER_INTERNAL_BALANCING}" = "true" ]; then

  # Check that local Envoy balancer is up and responding (/health)
  curl \
    --silent \
    --max-time 2 \
    --insecure \
    ${LOAD_BALANCER_CHECK_URL} \
    -o /dev/null \
    || errorExit "Error: GET ${LOAD_BALANCER_CHECK_URL}"
  # curl \
  #   --max-time 2 \
  #   --insecure \
  #   ${LOAD_BALANCER_CHECK_URL} \
  #   -o /dev/null \
  #   || errorExit "Error GET ${LOAD_BALANCER_CHECK_URL}" \
  #   >> "${LOG_FILE}" 2>&1


  # Check that ApiServer external port is open locally
  nc -z -w1 127.0.0.1 ${APISERVER_EXTERNAL_PORT} \
    || errorExit "Error: connecting to port ${APISERVER_EXTERNAL_PORT} on 127.0.0.1 (nc -z -w1 127.0.0.1 ${APISERVER_EXTERNAL_PORT})"

  # If the node is holding the virtual IP, check that ApiServer external port is open via the virtual IP
  if ip addr | grep -q ${APISERVER_VIRTUAL_IP}; then
    nc -z -w1 ${APISERVER_VIRTUAL_IP} ${APISERVER_EXTERNAL_PORT} \
      || errorExit "Error: connecting to port ${APISERVER_EXTERNAL_PORT} on ${APISERVER_VIRTUAL_IP} (nc -z -w1 ${APISERVER_VIRTUAL_IP} ${APISERVER_EXTERNAL_PORT})"
  fi

else

  # Check that local ApiServer is responding on internal port (6443) to an HTTPS request
  curl \
    --silent \
    --max-time 2 \
    --insecure \
    https://localhost:${APISERVER_INTERNAL_PORT}/ \
    -o /dev/null \
    || errorExit "Error GET https://localhost:${APISERVER_INTERNAL_PORT}/"
  # curl \
  #   --max-time 2 \
  #   --insecure \
  #   https://localhost:${APISERVER_INTERNAL_PORT}/ \
  #   -o /dev/null \
  #   || errorExit "Error GET https://localhost:${APISERVER_INTERNAL_PORT}/" \
  #   >> "${LOG_FILE}" 2>&1


  # If the node is holding the virtual IP, check that ApiServer is responding on external port (8443) of virtual IP
  if ip addr | grep -q ${APISERVER_VIRTUAL_IP}; then
    curl \
      --silent \
      --max-time 2 \
      --insecure \
      https://${APISERVER_VIRTUAL_IP}:${APISERVER_EXTERNAL_PORT}/ \
      -o /dev/null \
      || errorExit "Error GET https://${APISERVER_VIRTUAL_IP}:${APISERVER_EXTERNAL_PORT}/"
    # curl \
    #   --max-time 2 \
    #   --insecure \
    #   https://${APISERVER_VIRTUAL_IP}:${APISERVER_EXTERNAL_PORT}/ \
    #   -o /dev/null \
    #   || errorExit "Error GET https://${APISERVER_VIRTUAL_IP}:${APISERVER_EXTERNAL_PORT}/" \
    #   >> "${LOG_FILE}" 2>&1
  fi

fi
