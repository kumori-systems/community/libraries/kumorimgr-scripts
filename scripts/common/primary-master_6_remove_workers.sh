#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"



################################################################################
## ON PRIMARY MASTER NODE - REMOVE WORKER NODES                               ##
################################################################################

for WORKER_IP in ${REMOVE_WORKERS_IPS[@]}; do

  WORKER_NAME=$( kubectl get nodes -l kumori/public-ip=${WORKER_IP} --ignore-not-found -ojsonpath='{.items[0].metadata.name}' )

  if [ "${REMOVED_NAME}" != "" ]; then
    echo
    echo -e $COL_BLUE"Removing node ${WORKER_NAME} (${WORKER_IP}) from the cluster..."$COL_DEFAULT
    echo


    # Drain node (evict all Pods so they are rescheduled)
    echo
    echo "* Draining node..."
    echo
    n=0
    DRAINED="false"
    until [ $n -ge 5 ]
    do
      kubectl drain --ignore-daemonsets --delete-emptydir-data --timeout=30s ${WORKER_NAME} && DRAINED="true" && break
      echo
      echo -e $COL_YELLOW"[WARN] Drain command failed. Retrying in 5s..."$COL_DEFAULT
      echo
      n=$[$n+1]
      sleep 5
    done
    if [ "${DRAINED}" == "true" ]; then
      echo "Node drained."$COL_DEFAULT
      echo
    else
      echo -e $COL_RED"[ERROR] Node could not be drained in time."$COL_DEFAULT
      echo
    fi 


    # Delete node from Kubernetes node list (otherwise after reset it would still
    # show up as "NotReady,SchedulingDisabled")
    echo
    echo "* Deleting node..."
    echo
    n=0
    DELETED="false"
    until [ $n -ge 5 ]
    do
      kubectl delete node --now --wait ${WORKER_NAME} && DELETED="true" && break
      echo
      echo -e $COL_YELLOW"[WARN] Delete command failed. Retrying in 5s..."$COL_DEFAULT
      echo
      n=$[$n+1]
      sleep 5
    done
    if [ "${DELETED}" == "true" ]; then
      echo "Node deleted."$COL_DEFAULT
      echo
    else
      echo -e $COL_RED"[ERROR] Node could not be deleted in time."$COL_DEFAULT
      echo
    fi 

    if [ "${DRAINED}" == "true" ] && [ "${DELETED}" == "true" ]; then
      echo
      echo -e $COL_BLUE"Node ${WORKER_NAME} (${WORKER_IP}) has been removed from the cluster."$COL_DEFAULT
      echo
    else
      echo
      echo -e $COL_RED_BOLD"Some errors occurred while removing node ${WORKER_NAME} (${WORKER_IP}). Please review the logs above and take the necessary actions."$COL_DEFAULT
      echo
    fi
  else
    echo
    echo -e $COL_BLUE"Node ${REMOVED_IP} is not in cluster state so it can't be drained nor deleted."$COL_DEFAULT
    echo
  fi

done

