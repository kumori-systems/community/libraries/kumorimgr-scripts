#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

# NOTE: All nodes are supposed to be on Ubuntu 18.04 Bionic

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"


################################################################################
##  RECONFIGURE KEEPALIVED FOR INGRESS (only for Ingress nodes)               ##
################################################################################
if [ "${INGRESS_HANDLE_FLOATING_IP}" = "true" ]; then
  echo
  echo -e $COL_BLUE"Reconfiguring Ingress Keepalived..."$COL_DEFAULT
  echo

  # Extract previously configured Keepalived role (MASTER/BACKUP), priority and network interface
  # since these values don't come from config but are calculated at installation time
  CURRENT_ROLE="$(sed -n -e 's/^\s*state\s*\(.*\)\s*$/\1/p' /etc/keepalived/keepalived.conf)"
  CURRENT_PRIORITY="$(sed -n -e 's/^\s*priority\s*\(.*\)\s*$/\1/p' /etc/keepalived/keepalived.conf)"
  CURRENT_INTERFACE="$(sed -n -e 's/^\s*interface\s*\(.*\)\s*$/\1/p' /etc/keepalived/keepalived.conf)"

  # Prepare Keepalived configuration file and health check script
  KEEPALIVED_CONF_FILE_TEMPLATE_FILE="${SCRIPT_DIR}/provision/keepalived/keepalived-ingress.conf.template"
  KEEPALIVED_CONF_FILE="${SCRIPT_DIR}/provision/keepalived/keepalived-ingress.conf"

  # Use VRID set in configuration or use default values (11 for ApiServer, 12 for Ingress)
  KEEPALIVED_VIRTUAL_ROUTER_ID="${INGRESS_VRID:-11}"

  # We create a specific password (max length 8) for this Keepalived cluster, with the following format:
  # <prefix><hash>  where <prefix> can be AS for ApiServer or IN for Ingress and <hash> is a simple checksum
  # of the Kumori cluster name (truncated to 6 chars).
  KEEPALIVED_PASSWORD="IN$(echo ${RELEASE_NAME} | cksum | cut -f 1 -d ' ' | tail -c 6)"

  sed \
    -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" \
    -e "s?{{INITIAL_ROLE}}?${CURRENT_ROLE}?g" \
    -e "s?{{NETWORK_INTERFACE}}?${CURRENT_INTERFACE}?g" \
    -e "s?{{VIRTUAL_ROUTER_ID}}?${KEEPALIVED_VIRTUAL_ROUTER_ID}?g" \
    -e "s?{{PRIORITY}}?${CURRENT_PRIORITY}?g" \
    -e "s?{{PASSWORD}}?${KEEPALIVED_PASSWORD}?g" \
    -e "s?{{INGRESS_VIRTUAL_IP}}?${INGRESS_VIRTUAL_IP}?g" \
    -e "s?{{NODE_IP}}?${NODE_IP}?g" \
    ${KEEPALIVED_CONF_FILE_TEMPLATE_FILE} \
    > ${KEEPALIVED_CONF_FILE}

  # Add a VRRP peer for each ingress node (except itself)
  EMPTY_LIST="true"
  for INGRESS_NODE_IP in ${INGRESS_NODES_IPS[@]}; do
    echo "IP: ${INGRESS_NODE_IP}"
    if [[ "${INGRESS_NODE_IP}" == "${NODE_IP}" ]]; then
      echo "Skipping node (self)"
    elif [[ " ${MAINTENANCE_WORKERS_IPS[@]} " =~ " ${INGRESS_NODE_IP} " ]]; then
      echo "Skipping node (node in maintenance)"
    else
      EMPTY_LIST="false"
      # Duplicate the peer template line and replace the IP in one of the copies
      sed -i -e "s/^\(\s*\){{PEER_NODE_IP}}\s*$/\1${INGRESS_NODE_IP}\n&/g" ${KEEPALIVED_CONF_FILE}
    fi
  done

  # If peer list is empty, add 127.0.0.1 (localhost) to avoid Keepalived falling back to multicast
  if [[ "${EMPTY_LIST}" == "true" ]]; then
    DEFAULT_PEER_IP="127.0.0.1"
    sed -i -e "s/^\(\s*\){{PEER_NODE_IP}}\s*$/\1${DEFAULT_PEER_IP}\n&/g" ${KEEPALIVED_CONF_FILE}
  fi

  # Remove peer template line
  sed -i -e "/^.*{{PEER_NODE_IP}}.*$/d" ${KEEPALIVED_CONF_FILE}

  echo
  echo "Keepalived configuration to be applied:"
  echo
  cat ${KEEPALIVED_CONF_FILE}
  echo

  # Move configuration file to its correct location (keep a backup copy)
  TIMESTAMP=$(date +"%Y%m%d_%H%M%S")
  KEEPALIVED_CONFIG_BACKUP="${SCRIPT_DIR}/provision/keepalived/keepalived-ingress.conf_BACK_${TIMESTAMP}"
  cp /etc/keepalived/keepalived.conf ${KEEPALIVED_CONFIG_BACKUP}
  sudo mv ${KEEPALIVED_CONF_FILE} /etc/keepalived/keepalived.conf

  # Force a Keepalived configuration reload (no downtime, IP is not released)
  echo
  echo "Forcing a Keepalived configuration reload..."
  echo
  sudo kill -s $(keepalived --signum=RELOAD) $(cat /var/run/keepalived.pid)

  echo
  echo "Done."
  echo
fi


################################################################################
##  RECONFIGURE ENVOY FOR INGRESS (only for Ingress nodes)                    ##
################################################################################
IS_INGRESS_NODE="false"
if [[ " ${INGRESS_NODES_IPS[@]} " =~ " ${NODE_IP} " ]]; then
  IS_INGRESS_NODE="true"
fi

if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ] && [ "${IS_INGRESS_NODE}" = "true" ]; then
  echo
  echo -e $COL_BLUE"Reconfiguring Ingress Envoy proxy..."$COL_DEFAULT
  echo

  # Prepare updated Envoy EDS (Endpoint Discovery Service) configuration file
  echo
  echo "Preparing Envoy EDS file..."
  echo

  EDS_CONF_TEMPLATE_FILE="${SCRIPT_DIR}/provision/envoy/eds.conf.template"
  EDS_CONF_TMP_FILE="${SCRIPT_DIR}/provision/envoy/eds.conf.tmp"
  EDS_CONF_FILE="${SCRIPT_DIR}/provision/envoy/eds.conf"

  # Create a base configuration file based on the EDS config template
  cp ${EDS_CONF_TEMPLATE_FILE} ${EDS_CONF_FILE}

  # Add a new cluster for HTTPS configuration
  cat ${EDS_CONF_FILE} \
    | jq ".resources += [ {\"@type\":\"type.googleapis.com/envoy.config.endpoint.v3.ClusterLoadAssignment\",cluster_name:\"ingressHTTPS\",endpoints:[{lb_endpoints:[]}]} ]" \
    > ${EDS_CONF_TMP_FILE}
  mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}

  # Create an array with all valid endpoint IPs (Primary Master + Secondary Masters)
  ENDPOINTS=()
  for WORKER_IP in ${INGRESS_NODES_IPS[@]}; do
    if [[ " ${MAINTENANCE_WORKERS_IPS[@]} " =~ " ${WORKER_IP} " ]]; then
      echo "Skipping node ${WORKER_IP} (node in maintenance)"
    else
      ENDPOINTS+=(${WORKER_IP})
    fi
  done

  # For each IP add a new "endpoint" element to the configuration (JSON).
  # An endpoint has the following structure:
  #   {
  #     "endpoint": {
  #       "address": {
  #         "socket_address": {
  #           "address": "10.0.18.56",
  #           "port_value": 8443
  #         }
  #       },
  #       "health_check_config": {     <==== ONLY NEEDED IF HEALTHCHECK PORT IS DIFFERENT
  #         "port_value": 8080
  #       }
  #     }
  #   }
  # Add HTTPS endpoints to ingressHTTPS cluster
  for ENDPOINT_IP in ${ENDPOINTS[@]}; do
    echo "IP: ${ENDPOINT_IP}"
    # FOR REFERENCE: if healthcheck is different, use this replacement instead:
    #   | jq ".resources[0].endpoints[0].lb_endpoints += [ { endpoint: { address: { socket_address: { address: \"${ENDPOINT_IP}\", port_value: ${INGRESS_INTERNAL_PORT} } }, health_check_config: { port_value: 8080 } } } ]"  \
    cat ${EDS_CONF_FILE} \
      | jq ".resources[0].endpoints[0].lb_endpoints += [ { endpoint: { address: { socket_address: { address: \"${ENDPOINT_IP}\", port_value: ${INGRESS_INTERNAL_PORT} } } } } ]"  \
      > ${EDS_CONF_TMP_FILE}
    mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}
  done

  mv ${EDS_CONF_FILE} ${SCRIPT_DIR}/provision/envoy/eds_ingressHTTPS.conf
  echo
  echo "Envoy EDS file for ingressHTTPS: "
  echo
  cat ${SCRIPT_DIR}/provision/envoy/eds_ingressHTTPS.conf
  echo
  # Put EDS for ingressHTTPS in the appropriate directory
  sudo mv ${SCRIPT_DIR}/provision/envoy/eds_ingressHTTPS.conf /etc/envoy/

  if [ -n "${INGRESS_TCP_PORTS}" ]; then
    # For each custom TCP port, create a new EDS configuration file and populate it
    # Loop array indices
    for i in "${!INGRESS_TCP_PORTS[@]}"; do
      INTERNAL_PORT="${INGRESS_TCP_INTERNAL_PORTS[$i]}"
      EXTERNAL_PORT="${INGRESS_TCP_PORTS[$i]}"

      PORT_EDS_CONFIG_FILE="${SCRIPT_DIR}/provision/envoy/eds_tcp${EXTERNAL_PORT}.conf"

      # Create a base configuration file based on the EDS config template
      cp ${EDS_CONF_TEMPLATE_FILE} ${EDS_CONF_FILE}

      cat ${EDS_CONF_FILE} \
        | jq ".resources += [ {\"@type\":\"type.googleapis.com/envoy.config.endpoint.v3.ClusterLoadAssignment\",cluster_name:\"tcp${EXTERNAL_PORT}\",endpoints:[{lb_endpoints:[]}]} ]" \
        > ${EDS_CONF_TMP_FILE}
      mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}

      for ENDPOINT_IP in ${ENDPOINTS[@]}; do
        cat ${EDS_CONF_FILE} \
          | jq ".resources[0].endpoints[0].lb_endpoints += [ { endpoint: { address: { socket_address: { address: \"${ENDPOINT_IP}\", port_value: ${INTERNAL_PORT} } } } } ]"  \
          > ${EDS_CONF_TMP_FILE}
        mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}
      done

      mv ${EDS_CONF_FILE} ${PORT_EDS_CONFIG_FILE}
      echo
      echo "Envoy EDS file for TCP ${EXTERNAL_PORT}: "
      echo
      cat ${PORT_EDS_CONFIG_FILE}
      echo
      # Put EDS for ingressHTTPS in the appropriate directory
      sudo mv ${PORT_EDS_CONFIG_FILE} /etc/envoy/
    done
  fi
  echo
  echo "Done."
  echo

fi
