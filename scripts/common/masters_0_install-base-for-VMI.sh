#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

# NOTE: All nodes are supposed to be on Ubuntu 20.04 Focal

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"

# Avoid service restart confirmation prompts
export DEBIAN_FRONTEND=noninteractive


################################################################################
##  BASIC HOST CONFIGURATION                                                  ##
################################################################################
# Avoid "sudo: unable to resolve host <xxxx>" error
# echo "$(hostname -I | cut -d\  -f1) $(hostname)   # Added by Kumori installer" | sudo tee -a /etc/hosts > /dev/null 2>&1

# Make sure locale is correctly configured
if [[ -n "${DEFAULT_LOCALE}" ]]; then
  sudo locale-gen "${DEFAULT_LOCALE}"
else
  sudo locale-gen
fi

sudo DEBIAN_FRONTEND=noninteractive dpkg-reconfigure locales

# Set machine timezone
if [[ -n "${TIMEZONE}" ]]; then
  sudo timedatectl set-timezone "${TIMEZONE}"
fi

# Avoid service restart confirmation prompts (additional tweak)
echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections

# Filter out from /var/log/syslog false positive errors due to systemd/docker bug
# More info: https://github.com/kubernetes/kubernetes/issues/71887
sudo cp ${SCRIPT_DIR}/provision/rsyslog_10-ignore-systemd-session-slice.conf /etc/rsyslog.d/10-ignore-systemd-session-slice.conf
sudo cp ${SCRIPT_DIR}/provision/rsyslog_10-ignore-run-docker-runtime-mount.conf /etc/rsyslog.d/10-ignore-run-docker-runtime-mount.conf
sudo cp ${SCRIPT_DIR}/provision/rsyslog_10-ignore-keeplaived-advert-127-0-0-1.conf /etc/rsyslog.d/10-ignore-keeplaived-advert-127-0-0-1.conf
sudo service rsyslog restart


################################################################################
##  INSTALL BASIC UTILITIES                                                   ##
################################################################################
# Utilities to use with kubectl
echo
echo -e $COL_BLUE"Installing net-tools, jq, yq, zip, unzip..."$COL_DEFAULT
echo
sudo apt-get update
sudo apt-get install -y jq zip unzip net-tools

echo
echo "Installing yq..."
wget --quiet -O yq https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64
chmod +x yq
sudo mv yq /usr/local/bin

echo
echo "Installing vim (full)..."
# TWEAK FOR INSTALLING ON CAPI IMAGE
sudo apt-mark unhold vim-common vim-tiny
sudo apt-get install vim -y
sudo apt-mark hold vim-common vim-tiny vim


################################################################################
##  INSTALL DOCKER                                                            ##
################################################################################
echo
echo -e $COL_BLUE"Installing docker v${DOCKER_VERSION} and related stuff..."$COL_DEFAULT

# TWEAK FOR INSTALLING ON CAPI IMAGE
sudo apt-mark unhold curl libcurl4 apt-transport-https

# Remove previous Docker studd and add Docker repository
sudo apt-get -y remove docker docker-engine docker.io containerd runc > /dev/null 2>&1
sudo apt-get -y update
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get -y update

# Install specific containerd version
if [ -n "${CONTAINERD_VERSION}" ]; then
  sudo apt-get -y -o Dpkg::Options::="--force-confold" install containerd.io=${CONTAINERD_VERSION}
fi

# Install Docker
sudo apt-get -y -o Dpkg::Options::="--force-confold" install docker-ce=${DOCKER_VERSION}

# UNDO TWEAK FOR INSTALLING ON CAPI IMAGE
sudo apt-mark hold curl libcurl4 apt-transport-https

sudo groupadd docker
sudo usermod -aG docker $USER

# Configure Docker
DAEMON_CFG_FILE="/tmp/docker-daemon-cfg.json"

cat <<EOF > "${DAEMON_CFG_FILE}"
{
  "bip": "${DOCKER_BRIDGE_CIDR}",
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m",
    "max-file": "5",
    "compress": "true"
  },
  "storage-driver": "overlay2",
  "registry-mirrors": []
}
EOF

# Add Docker registry mirrors
if [ -n "${DOCKER_REGISTRY_MIRRORS}" ]; then
  ITI_MIRROR_DOMAIN="docker-mirror.iti.upv.es"
  ITI_MIRROR_IP="10.149.16.56"
  ITI_MIRROR_USED="false"

  DAEMON_CFG_TMP_FILE="/tmp/docker-daemon-cfg_TMP.json"
  for MIRROR in ${DOCKER_REGISTRY_MIRRORS[@]}; do
    # Patch: detect if ITI mirror is used
    [[ "${MIRROR}" =~ "${ITI_MIRROR_DOMAIN}" ]] && ITI_MIRROR_USED="true"
    # The jq '--arg key' stuff is for being able to use property names with dashes ('-')
    cat ${DAEMON_CFG_FILE} \
      | jq --arg key "registry-mirrors" '.[ $key ] += [ "'${MIRROR}'" ]' \
      > ${DAEMON_CFG_TMP_FILE}
    mv ${DAEMON_CFG_TMP_FILE} ${DAEMON_CFG_FILE}
  done

  # If ITI mirror is used, add its ITI-internal address to /etc/hosts.
  # The reason is that for ITI we override the default DNS resolvers.
  if [[ "${ITI_MIRROR_USED}" == "true" ]]; then
    echo -e "\n# ITI Docker registry mirror\n${ITI_MIRROR_IP} ${ITI_MIRROR_DOMAIN}" \
      | sudo tee -a /etc/hosts > /dev/null
    echo "Patched /etc/hosts with ITI docker mirror domain."
  fi
fi

echo
echo "Docker daemon configuration:"
echo
cat ${DAEMON_CFG_FILE}
echo

sudo mv ${DAEMON_CFG_FILE} /etc/docker/daemon.json

sudo mkdir -p /etc/systemd/system/docker.service.d
sudo systemctl daemon-reload
sudo systemctl restart docker


################################################################################
##  INSTALL CRI-DOCKER                                                        ##
################################################################################
echo
echo -e $COL_BLUE"Installing cri-dockerd v${CRI_DOCKERD_VERSION} ..."$COL_DEFAULT

# Temporary directory
mkdir tmp-cri-dockerd
cd tmp-cri-dockerd

# Obtain cri-dockerd binary
wget https://github.com/Mirantis/cri-dockerd/releases/download/v${CRI_DOCKERD_VERSION}/cri-dockerd-${CRI_DOCKERD_VERSION}.amd64.tgz
tar xvf cri-dockerd-${CRI_DOCKERD_VERSION}.amd64.tgz
sudo mv cri-dockerd/cri-dockerd /usr/local/bin/

# Create and start a linux service
wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/v${CRI_DOCKERD_VERSION}/packaging/systemd/cri-docker.service
wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/v${CRI_DOCKERD_VERSION}/packaging/systemd/cri-docker.socket
sudo mv cri-docker.socket cri-docker.service /etc/systemd/system/
sudo sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
sudo systemctl daemon-reload
sudo systemctl enable cri-docker.service
sudo systemctl enable --now cri-docker.socket
systemctl status cri-docker.socket

# Clean temporary directory
cd ..
rm -rf tmp-cri-dockerd

################################################################################
##  INSTALL KUBERNETES                                                        ##
################################################################################
echo
echo -e $COL_BLUE"Installing Kubernetes stuff..."$COL_DEFAULT
echo

# Install Kumori Etcdctl and KMan scripts
sudo cp ${SCRIPT_DIR}/provision/util-scripts/ketcdctl /usr/local/bin/
sudo cp ${SCRIPT_DIR}/provision/util-scripts/kman /usr/local/bin/
sudo cp ${SCRIPT_DIR}/provision/util-scripts/kubectl-kum /usr/local/bin/
sudo cp -r ${SCRIPT_DIR}/provision/util-scripts/kumori-custom-columns  ${HOME}/.kumori
sudo chmod +x /usr/local/bin/ketcdctl
sudo chmod +x /usr/local/bin/kman
sudo chmod +x /usr/local/bin/kubectl-kum


##############################################################################
## INSTALL AND CONFIGURE KEEPALIVED                                         ##
##                                                                          ##
## - Master nodes:                                                          ##
##   - Currently ApiServer Load-Balancing is performed with KubeVIP, so it  ##
##     is not necessary to install Keepalived of Envoy Balancer.            ##
## - Worker nodes:                                                          ##
##   - Ingress load-balancing is managed by us using Keepalived and Envoy.  ##
##   not necessary to install Keepalived of Envoy Balancer.                 ##
##                                                                          ##
##############################################################################
echo
echo -e $COL_BLUE"Installing Keepalived v${KEEPALIVED_VERSION} ..."$COL_DEFAULT
echo

sudo apt-get install -y keepalived=${KEEPALIVED_VERSION}

# Find out default network interface (will be used for floating IP)
DEFAULT_NETWORK_INTERFACE=$(route | grep '^default' | grep -o '[^ ]*$')

# Configure Keepalived service to automatically restart on failures
sudo sed -i '/\[Service\]/a RestartSec=5s' /lib/systemd/system/keepalived.service
sudo sed -i '/\[Service\]/a Restart=on-failure' /lib/systemd/system/keepalived.service
sudo systemctl daemon-reload

# Initially, leave Keepalived service disabled
sudo systemctl disable keepalived

# Add a networkd-dispatcher to force failover when network is restarted
sed -e "s?{{NETWORK_INTERFACE}}?${DEFAULT_NETWORK_INTERFACE}?g" \
  ${SCRIPT_DIR}/provision/keepalived/10-kumori-keepalived-restart.template \
  > ${SCRIPT_DIR}/provision/keepalived/10-kumori-keepalived-restart

sudo mkdir -p /usr/lib/networkd-dispatcher/routable.d
sudo mv ${SCRIPT_DIR}/provision/keepalived/10-kumori-keepalived-restart \
  /usr/lib/networkd-dispatcher/routable.d/10-kumori-keepalived-restart
sudo chmod 755 /usr/lib/networkd-dispatcher/routable.d/10-kumori-keepalived-restart
sudo chown root:root /usr/lib/networkd-dispatcher/routable.d/10-kumori-keepalived-restart



##############################################################################
## INSTALL AND CONFIGURE ENVOY                                              ##
##                                                                          ##
## - Master nodes:                                                          ##
##   - Currently ApiServer Load-Balancing is performed with KubeVIP, so it  ##
##     is not necessary to install Keepalived of Envoy Balancer.            ##
## - Worker nodes:                                                          ##
##   - Ingress load-balancing is managed by us using Keepalived and Envoy.  ##
##   not necessary to install Keepalived of Envoy Balancer.                 ##
##                                                                          ##
##############################################################################
echo
echo -e $COL_BLUE"Installing Envoy proxy v${ENVOY_VERSION} ..."$COL_DEFAULT
echo

wget https://archive.tetratelabs.io/envoy/download/v${ENVOY_VERSION}/envoy-v${ENVOY_VERSION}-linux-amd64.tar.xz
tar -xf envoy-v${ENVOY_VERSION}-linux-amd64.tar.xz
chmod +x envoy-v${ENVOY_VERSION}-linux-amd64/bin/envoy
sudo mv envoy-v${ENVOY_VERSION}-linux-amd64/bin/envoy /usr/bin/envoy
rm -rf envoy-v${ENVOY_VERSION}-linux-amd64.tar.xz envoy-v${ENVOY_VERSION}-linux-amd64

# Create a service for automatically manage Envoy life-cycle
sudo cp ${SCRIPT_DIR}/provision/envoy/envoy.service /etc/systemd/system/envoy.service
sudo systemctl daemon-reload

# Initially, leave Envoy service disabled
sudo systemctl disable envoy


################################################################################
##  INSTALL HELM                                                              ##
################################################################################
echo
echo -e $COL_BLUE"Installing Helm v${HELM_VERSION} ..."$COL_DEFAULT
echo

curl -o gethelm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get
chmod +x gethelm.sh
if [[ "${HELM_VERSION}" == "latest" ]]; then
  ./gethelm.sh
else
  ./gethelm.sh -v ${HELM_VERSION}
fi
rm gethelm.sh


################################################################################
##  CONFIGURE DNS CACHING                                                     ##
################################################################################
echo
echo -e $COL_BLUE"Configuring resolved cache..."$COL_DEFAULT
echo

# Configure DNS resolution for not caching negative operations (errors)
echo "Cache=no-negative" | sudo tee -a /etc/systemd/resolved.conf 2>&1>/dev/null
# Restart systemd-resolved service
sudo service systemd-resolved restart


################################################################################
##  CONFIGURE CRON JOB FOR PERIODICALLY CHECKING/RESTARTING KUBELET           ##
################################################################################
# Add a cron job for periodically checking for a specific Kubelet error log
# trace and restarting Kubelet if it is found
KUBELET_CRONJOB_FILE="${SCRIPT_DIR}/provision/cronjobs/kumori-kubelet-bug-restart"
KUBELET_SCRIPT_FILE="${SCRIPT_DIR}/provision/cronjobs/kumori-kubelet-bug-restart.sh"

sudo chown root:root ${KUBELET_CRONJOB_FILE} ${KUBELET_SCRIPT_FILE}
sudo chmod 644 ${KUBELET_CRONJOB_FILE}
sudo chmod 755 ${KUBELET_SCRIPT_FILE}
sudo mv ${KUBELET_CRONJOB_FILE} /etc/cron.d/kumori-kubelet-bug-restart
sudo mv ${KUBELET_SCRIPT_FILE} /usr/local/bin/kumori-kubelet-bug-restart.sh


# Small pause in case there is a shutdown afterwards
sleep 10s
