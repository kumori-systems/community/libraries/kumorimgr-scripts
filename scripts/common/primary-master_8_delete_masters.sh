#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"



################################################################################
## ON PRIMARY MASTER NODE - DELETE MASTER NODES                               ##
################################################################################

for REMOVED_IP in ${REMOVE_MASTERS_IPS[@]}; do

  # REMOVED_NAME=$( kubectl get nodes -l kumori/public-ip=${REMOVED_IP} --ignore-not-found -ojsonpath='{.items[0].metadata.name}' )
  REMOVED_NAME=$( kubectl get nodes -owide | grep -w "${REMOVED_IP} " | awk '{print $1}' )

  if [ "${REMOVED_NAME}" != "" ]; then
    echo
    echo -e $COL_BLUE"Deleting node ${REMOVED_NAME} (${REMOVED_IP}) from cluster state..."$COL_DEFAULT
    echo

    # Delete node from Kubernetes node list (otherwise after reset it would still
    # show up as "NotReady,SchedulingDisabled")
    n=0
    DELETED="false"
    until [ $n -ge 5 ]
    do
      kubectl delete node --now --wait ${REMOVED_NAME} && DELETED="true" && break
      echo
      echo -e $COL_YELLOW"[WARN] Delete command failed. Retrying in 5s..."$COL_DEFAULT
      echo
      n=$[$n+1]
      sleep 5
    done

    if [ "${DELETED}" == "true" ]; then
      echo
      echo -e $COL_BLUE"Node ${REMOVED_NAME} (${REMOVED_IP}) has been deleted from the cluster state."$COL_DEFAULT
      echo
    else
      echo
      echo -e $COL_RED_BOLD"[ERROR] Some errors occurred while deleting node ${REMOVED_NAME} (${REMOVED_IP}) from cluster state."$COL_DEFAULT
      echo -e $COL_RED_BOLD"        Please review the logs above and take the necessary actions."$COL_DEFAULT
      echo
    fi
  else
    echo
    echo -e $COL_BLUE"Node ${REMOVED_IP} is not in cluster state so it can't be deleted."$COL_DEFAULT
    REMOVE_NAME="<unavailable>"
    echo
  fi

  # Check if node is still a member of the Etcd cluster, then remove it
  echo
  echo -e $COL_BLUE"Making sure node ${REMOVED_NAME} (${REMOVED_IP}) is not an Etcd member anymore..."$COL_DEFAULT
  echo
  # Determine if node is still an Etcd cluster member
  REMOVED_ETCD_MEMBER_ID=$( ketcdctl member list | grep -w ${REMOVED_IP} | sed 's/,.*//' )
  if [ "${REMOVED_ETCD_MEMBER_ID}" != "" ]; then
    echo "Node ${REMOVED_NAME} (${REMOVED_IP}) is still a member of the Etcd cluster (member ID ${REMOVED_ETCD_MEMBER_ID}):"
    echo
    ketcdctl endpoint status --cluster
    echo
    echo
    echo "Removing Etcd cluster member ${REMOVED_ETCD_MEMBER_ID}..."
    echo
    ketcdctl member remove ${REMOVED_ETCD_MEMBER_ID}
    echo
    ketcdctl endpoint status --cluster
    echo
    MEMBER_ID=$( ketcdctl member list | grep -w ${REMOVED_IP} | sed 's/,.*//' )
    if [ "${MEMBER_ID}" != "" ]; then
      echo
      echo -e $COL_RED_BOLD"[ERROR] Failed to remove node ${REMOVED_NAME} (${REMOVED_IP}) from Etcd cluster."$COL_DEFAULT
      echo -e $COL_RED_BOLD"        The operation should be performed manually."$COL_DEFAULT
      echo
    else
      echo
      echo -e $COL_BLUE"Node ${REMOVED_NAME} (${REMOVED_IP}) has been removed from Etcd cluster."$COL_DEFAULT
      echo
    fi
  else
    echo
    echo -e $COL_BLUE"Node ${REMOVED_NAME} (${REMOVED_IP}) is not a member of the Etcd cluster."$COL_DEFAULT
    echo
  fi
done

