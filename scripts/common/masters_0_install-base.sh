#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

# NOTE: All nodes are supposed to be on Ubuntu 20.04 Focal

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"

# Avoid service restart confirmation prompts
export DEBIAN_FRONTEND=noninteractive


################################################################################
##  BASIC HOST CONFIGURATION                                                  ##
################################################################################
# Avoid "sudo: unable to resolve host <xxxx>" error
echo "$(hostname -I | cut -d\  -f1) $(hostname)   # Added by Kumori installer" | sudo tee -a /etc/hosts > /dev/null 2>&1

# Make sure locale is correctly configured
sudo locale-gen "${DEFAULT_LOCALE}"
sudo DEBIAN_FRONTEND=noninteractive dpkg-reconfigure locales

# Set machine timezone
sudo timedatectl set-timezone "${TIMEZONE}"

# Avoid service restart confirmation prompts (additional tweak)
echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections

# Filter out from /var/log/syslog false positive errors due to systemd/docker bug
# More info: https://github.com/kubernetes/kubernetes/issues/71887
sudo cp ${SCRIPT_DIR}/provision/rsyslog_10-ignore-systemd-session-slice.conf /etc/rsyslog.d/10-ignore-systemd-session-slice.conf
sudo cp ${SCRIPT_DIR}/provision/rsyslog_10-ignore-run-docker-runtime-mount.conf /etc/rsyslog.d/10-ignore-run-docker-runtime-mount.conf
sudo cp ${SCRIPT_DIR}/provision/rsyslog_10-ignore-keeplaived-advert-127-0-0-1.conf /etc/rsyslog.d/10-ignore-keeplaived-advert-127-0-0-1.conf
sudo service rsyslog restart


################################################################################
##  INSTALL BASIC UTILITIES                                                   ##
################################################################################
# Utilities to use with kubectl
echo
echo -e $COL_BLUE"Installing net-tools, jq, zip, unzip..."$COL_DEFAULT
echo
sudo apt-get update
sudo apt-get install -y jq zip unzip net-tools

echo
echo "Installing yq..."
wget --quiet -O yq https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64
chmod +x yq
sudo mv yq /usr/local/bin


################################################################################
##  INSTALL DOCKER                                                            ##
################################################################################
echo
echo -e $COL_BLUE"Installing docker stuff..."$COL_DEFAULT
echo

sudo apt-get -y remove docker docker-engine docker.io containerd runc
sudo apt-get -y update

# Install Docker
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get -y update

# Install specific containerd version
if [ -n "${CONTAINERD_VERSION}" ]; then
  sudo apt-get -y install containerd.io=${CONTAINERD_VERSION}
fi

sudo apt-get -y install docker-ce=${DOCKER_VERSION}
sudo groupadd docker
sudo usermod -aG docker $USER

# Configure Docker
DAEMON_CFG_FILE="/tmp/docker-daemon-cfg.json"

cat <<EOF > "${DAEMON_CFG_FILE}"
{
  "bip": "${DOCKER_BRIDGE_CIDR}",
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m",
    "max-file": "5",
    "compress": "true"
  },
  "storage-driver": "overlay2",
  "registry-mirrors": []
}
EOF

# Add Docker registry mirrors
if [ -n "${DOCKER_REGISTRY_MIRRORS}" ]; then
  ITI_MIRROR_DOMAIN="docker-mirror.iti.upv.es"
  ITI_MIRROR_IP="10.149.16.56"
  ITI_MIRROR_USED="false"

  DAEMON_CFG_TMP_FILE="/tmp/docker-daemon-cfg_TMP.json"
  for MIRROR in ${DOCKER_REGISTRY_MIRRORS[@]}; do
    # Patch: detect if ITI mirror is used
    [[ "${MIRROR}" =~ "${ITI_MIRROR_DOMAIN}" ]] && ITI_MIRROR_USED="true"
    # The jq '--arg key' stuff is for being able to use property names with dashes ('-')
    cat ${DAEMON_CFG_FILE} \
      | jq --arg key "registry-mirrors" '.[ $key ] += [ "'${MIRROR}'" ]' \
      > ${DAEMON_CFG_TMP_FILE}
    mv ${DAEMON_CFG_TMP_FILE} ${DAEMON_CFG_FILE}
  done

  # If ITI mirror is used, add its ITI-internal address to /etc/hosts.
  # The reason is that for ITI we override the default DNS resolvers.
  if [[ "${ITI_MIRROR_USED}" == "true" ]]; then
    echo -e "\n# ITI Docker registry mirror\n${ITI_MIRROR_IP} ${ITI_MIRROR_DOMAIN}" \
      | sudo tee -a /etc/hosts > /dev/null
    echo "Patched /etc/hosts with ITI docker mirror domain."
  fi
fi

echo
echo "Docker daemon configuration:"
echo
cat ${DAEMON_CFG_FILE}
echo

sudo mv ${DAEMON_CFG_FILE} /etc/docker/daemon.json

sudo mkdir -p /etc/systemd/system/docker.service.d
sudo systemctl daemon-reload
sudo systemctl restart docker

################################################################################
##  CRI-DOCKERD INSTALLATION                                                  ##
################################################################################
echo
echo -e $COL_BLUE"Installing cri-dockerd stuff..."$COL_DEFAULT
echo

# Temporary directory
mkdir tmp-cri-dockerd
cd tmp-cri-dockerd

# Obtain cri-dockerd binary
wget https://github.com/Mirantis/cri-dockerd/releases/download/v${CRI_DOCKERD_VERSION}/cri-dockerd-${CRI_DOCKERD_VERSION}.amd64.tgz
tar xvf cri-dockerd-${CRI_DOCKERD_VERSION}.amd64.tgz
sudo mv cri-dockerd/cri-dockerd /usr/local/bin/

# Create and start a linux service
wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/v${CRI_DOCKERD_VERSION}/packaging/systemd/cri-docker.service
wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/v${CRI_DOCKERD_VERSION}/packaging/systemd/cri-docker.socket
sudo mv cri-docker.socket cri-docker.service /etc/systemd/system/
sudo sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
sudo systemctl daemon-reload
sudo systemctl enable cri-docker.service
sudo systemctl enable --now cri-docker.socket
systemctl status cri-docker.socket

# Clean temporary directory
cd ..
rm -rf tmp-cri-dockerd

################################################################################
##  INSTALL KUBERNETES  (kubectl only for masters)                            ##
################################################################################
echo
echo -e $COL_BLUE"Installing Kubernetes stuff..."$COL_DEFAULT
echo

# DEPRECATED METHOD AS OF 04/03/2024
# sudo curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
# cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
# deb https://apt.kubernetes.io/ kubernetes-xenial main
# EOF
# sudo apt-get -y update

# Convert "1.25.3" to "1.25"
KUBERNETES_MAIN_VERSION=$( echo ${KUBERNETES_VERSION} | sed 's/\.[^.]*$//')

KUBERNETES_APT_DEPRECATED_VERSIONS=( "1.16" "1.21" )
if [[ " ${KUBERNETES_APT_DEPRECATED_VERSIONS[@]} " =~ " ${KUBERNETES_MAIN_VERSION} " ]]; then
  # This version of Kubernetes does not support APT installation anymore.
  source masters_0.1_install-older-kubernetes.sh
else
  # This version of Kubernetes supports APT installation
  # Get the GPG key
  sudo mkdir -p /etc/apt/keyrings
  curl -fsSL https://pkgs.k8s.io/core:/stable:/v${KUBERNETES_MAIN_VERSION}/deb/Release.key \
    | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

  # Add an APT source
  # Task #1278 "Problem with GPG key when a KCluster (Kube v1.25) is created"):
  # The GPG key is expired, so it is necessary to explicitly indicate that it is trusted.
  # echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v${KUBERNETES_MAIN_VERSION}/deb/ /" \
  #   | sudo tee /etc/apt/sources.list.d/kubernetes.list
  echo "deb [trusted=true] https://pkgs.k8s.io/core:/stable:/v${KUBERNETES_MAIN_VERSION}/deb/ /" \
    | sudo tee /etc/apt/sources.list.d/kubernetes.list

  # Update APT packages
  sudo apt-get -y update

  # Install the Kubernetes packages
  sudo apt-get install -y \
    kubelet=${KUBERNETES_VERSION}* \
    kubeadm=${KUBERNETES_VERSION}* \
    kubectl=${KUBERNETES_VERSION}*

  # Mark packages as "hold" to avoid modifications
  sudo apt-mark hold kubelet kubeadm kubectl
fi

# Install Kumori Etcdctl and KMan scripts
sudo cp ${SCRIPT_DIR}/provision/util-scripts/ketcdctl /usr/local/bin/
sudo cp ${SCRIPT_DIR}/provision/util-scripts/kman /usr/local/bin/
sudo cp ${SCRIPT_DIR}/provision/util-scripts/kubectl-kum /usr/local/bin/
sudo cp -r ${SCRIPT_DIR}/provision/util-scripts/kumori-custom-columns  ${HOME}/.kumori
sudo chmod +x /usr/local/bin/ketcdctl
sudo chmod +x /usr/local/bin/kman
sudo chmod +x /usr/local/bin/kubectl-kum


################################################################################
##  INSTALL AND CONFIGURE KEEPALIVED FOR APISERVER (only for masters)         ##
################################################################################
if [ "${APISERVER_HANDLE_FLOATING_IP}" = "true" ]; then
  echo
  echo -e $COL_BLUE"Installing and configuring ApiServer Keepalived..."$COL_DEFAULT
  echo

  sudo apt-get install -y keepalived=${KEEPALIVED_VERSION}

  # Find out default network interface (will be used for floating IP)
  DEFAULT_NETWORK_INTERFACE=$(route | grep '^default' | grep -o '[^ ]*$')

  # Determine master node order in list (to set Keepalived priority).
  # First node will get max priority, and so on
  POSITION=100  # if not found, set very low priority
  i=0
  for MASTER_IP in ${MASTERS_IPS[@]}; do
    if [[ "${MASTER_IP}" == "${NODE_IP}" ]]; then
      POSITION=$i
      break
    fi
    ((i++))
  done

  # Prepare Keepalived configuration file and health check script
  KEEPALIVED_CONF_FILE_TEMPLATE_FILE="${SCRIPT_DIR}/provision/keepalived/keepalived-apiserver.conf.template"
  KEEPALIVED_CONF_FILE="${SCRIPT_DIR}/provision/keepalived/keepalived-apiserver.conf"

  # Use VRID set in configuration or use default values (11 for ApiServer, 12 for Ingress)
  KEEPALIVED_VIRTUAL_ROUTER_ID="${APISERVER_VRID:-11}"

  # We create a specific password (max length 8) for this Keepalived cluster, with the following format:
  # <prefix><hash>  where <prefix> can be AS for ApiServer or IN for Ingress and <hash> is a simple checksum
  # of the Kumori cluster name (truncated to 6 chars).
  KEEPALIVED_PASSWORD="AS$(echo ${RELEASE_NAME} | cksum | cut -f 1 -d ' ' | tail -c 6)"

  MAX_PRIORITY=255
  NODE_PRIORITY="$((MAX_PRIORITY-POSITION))"
  KEEPALIVED_ROLE="BACKUP"
  KEEPALIVED_PRIORITY="${NODE_PRIORITY}"
  if [[ "${NODE_PRIORITY}" == "255" ]]; then
    KEEPALIVED_ROLE="MASTER"
  fi
  sed \
    -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" \
    -e "s?{{INITIAL_ROLE}}?${KEEPALIVED_ROLE}?g" \
    -e "s?{{NETWORK_INTERFACE}}?${DEFAULT_NETWORK_INTERFACE}?g" \
    -e "s?{{VIRTUAL_ROUTER_ID}}?${KEEPALIVED_VIRTUAL_ROUTER_ID}?g" \
    -e "s?{{PRIORITY}}?${KEEPALIVED_PRIORITY}?g" \
    -e "s?{{PASSWORD}}?${KEEPALIVED_PASSWORD}?g" \
    -e "s?{{APISERVER_VIRTUAL_IP}}?${APISERVER_VIRTUAL_IP}?g" \
    -e "s?{{NODE_IP}}?${NODE_IP}?g" \
    ${KEEPALIVED_CONF_FILE_TEMPLATE_FILE} \
    > ${KEEPALIVED_CONF_FILE}

  # Add a VRRP peer for each master node (except itself)
  EMPTY_LIST="true"
  for MASTER_IP in ${MASTERS_IPS[@]}; do
    echo "IP: ${MASTER_IP}"
    if [[ "${MASTER_IP}" == "${NODE_IP}" ]]; then
      echo "Skipping node (self)"
    else
      EMPTY_LIST="false"
      # Duplicate the peer template line and replace the IP in one of the copies
      sed -i -e "s/^\(\s*\){{PEER_NODE_IP}}\s*$/\1${MASTER_IP}\n&/g" ${KEEPALIVED_CONF_FILE}
    fi
  done

  # If peer list is empty, add 127.0.0.1 (localhost) to avoid Keepalived falling back to multicast
  if [[ "${EMPTY_LIST}" == "true" ]]; then
    DEFAULT_PEER_IP="127.0.0.1"
    sed -i -e "s/^\(\s*\){{PEER_NODE_IP}}\s*$/\1${DEFAULT_PEER_IP}\n&/g" ${KEEPALIVED_CONF_FILE}
  fi

  # Remove peer template line
  sed -i -e "/^.*{{PEER_NODE_IP}}.*$/d" ${KEEPALIVED_CONF_FILE}

  echo
  echo "Applying Keepalived configuration:"
  echo
  cat ${KEEPALIVED_CONF_FILE}
  echo

  # Prepare check and notify scripts
  sed \
    -e "s?{{APISERVER_VIRTUAL_IP}}?${APISERVER_VIRTUAL_IP}?g" \
    -e "s?{{APISERVER_EXTERNAL_PORT}}?${APISERVER_EXTERNAL_PORT}?g" \
    -e "s?{{APISERVER_INTERNAL_PORT}}?${APISERVER_INTERNAL_PORT}?g" \
    -e "s?{{APISERVER_INTERNAL_BALANCING}}?${APISERVER_INTERNAL_BALANCING}?g" \
    -e "s?{{NODE_IP}}?${NODE_IP}?g" \
    ${SCRIPT_DIR}/provision/keepalived/check_apiserver.sh.template \
    > ${SCRIPT_DIR}/provision/keepalived/check_apiserver.sh

  # Prepare default custom notify scripts
  CUSTOM_KEEPALIVED_SCRIPTS_DIR="/etc/keepalived/custom-notify-scripts"
  CUSTOM_KEEPALIVED_STATE_EVENTS_SCRIPT="${CUSTOM_KEEPALIVED_SCRIPTS_DIR}/custom_state_events_handler.sh"
  sudo mkdir -p ${CUSTOM_KEEPALIVED_SCRIPTS_DIR}
  sudo cp ${SCRIPT_DIR}/provision/keepalived/custom_state_events_handler.sh ${CUSTOM_KEEPALIVED_STATE_EVENTS_SCRIPT}

  sed \
    -e "s?{{CUSTOM_KEEPALIVED_STATE_EVENTS_SCRIPT}}?${CUSTOM_KEEPALIVED_STATE_EVENTS_SCRIPT}?g" \
    ${SCRIPT_DIR}/provision/keepalived/notify_fifo.sh.template \
    > ${SCRIPT_DIR}/provision/keepalived/notify_fifo.sh

  sudo mv ${SCRIPT_DIR}/provision/keepalived/check_apiserver.sh /etc/keepalived/check_apiserver_real.sh
  sudo mv ${SCRIPT_DIR}/provision/keepalived/check_force_failure.sh /etc/keepalived/check_force_failure.sh
  sudo mv ${SCRIPT_DIR}/provision/keepalived/notify_fifo.sh /etc/keepalived/notify_fifo.sh
  sudo mv ${KEEPALIVED_CONF_FILE} /etc/keepalived/keepalived.conf

  # Keepalived is strict about script security. Only root user must have acces to the script
  sudo chown -R root:root /etc/keepalived/*
  sudo chmod 700 /etc/keepalived/check_apiserver_real.sh
  sudo chmod 700 /etc/keepalived/check_force_failure.sh
  sudo chmod 700 /etc/keepalived/notify_fifo.sh
  sudo chmod 700 /etc/keepalived/custom-notify-scripts/custom_state_events_handler.sh

  # Initially set a health-check script that forces failure (for Keepalived to not start properly
  # until the script is replaced by the real one).
  sudo cp /etc/keepalived/check_force_failure.sh /etc/keepalived/check_apiserver.sh

  # Configure Keepalived service to automatically restart on failures
  sudo sed -i '/\[Service\]/a RestartSec=5s' /lib/systemd/system/keepalived.service
  sudo sed -i '/\[Service\]/a Restart=on-failure' /lib/systemd/system/keepalived.service
  sudo systemctl daemon-reload
  sudo systemctl restart keepalived

  # Add a networkd-dispatcher to force failover when network is restarted
  sed -e "s?{{NETWORK_INTERFACE}}?${DEFAULT_NETWORK_INTERFACE}?g" \
    ${SCRIPT_DIR}/provision/keepalived/10-kumori-keepalived-restart.template \
    > ${SCRIPT_DIR}/provision/keepalived/10-kumori-keepalived-restart

  sudo mkdir -p /usr/lib/networkd-dispatcher/routable.d
  sudo mv ${SCRIPT_DIR}/provision/keepalived/10-kumori-keepalived-restart \
    /usr/lib/networkd-dispatcher/routable.d/10-kumori-keepalived-restart
  sudo chmod 755 /usr/lib/networkd-dispatcher/routable.d/10-kumori-keepalived-restart
  sudo chown root:root /usr/lib/networkd-dispatcher/routable.d/10-kumori-keepalived-restart
fi


################################################################################
##  INSTALL AND CONFIGURE ENVOY FOR APISERVER (only for masters)              ##
################################################################################
if [ "${APISERVER_INTERNAL_BALANCING}" = "true" ]; then
  echo
  echo -e $COL_BLUE"Installing and configuring ApiServer Envoy proxy..."$COL_DEFAULT
  echo

  # TEMPORARILY DISABLED DUE TO BINTRAY SHUTDOWN (Envoy issue #17236)
  # curl -sL 'https://getenvoy.io/gpg' | sudo apt-key add -
  # sudo add-apt-repository "deb [arch=amd64] https://dl.bintray.com/tetrate/getenvoy-deb $(lsb_release -cs) stable"
  # sudo apt-get install -y getenvoy-envoy=${ENVOY_VERSION}

  # ALTERNATIVE INSTALLATION METHOD
  wget https://archive.tetratelabs.io/envoy/download/v${ENVOY_VERSION}/envoy-v${ENVOY_VERSION}-linux-amd64.tar.xz
  tar -xf envoy-v${ENVOY_VERSION}-linux-amd64.tar.xz
  chmod +x envoy-v${ENVOY_VERSION}-linux-amd64/bin/envoy
  sudo mv envoy-v${ENVOY_VERSION}-linux-amd64/bin/envoy /usr/bin/envoy
  rm -rf envoy-v${ENVOY_VERSION}-linux-amd64.tar.xz envoy-v${ENVOY_VERSION}-linux-amd64

  # Prepare Envoy configuration file
  HOSTNAME="$(hostname)"
  sed \
    -e "s?{{APISERVER_EXTERNAL_PORT}}?${APISERVER_EXTERNAL_PORT}?g" \
    -e "s?{{HOSTNAME}}?${HOSTNAME}?g" \
    -e "s?{{CLUSTER_NAME}}?${RELEASE_NAME}?g" \
    -e "s?{{NODE_IP}}?${NODE_IP}?g" \
    ${SCRIPT_DIR}/provision/envoy/envoy-config-masters.yaml.template \
    > ${SCRIPT_DIR}/provision/envoy/envoy-config.yaml


  # Prepare Envoy EDS (Endpoint Discovery Service) configuration file
  echo
  echo "Preparing Envoy EDS file..."
  echo

  EDS_CONF_TEMPLATE_FILE="${SCRIPT_DIR}/provision/envoy/eds.conf.template"
  EDS_CONF_TMP_FILE="${SCRIPT_DIR}/provision/envoy/eds.conf.tmp"
  EDS_CONF_FILE="${SCRIPT_DIR}/provision/envoy/eds.conf"

  # Create a base configuration file based on the EDS config template
  cp ${EDS_CONF_TEMPLATE_FILE} ${EDS_CONF_FILE}

  # Add a new cluster for ApiServer cluster configuration
  cat ${EDS_CONF_FILE} \
    | jq ".resources += [ {\"@type\":\"type.googleapis.com/envoy.config.endpoint.v3.ClusterLoadAssignment\",cluster_name:\"apiservers\",endpoints:[{lb_endpoints:[]}]} ]" \
    > ${EDS_CONF_TMP_FILE}
  mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}

  # Create an array with all endpoint IPS (Primary Master + Secondary Masters)
  ENDPOINTS=()
  for MASTER_IP in ${MASTERS_IPS[@]}; do
      ENDPOINTS+=(${MASTER_IP})
  done

  # For each IP add a new "endpoint" element to the configuration (JSON).
  # An endpoint has the following structure:
  #   {
  #     "endpoint": {
  #       "address": {
  #         "socket_address": {
  #           "address": "10.0.1.237",
  #           "port_value": 6443
  #         }
  #       }
  #     }
  #   }
  for ENDPOINT_IP in ${ENDPOINTS[@]}; do
    echo "IP: ${ENDPOINT_IP}"
    cat ${EDS_CONF_FILE} \
      | jq ".resources[0].endpoints[0].lb_endpoints += [ { endpoint: { address: { socket_address: { address: \"${ENDPOINT_IP}\", port_value: ${APISERVER_INTERNAL_PORT} } } } } ]"  \
      > ${EDS_CONF_TMP_FILE}
    mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}
  done

  echo
  echo "Envoy EDS file ready: "
  echo
  cat ${EDS_CONF_FILE}
  echo

  # Place Envoy configuration files in /etc/envoy
  sudo mkdir -p /etc/envoy
  sudo mv ${SCRIPT_DIR}/provision/envoy/envoy-config.yaml /etc/envoy/envoy-kumori-config.yaml
  sudo mv ${SCRIPT_DIR}/provision/envoy/eds.conf /etc/envoy/eds.conf

  # Create a service for automatically manage Envoy life-cycle
  sudo cp ${SCRIPT_DIR}/provision/envoy/envoy.service /etc/systemd/system/envoy.service
  sudo systemctl daemon-reload
  sudo systemctl start envoy
  sudo systemctl enable envoy
fi


################################################################################
##  CONFIGURE LOGROTATE FOR KUMORI LOGS                                       ##
################################################################################
echo
echo -e $COL_BLUE"Configuring logrotate for Kumori logs..."$COL_DEFAULT
echo

sudo cp ${SCRIPT_DIR}/provision/logrotate/kumori-masters /etc/logrotate.d/kumori-masters


################################################################################
##  CONFIGURE DNS CACHING                                                     ##
################################################################################
echo
echo -e $COL_BLUE"Configuring resolved cache..."$COL_DEFAULT
echo

# Configure DNS resolution for not caching negative operations (errors)
echo "Cache=no-negative" | sudo tee -a /etc/systemd/resolved.conf 2>&1>/dev/null
# Restart systemd-resolved service
sudo service systemd-resolved restart


################################################################################
##  CONFIGURE CUSTOM DNS RESOLVERS                                            ##
################################################################################
if [ -n "${DNS_RESOLVERS}" ]; then
  echo "Trying to set default DNS resolvers (${DNS_RESOLVERS})..."
  # echo "supersede domain-name-servers ${DNS_RESOLVERS};" | sudo tee -a /etc/dhcp/dhclient.conf

  # Detect what Netplan file is used:
  # - /etc/netplan/50-cloud-init.yaml  (works in Openstack)
  # - /etc/netplan/01-netcfg.yaml  (works in Virtualbox)
  if [ -f "/etc/netplan/50-cloud-init.yaml" ]; then
    NETPLAN_CONFIG_FILE="/etc/netplan/50-cloud-init.yaml"
  elif [ -f "/etc/netplan/01-netcfg.yaml" ]; then
    NETPLAN_CONFIG_FILE="/etc/netplan/01-netcfg.yaml"
  else
    NETPLAN_CONFIG_FILE=""
  fi

  if [ -n "${NETPLAN_CONFIG_FILE}" ] && [ -f "${NETPLAN_CONFIG_FILE}" ]; then
    echo "Found ${NETPLAN_CONFIG_FILE} file, patching it..."

    # Backup original file for restoring it on cluster destruction
    NETPLAN_CONFIG_BACKUP="${SCRIPT_DIR}/etc-netplan-50-cloud-init_BACKUP.yaml"
    sudo cp "${NETPLAN_CONFIG_FILE}" "${NETPLAN_CONFIG_BACKUP}"

    # Prepare DNS server list in the correct format ("xx","yy","zz")
    JOINED_DNS=$(printf ",\"%s\"" "${DNS_RESOLVERS[@]}")
    JOINED_DNS=${JOINED_DNS:1}  # remove comma from the beginning
    echo "DNS SERVERS:"${JOINED_DNS}

    # Add nameservers to every ethernet interface found
    SRC_YAML=$(sudo cat "${NETPLAN_CONFIG_FILE}")
    echo "Processing netplan configuration to add nameservers..."
    echo
    echo "Source:"
    echo
    echo "${SRC_YAML}"

    echo
    echo "Patched:"
    echo
    echo "${SRC_YAML}" \
      | yq r -j - \
      | jq ".network.ethernets[] += {nameservers: { addresses: [ ${JOINED_DNS} ] } }"  \
      | yq r --prettyPrint - \
      | sudo tee ${NETPLAN_CONFIG_FILE}
    echo
    echo "Applying netplan changes..."
    echo
    sudo netplan apply
  fi
fi


################################################################################
##  CONFIGURE CRON JOB FOR PERIODICALLY CHECKING/RESTARTING KUBELET           ##
################################################################################
# Add a cron job for periodically checking for a specific Kubelet error log
# trace and restarting Kubelet if it is found
KUBELET_CRONJOB_FILE="${SCRIPT_DIR}/provision/cronjobs/kumori-kubelet-bug-restart"
KUBELET_SCRIPT_FILE="${SCRIPT_DIR}/provision/cronjobs/kumori-kubelet-bug-restart.sh"

sudo chown root:root ${KUBELET_CRONJOB_FILE} ${KUBELET_SCRIPT_FILE}
sudo chmod 644 ${KUBELET_CRONJOB_FILE}
sudo chmod 755 ${KUBELET_SCRIPT_FILE}
sudo mv ${KUBELET_CRONJOB_FILE} /etc/cron.d/kumori-kubelet-bug-restart
sudo mv ${KUBELET_SCRIPT_FILE} /usr/local/bin/kumori-kubelet-bug-restart.sh


################################################################################
##  CUSTOM  - ADD TRUSTED CERTIFICATE AUTHORITIES                             ##
################################################################################
if [ -d "${SCRIPT_DIR}/custom/trusted-ca" ]; then
  crtCount=$(ls -1 ${SCRIPT_DIR}/custom/trusted-ca/*.crt 2>/dev/null | wc -l)
  if [ $crtCount != 0 ]; then
    echo
    echo -e $COL_BLUE"Installing custom trusted CAs..."$COL_DEFAULT
    echo
    CERTSPATH="/usr/local/share/ca-certificates"
    for file in ${SCRIPT_DIR}/custom/trusted-ca/*.crt; do
      echo "- Adding ${file##*/} to ${CERTSPATH}"
      sudo mv "${SCRIPT_DIR}/custom/trusted-ca/${file##*/}" "${CERTSPATH}"
    done
    sudo /usr/sbin/update-ca-certificates
  fi
fi


################################################################################
##  DISABLE TX OFFLOADING IN NODE IF NECESSARY                                ##
################################################################################
if [[ " ${TX_OFF_NODES_IPS[@]} " =~ " ${NODE_IP} " ]]; then
  echo
  echo -e $COL_BLUE"Disabling Transmission checksumming offloaing (TX Offload)..."$COL_DEFAULT
  echo
  # Add a Networkd-Dispatcher file to be run whenever network comes up (routable)
  TXOFF_TEMPLATE_FILE="${SCRIPT_DIR}/provision/tx-offloading/20-disable-tx-offloading.template"
  TXOFF_FILE="${SCRIPT_DIR}/provision/tx-offloading/20-disable-tx-offloading"

  # Find out default network interface
  DEFAULT_NETWORK_INTERFACE=$(route | grep '^default' | grep -o '[^ ]*$')

  sed \
    -e "s?{{NETWORK_INTERFACE}}?${DEFAULT_NETWORK_INTERFACE}?g" \
    ${TXOFF_TEMPLATE_FILE} \
    > ${TXOFF_FILE}

  sudo mv "${TXOFF_FILE}" /usr/lib/networkd-dispatcher/routable.d/20-kumori-disable-tx-offloading
  sudo chown root:root /usr/lib/networkd-dispatcher/routable.d/20-kumori-disable-tx-offloading
  sudo chmod 755 /usr/lib/networkd-dispatcher/routable.d/20-kumori-disable-tx-offloading
fi


################################################################################
##  INSTALL KUMORI OPENEBS MAYACTL WRAPPER AND KUBECTL OPENEBS PLUGIN         ##
################################################################################
# Only install if OpenEBS Storage is configured (although it won't interfere)
if [ "${INSTALL_OPENEBS_PROVIDER}" = "true" ]; then
  if [ -f "${SCRIPT_DIR}/provision/openebs/kmayactl" ]; then
    echo
    echo -e $COL_BLUE"Installing OpenEBS mayactl wrapper utility..."$COL_DEFAULT
    echo

    sudo cp ${SCRIPT_DIR}/provision/openebs/kmayactl /usr/local/bin/
    sudo chmod +x /usr/local/bin/kmayactl

    echo
    echo -e $COL_BLUE"Installing Kubectl OpenEBS plugin (via Krew)..."$COL_DEFAULT
    echo

    # If Krew version is not set, default to 0.4.2
    [ -z "${KREW_VERSION}" ] && KREW_VERSION="0.4.2"

    # Install Krew plugin installer: this is the official installation method for Ubuntu
    (
      set -x; cd "$(mktemp -d)" &&
      OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
      ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
      curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/download/v${KREW_VERSION}/krew-linux_amd64.tar.gz" &&
      tar zxvf krew-linux_amd64.tar.gz &&
      KREW=./krew-"${OS}_${ARCH}" &&
      "$KREW" install krew &&
      TMP_DIR="$(pwd)" &&
      cd - ; rm -rf ${TMP_DIR}
    )
    # Add Krew to PATH (necesary)
    echo '' >> ${HOME}/.bashrc
    echo '# Krew binary (Kubectl plugin installer)' >> ${HOME}/.bashrc
    echo 'export PATH="${PATH}:${HOME}/.krew/bin"' >> ${HOME}/.bashrc

    # Set path for installing plugin now
    export PATH="${PATH}:${HOME}/.krew/bin"

    # Install OpenEBS plugin
    kubectl krew install openebs

  fi
fi


# Small pause in case there is a shutdown afterwards
sleep 10s
