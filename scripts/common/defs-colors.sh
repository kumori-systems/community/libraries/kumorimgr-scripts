#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

# Colors for logging
COL_DEFAULT='\e[0;00m'
COL_BLUE='\e[0;96m'
COL_GREEN='\e[0;92m'
COL_RED='\e[0;91m'
COL_YELLOW='\e[0;93m'
COL_RED_BOLD='\e[1;91m'
COL_YELLOW_BOLD='\e[1;93m'

BACKGROUND_COLORS=(
  '\e[44m'    #  Blue
  '\e[45m'    #  Magenta
  '\e[42m'    #  Green
  '\e[43m'    #  Yellow
  '\e[46m'    #  Cyan
  '\e[101m'   #  Lightred
  '\e[102m'   #  Lightgreen
  '\e[103m'   #  Lightyellow
  '\e[104m'   #  Lightblue
  '\e[105m'   #  Lightmagenta
  '\e[106m'   #  Lightcyan
  '\e[47mL'   #  Lightgray
  '\e[100m'   #  Darkgray
  '\e[107m'   #  White
  '\e[40m'    #  Black
  '\e[41m'    #  Red
)

COL_BCK_BLUE='\e[44m'
COL_BCK_GREEN='\e[42m'
COL_BCK_MAGENTA='\e[45m'
