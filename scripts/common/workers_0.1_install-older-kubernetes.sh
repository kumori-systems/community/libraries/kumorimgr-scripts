#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################


# After Kubernetes old APT repositories deprecation, older versions (currently
# versions < v.1.24) must be installed manually.
#
# The steps for manually installing Kubernetes are:
#
# - Add a newer Kubernetes APT repository to be able to install some
#   dependencies that are still distributed as packages.
# - Install dependencies:
#   - conntrack v1:1.4.5-2
#   - cri-tools v1.26.0-00
#   - kubernetes-cni v1.2.0-00
#   - ebtables v2.0.11-3build1
# - Download Kubernetes v1.21.10 release node binaries tgz
# - Extract kubeadm, kubectl and kubelet binaries to /usr/bin
# - Configure Kubelet systemd service
# - Remove the Kubernetes APT repository

# This version is used for adding its APT repository to install dependencies
KUBERNETES_APT_REPO_DEFAULT_VERSION="1.25"

if [ "${KUBERNETES_MAIN_VERSION}" = "1.16" ]; then
  # IMPORTANT NOTE: THIS WILL BE FOR Ubuntu 18.04

  CONNTRACK_PKG_VERSION="1:1.4.4*"       # 1:1.4.4+snapshot20161117-6ubuntu2"
  CRI_TOOLS_PKG_VERSION="1.24.2-*"       # 1.24.2-00
  KUBERNETES_CNI_PKG_VERSION="0.8.7-*"   # 0.8.7-00

  # Systemd system units directory (chenges between Ubuntu versions)
  SYSTEMD_SYSTEM_UNITS_DIR="/lib/systemd/system"

elif [ "${KUBERNETES_MAIN_VERSION}" = "1.21" ]; then

  CONNTRACK_PKG_VERSION="1:1.4.5-*"        # 1:1.4.5-2
  EBTABLES_PKG_VERSION="2.0.11-3build1"
  CRI_TOOLS_PKG_VERSION="1.26.0-*"         # 1.26.0-00
  KUBERNETES_CNI_PKG_VERSION="1.2.0-*"     # 1.2.0-00

  # Systemd system units directory (chenges between Ubuntu versions)
  SYSTEMD_SYSTEM_UNITS_DIR="/usr/lib/systemd/system"

else
  echo "Kubernetes v${KUBERNETES_MAIN_VERSION} is not supported for older installation method."
  return
fi



echo
echo -e $COL_YELLOW"WARNING:"$COL_DEFAULT" Kubernetes v${KUBERNETES_MAIN_VERSION} does not support APT installation."
echo "Using alternative installation method."
echo

# Add Kubernetes newer APT repository (v1.25) to install kubernetes-cni and
# cri-tools dependencies with APT
echo "Configuring newer APT repository (Kubernetes v${KUBERNETES_APT_REPO_DEFAULT_VERSION})..."
echo

# Get the GPG key
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://pkgs.k8s.io/core:/stable:/v${KUBERNETES_APT_REPO_DEFAULT_VERSION}/deb/Release.key \
  | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

# Add an APT source
# Task #1278 "Problem with GPG key when a KCluster (Kube v1.25) is created"):
# The GPG key is expired, so it is necessary to explicitly indicate that it is trusted.
# echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v${KUBERNETES_APT_REPO_DEFAULT_VERSION}/deb/ /" \
#   | sudo tee /etc/apt/sources.list.d/kubernetes.list
echo "deb [trusted=true] https://pkgs.k8s.io/core:/stable:/v${KUBERNETES_APT_REPO_DEFAULT_VERSION}/deb/ /" \
  | sudo tee /etc/apt/sources.list.d/kubernetes.list

# Update APT packages
sudo apt-get -y update

echo
echo "Installing Kubernetes dependencies..."
echo

# Install the Kubernetes dependencies packages
if [ -n "${EBTABLES_PKG_VERSION}" ]; then
  sudo apt-get install -y ebtables=${EBTABLES_PKG_VERSION}
fi
sudo apt-get install -y \
  kubernetes-cni=${KUBERNETES_CNI_PKG_VERSION} \
  cri-tools=${CRI_TOOLS_PKG_VERSION} \
  conntrack=${CONNTRACK_PKG_VERSION} \
  socat

# Download Kubernetes v1.21.10 binaries and extract kubeadm, kubectl and kubelet
# to /usr/bin
echo
echo "Downloading Kubernetes binaries..."
echo

mkdir tmp
cd tmp
wget https://dl.k8s.io/v${KUBERNETES_VERSION}/kubernetes-node-linux-amd64.tar.gz
tar xzf kubernetes-node-linux-amd64.tar.gz
chmod +x kubernetes/node/bin/*
sudo mv kubernetes/node/bin/kubeadm /usr/bin/
sudo mv kubernetes/node/bin/kubectl /usr/bin/
sudo mv kubernetes/node/bin/kubelet /usr/bin/
cd ..
rm -rf tmp

# Configure Kubelet systemd service
echo
echo "Configuring Kubelet Systemd service..."
echo

KUBELET_SERVICE_CONF_FILE="10-kubeadm.conf"
KUBELET_SERVICE_UNIT_FILE="kubelet.service"

# IMPORTANT: Note that EOF is quoted ('EOF') to prevent bash variables to be
# expanded.
cat << 'EOF' > "${KUBELET_SERVICE_CONF_FILE}"
[Service]
Environment="KUBELET_KUBECONFIG_ARGS=--bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf --kubeconfig=/etc/kubernetes/kubelet.conf"
Environment="KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml"
# This is a file that "kubeadm init" and "kubeadm join" generates at runtime, populating the KUBELET_KUBEADM_ARGS variable dynamically
EnvironmentFile=-/var/lib/kubelet/kubeadm-flags.env
# This is a file that the user can use for overrides of the kubelet args as a last resort. Preferably, the user should use
# the .NodeRegistration.KubeletExtraArgs object in the configuration files instead. KUBELET_EXTRA_ARGS should be sourced from this file.
EnvironmentFile=-/etc/default/kubelet
ExecStart=
ExecStart=/usr/bin/kubelet $KUBELET_KUBECONFIG_ARGS $KUBELET_CONFIG_ARGS $KUBELET_KUBEADM_ARGS $KUBELET_EXTRA_ARGS
EOF

cat <<EOF > "${KUBELET_SERVICE_UNIT_FILE}"
[Unit]
Description=kubelet: The Kubernetes Node Agent
Documentation=https://kubernetes.io/docs/home/
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=/usr/bin/kubelet
Restart=always
StartLimitInterval=0
RestartSec=10

[Install]
WantedBy=multi-user.target
EOF

sudo mkdir -p /etc/systemd/system/kubelet.service.d
sudo mv 10-kubeadm.conf /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
sudo mv kubelet.service ${SYSTEMD_SYSTEM_UNITS_DIR}/kubelet.service

# Enable Kubelet service (nut do not start it)
sudo systemctl enable kubelet

# Cleanup: removed the Kubernetes APT repository
echo
echo "Removing Kubernetes APT repository..."
echo
sudo rm -f /etc/apt/keyrings/kubernetes-apt-keyring.gpg
sudo rm -f /etc/apt/sources.list.d/kubernetes.list
sudo apt update
