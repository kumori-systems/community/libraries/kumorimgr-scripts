#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"

################################################################################
##  ON PRIMARY MASTER NODE - UPDATE BALANCERS SERVICEMONITORS ENDPOINTS       ##
################################################################################

# Prepare internal balancers ServiceMonitors (ApiServer and Ingress)
# These require a manuallist of endpoints (Node IPs)
INTERNAL_BALANCER_SERVICEMONITOR_TEMPLATE="${SCRIPT_DIR}/provision/envoy-internal-balancer-servicemonitor.yaml_template"

if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  echo
  echo -e $COL_BLUE"Updating Ingress load-balancer ServiceMonitor endpoints..."$COL_DEFAULT
  echo

  INGRESS_BALANCER_SERVICEMONITOR="${SCRIPT_DIR}/provision/ingress-internal-balancer-servicemonitor.yaml"

  sed -e "s?{{INTERNAL_BALANCER_NAME}}?ingress?g" \
    ${INTERNAL_BALANCER_SERVICEMONITOR_TEMPLATE} \
    > ${INGRESS_BALANCER_SERVICEMONITOR}

  EMPTY_LIST="true"
  for INGRESS_NODE_IP in ${INGRESS_NODES_IPS[@]}; do
    echo "IP: ${INGRESS_NODE_IP}"
    EMPTY_LIST="false"
    # Duplicate the NODE_ADDRESS_ITEM template line and replace the IP in one of the copies
    sed -i -e "s/^\(\s*\)- {{NODE_ADDRESS_ITEM}}\s*$/\1- ip: ${INGRESS_NODE_IP}\n&/g" ${INGRESS_BALANCER_SERVICEMONITOR}
  done

  # If address list is empty, set it to []
  if [[ "${EMPTY_LIST}" == "true" ]]; then
    sed -i -e "s/^\(\s*\)- addresses:\s*$/- addresses: []/g" ${INGRESS_BALANCER_SERVICEMONITOR}
  fi

  # Remove NODE_ADDRESS_ITEM  template line, if any left
  sed -i -e "/^.*{{NODE_ADDRESS_ITEM}}.*$/d" ${INGRESS_BALANCER_SERVICEMONITOR}

  echo
  echo "NEW INGRESS SERVICEMONITOR CONFIG:"
  cat ${INGRESS_BALANCER_SERVICEMONITOR}
  echo
  # Update ServiceMonitor
  kubectl apply -f ${INGRESS_BALANCER_SERVICEMONITOR}

  echo
  echo -e $COL_BLUE"Done."$COL_DEFAULT
  echo
fi


if [ "${APISERVER_INTERNAL_BALANCING}" = "true" ] ; then
  echo
  echo -e $COL_BLUE"Updating ApiServer load-balancer ServiceMonitor endpoints..."$COL_DEFAULT
  echo
  APISERVER_BALANCER_SERVICEMONITOR="${SCRIPT_DIR}/provision/apiserver-internal-balancer-servicemonitor.yaml"

  sed -e "s?{{INTERNAL_BALANCER_NAME}}?apiserver?g" \
    ${INTERNAL_BALANCER_SERVICEMONITOR_TEMPLATE} \
    > ${APISERVER_BALANCER_SERVICEMONITOR}

  EMPTY_LIST="true"
  for MASTER_NODE_IP in ${MASTERS_IPS[@]}; do
    echo "IP: ${MASTER_NODE_IP}"
    EMPTY_LIST="false"
    # Duplicate the NODE_ADDRESS_ITEM template line and replace the IP in one of the copies
    sed -i -e "s/^\(\s*\)- {{NODE_ADDRESS_ITEM}}\s*$/\1- ip: ${MASTER_NODE_IP}\n&/g" ${APISERVER_BALANCER_SERVICEMONITOR}
  done

  # If address list is empty, set it to []
  if [[ "${EMPTY_LIST}" == "true" ]]; then
    sed -i -e "s/^\(\s*\)- addresses:\s*$/- addresses: []/g" ${APISERVER_BALANCER_SERVICEMONITOR}
  fi

  # Remove NODE_ADDRESS_ITEM  template line, if any left
  sed -i -e "/^.*{{NODE_ADDRESS_ITEM}}.*$/d" ${APISERVER_BALANCER_SERVICEMONITOR}

  echo
  echo "NEW APISERVER SERVICEMONITOR CONFIG:"
  cat ${APISERVER_BALANCER_SERVICEMONITOR}
  echo
  # Update ServiceMonitor
  kubectl apply -f ${APISERVER_BALANCER_SERVICEMONITOR}

  echo
  echo -e $COL_BLUE"Done."$COL_DEFAULT
  echo
fi

