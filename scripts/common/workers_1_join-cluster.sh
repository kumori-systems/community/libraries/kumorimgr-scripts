#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"


################################################################################
## ON NON PRIMARY MASTER NODES - CONFIGURE DOCKER CREDENTIALS                 ##
################################################################################
# If Docker credentials are provided for DockerHub or Kumori images registry
# configure them at node level. Create a docker credentials file with all ncessary
# credentials.
DOCKER_CREDENTIALS_BASE_FILE="$(pwd)/provision/docker-credentials/docker-credentials-base.json"
DOCKER_CREDENTIALS_FILE="$(pwd)/provision/docker-credentials/docker-credentials.json"
DOCKER_CREDENTIALS_FILE_TMP="$(pwd)/provision/docker-credentials/docker-credentials_TMP.json"

# Create a "blank" docker credentials file
cp "${DOCKER_CREDENTIALS_BASE_FILE}" "${DOCKER_CREDENTIALS_FILE}"

# DockerHub credentials
if [ -n "${DOCKERHUB_USERNAME}" ] && [ -n "${DOCKERHUB_PASSWORD}" ]; then
  echo
  echo "Adding DockerHub credentials..."
  echo
  # Base64-encode username and password
  ENCODED_AUTH=$(echo -n "${DOCKERHUB_USERNAME}:${DOCKERHUB_PASSWORD}" | base64)

  cat "${DOCKER_CREDENTIALS_FILE}" \
    | jq ".auths += { \"https://index.docker.io/v1/\": { auth: \"${ENCODED_AUTH}\" } }" \
    > ${DOCKER_CREDENTIALS_FILE_TMP}

  cp ${DOCKER_CREDENTIALS_FILE_TMP} ${DOCKER_CREDENTIALS_FILE}
fi

# Copy credentials file to home directory
mkdir -p ${HOME}/.docker
cp "${DOCKER_CREDENTIALS_FILE}" ${HOME}/.docker/config.json

# Copy credentials file to root user home directory
sudo mkdir -p /root/.docker
sudo cp "${DOCKER_CREDENTIALS_FILE}" /root/.docker/config.json

# Copy credentials file to Kubelet path
sudo mkdir -p /var/lib/kubelet
sudo cp "${DOCKER_CREDENTIALS_FILE}" /var/lib/kubelet/config.json



################################################################################
##  ON WORKER NODES - Ejecutar join                                           ##
################################################################################

if [ "${PRELOAD_DOCKER_IMAGES}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Preloading docker images..."$COL_DEFAULT
  echo
  ${SCRIPT_DIR}/preload-docker-images-worker.sh
  echo
  docker images
  echo
  echo
  echo -e $COL_BLUE"* Finished loading docker images"$COL_DEFAULT
  echo
fi



echo
echo -e $COL_BLUE"Joining worker node to cluster..."$COL_DEFAULT
echo

# Prepare join

# Somehow masters join script has been uploaded to /tmp
# Extract join data from script
echo "Extracting join data from join script..."
echo
TOKEN=""
CA_CERT_HASH=""

# Create an array of "words" from the script contents
ARR=($(cat /tmp/join-workers.sh))

# Iterate the array by index and assign the interesting values
for i in "${!ARR[@]}"; do
  # echo "WORD $i: \"${ARR[$i]}\""
  if [[ "${ARR[$i]}" == "--token" ]]; then
    TOKEN="${ARR[$i+1]}"
  elif [[ "${ARR[$i]}" == "--discovery-token-ca-cert-hash" ]]; then
    CA_CERT_HASH="${ARR[$i+1]}"
  fi
done

echo "Exctracted join data:"
echo "  - TOKEN: ${TOKEN}"
echo "  - CA_CERT_HASH: ${CA_CERT_HASH}"

# Find out if node will be an Ingress endpoint
IS_INGRESS_NODE="false"
if [[ " ${INGRESS_NODES_IPS[@]} " =~ " ${NODE_IP} " ]]; then
  IS_INGRESS_NODE="true"
fi

# Find out if node will be a Storage node
IS_STORAGE_NODE="false"
if [[ " ${STORAGE_NODES_IPS[@]} " =~ " ${NODE_IP} " ]]; then
  IS_STORAGE_NODE="true"
fi

# Find out the worker zone
NODE_ZONE=$( hostname )
for i in "${!WORKERS_IPS[@]}"; do
  if [[ "${WORKERS_IPS[$i]}" == "$NODE_IP" ]]; then
    if [[ "${WORKERS_ZONES[$i]}" != "" ]]; then
      NODE_ZONE="${WORKERS_ZONES[$i]}"
    fi
    break
  fi
done

# Put the obtained values in join configuration file
KUBEADM_JOIN_CONFIG_FILE_WORKERS="$(pwd)/provision/kubeadm-config/kubeadm-join-config-workers.yaml"
sed -i \
  -e "s?{{IP}}?${NODE_IP}?g" \
  -e "s?{{ZONE}}?${NODE_ZONE}?g" \
  -e "s?{{APISERVER_URL}}?${APISERVER_URL}?g" \
  -e "s?{{TOKEN}}?${TOKEN}?g" \
  -e "s?{{CA_CERT_HASH}}?${CA_CERT_HASH}?g" \
  -e "s?{{IS_INGRESS}}?${IS_INGRESS_NODE}?g" \
  -e "s?{{IS_STORAGE}}?${IS_STORAGE_NODE}?g" \
  "${KUBEADM_JOIN_CONFIG_FILE_WORKERS}"

# Print Kubeadm init configuration file
echo
echo "Calling Kubeadm join with config file ${KUBEADM_JOIN_CONFIG_FILE_WORKERS}:"
echo
cat "${KUBEADM_JOIN_CONFIG_FILE_WORKERS}"
echo

n=0
until [ $n -ge 5 ]
do
  sudo kubeadm join --config ${KUBEADM_JOIN_CONFIG_FILE_WORKERS} && break
  echo
  echo -e $COL_YELLOW"[WARN] Join command failed. Resetting and retrying in 10s..."$COL_DEFAULT
  echo
  sudo kubeadm reset --force --cri-socket unix:///var/run/cri-dockerd.sock --ignore-preflight-errors all
  echo
  n=$[$n+1]
  sleep 10
  # Copy credentials file to Kubelet path again (kubeadm reset deletes them!)
  sudo mkdir -p /var/lib/kubelet
  sudo cp "${DOCKER_CREDENTIALS_FILE}" /var/lib/kubelet/config.json
done
