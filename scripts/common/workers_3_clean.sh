#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"


################################################################################
##  ON NON PRIMARY MASTER NODES - Ejecutar join                               ##
################################################################################

HOSTNAME=$(hostname)

# Configure Kubernetes access with uploaded config file
mkdir -p ${HOME}/.kube
mv ${SCRIPT_DIR}/kubeConfig ${HOME}/.kube/config

# Drain node (evict all Pods so they are rescheduled)
echo
echo -e $COL_BLUE"Draining node..."$COL_DEFAULT
echo
n=0
until [ $n -ge 5 ]
do
  kubectl drain --ignore-daemonsets --delete-emptydir-data --grace-period=-1 --timeout=30s ${HOSTNAME} && break
  echo
  echo -e $COL_YELLOW"[WARN] Drain command failed. Retrying in 5s..."$COL_DEFAULT
  echo
  n=$[$n+1]
  sleep 5
done

# Delete node from Kubernetes node list (otherwise after reset it would still
# show up as "NotReady,SchedulingDisabled")
echo
echo -e $COL_BLUE"Deleting node..."$COL_DEFAULT
echo
n=0
until [ $n -ge 5 ]
do
  kubectl delete node --now --wait ${HOSTNAME} && break
  echo
  echo -e $COL_YELLOW"[WARN] Delete command failed. Retrying in 5s..."$COL_DEFAULT
  echo
  n=$[$n+1]
  sleep 5
done

# Reset node from Kubernetes perspective (clean all local Kubernetes processes,
# configuration, credentials, etc.). Notify Masters cluster this Master is
# leaving the group.
# NOTE: Only iptables are not reset (to be done manually, if you're a brave man)
echo
echo -e $COL_BLUE"Resetting Kubernetes node..."$COL_DEFAULT
echo

n=0
until [ $n -ge 5 ]
do
  sudo kubeadm reset --force --cri-socket unix:///var/run/cri-dockerd.sock --ignore-preflight-errors all && break
  echo
  echo -e $COL_YELLOW"[WARN] Reset command failed. Retrying in 5s..."$COL_DEFAULT
  echo
  n=$[$n+1]
  sleep 5
done




echo
echo -e $COL_BLUE"Uninstalling Kubernetes, Docker & Co..."$COL_DEFAULT
echo

# Uninstall and remove Kubernetes stuff
sudo apt-mark unhold kubelet kubeadm kubectl
sudo apt-get purge -y kubectl kubeadm kubelet
sudo rm -rf /etc/apt/sources.list.d/kubernetes.list

# Remove all remaining files
sudo rm -rf \
  /etc/kubernetes \
  /etc/cni/net.d \
  /var/lib/calico \
  /var/lib/cni \
  /var/lib/docker \
  /var/lib/docker-shim \
  /var/lib/etcd \
  /var/lib/filebeat* \
  /var/lib/kubelet \
  /usr/libexec/kubernetes \
  /etc/systemd/system/kubelet.service.d/10-kubeadm.conf \
  $HOME/.helm \
  $HOME/.kube

# Remove /opt/cni - it isn't deleted when uninstalling with apt
sudo rm -rf /opt/cni

# Remove Helm if installed
sudo rm -rf /usr/local/bin/helm

# Uninstall and clean cri-docker
sudo systemctl disable cri-docker.service
sudo systemctl stop cri-docker.service
sudo systemctl disable cri-docker.socket
sudo systemctl stop cri-docker.socket
sudo rm -rf /usr/local/bin/cri-dockerd
sudo rm -rf \
  /usr/local/bin/cri-dockerd \
  /etc/systemd/system/cri-docker.socket \
  /etc/systemd/system/cri-docker.service

# Uninstall and clean Docker stuff
sudo apt-get purge -y docker-ce containerd.io
sudo apt-get autoremove -y --purge
sudo groupdel docker
# Disable docker repository
sudo sed -i'.kumori-cleanup' '/docker/{s/^/#/}' /etc/apt/sources.list
# Remove all remaining files
sudo rm -rf \
  /var/lib/docker \
  /etc/docker \
  /etc/apparmor.d/docker \
  /var/run/docker.sock \
  /etc/systemd/system/docker.service.d \
  $HOME/.docker

# Uninstall other stuff
sudo apt-get purge -y jq
# sudo apt-get purge -y yq
# sudo add-apt-repository --remove -y ppa:rmescandon/yq
sudo rm -f /usr/local/bin/yq

# Remove systemd patch
sudo rm -rf /etc/rsyslog.d/10-ignore-systemd-session-slice.conf

# Remove Kumori drop caches cron job
sudo rm -rf /etc/cron.d/kumori-drop-caches

# Remove kumori TX Offload disable scritp
sudo rm -rf /usr/lib/networkd-dispatcher/routable.d/20-kumori-disable-tx-offloading

# Disable systemd-resolved cachedocker repository
sudo sed -i'.kumori-cleanup' '/^Cache=no-negative$/{s/^/#/}' /etc/systemd/resolved.conf

# Clean /etc/hosts
sudo sed -i '/Added by Kumori installer/d' /etc/hosts

echo
echo -e $COL_BLUE"Cleaning up iptables..."$COL_DEFAULT
echo
sudo iptables -F
sudo iptables -t nat -F
sudo iptables -t mangle -F
sudo iptables -X


if [ -n "${DNS_RESOLVERS}" ]; then
  echo
  echo -e $COL_BLUE"Removing custom DNS resolvers (${DNS_RESOLVERS})..."$COL_DEFAULT
  echo
  # sudo grep -v "supersede domain-name-servers ${DNS_RESOLVERS};" /etc/dhcp/dhclient.conf > /tmp/tmpdh
  # sudo mv /tmp/tmpdh /etc/dhcp/dhclient.conf
fi

################################################################################
##  CUSTOM  - REMOVE TRUSTED CERTIFICATE AUTHORITIES                          ##
################################################################################
if [ -d "${SCRIPT_DIR}/custom/trusted-ca" ]; then
  crtCount=$(ls -1 ${SCRIPT_DIR}/custom/trusted-ca/*.crt 2>/dev/null | wc -l)
  if [ $crtCount != 0 ]; then
    echo
    echo -e $COL_BLUE"Removing custom trusted CAs..."$COL_DEFAULT
    echo
    CERTSPATH="/usr/local/share/ca-certificates"
    for file in ${SCRIPT_DIR}/custom/trusted-ca/*.crt; do
      echo "- Deleting ${file##*/} from ${CERTSPATH}"
      sudo rm -f "${CERTSPATH}/${file##*/}"
    done
    sudo /usr/sbin/update-ca-certificates -f
  fi
fi
