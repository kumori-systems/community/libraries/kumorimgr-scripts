#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

# NOTE: All nodes are supposed to be on Ubuntu 18.04 Bionic

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"

# Avoid service restart confirmation prompts
export DEBIAN_FRONTEND=noninteractive


################################################################################
##  BASIC HOST CONFIGURATION                                                  ##
################################################################################
# Avoid "sudo: unable to resolve host <xxxx>" error
echo "$(hostname -I | cut -d\  -f1) $(hostname)   # Added by Kumori installer" | sudo tee -a /etc/hosts > /dev/null 2>&1

# Make sure locale is correctly configured
sudo locale-gen "${DEFAULT_LOCALE}"
sudo DEBIAN_FRONTEND=noninteractive dpkg-reconfigure locales

# Set machine timezone
sudo timedatectl set-timezone "${TIMEZONE}"

# Avoid service restart confirmation prompts (additional tweak)
echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections

# Filter out from /var/log/syslog false positive errors due to systemd/docker bug
# More info: https://github.com/kubernetes/kubernetes/issues/71887
sudo cp ${SCRIPT_DIR}/provision/rsyslog_10-ignore-systemd-session-slice.conf /etc/rsyslog.d/10-ignore-systemd-session-slice.conf
sudo cp ${SCRIPT_DIR}/provision/rsyslog_10-ignore-run-docker-runtime-mount.conf /etc/rsyslog.d/10-ignore-run-docker-runtime-mount.conf
sudo cp ${SCRIPT_DIR}/provision/rsyslog_10-ignore-keeplaived-advert-127-0-0-1.conf /etc/rsyslog.d/10-ignore-keeplaived-advert-127-0-0-1.conf
sudo service rsyslog restart

################################################################################
##  INSTALL BASIC UTILITIES                                                   ##
################################################################################
# Utilities to use with kubectl
echo
echo "Installing net-tools, jq..."
sudo apt-get update
sudo apt-get install -y jq net-tools

echo
echo "Installing yq..."
wget --quiet -O yq https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64
chmod +x yq
sudo mv yq /usr/local/bin

################################################################################
##  INSTALL DOCKER                                                            ##
################################################################################
echo
echo -e $COL_BLUE"Installing docker stuff..."$COL_DEFAULT
echo

sudo apt-get -y remove docker docker-engine docker.io containerd runc
sudo apt-get -y update

# Install Docker
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get -y update

# Install specific containerd version
if [ -n "${CONTAINERD_VERSION}" ]; then
  sudo apt-get -y install containerd.io=${CONTAINERD_VERSION}
fi

sudo apt-get -y install docker-ce=${DOCKER_VERSION}
sudo groupadd docker
sudo usermod -aG docker $USER

# Configure Docker
DAEMON_CFG_FILE="/tmp/docker-daemon-cfg.json"

cat <<EOF > "${DAEMON_CFG_FILE}"
{
  "bip": "${DOCKER_BRIDGE_CIDR}",
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m",
    "max-file": "5",
    "compress": "true"
  },
  "storage-driver": "overlay2",
  "registry-mirrors": []
}
EOF

# Add Docker registry mirrors
if [ -n "${DOCKER_REGISTRY_MIRRORS}" ]; then
  ITI_MIRROR_DOMAIN="docker-mirror.iti.upv.es"
  ITI_MIRROR_IP="10.149.16.56"
  ITI_MIRROR_USED="false"

  DAEMON_CFG_TMP_FILE="/tmp/docker-daemon-cfg_TMP.json"
  for MIRROR in ${DOCKER_REGISTRY_MIRRORS[@]}; do
    # Patch: detect if ITI mirror is used
    [[ "${MIRROR}" =~ "${ITI_MIRROR_DOMAIN}" ]] && ITI_MIRROR_USED="true"
    # The jq '--arg key' stuff is for being able to use property names with dashes ('-')
    cat ${DAEMON_CFG_FILE} \
      | jq --arg key "registry-mirrors" '.[ $key ] += [ "'${MIRROR}'" ]' \
      > ${DAEMON_CFG_TMP_FILE}
    mv ${DAEMON_CFG_TMP_FILE} ${DAEMON_CFG_FILE}
  done

  # If ITI mirror is used, add its ITI-internal address to /etc/hosts.
  # The reason is that for ITI we override the default DNS resolvers.
  if [[ "${ITI_MIRROR_USED}" == "true" ]]; then
    echo -e "\n# ITI Docker registry mirror\n${ITI_MIRROR_IP} ${ITI_MIRROR_DOMAIN}" \
      | sudo tee -a /etc/hosts > /dev/null
    echo "Patched /etc/hosts with ITI docker mirror domain."
  fi
fi

echo
echo "Docker daemon configuration:"
echo
cat ${DAEMON_CFG_FILE}
echo

sudo mv ${DAEMON_CFG_FILE} /etc/docker/daemon.json

sudo mkdir -p /etc/systemd/system/docker.service.d
sudo systemctl daemon-reload
sudo systemctl restart docker

################################################################################
##  CRI-DOCKERD INSTALLATION                                                  ##
################################################################################
echo
echo -e $COL_BLUE"Installing docker stuff..."$COL_DEFAULT
echo

# Temporary directory
mkdir tmp-cri-dockerd
cd tmp-cri-dockerd

# Obtain cri-dockerd binary
wget https://github.com/Mirantis/cri-dockerd/releases/download/v${CRI_DOCKERD_VERSION}/cri-dockerd-${CRI_DOCKERD_VERSION}.amd64.tgz
tar xvf cri-dockerd-${CRI_DOCKERD_VERSION}.amd64.tgz
sudo mv ./cri-dockerd/cri-dockerd /usr/local/bin/

# Create and start a linux service
wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/v${CRI_DOCKERD_VERSION}/packaging/systemd/cri-docker.service
wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/v${CRI_DOCKERD_VERSION}/packaging/systemd/cri-docker.socket
sudo mv cri-docker.socket cri-docker.service /etc/systemd/system/
sudo sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
sudo systemctl daemon-reload
sudo systemctl enable cri-docker.service
sudo systemctl enable --now cri-docker.socket
systemctl status cri-docker.socket

# Clean temporary directory
cd ..
rm -rf tmp-cri-dockerd

################################################################################
##  INSTALL KUBERNETES                                                        ##
################################################################################
echo
echo -e $COL_BLUE"Installing Kubernetes stuff..."$COL_DEFAULT
echo

# DEPRECATED METHOD AS OF 04/03/2024
# sudo curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
# cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
# deb https://apt.kubernetes.io/ kubernetes-xenial main
# EOF
# sudo apt-get -y update

# Convert "1.25.3" to "1.25"
KUBERNETES_MAIN_VERSION=$( echo ${KUBERNETES_VERSION} | sed 's/\.[^.]*$//')

KUBERNETES_APT_DEPRECATED_VERSIONS=( "1.16" "1.21" )
if [[ " ${KUBERNETES_APT_DEPRECATED_VERSIONS[@]} " =~ " ${KUBERNETES_MAIN_VERSION} " ]]; then
  # This version of Kubernetes does not support APT installation anymore.
  source workers_0.1_install-older-kubernetes.sh
else
  # This version of Kubernetes supports APT installation
  # Get the GPG key
  sudo mkdir -p /etc/apt/keyrings
  curl -fsSL https://pkgs.k8s.io/core:/stable:/v${KUBERNETES_MAIN_VERSION}/deb/Release.key \
    | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

  # Add an APT source
  # Task #1278 "Problem with GPG key when a KCluster (Kube v1.25) is created"):
  # The GPG key is expired, so it is necessary to explicitly indicate that it is trusted.
  # echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v${KUBERNETES_MAIN_VERSION}/deb/ /" \
  #   | sudo tee /etc/apt/sources.list.d/kubernetes.list
  echo "deb [trusted=true] https://pkgs.k8s.io/core:/stable:/v${KUBERNETES_MAIN_VERSION}/deb/ /" \
    | sudo tee /etc/apt/sources.list.d/kubernetes.list

  # Update APT packages
  sudo apt-get -y update

  # Install the Kubernetes packages
  sudo apt-get install -y \
    kubelet=${KUBERNETES_VERSION}* \
    kubeadm=${KUBERNETES_VERSION}* \
    kubectl=${KUBERNETES_VERSION}*

  # Mark packages as "hold" to avoid modifications
  sudo apt-mark hold kubelet kubeadm kubectl
fi


################################################################################
##  INSTALL/UPDATE LVM TOOLS (only for Storage nodes if LVM is enabled)       ##
################################################################################

# Determine if node is a Storage node
IS_STORAGE_NODE="false"
if [[ " ${STORAGE_NODES_IPS[@]} " =~ " ${NODE_IP} " ]]; then
  IS_STORAGE_NODE="true"
fi

if [ "${IS_STORAGE_NODE}" = "true" ] && [ "${OPENEBS_STORAGE_LVM_ENABLED}" = "true" ]; then
  echo
  echo -e $COL_BLUE"Installing LVM2 utilities..."$COL_DEFAULT
  echo
  sudo apt-get install -y lvm2
  echo
fi

###  ################################################################################
###  ##  INSTALL/UPDATE iSCSI TOOLS (only for Storage nodes)                       ##
###  ################################################################################
###
###  # Determine if node is a Storage node
###  # IS_STORAGE_NODE="false"
###  # if [[ " ${STORAGE_NODES_IPS[@]} " =~ " ${NODE_IP} " ]]; then
###  #   IS_STORAGE_NODE="true"
###  # fi
###
###  # Temporarily, all Worker nodes are Storage nodes
###  IS_STORAGE_NODE="true"
###
###  if [ "${IS_STORAGE_NODE}" = "true" ]; then
###    echo
###    echo -e $COL_BLUE"Installing/updating iSCSI Tools..."$COL_DEFAULT
###    echo


################################################################################
##  INSTALL AND CONFIGURE KEEPALIVED FOR INGRESS (only for Ingress nodes)     ##
################################################################################

# Determine if node is an Ingress node and if it's the first in the Ingress list
IS_INGRESS_NODE="false"
IS_FIRST="false"
if [[ " ${INGRESS_NODES_IPS[@]} " =~ " ${NODE_IP} " ]]; then
  IS_INGRESS_NODE="true"
  if [ "${INGRESS_NODES_IPS[0]}" = "${NODE_IP}" ]; then
    IS_FIRST="true"
  fi
fi


if [ "${INGRESS_HANDLE_FLOATING_IP}" = "true" ] && [ "${IS_INGRESS_NODE}" = "true" ]; then
  echo
  echo -e $COL_BLUE"Installing and configuring Ingress Keepalived..."$COL_DEFAULT
  echo

  sudo apt-get install -y keepalived=${KEEPALIVED_VERSION}

  # Find out default network interface (will be used for floating IP)
  DEFAULT_NETWORK_INTERFACE=$(route | grep '^default' | grep -o '[^ ]*$')

  # Prepare Keepalived configuration file and health check script
  KEEPALIVED_CONF_FILE_TEMPLATE_FILE="${SCRIPT_DIR}/provision/keepalived/keepalived-ingress.conf.template"
  KEEPALIVED_CONF_FILE="${SCRIPT_DIR}/provision/keepalived/keepalived-ingress.conf"

  # Use VRID set in configuration or use default values (11 for ApiServer, 12 for Ingress)
  KEEPALIVED_VIRTUAL_ROUTER_ID="${INGRESS_VRID:-12}"

  # We create a specific password (max length 8) for this Keepalived cluster, with the following format:
  # <prefix><hash>  where <prefix> can be AS for ApiServer or IN for Ingress and <hash> is a simple checksum
  # of the Kumori cluster name (truncated to 6 chars).
  KEEPALIVED_PASSWORD="IN$(echo ${RELEASE_NAME} | cksum | cut -f 1 -d ' ' | tail -c 6)"

  KEEPALIVED_ROLE="BACKUP"
  KEEPALIVED_PRIORITY="254"
  if [ "${IS_FIRST}" = "true" ]; then
    KEEPALIVED_ROLE="MASTER"
    KEEPALIVED_PRIORITY="255"
  fi

  # TODO: THIS VARIABLE IS TEMPORARY, UNTIL BETTER CONFIGURATION IS POSSIBLE
  #       Checks to port 80 or 443 with no domain will fail.
  #       For now, check Ambassador admin endpoint on port 8080 (the official
  #       documented method).
  #       See: https://www.getambassador.io/docs/latest/topics/running/ambassador-with-gke/#redirecting-the-health-check
  INGRESS_CHECK_PORT="8080"

  sed \
    -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" \
    -e "s?{{INITIAL_ROLE}}?${KEEPALIVED_ROLE}?g" \
    -e "s?{{NETWORK_INTERFACE}}?${DEFAULT_NETWORK_INTERFACE}?g" \
    -e "s?{{VIRTUAL_ROUTER_ID}}?${KEEPALIVED_VIRTUAL_ROUTER_ID}?g" \
    -e "s?{{PRIORITY}}?${KEEPALIVED_PRIORITY}?g" \
    -e "s?{{PASSWORD}}?${KEEPALIVED_PASSWORD}?g" \
    -e "s?{{INGRESS_VIRTUAL_IP}}?${INGRESS_VIRTUAL_IP}?g" \
    -e "s?{{NODE_IP}}?${NODE_IP}?g" \
    ${KEEPALIVED_CONF_FILE_TEMPLATE_FILE} \
    > ${KEEPALIVED_CONF_FILE}

  # Add a VRRP peer for each ingress node (except itself)
  EMPTY_LIST="true"
  for INGRESS_NODE_IP in ${INGRESS_NODES_IPS[@]}; do
    echo "IP: ${INGRESS_NODE_IP}"
    if [[ "${INGRESS_NODE_IP}" == "${NODE_IP}" ]]; then
      echo "Skipping node (self)"
    else
      EMPTY_LIST="false"
      # Duplicate the peer template line and replace the IP in one of the copies
      sed -i -e "s/^\(\s*\){{PEER_NODE_IP}}\s*$/\1${INGRESS_NODE_IP}\n&/g" ${KEEPALIVED_CONF_FILE}
    fi
  done

  # If peer list is empty, add 127.0.0.1 (localhost) to avoid Keepalived falling back to multicast
  if [[ "${EMPTY_LIST}" == "true" ]]; then
    DEFAULT_PEER_IP="127.0.0.1"
    sed -i -e "s/^\(\s*\){{PEER_NODE_IP}}\s*$/\1${DEFAULT_PEER_IP}\n&/g" ${KEEPALIVED_CONF_FILE}
  fi

  # Remove peer template line
  sed -i -e "/^.*{{PEER_NODE_IP}}.*$/d" ${KEEPALIVED_CONF_FILE}

  echo
  echo "Applying Keepalived configuration:"
  echo
  cat ${KEEPALIVED_CONF_FILE}
  echo

  # Prepare check and notify scripts
  sed \
    -e "s?{{INGRESS_VIRTUAL_IP}}?${INGRESS_VIRTUAL_IP}?g" \
    -e "s?{{INGRESS_EXTERNAL_PORT}}?${INGRESS_EXTERNAL_PORT}?g" \
    -e "s?{{INGRESS_INTERNAL_PORT}}?${INGRESS_INTERNAL_PORT}?g" \
    -e "s?{{INGRESS_INTERNAL_BALANCING}}?${INGRESS_INTERNAL_BALANCING}?g" \
    -e "s?{{NODE_IP}}?${NODE_IP}?g" \
    ${SCRIPT_DIR}/provision/keepalived/check_ingress.sh.template \
    > ${SCRIPT_DIR}/provision/keepalived/check_ingress.sh

  # Prepare default custom notify scripts
  CUSTOM_KEEPALIVED_SCRIPTS_DIR="/etc/keepalived/custom-notify-scripts"
  CUSTOM_KEEPALIVED_STATE_EVENTS_SCRIPT="${CUSTOM_KEEPALIVED_SCRIPTS_DIR}/custom_state_events_handler.sh"
  sudo mkdir -p ${CUSTOM_KEEPALIVED_SCRIPTS_DIR}
  sudo cp ${SCRIPT_DIR}/provision/keepalived/custom_state_events_handler.sh ${CUSTOM_KEEPALIVED_STATE_EVENTS_SCRIPT}

  sed \
    -e "s?{{CUSTOM_KEEPALIVED_STATE_EVENTS_SCRIPT}}?${CUSTOM_KEEPALIVED_STATE_EVENTS_SCRIPT}?g" \
    ${SCRIPT_DIR}/provision/keepalived/notify_fifo.sh.template \
    > ${SCRIPT_DIR}/provision/keepalived/notify_fifo.sh

  sudo mv ${SCRIPT_DIR}/provision/keepalived/check_ingress.sh /etc/keepalived/check_ingress_real.sh
  sudo mv ${SCRIPT_DIR}/provision/keepalived/check_force_failure.sh /etc/keepalived/check_force_failure.sh
  sudo mv ${SCRIPT_DIR}/provision/keepalived/notify_fifo.sh /etc/keepalived/notify_fifo.sh
  sudo mv ${KEEPALIVED_CONF_FILE} /etc/keepalived/keepalived.conf

  # Keepalived is strict about script security. Only root user must have acces to the script
  sudo chown -R root:root /etc/keepalived/*
  sudo chmod 700 /etc/keepalived/check_ingress_real.sh
  sudo chmod 700 /etc/keepalived/check_force_failure.sh
  sudo chmod 700 /etc/keepalived/notify_fifo.sh
  sudo chmod 700 /etc/keepalived/custom-notify-scripts/custom_state_events_handler.sh

  # Initially set a health-check script that forces failure (for Keepalived to not start properly
  # until the script is replaced by the real one).
  sudo cp /etc/keepalived/check_force_failure.sh /etc/keepalived/check_ingress.sh

  # Configure Keepalived service to automatically restart on failures
  sudo sed -i '/\[Service\]/a RestartSec=5s' /lib/systemd/system/keepalived.service
  sudo sed -i '/\[Service\]/a Restart=on-failure' /lib/systemd/system/keepalived.service
  sudo systemctl daemon-reload
  sudo systemctl restart keepalived


  # Add a networkd-dispatcher to force failover when network is restarted
  sed -e "s?{{NETWORK_INTERFACE}}?${DEFAULT_NETWORK_INTERFACE}?g" \
    ${SCRIPT_DIR}/provision/keepalived/10-kumori-keepalived-restart.template \
    > ${SCRIPT_DIR}/provision/keepalived/10-kumori-keepalived-restart

  sudo mkdir -p /usr/lib/networkd-dispatcher/routable.d
  sudo mv ${SCRIPT_DIR}/provision/keepalived/10-kumori-keepalived-restart \
    /usr/lib/networkd-dispatcher/routable.d/10-kumori-keepalived-restart
  sudo chmod 755 /usr/lib/networkd-dispatcher/routable.d/10-kumori-keepalived-restart
  sudo chown root:root /usr/lib/networkd-dispatcher/routable.d/10-kumori-keepalived-restart
fi


################################################################################
##  INSTALL AND CONFIGURE ENVOY FOR INGRESS (only for Ingress nodes)          ##
################################################################################
if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ] && [ "${IS_INGRESS_NODE}" = "true" ]; then
  echo
  echo -e $COL_BLUE"Installing and configuring Ingress Envoy proxy..."$COL_DEFAULT
  echo

  # TEMPORARILY DISABLED DUE TO BINTRAY SHUTDOWN (Envoy issue #17236)
  # curl -sL 'https://getenvoy.io/gpg' | sudo apt-key add -
  # sudo add-apt-repository "deb [arch=amd64] https://dl.bintray.com/tetrate/getenvoy-deb $(lsb_release -cs) stable"
  # sudo apt-get install -y getenvoy-envoy=${ENVOY_VERSION}

  # ALTERNATIVE INSTALLATION METHOD
  wget https://archive.tetratelabs.io/envoy/download/v${ENVOY_VERSION}/envoy-v${ENVOY_VERSION}-linux-amd64.tar.xz
  tar -xf envoy-v${ENVOY_VERSION}-linux-amd64.tar.xz
  chmod +x envoy-v${ENVOY_VERSION}-linux-amd64/bin/envoy
  sudo mv envoy-v${ENVOY_VERSION}-linux-amd64/bin/envoy /usr/bin/envoy
  rm -rf envoy-v${ENVOY_VERSION}-linux-amd64.tar.xz envoy-v${ENVOY_VERSION}-linux-amd64

  # Prepare Envoy configuration directory
  sudo mkdir -p /etc/envoy

  # Prepare a basic Envoy configuration file with HTTPS configuration
  HOSTNAME="$(hostname)"
  sed \
    -e "s?{{HOSTNAME}}?${HOSTNAME}?g" \
    -e "s?{{CLUSTER_NAME}}?${RELEASE_NAME}?g" \
    -e "s?{{INGRESS_EXTERNAL_PORT}}?${INGRESS_EXTERNAL_PORT}?g" \
    -e "s?{{NODE_IP}}?${NODE_IP}?g" \
    ${SCRIPT_DIR}/provision/envoy/envoy-config-workers.yaml.template \
    > ${SCRIPT_DIR}/provision/envoy/envoy-config.yaml

  # Add custom TCP ports configuration
  if [ -n "${INGRESS_TCP_PORTS}" ]; then
    # Work is done converting to JSON for easier data handling
    ENVOY_CFG_JSON_FILE="${SCRIPT_DIR}/provision/envoy/envoy-config.json"
    ENVOY_CFG_JSON_FILE_TMP="${SCRIPT_DIR}/provision/envoy/envoy-config_tmp.json"

    cat ${SCRIPT_DIR}/provision/envoy/envoy-config.yaml | yq r -j - > ${ENVOY_CFG_JSON_FILE}

    # For each TCP port, add  new listener and a new cluster definition

    # Circuit breaker config slug, for readability
    CIRCUIT_BREAKER_CFG_SLUG="\"circuit_breakers\": { \"thresholds\": [ \
      { \"priority\": \"DEFAULT\", \"max_connections\": 1000000000, \"max_pending_requests\": 1000000000, \"max_requests\": 1000000000, \"max_retries\": 1000000000 }, \
      { \"priority\": \"HIGH\", \"max_connections\": 1000000000, \"max_pending_requests\": 1000000000, \"max_requests\": 1000000000, \"max_retries\": 1000000000 } \
    ]}"

    ACCESS_LOG_CFG_SLUG='"access_log": [ { "name": "envoy.access_loggers.file", "typed_config": { "@type": "type.googleapis.com/envoy.extensions.access_loggers.file.v3.FileAccessLog", "log_format":{"text_format_source": {"inline_string": "[%START_TIME%] \"%REQ(:METHOD)% %REQ(X-ENVOY-ORIGINAL-PATH?:PATH)% %PROTOCOL%\" %RESPONSE_CODE% %RESPONSE_FLAGS% %BYTES_RECEIVED% %BYTES_SENT% %DURATION% %RESP(X-ENVOY-UPSTREAM-SERVICE-TIME)% \"%DOWNSTREAM_REMOTE_ADDRESS%\" \"%REQ(USER-AGENT)%\" \"%REQ(X-REQUEST-ID)%\" \"%REQ(:AUTHORITY)%\" \"%UPSTREAM_HOST%\"\n"}}, "path": "/var/log/kumori-ingress-envoy-lb.log" } } ]'

    # for PORT in ${INGRESS_TCP_PORTS[@]}; do
    for i in "${!INGRESS_TCP_PORTS[@]}"; do
      PORT="${INGRESS_TCP_PORTS[$i]}"
      cat ${ENVOY_CFG_JSON_FILE} \
        | jq ".static_resources.listeners += [ {address:{socket_address:{address:\"0.0.0.0\",port_value:${PORT}}},filter_chains:[{filters:[{typed_config:{\"@type\":\"type.googleapis.com/envoy.extensions.filters.network.tcp_proxy.v3.TcpProxy\",cluster:\"tcp${PORT}\",stat_prefix:\"ingress_tcp\",idle_timeout:\"36000s\"},name:\"envoy.tcp_proxy\"}]}],${ACCESS_LOG_CFG_SLUG},name:\"tcp${PORT}\"} ]" \
        | jq ".static_resources.clusters += [ {connect_timeout:\"0.25s\",${CIRCUIT_BREAKER_CFG_SLUG},eds_cluster_config:{eds_config:{resource_api_version:\"V3\",path_config_source:{path:\"/etc/envoy/eds_tcp${PORT}.conf\"}},service_name:\"tcp${PORT}\"},health_checks:[{healthy_threshold:1,tcp_health_check:{},interval:\"5s\",no_traffic_interval:\"5s\",timeout:\"1s\",unhealthy_threshold:1}],lb_policy:\"round_robin\",common_lb_config:{healthy_panic_threshold:{value:0.0}},name:\"tcp${PORT}\",type:\"EDS\"} ]" \
        > ${ENVOY_CFG_JSON_FILE_TMP}
      mv ${ENVOY_CFG_JSON_FILE_TMP} ${ENVOY_CFG_JSON_FILE}
    done

    cat ${ENVOY_CFG_JSON_FILE} \
      | yq r --prettyPrint - \
      > ${SCRIPT_DIR}/provision/envoy/envoy-config.yaml
  fi

  echo
  echo "Envoy configuration file:"
  echo
  cat ${SCRIPT_DIR}/provision/envoy/envoy-config.yaml
  echo
  # Put Envoy config file in the appropriate directory
  sudo mv ${SCRIPT_DIR}/provision/envoy/envoy-config.yaml /etc/envoy/envoy-kumori-config.yaml

  # Prepare Envoy EDS (Endpoint Discovery Service) configuration file
  echo
  echo "Preparing Envoy EDS files..."
  echo

  EDS_CONF_TEMPLATE_FILE="${SCRIPT_DIR}/provision/envoy/eds.conf.template"
  EDS_CONF_TMP_FILE="${SCRIPT_DIR}/provision/envoy/eds.conf.tmp"
  EDS_CONF_FILE="${SCRIPT_DIR}/provision/envoy/eds.conf"

  # Create a base configuration file based on the EDS config template
  cp ${EDS_CONF_TEMPLATE_FILE} ${EDS_CONF_FILE}

  # Add a new cluster for HTTPS configuration
  cat ${EDS_CONF_FILE} \
    | jq ".resources += [ {\"@type\":\"type.googleapis.com/envoy.config.endpoint.v3.ClusterLoadAssignment\",cluster_name:\"ingressHTTPS\",endpoints:[{lb_endpoints:[]}]} ]" \
    > ${EDS_CONF_TMP_FILE}
  mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}

  # For each IP add a new "endpoint" element to the configuration (JSON).
  # An endpoint has the following structure:
  #   {
  #     "endpoint": {
  #       "address": {
  #         "socket_address": {
  #           "address": "10.0.18.56",
  #           "port_value": 8443
  #         }
  #       },
  #       "health_check_config": {     <==== ONLY NEEDED IF HEALTHCHECK PORT IS DIFFERENT
  #         "port_value": 8080
  #       }
  #     }
  #   }
  # Add HTTPS endpoints to ingressHTTPS cluster
  for ENDPOINT_IP in ${INGRESS_NODES_IPS[@]}; do
    echo "IP: ${ENDPOINT_IP}"
    # FOR REFERENCE: if healthcheck is different, use this replacement instead:
    #   | jq ".resources[0].endpoints[0].lb_endpoints += [ { endpoint: { address: { socket_address: { address: \"${ENDPOINT_IP}\", port_value: ${INGRESS_INTERNAL_PORT} } }, health_check_config: { port_value: 8080 } } } ]"  \
    cat ${EDS_CONF_FILE} \
      | jq ".resources[0].endpoints[0].lb_endpoints += [ { endpoint: { address: { socket_address: { address: \"${ENDPOINT_IP}\", port_value: ${INGRESS_INTERNAL_PORT} } } } } ]"  \
      > ${EDS_CONF_TMP_FILE}
    mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}
  done

  mv ${EDS_CONF_FILE} ${SCRIPT_DIR}/provision/envoy/eds_ingressHTTPS.conf
  echo
  echo "Envoy EDS file for ingressHTTPS: "
  echo
  cat ${SCRIPT_DIR}/provision/envoy/eds_ingressHTTPS.conf
  echo
  # Put EDS for ingressHTTPS in the appropriate directory
  sudo mv ${SCRIPT_DIR}/provision/envoy/eds_ingressHTTPS.conf /etc/envoy/


  if [ -n "${INGRESS_TCP_PORTS}" ]; then
    # For each custom TCP port, create a new EDS configuration file and populate it
    # Loop array indices
    for i in "${!INGRESS_TCP_PORTS[@]}"; do
      INTERNAL_PORT="${INGRESS_TCP_INTERNAL_PORTS[$i]}"
      EXTERNAL_PORT="${INGRESS_TCP_PORTS[$i]}"

      PORT_EDS_CONFIG_FILE="${SCRIPT_DIR}/provision/envoy/eds_tcp${EXTERNAL_PORT}.conf"

      # Create a base configuration file based on the EDS config template
      cp ${EDS_CONF_TEMPLATE_FILE} ${EDS_CONF_FILE}

      cat ${EDS_CONF_FILE} \
        | jq ".resources += [ {\"@type\":\"type.googleapis.com/envoy.config.endpoint.v3.ClusterLoadAssignment\",cluster_name:\"tcp${EXTERNAL_PORT}\",endpoints:[{lb_endpoints:[]}]} ]" \
        > ${EDS_CONF_TMP_FILE}
      mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}

      for ENDPOINT_IP in ${INGRESS_NODES_IPS[@]}; do
        cat ${EDS_CONF_FILE} \
          | jq ".resources[0].endpoints[0].lb_endpoints += [ { endpoint: { address: { socket_address: { address: \"${ENDPOINT_IP}\", port_value: ${INTERNAL_PORT} } } } } ]"  \
          > ${EDS_CONF_TMP_FILE}
        mv ${EDS_CONF_TMP_FILE} ${EDS_CONF_FILE}
      done

      mv ${EDS_CONF_FILE} ${PORT_EDS_CONFIG_FILE}
      echo
      echo "Envoy EDS file for TCP ${EXTERNAL_PORT}: "
      echo
      cat ${PORT_EDS_CONFIG_FILE}
      echo
      # Put EDS for ingressHTTPS in the appropriate directory
      sudo mv ${PORT_EDS_CONFIG_FILE} /etc/envoy/
    done
  fi

  # Create a service for automatically manage Envoy life-cycle
  sudo cp ${SCRIPT_DIR}/provision/envoy/envoy.service /etc/systemd/system/envoy.service
  sudo systemctl start envoy
  sudo systemctl enable envoy
fi


################################################################################
##  CONFIGURE LOGROTATE FOR KUMORI LOGS                                       ##
################################################################################
echo
echo -e $COL_BLUE"Configuring logrotate for Kumori logs..."$COL_DEFAULT
echo

sudo cp ${SCRIPT_DIR}/provision/logrotate/kumori-masters /etc/logrotate.d/kumori-workers


################################################################################
##  CONFIGURE DNS CACHING                                                     ##
################################################################################
echo
echo -e $COL_BLUE"Configuring resolved cache..."$COL_DEFAULT
echo

# Configure DNS resolution for not caching negative operations (errors)
echo "Cache=no-negative" | sudo tee -a /etc/systemd/resolved.conf 2>&1>/dev/null
# Restart systemd-resolved service
sudo service systemd-resolved restart


################################################################################
##  CONFIGURE CUSTOM DNS RESOLVERS                                            ##
################################################################################
if [ -n "${DNS_RESOLVERS}" ]; then
  echo "Trying to set default DNS resolvers (${DNS_RESOLVERS})..."
  # echo "supersede domain-name-servers ${DNS_RESOLVERS};" | sudo tee -a /etc/dhcp/dhclient.conf

  # Detect what Netplan file is used:
  # - /etc/netplan/50-cloud-init.yaml  (works in Openstack)
  # - /etc/netplan/01-netcfg.yaml  (works in Virtualbox)
  if [ -f "/etc/netplan/50-cloud-init.yaml" ]; then
    NETPLAN_CONFIG_FILE="/etc/netplan/50-cloud-init.yaml"
  elif [ -f "/etc/netplan/01-netcfg.yaml" ]; then
    NETPLAN_CONFIG_FILE="/etc/netplan/01-netcfg.yaml"
  else
    NETPLAN_CONFIG_FILE=""
  fi

  if [ -n "${NETPLAN_CONFIG_FILE}" ] && [ -f "${NETPLAN_CONFIG_FILE}" ]; then
    echo "Found ${NETPLAN_CONFIG_FILE} file, patching it..."

    # Backup original file for restoring it on cluster destruction
    NETPLAN_CONFIG_BACKUP="${SCRIPT_DIR}/etc-netplan-50-cloud-init_BACKUP.yaml"
    sudo cp "${NETPLAN_CONFIG_FILE}" "${NETPLAN_CONFIG_BACKUP}"

    # Prepare DNS server list in the correct format ("xx","yy","zz")
    JOINED_DNS=$(printf ",\"%s\"" "${DNS_RESOLVERS[@]}")
    JOINED_DNS=${JOINED_DNS:1}  # remove comma from the beginning
    echo "DNS SERVERS:"${JOINED_DNS}

    # Add nameservers to every ethernet interface found
    SRC_YAML=$(sudo cat "${NETPLAN_CONFIG_FILE}")
    echo "Processing netplan configuration to add nameservers:"
    echo
    echo "Source:"
    echo
    echo "${SRC_YAML}"

    echo
    echo "Patched:"
    echo
    echo "${SRC_YAML}" \
      | yq r -j - \
      | jq ".network.ethernets[] += {nameservers: { addresses: [ ${JOINED_DNS} ] } }"  \
      | yq r --prettyPrint - \
      | sudo tee ${NETPLAN_CONFIG_FILE}
    echo
    echo "Applying netplan changes..."
    echo
    sudo netplan apply
  fi
fi

################################################################################
##  CONFIGURE CRON JOB FOR PERIODICALLY FLUSHING PAGE CACHE                   ##
################################################################################
# If using Ambassador v11, then activate the memory flushing cron job on Ingress nodes

# Warning: comparison with wildcard (starts with) requires double-brackets and no quoting on the right side
RELEVANT_VERSION="1.11"
if [[ "${AMBASSADOR_VERSION}" == ${RELEVANT_VERSION}* ]]; then
  PERIODICALLY_DROP_CACHES="true"
fi

if [ "${PERIODICALLY_DROP_CACHES}" = "true" ] && [ "${IS_INGRESS_NODE}" = "true" ]; then
  # Add a cron job for periodically flushing the OS page cache
  CRONJOB_TEMPLATE_FILE="${SCRIPT_DIR}/provision/cronjobs/kumori-drop-caches.template"
  CRONJOB_FILE="${SCRIPT_DIR}/provision/cronjobs/kumori-drop-caches"

  sed \
    -e "s?{{DROP_CACHES_SCHEDULE}}?${DROP_CACHES_SCHEDULE}?g" \
    ${CRONJOB_TEMPLATE_FILE} \
    > ${CRONJOB_FILE}

  sudo chown root:root ${CRONJOB_FILE}
  sudo chmod 644 ${CRONJOB_FILE}
  sudo mv ${CRONJOB_FILE} /etc/cron.d/kumori-drop-caches
fi

################################################################################
##  CONFIGURE CRON JOB FOR PERIODICALLY CHECKING/RESTARTING KUBELET           ##
################################################################################
# Add a cron job for periodically checking for a specific Kubelet error log
# trace and restarting Kubelet if it is found
KUBELET_CRONJOB_FILE="${SCRIPT_DIR}/provision/cronjobs/kumori-kubelet-bug-restart"
KUBELET_SCRIPT_FILE="${SCRIPT_DIR}/provision/cronjobs/kumori-kubelet-bug-restart.sh"

sudo chown root:root ${KUBELET_CRONJOB_FILE} ${KUBELET_SCRIPT_FILE}
sudo chmod 644 ${KUBELET_CRONJOB_FILE}
sudo chmod 755 ${KUBELET_SCRIPT_FILE}
sudo mv ${KUBELET_CRONJOB_FILE} /etc/cron.d/kumori-kubelet-bug-restart
sudo mv ${KUBELET_SCRIPT_FILE} /usr/local/bin/kumori-kubelet-bug-restart.sh


################################################################################
##  CUSTOM  - ADD TRUSTED CERTIFICATE AUTHORITIES                             ##
################################################################################
if [ -d "${SCRIPT_DIR}/custom/trusted-ca" ]; then
  crtCount=$(ls -1 ${SCRIPT_DIR}/custom/trusted-ca/*.crt 2>/dev/null | wc -l)
  if [ $crtCount != 0 ]; then
    echo
    echo -e $COL_BLUE"Installing custom trusted CAs..."$COL_DEFAULT
    echo
    CERTSPATH="/usr/local/share/ca-certificates"
    for file in ${SCRIPT_DIR}/custom/trusted-ca/*.crt; do
      echo "- Adding ${file##*/} to ${CERTSPATH}"
      sudo mv "${SCRIPT_DIR}/custom/trusted-ca/${file##*/}" "${CERTSPATH}"
    done
    sudo /usr/sbin/update-ca-certificates
  fi
fi

################################################################################
##  DISABLE TX OFFLOADING IN NODE IF NECESSARY                                ##
################################################################################
if [[ " ${TX_OFF_NODES_IPS[@]} " =~ " ${NODE_IP} " ]]; then
  echo
  echo -e $COL_BLUE"Disabling Transmission checksumming offloaing (TX Offload)..."$COL_DEFAULT
  echo
  # Add a Networkd-Dispatcher file to be run whenever network comes up (routable)
  TXOFF_TEMPLATE_FILE="${SCRIPT_DIR}/provision/tx-offloading/20-disable-tx-offloading.template"
  TXOFF_FILE="${SCRIPT_DIR}/provision/tx-offloading/20-disable-tx-offloading"

  # Find out default network interface
  DEFAULT_NETWORK_INTERFACE=$(route | grep '^default' | grep -o '[^ ]*$')

  sed \
    -e "s?{{NETWORK_INTERFACE}}?${DEFAULT_NETWORK_INTERFACE}?g" \
    ${TXOFF_TEMPLATE_FILE} \
    > ${TXOFF_FILE}

  sudo mv "${TXOFF_FILE}" /usr/lib/networkd-dispatcher/routable.d/20-kumori-disable-tx-offloading
  sudo chown root:root /usr/lib/networkd-dispatcher/routable.d/20-kumori-disable-tx-offloading
  sudo chmod 755 /usr/lib/networkd-dispatcher/routable.d/20-kumori-disable-tx-offloading
fi


# Small pause in case there is a shutdown afterwards
sleep 10s
