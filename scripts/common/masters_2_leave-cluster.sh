#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"

HOSTNAME=$(hostname)

################################################################################
##  ON NON PRIMARY MASTER NODES - Reset node                                  ##
################################################################################

# Stop and disable Keepalived (floating virtual IP) and Envoy (internal load
# balancing) services.
echo
echo -e $COL_BLUE"Stopping and disabling Keepalived and Envoy services..."$COL_DEFAULT
echo

# Keepalived is a SysV service
if sudo service --status-all |& grep -Fq ' keepalived'; then
  sudo systemctl disable keepalived
  sudo systemctl stop keepalived
  echo "Keepalived service stopped and disabled."
else
  echo "Keepalived service not found."
fi

# Envoy is a Systemd service
if systemctl list-units --full -all | grep -Fq "envoy.service"; then
  sudo systemctl disable envoy
  sudo systemctl stop envoy
  echo "Envoy service stopped and disabled."
else
  echo "Envoy service not found."
fi
echo


# IMPORTANT: Delete node must be done *after* `kubeadm reset` since reset
#            command depends on the node configuration from Etcd. If it is
#            already deleted, its configuration is not available and some
#            actions of reset will fail, notably removing the corresponding
#            etcd member, leaving the Etcd cluster in 'degraded' or 'unhealthy'
#            states.

# Reset node from Kubernetes perspective (clean all local Kubernetes processes,
# configuration, credentials, etc.). Notify Masters cluster this Master is
# leaving the group.
# NOTE: Only iptables are not reset (to be done manually, if you're a brave man)
echo
echo -e $COL_BLUE"Resetting Kubernetes node..."$COL_DEFAULT
echo
n=0
RESET="false"
until [ $n -ge 5 ]
do
  sudo kubeadm reset --v=5 --force --cri-socket unix:///var/run/cri-dockerd.sock --ignore-preflight-errors all && RESET="true" && break
  echo
  echo -e $COL_YELLOW"[WARN] Reset command failed. Retrying in 5s..."$COL_DEFAULT
  echo
  n=$[$n+1]
  sleep 5
done

if [ "${RESET}" == "true" ]; then
  echo
  echo -e $COL_BLUE"Kubernetes reset of node ${HOSTNAME} completed."$COL_DEFAULT
  echo
else
  echo -e $COL_RED_BOLD"[ERROR] Kubernetes reset of node ${HOSTNAME} failed."$COL_DEFAULT
  echo -e $COL_RED_BOLD"        The machine should be manually reset or shut down to avoid interfering with the cluster."$COL_DEFAULT
  echo
fi


# Delete kubernetes configuration file
rm -rf $HOME/.kube
