#!/bin/bash

# Helper functions

# add_allowed_ip_to_mapping() modifies a YAML file containing an Ambassador 
# mapping definition, adding a "labels" section containing the allowed IP/CIDR
# list related to the inbound.
# The YAML file can contain several documents, but only the first mapping will
# be modified.
# It is required that yq v3.x is available.
# 
# Parameters:
# - $1: IPFiltering feature enabled? ("true"/"false") 
# - $2: File containing mapping (and maybe more things) to be modified
# - $3: List of allowed IP/CIDR of this addon (for example, '["10.10.1.1","192.168.20.0/16"]')
#
add_allowed_ip_to_mapping () {
  # Basic kumorimgr-scripts doesnt includes the IPFiltering feature, so this
  # function do nothing.
  echo "add_allowed_ip_to_mapping function: do nothing"
}