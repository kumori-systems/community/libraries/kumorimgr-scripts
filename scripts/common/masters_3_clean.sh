#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

SCRIPT_DIR="$(dirname $(readlink -f $0))"

# Load install configuration variables from file
source "${SCRIPT_DIR}/defs-colors.sh"
source "${SCRIPT_DIR}/variables.sh"

################################################################################
## THIS SCRIPT EXPECTS THE FOLLOWING PARAMETERS:                              ##
################################################################################
# The IP to be assigned to ingress domain, currently the public IP of the node
# selected to be the ingress etrypoint.
INGRESS_NODE_IP="$1"
echo
echo "Received parameters:"
echo
echo "  -INGRESS_NODE_IP : ${INGRESS_NODE_IP}"
echo


################################################################################
### REMOVING INGRESS DOMAIN FROM DNS (requires docker to be installed!)
################################################################################
if [ "${MANAGED_DNS}" = "true" ] && [ -n "${INGRESS_NODE_IP}" ]; then
  echo
  echo -e $COL_BLUE"* Deleting Cluster DNS entries..."$COL_DEFAULT
  echo


  if [ "${MANAGED_DNS_PROVIDER}" = "route53" ]; then

    AWS_CONFIG_DIR="${SCRIPT_DIR}/awsConfiguration"

    # Define AWS base command based on Docker image
    AWS_CLI="docker run --rm --env AWS_PAGER= -v ${AWS_CONFIG_DIR}:/root/.aws ${MANAGED_DNS_ROUTE53_CLI_IMAGE}"

    # Find out HostedZone ID from reference domain
    echo "Running command: ${AWS_CLI} route53 list-hosted-zones"
    RESULT=$(${AWS_CLI} route53 list-hosted-zones --output json)
    EXIT_CODE="$?"
    if [ "${EXIT_CODE}" != "0" ]; then
      echo
      echo -e $COL_RED_BOLD"Error getting DNS HostedZones. This will require manual corrective actions."
      echo
      exit 1
    fi

    echo
    echo "Current available HostedZones:"
    echo "${RESULT}"
    echo

    HOSTEDZONE_ID=$(echo ${RESULT} | jq -cr ".HostedZones[] | select(.Name==\"$REFERENCE_DOMAIN.\") | .Id")
    if [[ ${HOSTEDZONE_ID} != /hostedzone/* ]]; then
      echo
      echo -e $COL_RED_BOLD"Error getting HostedZone ID for ${REFERENCE_DOMAIN}. This will require manual corrective actions."
      echo
      exit 1
    fi

    echo "HostedZone domain: ${REFERENCE_DOMAIN}"
    echo "HostedZone ID: ${HOSTEDZONE_ID}"
    echo

    # Retrieve the list of DNS records belonging to the cluster
    OUTPUT=$(${AWS_CLI} route53 list-resource-record-sets \
      --query "ResourceRecordSets[?ends_with(Name, '-${RELEASE_NAME}.${REFERENCE_DOMAIN}.')]" \
      --hosted-zone-id ${HOSTEDZONE_ID})
    echo "Deleting the following DNS records:"
    echo
    echo ${OUTPUT} | jq -r '.[].Name'
    echo

    # Build the list of DNS changes and apply it
    JSON_DATA=$(echo ${OUTPUT} | jq '[ .[] | { Action: "DELETE", ResourceRecordSet: . } ] | { Comment: "DELETE all clusterrecords", Changes: . }')
    ${AWS_CLI} route53 change-resource-record-sets \
      --hosted-zone-id ${HOSTEDZONE_ID} \
      --change-batch "${JSON_DATA}"

    EXIT_CODE="$?"
    if [ "${EXIT_CODE}" == "0" ]; then
      echo "Domains deleted successfully."
      sudo rm -rf "${AWS_CONFIG_DIR}"
    else
      echo
      echo -e $COL_RED_BOLD"An error ocurred when deleting cluster domains. This will require manual corrective actions."$COL_DEFAULT
      echo
    fi
  else
    echo
    echo -e $COL_YELLOW"DNS Provider ${MANAGED_DNS_PROVIDER} not supported. Skipping DNS cleanup."$COL_DEFAULT
    echo
  fi
fi


################################################################################
##  ON NON PRIMARY MASTER NODES - Ejecutar join                               ##
################################################################################

HOSTNAME=$(hostname)

# Drain node (evict all Pods so they are rescheduled)
echo
echo -e $COL_BLUE"Draining node..."$COL_DEFAULT
echo
n=0
until [ $n -ge 5 ]
do
  kubectl drain --ignore-daemonsets --delete-emptydir-data --grace-period=-1 --timeout=30s ${HOSTNAME} && break
  echo
  echo -e $COL_YELLOW"[WARN] Drain command failed. Retrying in 5s..."$COL_DEFAULT
  echo
  n=$[$n+1]
  sleep 5
done

# IMPORTANT: Delete node must be done *after* `kubeadm reset` since reset
#            command depends on the node configuration from Etcd. If it is
#            already deleted, its configuration is not available and some
#            actions of reset will fail, notably removing the corresponding
#            etcd member, leaving the Etcd cluster in 'degraded' or 'unhealthy'
#            states.

# Reset node from Kubernetes perspective (clean all local Kubernetes processes,
# configuration, credentials, etc.). Notify Masters cluster this Master is
# leaving the group.
# NOTE: Only iptables are not reset (to be done manually, if you're a brave man)
echo
echo -e $COL_BLUE"Resetting Kubernetes node..."$COL_DEFAULT
echo

n=0
until [ $n -ge 5 ]
do
  sudo kubeadm reset --force --cri-socket unix:///var/run/cri-dockerd.sock --ignore-preflight-errors all && break
  echo
  echo -e $COL_YELLOW"[WARN] Reset command failed. Retrying in 5s..."$COL_DEFAULT
  echo
  n=$[$n+1]
  sleep 5
done

# Delete node from Kubernetes node list (otherwise after reset it would still
# show up as "NotReady,SchedulingDisabled")
echo
echo -e $COL_BLUE"Deleting node..."$COL_DEFAULT
echo
n=0
until [ $n -ge 5 ]
do
  kubectl delete node --now --wait ${HOSTNAME} && break
  echo
  echo -e $COL_YELLOW"[WARN] Delete command failed. Retrying in 5s..."$COL_DEFAULT
  echo
  n=$[$n+1]
  sleep 5
done




echo
echo -e $COL_BLUE"Uninstalling Kubernetes, Docker & Co..."$COL_DEFAULT
echo

# Uninstall and remove Kubernetes stuff
sudo apt-mark unhold kubelet kubeadm kubectl
sudo apt-get purge -y kubectl kubeadm kubelet
sudo rm -rf /etc/apt/sources.list.d/kubernetes.list

# Remove all remaining files
sudo rm -rf \
  /etc/kubernetes \
  /etc/cni/net.d \
  /var/lib/calico \
  /var/lib/cni \
  /var/lib/docker \
  /var/lib/docker-shim \
  /var/lib/etcd \
  /var/lib/filebeat* \
  /var/lib/kubelet \
  /usr/libexec/kubernetes \
  $HOME/.helm \
  $HOME/.krew \
  $HOME/.kube

# Remove /opt/cni - it isn't deleted when uninstalling with apt
sudo rm -rf /opt/cni

# Remove Kumori Etcdctl and KMan scripts
sudo rm -rf /usr/local/bin/ketcdctl
sudo rm -rf /usr/local/bin/kman

# Remove Kumori mayactl script
sudo rm -rf /usr/local/bin/kmayactl

# Remove Helm if installed
sudo rm -rf /usr/local/bin/helm /usr/local/bin/tiller

# Uninstall and clean cri-docker
sudo systemctl disable cri-docker.service
sudo systemctl stop cri-docker.service
sudo systemctl disable cri-docker.socket
sudo systemctl stop cri-docker.socket
sudo rm -rf \
  /usr/local/bin/cri-dockerd \
  /etc/systemd/system/cri-docker.socket \
  /etc/systemd/system/cri-docker.service

# Uninstall and clean Docker stuff
sudo apt-get purge -y docker-ce containerd.io
sudo apt-get autoremove -y --purge
sudo groupdel docker
# Disable docker repository
sudo sed -i'.kumori-cleanup' '/docker/{s/^/#/}' /etc/apt/sources.list
# Remove all remaining files
sudo rm -rf \
  /var/lib/docker \
  /etc/docker \
  /etc/apparmor.d/docker \
  /var/run/docker.sock \
  /etc/systemd/system/docker.service.d \
  $HOME/.docker

# Uninstall other stuff
sudo apt-get purge -y jq yq zip unzip
sudo add-apt-repository --remove -y ppa:rmescandon/yq
sudo rm -f /usr/local/bin/mc

# Remove systemd patch
sudo rm -rf /etc/rsyslog.d/10-ignore-systemd-session-slice.conf

# Remove kumori TX Offload disable scritp
sudo rm -rf /usr/lib/networkd-dispatcher/routable.d/20-kumori-disable-tx-offloading

# Disable systemd-resolved cachedocker repository
sudo sed -i'.kumori-cleanup' '/^Cache=no-negative$/{s/^/#/}' /etc/systemd/resolved.conf

echo
echo -e $COL_BLUE"Cleaning up iptables..."$COL_DEFAULT
echo
sudo iptables -F
sudo iptables -t nat -F
sudo iptables -t mangle -F
sudo iptables -X


if [ -n "${DNS_RESOLVERS}" ]; then
  echo
  echo -e $COL_BLUE"Removing custom DNS resolvers (${DNS_RESOLVERS})..."$COL_DEFAULT
  echo
  # sudo grep -v "supersede domain-name-servers ${DNS_RESOLVERS};" /etc/dhcp/dhclient.conf > /tmp/tmpdh
  # sudo mv /tmp/tmpdh /etc/dhcp/dhclient.conf
fi

################################################################################
##  CUSTOM  - REMOVE TRUSTED CERTIFICATE AUTHORITIES                          ##
################################################################################
if [ -d "${SCRIPT_DIR}/custom/trusted-ca" ]; then
  crtCount=$(ls -1 ${SCRIPT_DIR}/custom/trusted-ca/*.crt 2>/dev/null | wc -l)
  if [ $crtCount != 0 ]; then
    echo
    echo -e $COL_BLUE"Removing custom trusted CAs..."$COL_DEFAULT
    echo
    CERTSPATH="/usr/local/share/ca-certificates"
    for file in ${SCRIPT_DIR}/custom/trusted-ca/*.crt; do
      echo "- Deleting ${file##*/} from ${CERTSPATH}"
      sudo rm -f "${CERTSPATH}/${file##*/}"
    done
    sudo /usr/sbin/update-ca-certificates -f
  fi
fi
