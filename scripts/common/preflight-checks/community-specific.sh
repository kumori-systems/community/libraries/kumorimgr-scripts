#!/bin/bash

# This script checks that the provided configuration does not include any
# functionality not supported in Community version. 

# Expects one parameter, the error message to print
logError()
{
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - Not supported in Community version: $1."$COL_DEFAULT
  echo
  exit -1
}


####################################
##   FUNCTIONALITIES/OPERATIONS   ##
####################################

# Etcd/PKI backups
if [ "${INSTALL_ETCD_BACKUP}" = "true" ] || [ "${PKI_BACKUP}" = "true" ]; then
  logError "cluster state backup functionality (etcd and PKI backup/restore)"
fi

# Install cluster from Etcd backup
if [ "${USE_ETCDBACKUP}" = "true" ]; then
  logError "cluster installation from Etcd backup"
fi

# Maintenance nodes
if [ -n "${MAINTENANCE_MASTERS_IPS}" ] || \
   [ -n "${MAINTENANCE_WORKERS_IPS}" ] || \
   [ -n "${ADD_MAINTENANCE_MASTERS_IPS}" ] || \
   [ -n "${REMOVE_MAINTENANCE_MASTERS_IPS}" ] || \
   [ -n "${ADD_MAINTENANCE_WORKERS_IPS}" ] || \
   [ -n "${REMOVE_MAINTENANCE_WORKERS_IPS}" ]; then

  logError "node maintenance operations"
fi

# Storage provider - OpenEBS
if [ "${INSTALL_OPENEBS_PROVIDER}" = "true" ]; then
  logError "installation of OpenEBS storage provider"
fi

# Storage provider - Openstack Cinder CSI
if [ "${INSTALL_CSI_CINDER_PROVIDER}" = "true" ]; then
  logError "installation of Openstack Cinder storage provider"
fi

# Storage provider - CSI NFS
if [ "${INSTALL_CSI_NFS_PROVIDER}" = "true" ]; then
  logError "installation of CSI NFS storage provider"
fi

# Kumori Alarms
if [ "${INSTALL_KUALARM}" = "true" ]; then
  logError "installation of Kumori Alarms system"
fi

# MINIO 
if [ "${INSTALL_MINIO}" = "true" ]; then
  logError "installation of MinIO S3 server"
fi

# Descheduler installation
if [ "${INSTALL_DESCHEDULER}" = "true" ]; then
  logError "installation of Descheduler"
fi

# Event-Exporter
if [ "${EVENTS_STORE_TYPE}" != "k8s" ]; then
  logError "out-of-cluster event persistence"
fi

# Prometheus HA
if [ "${PROMETHEUS_HA}" = "true" ] || [ "${ALERMANAGER_HA}" = "true" ] || [ "${GRAFANA_HA}" = "true" ]; then
  logError "monitoring in High-Availability mode"
fi

# Prometheus persistence
if [ "${PROMETHEUS_PERSISTENCE}" = "true" ]; then
  logError "monitoring with remote persistence"
fi

# Filebeat
if [ "${INSTALL_FILEBEAT}" = "true" ]; then
  logError "installation of Filebeat (log exporter)"
fi

# Elasticsearch and Kibana
if [ "${INSTALL_ELASTICSEARCH}" = "true" ] || [ "${INSTALL_KIBANA}" = "true" ]; then
  logError "installation of Elasticsearch/Kibana"
fi

# IPFiltering feature in Ambassador
if [ "${INSTALL_IPFILTERING}" = "true" ]; then
  logError "installation of IPFiltering feature"
fi

# OUTOFSERVICE
# This should be limited at distribution level



