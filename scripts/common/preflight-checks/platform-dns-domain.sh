#!/bin/bash

# This script will check that the basic domains that must be publicly available
# in a cluster are registered in DNS and can be resolved:
#
# - ApiServer
# - Ingress
# - Cluster services domains:
#   - Admission
#   - Keycloak
#   - Grafana
#   - MinIO (if configured to store the cluster backups)
#
# For cluster services domains, the script will also check they resolve to the
# same IP address as the Ingress domain.
#
# NOTE: name resolution is verified using 'getent hosts' command, which
#       includes looking at the /etc/hosts file.


ADMISSION_DOMAIN="admission-${RELEASE_NAME}.${REFERENCE_DOMAIN}"
KEYCLOAK_DOMAIN="keycloak-${RELEASE_NAME}.${REFERENCE_DOMAIN}"
GRAFANA_DOMAIN="grafana-${RELEASE_NAME}.${REFERENCE_DOMAIN}"
MINIO_DOMAIN="minio-${RELEASE_NAME}.${REFERENCE_DOMAIN}"

if [ ! "${MANAGED_DNS}" = "true" ]; then

  echo  -e $COL_GREEN" **************************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" *  Pre-Flight check: Platform DNS Domains (unmanaged DNS)..."$COL_DEFAULT
  echo  -e $COL_GREEN" **************************************************************"$COL_DEFAULT
  echo
  echo "DNS is managed by operator. Checking if platform domains are correctly registered:"
  echo

  ERRORS=()
  INGRESS_IP=""

  DOMAIN="${APISERVER_DOMAIN}"
  echo "Domain      : ${DOMAIN}"
  DNS_IP=$(getent hosts ${DOMAIN} | awk '{ print $1 }')
  if [ -n "${DNS_IP}" ]; then
    echo -e "Resolves to : "$COL_GREEN"${DNS_IP}"$COL_DEFAULT
  else
    ERRORS+=(${DOMAIN})
    echo -e "Resolves to : "$COL_RED"COULD NOT BE RESOLVED"$COL_DEFAULT
  fi

  DOMAIN="${INGRESS_DOMAIN}"
  echo "Domain      : ${DOMAIN}"
  DNS_IP=$(getent hosts ${DOMAIN} | awk '{ print $1 }')
  if [ -n "${DNS_IP}" ]; then
    echo -e "Resolves to : "$COL_GREEN"${DNS_IP}"$COL_DEFAULT
    INGRESS_IP="${DNS_IP}"
  else
    ERRORS+=(${DOMAIN})
    echo -e "Resolves to : "$COL_RED"COULD NOT BE RESOLVED"$COL_DEFAULT
  fi

  # # For testing:
  # echo
  # INGRESS_IP="192.168.185.170"
  # echo "INGRESS IP: ${INGRESS_IP}"
  # echo
  
  DOMAIN="${ADMISSION_DOMAIN}"
  echo "Domain      : ${DOMAIN}"
  DNS_IP=$(getent hosts ${DOMAIN} | awk '{ print $1 }')
  if [ -n "${DNS_IP}" ]; then
    if [ -n "${INGRESS_IP}" ] && [ "${DNS_IP}" != "${INGRESS_IP}" ]; then
      ERRORS+=(${DOMAIN})
      echo -e "Resolves to : "$COL_YELLOW"${DNS_IP} - SHOULD MATCH INGRESS IP (${INGRESS_IP})."$COL_DEFAULT
    else
      echo -e "Resolves to : "$COL_GREEN"${DNS_IP}"$COL_DEFAULT
    fi
  else
    ERRORS+=(${DOMAIN})
    echo -e "Resolves to : "$COL_RED"COULD NOT BE RESOLVED"$COL_DEFAULT
  fi


  DOMAIN="${KEYCLOAK_DOMAIN}"
  echo "Domain      : ${DOMAIN}"
  DNS_IP=$(getent hosts ${DOMAIN} | awk '{ print $1 }')
  if [ -n "${DNS_IP}" ]; then
    if [ -n "${INGRESS_IP}" ] && [ "${DNS_IP}" != "${INGRESS_IP}" ]; then
      ERRORS+=(${DOMAIN})
      echo -e "Resolves to : "$COL_YELLOW"${DNS_IP} - SHOULD MATCH INGRESS IP (${INGRESS_IP})."$COL_DEFAULT
    else
      echo -e "Resolves to : "$COL_GREEN"${DNS_IP}"$COL_DEFAULT
    fi
  else
    ERRORS+=(${DOMAIN})
    echo -e "Resolves to : "$COL_RED"COULD NOT BE RESOLVED"$COL_DEFAULT
  fi

  DOMAIN="${GRAFANA_DOMAIN}"
  echo "Domain      : ${DOMAIN}"
  DNS_IP=$(getent hosts ${DOMAIN} | awk '{ print $1 }')
  if [ -n "${DNS_IP}" ]; then
    if [ -n "${INGRESS_IP}" ] && [ "${DNS_IP}" != "${INGRESS_IP}" ]; then
      ERRORS+=(${DOMAIN})
      echo -e "Resolves to : "$COL_YELLOW"${DNS_IP} - SHOULD MATCH INGRESS IP (${INGRESS_IP})."$COL_DEFAULT
    else
      echo -e "Resolves to : "$COL_GREEN"${DNS_IP}"$COL_DEFAULT
    fi
  else
    ERRORS+=(${DOMAIN})
    echo -e "Resolves to : "$COL_RED"COULD NOT BE RESOLVED"$COL_DEFAULT
  fi

  if [ "${INSTALL_MINIO}" == "true" ]; then
    DOMAIN="${MINIO_DOMAIN}"
    echo "Domain      : ${DOMAIN}"
    DNS_IP=$(getent hosts ${DOMAIN} | awk '{ print $1 }')
    if [ -n "${DNS_IP}" ]; then
      if [ -n "${INGRESS_IP}" ] && [ "${DNS_IP}" != "${INGRESS_IP}" ]; then
        ERRORS+=(${DOMAIN})
        echo -e "Resolves to : "$COL_YELLOW"${DNS_IP} - SHOULD MATCH INGRESS IP (${INGRESS_IP})."$COL_DEFAULT
      else
        echo -e "Resolves to : "$COL_GREEN"${DNS_IP}"$COL_DEFAULT
      fi
    else
      ERRORS+=(${DOMAIN})
      echo -e "Resolves to : "$COL_RED"COULD NOT BE RESOLVED"$COL_DEFAULT
    fi
  fi

  if [[ "${#ERRORS[@]}" -ne 0 ]]; then
    echo
    echo -e $COL_RED_BOLD"The following platform DNS domains are not correctly configured:"$COL_DEFAULT
    for ERR in ${ERRORS[@]}; do
      echo -e $COL_RED_BOLD"  - ${ERR}"$COL_DEFAULT
    done
    echo
    echo -e $COL_RED"In case of doubt, please refer to Kumori Platform documentation and tutorials."$COL_DEFAULT
    echo
    exit 1
  else
    echo
    echo -e $COL_GREEN"All platform DNS domains checked successfully."$COL_DEFAULT
    echo
  fi
else
  echo "DNS is managed by Kumori Platform."
fi
