#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################


################################################################################
##  ON PRIMARY MASTER NODE - KUBEADM INIT                                     ##
################################################################################
echo
echo -e $COL_BLUE"Initializing Kubernetes primary master..."$COL_DEFAULT
echo

# Find out the worker zone
NODE_ZONE=$( hostname )
for i in "${!MASTERS_IPS[@]}"; do
  if [[ "${MASTERS_IPS[$i]}" == "$NODE_IP" ]]; then
    if [[ "${MASTERS_ZONES[$i]}" != "" ]]; then
      NODE_ZONE="${MASTERS_ZONES[$i]}"
    fi
    break
  fi
done

# Prepare Kubeadm init configuration file
KUBEADM_INIT_CONFIG_FILE="$(pwd)/provision/kubeadm-config/kubeadm-init-config.yaml"
sed -i \
  -e "s?{{IP}}?${NODE_IP}?g" \
  -e "s?{{ZONE}}?${NODE_ZONE}?g" \
  -e "s?{{APISERVER_INTERNAL_PORT}}?${APISERVER_INTERNAL_PORT}?g" \
  -e "s?{{APISERVER_URL}}?${APISERVER_URL}?g" \
  -e "s?{{APISERVER_DOMAIN}}?${APISERVER_DOMAIN}?g" \
  -e "s?{{KUMORI_COREDNS_VERSION}}?${KUMORI_COREDNS_VERSION}?g" \
  -e "s?{{KUBERNETES_COREDNS_VERSION}}?${KUBERNETES_COREDNS_VERSION}?g" \
  -e "s?{{PODS_NETWORK_CIDR}}?${PODS_NETWORK_CIDR}?g" \
  -e "s?{{SERVICE_CIDR}}?${SERVICE_CIDR}?g" \
  -e "s?{{KUMORI_IMAGES_REGISTRY}}?${KUMORI_IMAGES_REGISTRY}?g" \
  -e "s?{{KUBERNETES_VERSION}}?${KUBERNETES_VERSION}?g" \
  -e "s?{{IS_INGRESS}}?${IS_INGRESS_NODE}?g" \
  -e "s?{{IS_STORAGE}}?${IS_STORAGE_NODE}?g" \
  "${KUBEADM_INIT_CONFIG_FILE}"

# Print Kubeadm init configuration file
echo
echo "Calling Kubeadm init with config file ${KUBEADM_INIT_CONFIG_FILE}:"
echo
cat "${KUBEADM_INIT_CONFIG_FILE}"
echo

sudo kubeadm init \
  --v=5 \
  --config "${KUBEADM_INIT_CONFIG_FILE}" \
  --upload-certs \
  > /tmp/init.log

CERT_KEY=$(sudo kubeadm init phase upload-certs --upload-certs --config "${KUBEADM_INIT_CONFIG_FILE}"| tail -1)

# Create a new bootstrap token, generate join commands for workers and master
# and save them to script files
JOIN_COMMAND=$(sudo kubeadm token create --print-join-command | tail -1)
echo ${JOIN_COMMAND} > /tmp/join-workers.sh

JOIN_COMMAND=$(sudo kubeadm token create --print-join-command --certificate-key ${CERT_KEY} | tail -1)
echo ${JOIN_COMMAND} > /tmp/join-masters.sh  

# cat /tmp/init.log | tail -2 | awk '{$1=$1};1' > /tmp/join-workers.sh
# cat /tmp/init.log | tail -12 | head -3 | awk '{$1=$1};1' > /tmp/join-masters.sh
