#!/bin/bash

# NOT USED
# This images are obsolet (kubernetes 1.16)

docker pull registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9
docker pull registry.gitlab.com/kumori-systems/docker-cache/calico-pod2daemon-flexvol:v3.8.9
docker pull registry.gitlab.com/kumori-systems/docker-cache/calico-cni:v3.8.9
docker pull registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-proxy:v1.16.10
docker pull registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-apiserver:v1.16.10
docker pull registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-controller-manager:v1.16.10
docker pull registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-scheduler:v1.16.10
docker pull registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-etcd:3.3.15-0
docker pull registry.gitlab.com/kumori-systems/docker-cache/quay.io-prometheus-node-exporter:v0.18.1
docker pull registry.gitlab.com/kumori-systems/docker-cache/quay.io-coreos-kube-rbac-proxy:v0.4.1
docker pull registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-pause:3.1
docker pull registry.gitlab.com/kumori-systems/docker-cache/amazon-aws-cli:2.0.8
docker pull registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-metrics-scraper:v1.0.3


docker tag registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9                           calico/node:v3.8.9
docker tag registry.gitlab.com/kumori-systems/docker-cache/calico-pod2daemon-flexvol:v3.8.9             calico/pod2daemon-flexvol:v3.8.9
docker tag registry.gitlab.com/kumori-systems/docker-cache/calico-cni:v3.8.9                            calico/cni:v3.8.9
docker tag registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-proxy:v1.16.10               k8s.gcr.io/kube-proxy:v1.16.10
docker tag registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-apiserver:v1.16.10           k8s.gcr.io/kube-apiserver:v1.16.10
docker tag registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-controller-manager:v1.16.10  k8s.gcr.io/kube-controller-manager:v1.16.10
docker tag registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-scheduler:v1.16.10           k8s.gcr.io/kube-scheduler:v1.16.10
docker tag registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-etcd:3.3.15-0                     k8s.gcr.io/etcd:3.3.15-0
docker tag registry.gitlab.com/kumori-systems/docker-cache/quay.io-prometheus-node-exporter:v0.18.1     quay.io/prometheus/node-exporter:v0.18.1
docker tag registry.gitlab.com/kumori-systems/docker-cache/quay.io-coreos-kube-rbac-proxy:v0.4.1        quay.io/coreos/kube-rbac-proxy:v0.4.1
docker tag registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-pause:3.1                         k8s.gcr.io/pause:3.1
docker tag registry.gitlab.com/kumori-systems/docker-cache/amazon-aws-cli:2.0.8                         amazon/aws-cli:2.0.8
docker tag registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-metrics-scraper:v1.0.3          kubernetesui/metrics-scraper:v1.0.3

docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9
docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-pod2daemon-flexvol:v3.8.9
docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-cni:v3.8.9
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-proxy:v1.16.10
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-apiserver:v1.16.10
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-controller-manager:v1.16.10
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-scheduler:v1.16.10
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-etcd:3.3.15-0
docker rmi registry.gitlab.com/kumori-systems/docker-cache/quay.io-prometheus-node-exporter:v0.18.1
docker rmi registry.gitlab.com/kumori-systems/docker-cache/quay.io-coreos-kube-rbac-proxy:v0.4.1
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-pause:3.1
docker rmi registry.gitlab.com/kumori-systems/docker-cache/amazon-aws-cli:2.0.8
docker rmi registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-metrics-scraper:v1.0.3
