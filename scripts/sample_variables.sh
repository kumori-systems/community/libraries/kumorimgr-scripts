#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

################################################################################
################################################################################
###                                                                          ###
###    THIS IS AN EXAMPLE OF ALL CONFIGURATION VARIABLES THAT CAN BE USED    ###
###    FOR MANAGING THE CLUSTER.                                             ###
###                                                                          ###
### For running any script, a 'variables.sh' must be present in the script   ###
### directory and it must follow this format, even if some variables don't   ###
### apply to the operation.                                                  ###
###                                                                          ###
################################################################################
################################################################################

################################################################################
## CLUSTER MACHINES IP ADDRESSES AND CONNECTIVITY                             ##
################################################################################

# MASTERS
MASTERS_IPS=(
  "10.0.1.237"
  "10.0.1.239"
  "10.0.1.238"
)

# MASTERS ZONES
MASTERS_ZONES=(
  ""
  ""
  ""
)

# WORKERS
WORKERS_IPS=(
  "10.0.1.240"
  "10.0.1.241"
  "10.0.1.242"
)

# WORKERS ZONES
WORKERS_ZONES=(
  ""
  ""
  ""
)

# Nodes that will be Ingress entrypoints
INGRESS_NODES_IPS=(
  "10.0.1.240"
  "10.0.1.241"
  "10.0.1.242"
)

STORAGE_NODES_IPS=(
  "10.0.1.240"
  "10.0.1.241"
  "10.0.1.242"
)

# Nodes in maintenance mode
MAINTENANCE_MASTERS_IPS=()
MAINTENANCE_WORKERS_IPS=()

# CLUSTER UPDATE DEFINITIONS
ADD_MASTERS_IPS=()
ADD_WORKERS_IPS=()
REMOVE_MASTERS_IPS=()
REMOVE_WORKERS_IPS=()

# Disable TX offload on these nodes
TX_OFF_NODES_IPS=()

# List of Storage nodes to add
ADD_STORAGE_IPS=()
# List of Storage nodes to remove
REMOVE_STORAGE_IPS=()
# List of Master nodes to put into maintenance
ADD_MAINTENANCE_MASTERS_IPS=()
# List of Worker nodes to put into maintenance
ADD_MAINTENANCE_WORKERS_IPS=()
# List of Master nodes to get back from maintenance
REMOVE_MAINTENANCE_MASTERS_IPS=()
# List of Worker nodes to get back from maintenance
REMOVE_MAINTENANCE_WORKERS_IPS=()

# SSH CONNECTION DETAILS
SSH_USER="ubuntu"
SSH_KEY="${HOME}/.ssh/ecloud_deployment_key"

# BASIC CONFIGURATION
DEFAULT_LOCALE="es_ES.UTF-8"
TIMEZONE="CET"

################################################################################
## INSTALLATION CUSTOMIZATION PROPERTIES                                      ##
################################################################################

CLUSTER_VERSION="1.1.0"
RELEASE_NAME="kha"
REFERENCE_DOMAIN="test.kumori.cloud"
# This directory must exist and contain the SSL certificate files for reference
# domain, with a strict naming schema: "wildcard.<reference-domain>.[crt|key]"
# Example:
# - wildcard.test.kumori.cloud.crt
# - wildcard.test.kumori.cloud.key
REFERENCE_DOMAIN_CERT_DIR="${HOME}/clusterCerts"

# Kumori PKI: RootCA and Tool ICA
CLUSTER_PKI_ROOT_CA_FILE="${HOME}/certs/rootCA/ca.crt"


KUMORICTL_SUPPORTED_VERSIONS=(
  "1.1.0"
)

KUMORIMGR_SUPPORTED_VERSIONS=(
  "1.0.2"
)

#######################
##     APISERVER     ##
#######################

# APISERVER - GENERAL SETTINGS
#
# ApiServer Endpoint configuration (ApiServer domain, LB IP or master IP)
APISERVER_DOMAIN="apiserver-${RELEASE_NAME}.${REFERENCE_DOMAIN}"
APISERVER_EXTERNAL_PORT="8443"
# Domains or IPs to be added as accepted in ApiServer TLS certificate
# If several, set as comma separated string list (no spaces)
APISERVER_CERT_EXTRA_SANS=""

# APISERVER - MANAGE APISERVER DNS DOMAIN
#
# If enabled, the folowing configuration settings are mandatory:
# - APISERVER_DNS_IP / APISERVER_DNS_CNAME : the IP or CNAME to be registered in DNS for ApiServer domain
# - APISERVER_DOMAIN_TTL
APISERVER_REGISTER_DOMAIN="true"
APISERVER_DNS_IP="192.168.185.154"
APISERVER_DNS_CNAME=""  # CURRENTLY NOT USED
APISERVER_DNS_TTL="300"

# APISERVER - INTERNAL BALANCING
#
# Install an internal load-balancer in every Master node to balance ApiServer
# requests among all endpoints.
# Internal load-balancers are automatically configured to listen on APISERVER_EXTERNAL_PORT
# and their target list includes all Master nodes at ApiServer on APISERVER_INTERNAL_PORT.
APISERVER_INTERNAL_BALANCING="true"

# Automatically set up an external ApiServer Load Balancer (used for ClusterAPI setups)
APISERVER_AUTO_BALANCING="false"

# ApiServer external port (used by clients) depends on wether internal load
# balancing is enabled or not.
# ApiServer internal port is fixed and currently not configurable (6443).
if [ "${APISERVER_INTERNAL_BALANCING}" = "true" ]; then
  # If balancing is internal, ignore external port set by operator
  APISERVER_EXTERNAL_PORT="8443"
  APISERVER_INTERNAL_PORT="6443"
else
  # If balancing isn't internal use external port set by operator or default
  APISERVER_EXTERNAL_PORT="${APISERVER_EXTERNAL_PORT:-6443}"
  APISERVER_INTERNAL_PORT="6443"
fi

# APISERVER - HANDLE FLOATING IP
#
# User provides a Virtual IP for ApiServer, and the platform is in charge of ensuring
# that IP is always assigned to a healthy Master node.
# If enabled, the folowing configuration settings are mandatory:
# - APISERVER_VIRTUAL_IP           (user provided)
APISERVER_HANDLE_FLOATING_IP="true"
APISERVER_VIRTUAL_IP="10.0.0.100"

# APISERVER URL - used for publishing ApiServer to the outside
APISERVER_URL="${APISERVER_DOMAIN}:${APISERVER_EXTERNAL_PORT}"

# Virtual Router ID for ApiServer Keepalived configuration (optional because we use unicast)
APISERVER_VRID=""

#####################
##     INGRESS     ##
#####################
# INGRESS - GENERAL SETTINGS
#
# Ingress domain
INGRESS_DOMAIN="ingress-${RELEASE_NAME}.${REFERENCE_DOMAIN}"
# Namespace where the Ingress controller will be deployed
INGRESS_NAMESPACE="kumori"

# Format (PEM/XFFC) for the client-certificate header
INGRESS_CERTHEADER_FORMAT="PEM"

# INGRESS - INTERNAL BALANCING
#
# Install an internal load-balancer in every Ingress node to balance Ingress
# requests among all endpoints.
# Internal load-balancers are automatically configured to listen on ports INGRESS_EXTERNAL_PORT
# (always 443) and their target list includes all Ingress nodes on INGRESS_INTERNAL_PORT (8443).
INGRESS_INTERNAL_BALANCING="true"

# Automatically set up an external Ingress Load Balancer (used for ClusterAPI setups)
INGRESS_AUTO_BALANCING="false"

# Internal Ingress HTTPS port (where Ingress pods listen on host network) depends on
# whether internal load balancing is enabled or not.
# External Ingress HTTPS port will always be 443.
if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  INGRESS_EXTERNAL_PORT="443"
  INGRESS_INTERNAL_PORT="8443"
else
  INGRESS_EXTERNAL_PORT="443"
  INGRESS_INTERNAL_PORT="443"
fi

# List of ports that ingress will listen to for TCP connections.
INGRESS_TCP_PORTS=(
  9031
  9032
)

# Assign an internal port to each TCP port. If internal load balancing is disabled, the internal
# and external ports will be the same.
INGRESS_TCP_INTERNAL_PORTS=()
if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ]; then
  for PORT in ${INGRESS_TCP_PORTS[@]}; do
    # Currently allowed TCP ports are in the range 9000-9999, so we will just add
    # 10000 to convert them to internal: 19000-19999
    INGRESS_TCP_INTERNAL_PORTS+=("1${PORT}")
  done
else
  # Simply copy the original TCP port list
  INGRESS_TCP_INTERNAL_PORTS=( "${INGRESS_TCP_PORTS[@]}" )
fi

# INGRESS - HANDLE FLOATING IP
#
# User provides a Virtual IP for Ingress, and the platform is in charge of ensuring
# that IP is always assigned to a healthy Ingress node.
# If enabled, the folowing configuration settings are mandatory:
# - INGRESS_VIRTUAL_IP           (user provided)
INGRESS_HANDLE_FLOATING_IP="true"
INGRESS_VIRTUAL_IP="10.0.0.101"

# INGRESS - MANAGE INGRESS DNS DOMAIN
#
# If enabled, the folowing configuration settings are mandatory:
# - INGRESS_DOMAIN
# - INGRESS_DNS_IP / INGRESS_DNS_CNAME: the IP or CNAME to be registered in DNS for ingress domain
# - INGRESS_DOMAIN_TTL
INGRESS_REGISTER_DOMAIN="true"
INGRESS_DNS_IP="192.168.185.156"
INGRESS_DNS_CNAME=""  # CURRENTLY NOT USED
INGRESS_DNS_TTL="300"

# Virtual Router ID for Ingress Keepalived configuration (optional because we use unicast)
INGRESS_VRID=""

##############################
##  STORAGE CONFIGURATION   ##
##############################
#
# The following variables describe the different volume types that will be
# available to users. For each volumetype we specify:
# - a name
# - a StorageClass (used to provide volumes of that type)
# - a list of properties (a comma-separated list of words)

VOLUME_TYPE_NAMES=(
	"cinder-regular"
	"cinder-volatile"
	"nfs-persistent"
	"nfs-shared-persistent"
	"nfs-shared-volatile"
	"nfs-volatile"
	"openebs-cstor"
	"openebs-lvm"
)

VOLUME_TYPE_STORAGECLASSES=(
  "csi-cinder-persistent"
  "csi-cinder-volatile"
  "csi-nfs-persistent"
  "csi-nfs-persistent"
  "csi-nfs-volatile"
  "csi-nfs-volatile"
  "openebs-replicated"
  "openebs-locallvm"
)

VOLUME_TYPE_PROPERTIES=(
	"persistent"
	"volatile"
	"persistent"
	"persistent,shared"
	"volatile,shared"
	"volatile"
	"persistent"
	"volatile"
)


##############################################
##  OPENEBS STORAGE PROVIDER CONFIGURATION  ##
##############################################

# Install OpenEBS storage provider or not
INSTALL_OPENEBS_PROVIDER="true"

# Node Device Manager: list of devices to ignore (don't manage these devices).
OPENEBS_STORAGE_IGNORED_DEVICES=(
  "/dev/vdc"
  "/dev/vdj"
)

#
# OPENEBS REPLICATED CSTOR CONFIGURATION
#
OPENEBS_STORAGE_REPLICATED_ENABLED="true"
# Replication level
OPENEBS_STORAGE_REPLICATED_REPLICATION_LEVEL="3"
# Disk devices to use
#
# For each Storage Node, a dedicated disk (clean, with no partitions)
OPENEBS_STORAGE_REPLICATED_DEVICES=(
	"/dev/vdb"
	"/dev/vdb"
	"/dev/vdb"
)

#
# OPENEBS LOCAL PV HOSTPATH CONFIGURATION
#
OPENEBS_STORAGE_HOSTPATH_ENABLED="true"
# Directory tobe used as base dir in all the Storage Nodes.
# If the directory doesn't exist it will be created.
OPENEBS_STORAGE_HOSTPATH_DIR="/var/lib/kumori"


#
# OPENEBS LOCAL PV LVM CONFIGURATION
#
OPENEBS_STORAGE_LVM_ENABLED="true"
# LVM VolumeGroup to use. The LVM VolumeGroup must exist in all Storage Nodes.
OPENEBS_STORAGE_LVM_VOLUMEGROUP="lvm-volumegroup-kumori-workers"


###########################################################
##  OPENSTACK CSI CINDER STORAGE PROVIDER CONFIGURATION  ##
###########################################################
INSTALL_CSI_CINDER_PROVIDER="true"

CSI_CINDER_OPENSTACK_CLOUDS_YAML_BASE64="dW5vCmRvcwp0cmVzCmN1YXRybwoK"
CSI_CINDER_OPENSTACK_CLOUDS_YAML_KEY="openstack"

# CSI Openstack Cinder classes details
CSI_CINDER_OPENSTACK_CLASS_NAMES=(
  "persistent"
  "persistent-high-speed"
  "volatile"
)
CSI_CINDER_OPENSTACK_CLASS_CONFIGURATIONS=(
  '{"volumeType":""}'
  '{"volumeType":"high-speed"}'
  '{"volumeType":""}'
)
CSI_CINDER_OPENSTACK_CLASS_PROPERTIES=(
  'persistent'
  'persistent'
  'volatile'
)


###########################################################
##       CSI NFS STORAGE PROVIDER CONFIGURATION          ##
###########################################################
INSTALL_CSI_NFS_PROVIDER="true"

# CSI NFS classes details
CSI_NFS_CLASS_NAMES=(
  "persistent"
  "persistent-shared"
  "volatile"
  "volatile-shared"
)
CSI_NFS_CLASS_CONFIGURATIONS=(
  '{"server":"192.168.1.100","sharePath":"/"}'
  '{"server":"192.168.1.100","sharePath":"/"}'
  '{"server":"192.168.1.100","sharePath":"/"}'
  '{"server":"192.168.1.100","sharePath":"/"}'
)
CSI_NFS_CLASS_PROPERTIES=(
  'persistent'
  'persistent,shared'
  'volatile'
  'volatile,shared'
)


###########################################################
##      CSI CEPH RBD STORAGE PROVIDER CONFIGURATION      ##
###########################################################
INSTALL_CSI_CEPH_RBD_PROVIDER="true"

CSI_CEPH_RBD_HA="true"

# Ceph clusters configuration
CSI_CEPH_RBD_CLUSTER_NAMES=(
  "cluster-01"
  "cluster-02"
  "cluster-03"
)
CSI_CEPH_RBD_CLUSTER_IDS=(
  "12345678-1234-1234-1234-111111111111"
  "12345678-1234-1234-1234-222222222222"
  "12345678-1234-1234-1234-333333333333"
)
CSI_CEPH_RBD_CLUSTER_MONITORS=(
  '["192.168.0.10:6789","192.168.0.11:6789","192.168.0.12:6789"]'
  '["192.168.0.20:6789","192.168.0.21:6789","192.168.0.22:6789"]'
  '["192.168.0.30:6789","192.168.0.31:6789","192.168.0.32:6789"]'
)

# CSI Ceph RBD classes details
CSI_CEPH_RBD_CLASS_NAMES=(
  "cluster01-pool1"
  "cluster01-pool2"
  "cluster02-pool1"
)

CSI_CEPH_RBD_CLASS_CONFIGURATIONS=(
  '{"clusterID":"12345678-1234-1234-1234-111111111111","fsType":"ext4","poolName":"rbd-pool-1","poolUserID":"username","poolUserKey":"password"}'
  '{"clusterID":"12345678-1234-1234-1234-111111111111","fsType":"ext4","poolName":"rbd-pool-2","poolUserID":"username2","poolUserKey":"password2"}'
  '{"clusterID":"12345678-1234-1234-1234-222222222222","fsType":"ext4","poolName":"rbd-pool-1","poolUserID":"username3","poolUserKey":"password3"}'
)

CSI_CEPH_RBD_CLASS_PROPERTIES=(
  'volatile'
  'persistent'
  'persistent'
)


#################################################################
## PERIODICALLY FLUSH PAGE CACHE (fix for Ambassador Ingress)  ##
#################################################################
PERIODICALLY_DROP_CACHES="false"
DROP_CACHES_SCHEDULE="00 11 * * *"

############################
##     DNS MANAGEMENT     ##
############################
# If true, the platform is in charge of managing *all* cluster related DNS domains.
# If false, the operator is in charge of managing *all* cluster related DNS domains.
MANAGED_DNS="true"
MANAGED_DNS_PROVIDER="route53"  # Supported values are 'route53' and 'ovh'
# AWS Route53 specific configuration
MANAGED_DNS_ROUTE53_CLI_IMAGE="amazon/aws-cli:2.0.8"
MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR="${HOME}/awsConfig"
# OVH DNS specific configuration
MANAGED_DNS_OVH_CLI_IMAGE="kumoripublic/kovhcli:v0.0.1"
MANAGED_DNS_OVH_CONFIG_FILE="${HOME}/ovhConfig/ovh-kumori2-externaldns-creds.ini"

########################
##     NETWORKING     ##
########################
# IMPORTANT WARNINGS:
# - these CIDRs should never overlap
# - these CIDRs should never overlap with other clusters on the same network
# - Docker CIDR must be based on a valid IP, since that IP will be set for the
#   docker0 interface. This means the broadcast address (x.x.x.0) IS NOT valid.
# - CIDR syntax (reminder):
#     - 172.20.0.1/24  -->  172.20.0.x
#     - 172.20.0.1/16  -->  172.20.x.x
#     - 172.20.0.1/8   -->  172.x.x.x
# - Valid private CIDR (reminder)
#     - 10.0.0.0    - 10.255.255.255
#     - 172.16.0.0  - 172.31.255.255
#     - 192.168.0.0 - 192.168.255.255
DOCKER_BRIDGE_CIDR="172.20.0.1/16"
PODS_NETWORK_CIDR="172.21.0.0/16"
SERVICE_CIDR="172.22.0.0/16"

# Optional: DNS resolvers for replacing DHCP configuration.
# This is *very* recommended for clusters deployed on ITI's Openstack.
DNS_RESOLVERS=(
  "8.8.8.8"
  "8.8.4.4"
)


################################
##     KUMORI CONTROLLERS     ##
################################

INSTALL_KUALARM="true"

INSTALL_VOLUME_CONTROLLER="true"

ADMISSION_AUTHENTICATION_TYPE="clientcertificate"
ADMISSION_SERVER_CERT_DIR="${HOME}/certs/leaf/admission"
ADMISSION_CLIENT_CERT_REQUIRED="true"
ADMISSION_SERVER_CLIENT_CERT_VALIDATE_WITH_TRUSTED_CA="false"
ADMISSION_ADMIN_PASSWORD="admin"
ADMISSION_DEVEL_PASSWORD="devel"
ADMISSION_ALLOWEDIP=''

KUINBOUND_MINTLSVERSION="v1.2"
KUINBOUND_MAXTLSVERSION=""
KUINBOUND_CIPHERSUITETLS="ECDHE-RSA-AES256-GCM-SHA384,ECDHE-RSA-AES128-GCM-SHA256,ECDHE-RSA-CHACHA20-POLY1305"
KUINBOUND_ECDHCURVESTLS=""
KUINBOUND_HSTS="true"

KUCONTROLLER_DEFAULT_LIVENESS_TIMEOUT="1s"
KUCONTROLLER_DEFAULT_READINESS_TIMEOUT="1s"
KUCONTROLLER_TSC_MAX_SKEW="1"
KUCONTROLLER_REVISION_HISTORY_LIMIT="10"
KUCONTROLLER_FORCE_REBOOT_ON_UPDATE="false"
KUCONTROLLER_DEFAULT_DISK_REQUEST_SIZE="1"
KUCONTROLLER_DEFAULT_DISK_REQUEST_UNIT="Gi"
KUCONTROLLER_DEFAULT_VOLATILE_VOLUMES_TYPE="volatile1"
KUCONTROLLER_EXTERNAL_SERVICES_ACCESS=""

INSTALL_IPFILTERING="true"
IPFILTERING_REJECTIFFAILURE="false"
IPFILTERING_TIMEOUTMSEC="20"
IPFILTERING_MAXLISTSIZE="50"
IPFILTERING_LOGLEVEL="info"

INSTALL_AUTHSERVICE="true"

####################
##     ADDONS     ##
####################

ADDONS_STORAGE_CLASS="openebs-localhostpath"

INSTALL_CALICO="true"

AMBASSADOR_XFF_TRUSTED_HOPS="0"
AMBASSADOR_APIEXT_HA="true"
AMBASSADOR_APIEXT_REPLICAS="3"

INSTALL_KUBE_PROMETHEUS="true"
MONITORING_REQUIRE_CLIENTCERT="true"
MONITORING_CLIENTCERT_TRUSTED_CA_FILE="${HOME}/certs/toolICA/ca.crt"
PROMETHEUS_PERSISTENCE="false"
PROMETHEUS_REMOTE_READ_URL="https://victoria-metrics.mydomain.com/insert/prometheus/api/v1/read"
PROMETHEUS_REMOTE_WRITE_URL="https://victoria-metrics.mydomain.com/insert/prometheus/api/v1/write"
PROMETHEUS_REMOTE_AUTH_TYPE="none"   # Supported: none, clientcertificate
PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_CERT_FILE="${HOME}/certs/monitoring-client.crt"
PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_KEY_FILE="${HOME}/certs/monitoring-client.key"
GRAFANA_REMOTE_DATASOURCE_NAME="Remote Write"
PROMETHEUS_RETENTION_TIME="3d"
PROMETHEUS_ALLOWEDIP='["192.168.20.0/32"]'
PROMETHEUS_HA="true"
PROMETHEUS_HA_REPLICAS="3"

INSTALL_CADVISOR="true"

ALERTMANAGER_HA="true"
ALERTMANAGER_HA_REPLICAS="3"
ALERTMANAGER_ALLOWEDIP='["192.168.20.0/32"]'

GRAFANA_HA="true"
GRAFANA_HA_REPLICAS="3"
GRAFANA_ADMIN_USERNAME="grafadmin"
GRAFANA_ADMIN_PASSWORD="s3cr3t"
GRAFANA_VIEWER_USERNAME="grafviewer"
GRAFANA_VIEWER_PASSWORD="v13w3r"
# Configure OAuth role mapping to be strict or not (forbid access if user has no Grafana role)
GRAFANA_OAUTH_ROLE_STRICT="false"
# Enable 'cluster' selector in grafana Dasboards
GRAFANA_ENABLE_MULTICLUSTER_SUPPORT="true"
GRAFANA_ALLOWEDIP='["192.168.20.0/32"]'

INSTALL_ELASTICSEARCH="false"
ELASTICSEARCH_REPLICAS="3"
ELASTICSEARCH_VOLUME_SIZE="1Gi"

INSTALL_KIBANA="false"
KIBANA_ELASTICSEARCH_URL="http://elasticsearch-master:9200"
KIBANA_ELASTICSEARCH_USERNAME="username"
KIBANA_ELASTICSEARCH_PASSWORD="password"
KIBANA_ALLOWEDIP='["192.168.20.0/32"]'

INSTALL_FILEBEAT="true"
FILEBEAT_ELASTICSEARCH_URL="http://elasticsearch-master:9200"
FILEBEAT_ELASTICSEARCH_USERNAME="username"
FILEBEAT_ELASTICSEARCH_PASSWORD="password"
FILEBEAT_INDEX_PATTERN="mycluster-logs-%{+yyyy.MM.dd}"
FILEBEAT_ENABLE_USER_SERVICES_LOGS="false"

INSTALL_KEYCLOAK="true"
KEYCLOAK_ADMIN_USERNAME="username"
KEYCLOAK_ADMIN_PASSWORD="password"
KEYCLOAK_ALLOWEDIP='["192.168.20.0/32"]'
# Create UUIDs for configuring Keycloak Admission and Grafana clients secrets
KEYCLOAK_ADMISSION_CLIENT_SECRET="$(cat /proc/sys/kernel/random/uuid)"
KEYCLOAK_GRAFANA_CLIENT_SECRET="$(cat /proc/sys/kernel/random/uuid)"

INSTALL_KUBERNETES_DASHBOARD="false"

INSTALL_MINIO="true"
MINIO_ACCESS_KEY="minio"
MINIO_SECRET_KEY="kumoriminio"
MINIO_ALLOWEDIP='["192.168.20.0/32"]'

INSTALL_ETCD_BACKUP="true"
ETCD_BACKUP_ETCD_ENDPOINT="https://localhost:2379"
ETCD_BACKUP_SCHEDULE="*/30 * * * *"
ETCD_BACKUP_DELTA_PERIOD="5m"
ETCD_BACKUP_S3_ENDPOINT="https://minio-${RELEASE_NAME}.${REFERENCE_DOMAIN}"
ETCD_BACKUP_S3_REGION=""
ETCD_BACKUP_S3_ACCESS_KEY="minio"
ETCD_BACKUP_S3_SECRET_KEY="kumoriminio"
ETCD_BACKUP_S3_BUCKET="clustersbackups"
ETCD_BACKUP_STORAGE_PREFIX="${RELEASE_NAME}-backups"
ETCD_BACKUP_TRUSTED_CA_CONFIGMAP=""

PKI_BACKUP="true"
PKI_BACKUP_S3_ENDPOINT="https://minio-${RELEASE_NAME}.${REFERENCE_DOMAIN}"
PKI_BACKUP_S3_REGION=""
PKI_BACKUP_S3_ACCESS_KEY="minio"
PKI_BACKUP_S3_SECRET_KEY="kumoriminio"
PKI_BACKUP_S3_BUCKET="pki"
PKI_BACKUP_STORAGE_PREFIX="${RELEASE_NAME}-backups"

# ExternalDNS will only be installed if platform is in charge of DNS management.
# It will be configured according to the MANAGED_DNS_PROVIDER.
INSTALL_EXTERNALDNS="${MANAGED_DNS}"
EXTERNALDNS_OWNER_ID="kumori-cluster-${RELEASE_NAME}"
EXTERNALDNS_INTERVAL="5m"
# AWS Route 53 provider specific settings
EXTERNALDNS_ROUTE53_AWSBATCHCHANGESIZE="100"
EXTERNALDNS_ROUTE53_AWSBATCHCHANGEINTERVAL="6s"

# OutOfService image configuration
OUTOFSERVICE_REGISTRY="docker.io"
OUTOFSERVICE_IMAGE="kumori/outofservice:v1.0.1"
OUTOFSERVICE_USERNAME=""
OUTOFSERVICE_PASSWORD=""

# Configure where platform events are stored.
# Supported store types:
# - 'k8s': events are stored in the Kubernetes cluster (no configuration required)
# - 'elasticsearch': events are stored in an external Elastcsearch database.
#
# Selecting an Elasticsearch store, will have the following effect:
# - Event-Exporter will be installed and configured to send all platform events to the
#   configured Elasticsearch server
# - Admission will be configured for using Elasticsearch as the source for platform Events
EVENTS_STORE_TYPE="elasticsearch"

# Deploy Event-Exporter in High Availability mode
EVENTS_EXPORTER_HA="true"
EVENTS_EXPORTER_HA_REPLICAS="3"
# Elasticsearch backend configuration used by Event-Exporter
EVENTS_ELASTICSEARCH_URL="http://elasticsearch-${RELEASE_NAME}.${REFERENCE_DOMAIN}:9200/"
EVENTS_ELASTICSEARCH_USERNAME=""
EVENTS_ELASTICSEARCH_PASSWORD=""
EVENTS_ELASTICSEARCH_INDEX_PREFIX="${RELEASE_NAME}-events"
EVENTS_ELASTICSEARCH_INDEX_DATE_PATTERN="{2006-01-02}"

# Calculated from the above, do not edit
EVENTS_ELASTICSEARCH_INDEX="${EVENTS_ELASTICSEARCH_INDEX_PREFIX}-${EVENTS_ELASTICSEARCH_INDEX_DATE_PATTERN}"
EVENTS_ELASTICSEARCH_INDEX_ADMISSION="${EVENTS_ELASTICSEARCH_INDEX_PREFIX}-*"


# Determine if Helm must be installed. Currently only needed for installing EFK addon.
if [ "${INSTALL_FILEBEAT}" = "true" ] || \
   [ "${INSTALL_ELASTICSEARCH}" = "true" ] || \
   [ "${INSTALL_KIBANA}" = "true" ]
then
  INSTALL_HELM="true"
else
  INSTALL_HELM="false"
fi

# Configure the descheduler strategies
INSTALL_DESCHEDULER="true"
DESCHEDULER_NODE_SELECTOR="kumori/role=worker"
DESCHEDULER_INTERVAL="5m"
DESCHEDULER_SPREAD_ENABLED="true"
DESCHEDULER_SPREAD_PRIORITY_CLASS="system-cluster-critical"
DESCHEDULER_LOW_ENABLED="true"
DESCHEDULER_LOW_PRIORITY_CLASS="openebs-csi-controller-critical"
DESCHEDULER_LOW_THRESHOLD_CPU="30"
DESCHEDULER_LOW_THRESHOLD_MEM="30"
DESCHEDULER_LOW_THRESHOLD_POD="30"
DESCHEDULER_LOW_TARGET_CPU="30"
DESCHEDULER_LOW_TARGET_MEM="30"
DESCHEDULER_LOW_TARGET_POD="30"

# Install CertManager
INSTALL_CERT_MANAGER="true"

# Install CertManager Webhook OVH
INSTALL_CERT_MANAGER_WEBHOOK_OVH="true"
CERT_MANAGER_WEBHOOK_OVH_CREDENTIALS_BASE64="dW5vCmRvcwp0cmVzCmN1YXRybwoK=="
CERT_MANAGER_WEBHOOK_OVH_EMAIL="me@mycompany.com"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_LETSENCRYPT_ENABLED="false"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_LETSENCRYPT_USE_STAGING="false"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_ENABLED="false"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_KEY_ID="abc"
CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_EABKEYHMAC="xyz"


################################
##     ADDONS - CLUSTERAPI    ##
################################
INSTALL_CLUSTERAPI="true"
if [ "${INSTALL_CLUSTERAPI}" = "true" ]; then
  # CertManager is a requirement for running ClusterAPI
  INSTALL_CERT_MANAGER="true"
fi

INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_OPENSTACK="true"
INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_AWS="true"
if [ "${INSTALL_CLUSTERAPI}" = "false" ]; then
  # ClusterAPI providers require ClusterAPI to be installed
  INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_OPENSTACK="false"
  INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_AWS="false"
fi

##################################################
##     ADDONS - IAAS-SPECIFIC CLOUD-MANAGERS    ##
##################################################
INSTALL_OPENSTACK_CLOUD_CONTROLLER="false"
if [ "${KUMORI_IAAS}" = "openstack" ]; then
  INSTALL_OPENSTACK_CLOUD_CONTROLLER="true"
fi
OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_FILE="${HOME}/openstack-cloud-credentials/clouds.yaml"
OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_KEY="openstack"

INSTALL_AWS_CLOUD_CONTROLLER="false"
if [ "${KUMORI_IAAS}" = "aws" ]; then
  INSTALL_AWS_CLOUD_CONTROLLER="true"
fi


################################################################################
## RESOURCES ASSIGNED TO PLATFORM ELEMENTS                                     #
################################################################################

# CoreDNS
COREDNS_CPU_REQUEST="100m"
COREDNS_CPU_LIMIT=""
COREDNS_MEM_REQUEST="70Mi"
COREDNS_MEM_LIMIT="170Mi"

# Ambassador
AMBASSADOR_CPU_REQUEST="200m"
AMBASSADOR_CPU_LIMIT="1000m"
AMBASSADOR_MEM_REQUEST="300Mi"
AMBASSADOR_MEM_LIMIT="800Mi"
AMBASSADOR_APIEXT_CPU_REQUEST="75m"
AMBASSADOR_APIEXT_CPU_LIMIT="150m"
AMBASSADOR_APIEXT_MEM_REQUEST="100Mi"
AMBASSADOR_APIEXT_MEM_LIMIT="200Mi"

# Prometheus
PROMETHEUS_CPU_REQUEST="300m"
PROMETHEUS_CPU_LIMIT="1000m"
PROMETHEUS_MEM_REQUEST="500Mi"
PROMETHEUS_MEM_LIMIT="1500Mi"

# Config Reloader
PROMETHEUS_CONFIG_RELOADER_CPU_REQUEST="100m"
PROMETHEUS_CONFIG_RELOADER_CPU_LIMIT="100m"
PROMETHEUS_CONFIG_RELOADER_MEM_REQUEST="50Mi"
PROMETHEUS_CONFIG_RELOADER_MEM_LIMIT="50Mi"

# AlertManager
ALERTMANAGER_CPU_REQUEST="100m"
ALERTMANAGER_CPU_LIMIT="200m"
ALERTMANAGER_MEM_REQUEST="100Mi"
ALERTMANAGER_MEM_LIMIT="500Mi"

# Grafana
GRAFANA_CPU_REQUEST="100m"
GRAFANA_CPU_LIMIT="200m"
GRAFANA_MEM_REQUEST="100Mi"
GRAFANA_MEM_LIMIT="200Mi"

# CAdvisor
CADVISOR_CPU_REQUEST="400m"
CADVISOR_CPU_LIMIT="800m"
CADVISOR_MEM_REQUEST="400Mi"
CADVISOR_MEM_LIMIT="2000Mi"

# Filebeat
FILEBEAT_CPU_REQUEST="100m"
FILEBEAT_CPU_LIMIT="1000m"
FILEBEAT_MEM_REQUEST="150Mi"
FILEBEAT_MEM_LIMIT="250Mi"

# Keycloak
KEYCLOAK_CPU_REQUEST="1000m"
KEYCLOAK_CPU_LIMIT="1500m"
KEYCLOAK_MEM_REQUEST="700Mi"
KEYCLOAK_MEM_LIMIT="1500Mi"

# OPENEBS
OPENEBS_REPLICATED_CPU_REQUEST="250m"
OPENEBS_REPLICATED_CPU_LIMIT="500m"
OPENEBS_REPLICATED_MEM_REQUEST="500Mi"
OPENEBS_REPLICATED_MEM_LIMIT="1Gi"
OPENEBS_REPLICATED_AUX_CPU_REQUEST="100m"
OPENEBS_REPLICATED_AUX_CPU_LIMIT="200m"
OPENEBS_REPLICATED_AUX_MEM_REQUEST="250Mi"
OPENEBS_REPLICATED_AUX_MEM_LIMIT="500Mi"

# DESCHEDULER
DESCHEDULER_CPU_REQUEST="500m"
DESCHEDULER_CPU_LIMIT="500m"
DESCHEDULER_MEM_REQUEST="256Mi"
DESCHEDULER_MEM_LIMIT="256Mi"

# IpFiltering
IPFILTERING_CPU_REQUEST="250m"
IPFILTERING_CPU_LIMIT="750m"
IPFILTERING_MEM_REQUEST="50mi"
IPFILTERING_MEM_LIMIT="150mi"

# AuthService
AUTHSERVICE_CPU_REQUEST="250m"
AUTHSERVICE_CPU_LIMIT="750m"
AUTHSERVICE_MEM_REQUEST="100mi"
AUTHSERVICE_MEM_LIMIT="250mi"

################################################################################
## PATHS                                                                      ##
################################################################################

CUSTOM_DIR=""
# CUSTOM_DIR="${LOCAL_WORKDIR}/../sample-custom-scripts"
JOIN_SCRIPTS_DIR="${LOCAL_WORKDIR}/join-scripts"
PATCH_DIR=""

################################################################################
## ETCD BACKUP                                                                ##
################################################################################

USE_ETCDBACKUP="false"
ETCDBACKUP_FILE=""

################################################################################
## VERSIONS                                                                   ##
################################################################################
OS_FLAVOUR="ubuntu"
OS_VERSION="20.04"
KERNEL_VERSION="5.15.0"
SYSTEMD_VERSION=""   # Deprecated for Ubuntu 20.04 installer
KUBERNETES_VERSION="1.21.10"
HELM_VERSION="v2.17.0"
DOCKER_VERSION="5:20.10.10~3-0~ubuntu-focal"
CONTAINERD_VERSION="1.6.18-1"
CRI_DOCKERD_VERSION="0.3.1"
KEEPALIVED_VERSION="1:2.0.19*"
ENVOY_VERSION="1.16.5"
YQ_VERSION="3.3.2"

CALICO_VERSION="v3.19"
AMBASSADOR_VERSION="3.4.1"
KUBE_PROMETHEUS_VERSION="release-0.9"
ELASTICSEARCH_VERSION="7.4.0"
FILEBEAT_VERSION="7.4.0"
KIBANA_VERSION="7.4.0"
K8S_DASHBOARD_VERSION="2.0.0-rc5"
KEYCLOAK_VERSION="19.0.3"
MINIO_VERSION="RELEASE.2020-03-19T21-49-00Z"
MINIO_MC_VERSION="RELEASE.2020-03-14T01-23-37Z"
ETCD_BACKUP_VERSION="0.0.2"
ETCD_RESTORE_IMAGE="k8s.gcr.io/etcd:3.4.13-0"
EXTERNALDNS_VERSION="v0.8.0"
EVENT_EXPORTER_VERSION="v0.9"
OPENEBS_VERSION="3.1.0"
DESCHEDULER_VERSION="0.22"
CADVISOR_VERSION="0.47.0"
CSI_CINDER_PROVIDER_VERSION="2.28.1"
CSI_NFS_PROVIDER_VERSION="4.5.0"
CSI_CEPH_RBD_PROVIDER_VERSION="3.10.1"
CERT_MANAGER_VERSION="1.13.3"
CERT_MANAGER_WEBHOOK_OVH_VERSION="0.3.1"
CLUSTERAPI_VERSION="1.8.4"
CLUSTERAPI_OPENSTACK_PROVIDER_VERSION="0.10.5"
CLUSTERAPI_AWS_PROVIDER_VERSION="2.6.1"
OPENSTACK_CLOUD_CONTROLLER_VERSION="1.30.1"
AWS_CLOUD_CONTROLLER_VERSION="1.25.13"

KUMORI_IMAGES_PULL_POLICY="Always"
KUMORI_CRD_VERSION="1.1.1"
KUMORI_KUVOLUME_VERSION="1.0.0"
KUMORI_COREDNS_VERSION="3.0.0"
KUMORI_TOPOLOGY_CONTROLLER_VERSION="2.0.0"
KUMORI_KUCONTROLLER_VERSION="2.0.0"
KUMORI_KUINBOUND_VERSION="2.0.0-ticket534"
KUMORI_SOLUTION_CONTROLLER_VERSION="2.0.0"
KUMORI_ADMISSION_VERSION="2.0.0"
KUMORI_KUALARM_VERSION="1.0.0"
KUMORI_VOLUME_CONTROLLER_VERSION="1.0.1"
KUMORI_IPFILTERING_VERSION="0.0.1"
KUMORI_AUTHSERVICE_VERSION="0.0.1"

################################################################################
## KUMORI CONTROLLERS IMAGE REGISTRY                                          ##
################################################################################
KUMORI_IMAGES_REGISTRY="registry.gitlab.com/kumori/kv3"
KUMORI_IMAGES_REGISTRY_USERNAME=""
KUMORI_IMAGES_REGISTRY_PASSWORD=""

PRELOAD_DOCKER_IMAGES="false"

# Optional Docker registry mirror (applies to the whole cluster)
DOCKER_REGISTRY_MIRRORS=(
  "https://docker-mirror.iti.upv.es"
)

DOCKERHUB_USERNAME=""
DOCKERHUB_PASSWORD=""


# Determine CoreDNS version expected by Kubernetes, based on versioning table:
# https://github.com/coredns/deployment/blob/master/kubernetes/CoreDNS-k8s_version.md
if [[ "${KUBERNETES_VERSION}" =~ ^1.16.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.6.2"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.17.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.6.5"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.18.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.6.7"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.19.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.7.0"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.20.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.7.0"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.21.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.8.0"
elif [[ "${KUBERNETES_VERSION}" =~ ^1.25.* ]]; then
  KUBERNETES_COREDNS_VERSION="1.9.3"
else
  # Unsupported Kubernetes version! Set a default value
  KUBERNETES_COREDNS_VERSION="1.9.3"
fi
