#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

ssh-keygen -f "/home/jferrer/.ssh/known_hosts" -R 192.168.185.114
ssh-keygen -f "/home/jferrer/.ssh/known_hosts" -R 192.168.185.115
ssh-keygen -f "/home/jferrer/.ssh/known_hosts" -R 192.168.185.116
ssh-keygen -f "/home/jferrer/.ssh/known_hosts" -R 192.168.185.199
ssh-keygen -f "/home/jferrer/.ssh/known_hosts" -R 192.168.185.2
ssh-keygen -f "/home/jferrer/.ssh/known_hosts" -R 192.168.185.20

