#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

# Timestamp label to use in files, traces, etc.
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

SSH_COMMAND="ssh -o ServerAliveInterval=15 -o ServerAliveCountMax=4"

REMOTE_WORKDIR=".kumori/Kumori_installer_${TIMESTAMP}"



################################################################################
##  VALIDATE TOPOLOGY CONFIGURATION                                           ##
################################################################################
# Validate that a list of MASTER nodes has been provided
if [[ "${#MASTERS_IPS[@]}" -eq 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - MASTERS_IPS can't be empty."$COL_DEFAULT
  echo
  exit -1
fi

# Validate that a list of WORKER nodes has been provided
if [[ "${#WORKERS_IPS[@]}" -eq 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - WORKERS_IPS can't be empty."$COL_DEFAULT
  echo
  exit -1
fi

# Validate that a list of INGRESS nodes has been provided
if [[ "${#INGRESS_NODES_IPS[@]}" -eq 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - INGRESS_NODES_IPS can't be empty."$COL_DEFAULT
  echo
  exit -1
fi

# Validate that all INGRESS nodes are also WORKER nodes
ERRORS=()
for INGRESS_NODE_IP in ${INGRESS_NODES_IPS[@]}; do
  if [[ ! " ${WORKERS_IPS[@]} " =~ " ${INGRESS_NODE_IP} " ]]; then
    ERRORS+=(${INGRESS_NODE_IP})
  fi
done
if [[ "${#ERRORS[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - Some INGRESS nodes are not in WORKER list: ${ERRORS[*]}"$COL_DEFAULT
  exit -1
fi


################################################################################
##  SELECT A MASTER NODE TO ACT AS PRIMARY MASTER DURING INSTALLATION         ##
################################################################################
# Create a copy of the original Masters list (to print it at the end)
INITIAL_MASTERS_IPS=( "${MASTERS_IPS[@]}" )
if [[ -z "${SUPER_MASTER_IP}" ]]; then
  # No supermaster was provided, use the first master of the list as supermaster
  NEW_MASTERS_IPS=()
  i=0
  for MASTER_IP in ${MASTERS_IPS[@]}; do
    if [[ $i -eq 0 ]]; then
      SUPER_MASTER_IP="${MASTERS_IPS[$i]}"
    else
      NEW_MASTERS_IPS+=("${MASTERS_IPS[$i]}")
    fi
    ((i++))
  done
  MASTERS_IPS=( "${NEW_MASTERS_IPS[@]}" )
fi


################################################################################
##  PREPARE ASSOCIATION OF EACH MACHINE IP WITH A COLOR FOR CLEARER LOGGING   ##
################################################################################
declare -A IP_COLORS
IP_COLORS[$SUPER_MASTER_IP]=${BACKGROUND_COLORS[0]}
i=1
for MASTER_IP in ${MASTERS_IPS[@]}; do
  IP_COLORS[$MASTER_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done
i=1
for WORKERS_IP in ${WORKERS_IPS[@]}; do
  IP_COLORS[$WORKERS_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done




#####################################################################################
##  TEMPORARY PATCH TO ALLOW MULTIPLE CLUSTERS TO USE SAME S3 BUCKET (ticket334)   ##
#####################################################################################
if [ -z "${PKI_BACKUP_STORAGE_PREFIX}" ]; then
  PKI_BACKUP_STORAGE_PREFIX="${RELEASE_NAME}-backups"
fi
if [ -z "${ETCD_BACKUP_STORAGE_PREFIX}" ]; then
  ETCD_BACKUP_STORAGE_PREFIX="${RELEASE_NAME}-backups"
fi
#################################
##  END OF TEMPORARY PATCH     ##
#################################



echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                    MULTI-NODE CLUSTER INSTALLER                            ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER NAME : ${RELEASE_NAME}                                               "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## NODE TOPOLOGY:                                                               "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Masters IPs       : ${INITIAL_MASTERS_IPS[*]}                             "$COL_DEFAULT
echo -e $COL_GREEN"##    Acting as primary : ${SUPER_MASTER_IP}                                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - Masters Zones     : ${MASTERS_ZONES[*]}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - Workers IPs       : ${WORKERS_IPS[*]}                                     "$COL_DEFAULT
echo -e $COL_GREEN"##  - Workers Zones     : ${WORKERS_ZONES[*]}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress nodes IPs : ${INGRESS_NODES_IPS[*]}                               "$COL_DEFAULT
if [ -n "${STORAGE_NODES_IPS}" ]; then
  echo -e $COL_GREEN"##  - Storage Nodes IPs : ${STORAGE_NODES_IPS[*]}                               "$COL_DEFAULT
fi
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
if [ -n "${TX_OFF_NODES_IPS}" ]; then
  echo -e $COL_GREEN"##  - Disable TX Offloading : ${TX_OFF_NODES_IPS[*]}                            "$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
fi
if [ -n "${MASTER_MIN_MEMORY_KB}" ]; then
  echo -e $COL_GREEN"##  - Master Nodes required Memory: ${MASTER_MIN_MEMORY_KB} KB                  "$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
fi
echo -e $COL_GREEN"## API-SERVER ACCESS:                                                           "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - ApiServer domain        : ${APISERVER_DOMAIN}                             "$COL_DEFAULT
echo -e $COL_GREEN"##  - ApiServer internal port : ${APISERVER_INTERNAL_PORT}                      "$COL_DEFAULT
echo -e $COL_GREEN"##  - ApiServer external port : ${APISERVER_EXTERNAL_PORT}                      "$COL_DEFAULT
echo -e $COL_GREEN"##  - ApiServer URL           : ${APISERVER_URL}                                "$COL_DEFAULT
if [ -n "${APISERVER_CERT_EXTRA_SANS}" ]; then
  echo -e $COL_GREEN"##  - ApiServer SANs          : ${APISERVER_CERT_EXTRA_SANS}                    "$COL_DEFAULT
fi
echo -e $COL_GREEN"##  - ApiServer DNS           : ${APISERVER_REGISTER_DOMAIN}                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - ApiServer DNS IP        : ${APISERVER_DNS_IP}                             "$COL_DEFAULT
echo -e $COL_GREEN"##  - Internal balancing      : ${APISERVER_INTERNAL_BALANCING}                 "$COL_DEFAULT
echo -e $COL_GREEN"##  - Handle Virtual IP       : ${APISERVER_HANDLE_FLOATING_IP}                 "$COL_DEFAULT
if [ "${APISERVER_HANDLE_FLOATING_IP}" == "true" ]; then
  echo -e $COL_GREEN"##  - Virtual IP              : ${APISERVER_VIRTUAL_IP}                         "$COL_DEFAULT
fi
if [ -n "${APISERVER_VRID}" ]; then
  echo -e $COL_GREEN"##  - ApiServer VRID          : ${APISERVER_VRID}                               "$COL_DEFAULT
fi
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## INGRESS ACCESS:                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress domain        : ${INGRESS_DOMAIN}                                 "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress internal port : ${INGRESS_INTERNAL_PORT}                          "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress external port : ${INGRESS_EXTERNAL_PORT}                          "$COL_DEFAULT
echo -e $COL_GREEN"##  - Internal balancing    : ${INGRESS_INTERNAL_BALANCING}                     "$COL_DEFAULT
echo -e $COL_GREEN"##  - Handle Virtual IP     : ${INGRESS_HANDLE_FLOATING_IP}                     "$COL_DEFAULT
if [ "${INGRESS_HANDLE_FLOATING_IP}" == "true" ]; then
  echo -e $COL_GREEN"##  - Virtual IP            : ${INGRESS_VIRTUAL_IP}                             "$COL_DEFAULT
fi
if [ -n "${INGRESS_VRID}" ]; then
  echo -e $COL_GREEN"##  - Ingress VRID          : ${INGRESS_VRID}                                   "$COL_DEFAULT
fi
echo -e $COL_GREEN"##  - Ingress DNS           : ${INGRESS_REGISTER_DOMAIN}                        "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress DNS IP        : ${INGRESS_DNS_IP}                                 "$COL_DEFAULT
if [ -n "${INGRESS_TCP_PORTS}" ]; then
  echo -e $COL_GREEN"##  - Ingress TCP Ports (external) : ${INGRESS_TCP_PORTS[*]}                    "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Ingress TCP Ports (internal) : ${INGRESS_TCP_INTERNAL_PORTS[*]}           "$COL_DEFAULT
fi
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER DNS MANAGEMENT:                                                      "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Managed by platform: ${MANAGED_DNS}                                       "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
if [ "${MANAGED_DNS}" = "true" ]; then
  echo -e $COL_GREEN"##  - DNS Provider   : ${MANAGED_DNS_PROVIDER}                                  "$COL_DEFAULT
  if [ "${MANAGED_DNS_PROVIDER}" = "route53" ]; then
    echo -e $COL_GREEN"##  - AWS config dir : ${MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR}                    "$COL_DEFAULT
  fi
  if [ "${MANAGED_DNS_PROVIDER}" = "ovh" ]; then
    echo -e $COL_GREEN"##  - OVH credentials file: ${MANAGED_DNS_OVH_CONFIG_FILE}                      "$COL_DEFAULT
  fi
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
fi

if [ -n "${DNS_RESOLVERS}" ]; then
  echo -e $COL_GREEN"##  - Override DNS resolvers: ${DNS_RESOLVERS[*]}                               "$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
fi
echo -e $COL_GREEN"## STORAGE:                                                                     "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
if [[ "${#VOLUME_TYPE_NAMES[@]}" -gt 0  ]]; then
  echo -e $COL_GREEN"##  - Enabled:  true                                                            "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Volume Types:                                                             "$COL_DEFAULT
  for i in "${!VOLUME_TYPE_NAMES[@]}"; do
    VT_NAME="${VOLUME_TYPE_NAMES[$i]}"
    VT_STORAGECLASS="${VOLUME_TYPE_STORAGECLASSES[$i]}"
    VT_PROPERTIES="${VOLUME_TYPE_PROPERTIES[$i]}"
    echo -e $COL_GREEN"##     - Name         : ${VT_NAME}                                              "$COL_DEFAULT
    echo -e $COL_GREEN"##       StorageClass : ${VT_STORAGECLASS}                                      "$COL_DEFAULT
    echo -e $COL_GREEN"##       Properties   : ${VT_PROPERTIES}                                        "$COL_DEFAULT
  done
else
  echo -e $COL_GREEN"##  - Enabled:  false                                                           "$COL_DEFAULT
fi
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
if [ "${INSTALL_OPENEBS_PROVIDER}" = "true" ]; then
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"## Storage Provider - OpenEBS:                                                            "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Install:             true                                                  "$COL_DEFAULT
  if [ -n "${OPENEBS_STORAGE_REPLICATED_DEVICES}" ]; then
    echo -e $COL_GREEN"##  - cStor devices:       ${OPENEBS_STORAGE_REPLICATED_DEVICES[*]}             "$COL_DEFAULT
    echo -e $COL_GREEN"##  - cStor replicas:      ${OPENEBS_STORAGE_REPLICATED_REPLICATION_LEVEL}      "$COL_DEFAULT
    echo -e $COL_GREEN"##  - cStor CPU:           Request: ${OPENEBS_REPLICATED_CPU_REQUEST} / Limit: ${OPENEBS_REPLICATED_CPU_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - cStor MEM:           Request: ${OPENEBS_REPLICATED_MEM_REQUEST} / Limit: ${OPENEBS_REPLICATED_MEM_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - cStor aux CPU:       Request: ${OPENEBS_REPLICATED_AUX_CPU_REQUEST} / Limit: ${OPENEBS_REPLICATED_AUX_CPU_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - cStor aux MEM:       Request: ${OPENEBS_REPLICATED_AUX_MEM_REQUEST} / Limit: ${OPENEBS_REPLICATED_AUX_MEM_LIMIT} "$COL_DEFAULT
  fi
  if [ -n "${OPENEBS_STORAGE_LVM_VOLUMEGROUP}" ]; then
    echo -e $COL_GREEN"##  - Local LVM group:     ${OPENEBS_STORAGE_LVM_VOLUMEGROUP}                   "$COL_DEFAULT
  fi
  if [ -n "${OPENEBS_STORAGE_HOSTPATH_DIR}" ]; then
    echo -e $COL_GREEN"##  - Local HostPath dir:  ${OPENEBS_STORAGE_HOSTPATH_DIR}                      "$COL_DEFAULT
  fi
  if [ -n "${OPENEBS_STORAGE_IGNORED_DEVICES}" ]; then
    echo -e $COL_GREEN"##  - Ignored devices:     ${OPENEBS_STORAGE_IGNORED_DEVICES[*]}                "$COL_DEFAULT
  fi
fi
if [ "${INSTALL_CSI_CINDER_PROVIDER}" = "true" ]; then
  echo -e $COL_GREEN"##                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"## Storage Provider - Openstack Cinder CSI:                     "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Install:     true                                                  "$COL_DEFAULT
  if [ -n "${CSI_CINDER_OPENSTACK_CLOUDS_YAML_BASE64}" ]; then
    echo -e $COL_GREEN"##  - Clouds.yaml: yes (base64)                                 "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Key:         ${CSI_CINDER_OPENSTACK_CLOUDS_YAML_KEY}      "$COL_DEFAULT
  else
    echo -e $COL_GREEN"##  - Clouds.yaml: no                                           "$COL_DEFAULT
  fi
  echo -e $COL_GREEN"##  - Classes:                                                  "$COL_DEFAULT
  for i in "${!CSI_CINDER_OPENSTACK_CLASS_NAMES[@]}"; do
    CLASS_NAME="${CSI_CINDER_OPENSTACK_CLASS_NAMES[$i]}"
    CLASS_CONFIG="${CSI_CINDER_OPENSTACK_CLASS_CONFIGURATIONS[$i]}"
    CLASS_PROPS="${CSI_CINDER_OPENSTACK_CLASS_PROPERTIES[$i]}"
    echo -e $COL_GREEN"##    - Name       : ${CLASS_NAME}                                                  "$COL_DEFAULT
    echo -e $COL_GREEN"##      Config     : ${CLASS_CONFIG}                                                "$COL_DEFAULT
    echo -e $COL_GREEN"##      Properties : ${CLASS_PROPS}                                                 "$COL_DEFAULT
  done
fi
if [ "${INSTALL_CSI_NFS_PROVIDER}" = "true" ]; then
  echo -e $COL_GREEN"##                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"## Storage Provider - CSI NFS:                                  "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Install: true                                             "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Classes:                                                  "$COL_DEFAULT
  for i in "${!CSI_NFS_CLASS_NAMES[@]}"; do
    CLASS_NAME="${CSI_NFS_CLASS_NAMES[$i]}"
    CLASS_CONFIG="${CSI_NFS_CLASS_CONFIGURATIONS[$i]}"
    CLASS_PROPS="${CSI_NFS_CLASS_PROPERTIES[$i]}"
    echo -e $COL_GREEN"##    - Name       : ${CLASS_NAME}                                                  "$COL_DEFAULT
    echo -e $COL_GREEN"##      Config     : ${CLASS_CONFIG}                                                "$COL_DEFAULT
    echo -e $COL_GREEN"##      Properties : ${CLASS_PROPS}                                                 "$COL_DEFAULT
  done
fi
if [ "${INSTALL_CSI_CEPH_RBD_PROVIDER}" = "true" ]; then
  echo -e $COL_GREEN"##                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"## Storage Provider - CSI Ceph RBD:                             "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Install: true                                             "$COL_DEFAULT
  echo -e $COL_GREEN"##  - HA: ${CSI_CEPH_RBD_HA}                                    "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Ceph clusters:                                            "$COL_DEFAULT
  for i in "${!CSI_CEPH_RBD_CLUSTER_NAMES[@]}"; do
    CLUSTER_NAME="${CSI_CEPH_RBD_CLUSTER_NAMES[$i]}"
    CLUSTER_ID="${CSI_CEPH_RBD_CLUSTER_IDS[$i]}"
    CLUSTER_MONITORS="${CSI_CEPH_RBD_CLUSTER_MONITORS[$i]}"
    echo -e $COL_GREEN"##    - Name       : ${CLUSTER_NAME}                                              "$COL_DEFAULT
    echo -e $COL_GREEN"##      ClusterID  : ${CLUSTER_ID}                                                "$COL_DEFAULT
    echo -e $COL_GREEN"##      Monitors   : ${CLUSTER_MONITORS}                                          "$COL_DEFAULT
  done
  echo -e $COL_GREEN"##  - Classes:                                                  "$COL_DEFAULT
  for i in "${!CSI_CEPH_RBD_CLASS_NAMES[@]}"; do
    CLASS_NAME="${CSI_CEPH_RBD_CLASS_NAMES[$i]}"
    CLASS_CONFIG="${CSI_CEPH_RBD_CLASS_CONFIGURATIONS[$i]}"
    CLASS_PROPS="${CSI_CEPH_RBD_CLASS_PROPERTIES[$i]}"
    echo -e $COL_GREEN"##    - Name       : ${CLASS_NAME}                                                  "$COL_DEFAULT
    echo -e $COL_GREEN"##      Config     : ${CLASS_CONFIG}                                                "$COL_DEFAULT
    echo -e $COL_GREEN"##      Properties : ${CLASS_PROPS}                                                 "$COL_DEFAULT
  done
fi
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## SOFTWARE VERSIONS:                                                           "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT

if [ -n "${KERNEL_VERSION}" ]; then
  echo -e $COL_GREEN"##  - Linux Kernel:            ${KERNEL_VERSION}                                        "$COL_DEFAULT
else
  echo -e $COL_GREEN"##  - Linux Kernel:            Any                                                      "$COL_DEFAULT
fi
echo -e $COL_GREEN"##  - Kubernetes:              ${KUBERNETES_VERSION}                                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - Docker:                  ${DOCKER_VERSION}                                        "$COL_DEFAULT
echo -e $COL_GREEN"##  - Cri-Dockerd:             ${CRI_DOCKERD_VERSION}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - Containerd:              ${CONTAINERD_VERSION}                                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - Helm:                    ${HELM_VERSION}                                          "$COL_DEFAULT
echo -e $COL_GREEN"##  - Keepalived:              ${KEEPALIVED_VERSION}                                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - Envoy:                   ${ENVOY_VERSION}                                         "$COL_DEFAULT
echo -e $COL_GREEN"##  - Calico:                  ${CALICO_VERSION}                                        "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ambassador:              ${AMBASSADOR_VERSION}                                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - Kube-Prometheus:         ${KUBE_PROMETHEUS_VERSION}                               "$COL_DEFAULT
echo -e $COL_GREEN"##  - Elasticsearch:           ${ELASTICSEARCH_VERSION}                                 "$COL_DEFAULT
echo -e $COL_GREEN"##  - Filebeat:                ${FILEBEAT_VERSION}                                      "$COL_DEFAULT
echo -e $COL_GREEN"##  - Kibana:                  ${KIBANA_VERSION}                                        "$COL_DEFAULT
echo -e $COL_GREEN"##  - K8s Dashboard:           ${K8S_DASHBOARD_VERSION}                                 "$COL_DEFAULT
echo -e $COL_GREEN"##  - Keycloak chart:          ${KEYCLOAK_VERSION}                                      "$COL_DEFAULT
echo -e $COL_GREEN"##  - Minio:                   ${MINIO_VERSION}                                         "$COL_DEFAULT
echo -e $COL_GREEN"##  - Etcd-Backup:             ${ETCD_BACKUP_VERSION}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - External-DNS:            ${EXTERNALDNS_VERSION}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - Event-Exporter:          ${EVENT_EXPORTER_VERSION}                                "$COL_DEFAULT
echo -e $COL_GREEN"##  - OpenEBS:                 ${OPENEBS_VERSION}                                       "$COL_DEFAULT
echo -e $COL_GREEN"##  - CSI Cinder:              ${CSI_CINDER_PROVIDER_VERSION}                           "$COL_DEFAULT
echo -e $COL_GREEN"##  - CSI NFS:                 ${CSI_NFS_PROVIDER_VERSION}                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - CSI Ceph RBD             ${CSI_CEPH_RBD_PROVIDER_VERSION}                         "$COL_DEFAULT
echo -e $COL_GREEN"##  - Descheduler:             ${DESCHEDULER_VERSION}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - CertManager:             ${CERT_MANAGER_VERSION}                                  "$COL_DEFAULT
echo -e $COL_GREEN"##  - CertManager Webhook OVH: ${CERT_MANAGER_WEBHOOK_OVH_VERSION}                      "$COL_DEFAULT
echo -e $COL_GREEN"##  - ClusterAPI:              ${CLUSTERAPI_VERSION}                                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - CAPI Openstack provider: ${CLUSTERAPI_OPENSTACK_PROVIDER_VERSION}                 "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## KUMORI IMAGE REPOSITORY:                                                     "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Repository:      ${KUMORI_IMAGES_REGISTRY}                                "$COL_DEFAULT
if [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ]; then
  echo -e $COL_GREEN"##  - Username:        ${KUMORI_IMAGES_REGISTRY_USERNAME}                    "$COL_DEFAULT
  if [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
    echo -e $COL_GREEN"##  - Password:        yes <hidden>                                          "$COL_DEFAULT
  else
    echo -e $COL_GREEN"##  - Password:        "$COL_RED_BOLD"NO (credentials won't be used!)        "$COL_DEFAULT
  fi
else
  echo -e $COL_GREEN"##  - No username configured                                                    "$COL_DEFAULT
fi
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## KUMORI ELEMENTS VERSIONS:                                                    "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Resource definitions:  ${KUMORI_CRD_VERSION}                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - KuController:          ${KUMORI_KUCONTROLLER_VERSION}                     "$COL_DEFAULT
echo -e $COL_GREEN"##  - KuInbound:             ${KUMORI_KUINBOUND_VERSION}                        "$COL_DEFAULT
echo -e $COL_GREEN"##  - Admission:             ${KUMORI_ADMISSION_VERSION}                        "$COL_DEFAULT
echo -e $COL_GREEN"##  - SolutionController:    ${KUMORI_SOLUTION_CONTROLLER_VERSION}              "$COL_DEFAULT
echo -e $COL_GREEN"##  - TopologyController:    ${KUMORI_TOPOLOGY_CONTROLLER_VERSION}              "$COL_DEFAULT
echo -e $COL_GREEN"##  - VolumeController:      ${KUMORI_VOLUME_CONTROLLER_VERSION}                "$COL_DEFAULT
echo -e $COL_GREEN"##  - KuAlarm:               ${KUMORI_KUALARM_VERSION}                          "$COL_DEFAULT
echo -e $COL_GREEN"##  - KuVolume Driver:       ${KUMORI_KUVOLUME_VERSION}                         "$COL_DEFAULT
echo -e $COL_GREEN"##  - CoreDNS (custom):      ${KUMORI_COREDNS_VERSION} (as ${KUBERNETES_COREDNS_VERSION}) "$COL_DEFAULT
echo -e $COL_GREEN"##  - Out-of-Service:        ${KUMORI_OUTOFSERVICE_VERSION:-default}            "$COL_DEFAULT
echo -e $COL_GREEN"##  - PullPolicy:            ${KUMORI_IMAGES_PULL_POLICY}                       "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER CONFIGURATION:                                                       "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Release Name:           ${RELEASE_NAME}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - K8s APIServer Domain:   ${APISERVER_DOMAIN}                               "$COL_DEFAULT
echo -e $COL_GREEN"##  - K8s APIServer Endpoint: ${APISERVER_URL}                                  "$COL_DEFAULT
echo -e $COL_GREEN"##  - K8s Pods Network CIDR:  ${PODS_NETWORK_CIDR}                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - K8s Service CIDR:       ${SERVICE_CIDR}                                   "$COL_DEFAULT
echo -e $COL_GREEN"##  - Docker bridge CIDR:     ${DOCKER_BRIDGE_CIDR}                             "$COL_DEFAULT
echo -e $COL_GREEN"##  - CoreDNS CPU resources:  Request: ${COREDNS_CPU_REQUEST} / Limit: ${COREDNS_CPU_LIMIT} "$COL_DEFAULT
echo -e $COL_GREEN"##  - CoreDNS MEM resources:  Request: ${COREDNS_MEM_REQUEST} / Limit: ${COREDNS_MEM_LIMIT} "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
if [ "${USE_ETCDBACKUP}" = "true" ]; then
  echo -e $COL_GREEN"## USING ETCD BACKUP:                                                         "$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                            "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Backup file:                ${ETCDBACKUP_FILE}                          "$COL_DEFAULT
else
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
  if [ -n "${OUTOFSERVICE_IMAGE}" ]; then
    echo -e $COL_GREEN"## USING CUSTOM OutOfService IMAGE:                                             "$COL_DEFAULT
    echo -e $COL_GREEN"##                                                                            "$COL_DEFAULT
    echo -e $COL_GREEN"##  - OutOfService image:         ${OUTOFSERVICE_IMAGE}                       "$COL_DEFAULT
    echo -e $COL_GREEN"##  - OutOfService registry:      ${OUTOFSERVICE_REGISTRY}                    "$COL_DEFAULT
    if [ -n "${OUTOFSERVICE_USERNAME}" ] && [ -n "${OUTOFSERVICE_PASSWORD}" ]; then
      echo -e $COL_GREEN"##  - OutOfService credentials:   Yes <hidden>                                "$COL_DEFAULT
    else
      echo -e $COL_GREEN"##  - OutOfService credentials:   "$COL_RED_BOLD"NO (image must be public)    "$COL_DEFAULT
    fi
    echo -e $COL_GREEN"##                                                                            "$COL_DEFAULT
  fi
  echo -e $COL_GREEN"## ADDONS CONFIGURATION:                                                      "$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                            "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Install Calico:                   ${INSTALL_CALICO}                     "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Ingress Namespace:                ${INGRESS_NAMESPACE}                  "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Reference Domain:                 ${REFERENCE_DOMAIN}                   "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Ambassador CPU resources:         Request: ${AMBASSADOR_CPU_REQUEST} / Limit: ${AMBASSADOR_CPU_LIMIT} "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Ambassador Memory resources:      Request: ${AMBASSADOR_MEM_REQUEST} / Limit: ${AMBASSADOR_MEM_LIMIT} "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Admission authentication:         ${ADMISSION_AUTHENTICATION_TYPE}      "$COL_DEFAULT
  echo -e $COL_GREEN"##  - SSL Certificates directory:       ${REFERENCE_DOMAIN_CERT_DIR}          "$COL_DEFAULT
  echo -e $COL_GREEN"##  - KPKI root CA:                     ${CLUSTER_PKI_ROOT_CA_FILE}           "$COL_DEFAULT
  echo -e $COL_GREEN"##  - KPKI Admission certificate:       ${ADMISSION_SERVER_CERT_DIR}          "$COL_DEFAULT

  echo -e $COL_GREEN"##  - Install External-DNS:             ${INSTALL_EXTERNALDNS}                "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Storage Class for Addons:         ${ADDONS_STORAGE_CLASS}               "$COL_DEFAULT

  if [ "${INSTALL_KEYCLOAK}" == "true" ]; then
    echo -e $COL_GREEN"##  - Install Keycloak:                 ${INSTALL_KEYCLOAK}                   "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Keycloak username:                ${KEYCLOAK_ADMIN_USERNAME}            "$COL_DEFAULT
    if [ -n "${KEYCLOAK_ADMIN_PASSWORD}" ]; then
      echo -e $COL_GREEN"##  - Keycloak password:                yes <hidden>                          "$COL_DEFAULT
    else
      echo -e $COL_GREEN"##  - Keycloak password:                "$COL_RED_BOLD"NO                     "$COL_DEFAULT
    fi
    echo -e $COL_GREEN"##  - Keycloack CPU resources:          Request: ${KEYCLOAK_CPU_REQUEST} / Limit: ${KEYCLOAK_CPU_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Keycloack Memory resources:       Request: ${KEYCLOAK_MEM_REQUEST} / Limit: ${KEYCLOAK_MEM_LIMIT} "$COL_DEFAULT
  else
    echo -e $COL_GREEN"##  - Install Keycloak                  false                                 "$COL_DEFAULT
  fi

  echo -e $COL_GREEN"##  - Install Kube-Prometheus:          ${INSTALL_KUBE_PROMETHEUS}            "$COL_DEFAULT
  if [ "${INSTALL_KUBE_PROMETHEUS}" == "true" ]; then
    if [ "${MONITORING_REQUIRE_CLIENTCERT}" == "true" ]; then
      echo -e $COL_GREEN"##  - Secure monitoring endpoints:      true                                  "$COL_DEFAULT
      echo -e $COL_GREEN"##  - Monitoring CA:                    ${MONITORING_CLIENTCERT_TRUSTED_CA_FILE}  "$COL_DEFAULT
    else
      echo -e $COL_GREEN"##  - Secure monitoring endpoints:      false                                 "$COL_DEFAULT
    fi
    echo -e $COL_GREEN"##  - Prometheus HA:                    ${PROMETHEUS_HA}                      "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Prometheus HA Replicas:           ${PROMETHEUS_HA_REPLICAS}             "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Prometheus Retention time:        ${PROMETHEUS_RETENTION_TIME}          "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Prometheus Persistence:           ${PROMETHEUS_PERSISTENCE}             "$COL_DEFAULT
    if [ "${PROMETHEUS_PERSISTENCE}" = "true" ]; then
      echo -e $COL_GREEN"##  - Prometheus remote_read_url:       ${PROMETHEUS_REMOTE_READ_URL}         "$COL_DEFAULT
      echo -e $COL_GREEN"##  - Prometheus remote_write_url:      ${PROMETHEUS_REMOTE_WRITE_URL}        "$COL_DEFAULT
      echo -e $COL_GREEN"##  - Prometheus remote auth type:      ${PROMETHEUS_REMOTE_AUTH_TYPE}        "$COL_DEFAULT
      if [ "${PROMETHEUS_REMOTE_AUTH_TYPE}" = "clientcertificate" ]; then
        echo -e $COL_GREEN"##  - Prometheus remote cert file:      ${PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_CERT_FILE}     "$COL_DEFAULT
        echo -e $COL_GREEN"##  - Prometheus remote key file:       ${PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_KEY_FILE}      "$COL_DEFAULT
      fi
    fi
    echo -e $COL_GREEN"##  - Prometheus CPU resources:         Request: ${PROMETHEUS_CPU_REQUEST} / Limit: ${PROMETHEUS_CPU_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Prometheus Memory resources:      Request: ${PROMETHEUS_MEM_REQUEST} / Limit: ${PROMETHEUS_MEM_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Config reloader CPU resources:    Request: ${PROMETHEUS_CONFIG_RELOADER_CPU_REQUEST} / Limit: ${PROMETHEUS_CONFIG_RELOADER_CPU_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Config reloader Memory resources: Request: ${PROMETHEUS_CONFIG_RELOADER_MEM_REQUEST} / Limit: ${PROMETHEUS_CONFIG_RELOADER_MEM_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - AlertManager HA:                  ${ALERTMANAGER_HA}                    "$COL_DEFAULT
    echo -e $COL_GREEN"##  - AlertManager HA Replicas:         ${ALERTMANAGER_HA_REPLICAS}           "$COL_DEFAULT
    echo -e $COL_GREEN"##  - AlertManager CPU resources:       Request: ${ALERTMANAGER_CPU_REQUEST} / Limit: ${ALERTMANAGER_CPU_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - AlertManager Memory resources:    Request: ${ALERTMANAGER_MEM_REQUEST} / Limit: ${ALERTMANAGER_MEM_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Grafana HA:                       ${GRAFANA_HA}                         "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Grafana HA Replicas:              ${GRAFANA_HA_REPLICAS}                "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Grafana Admin username:           ${GRAFANA_ADMIN_USERNAME}             "$COL_DEFAULT
    if [ -n "${GRAFANA_ADMIN_PASSWORD}" ]; then
      echo -e $COL_GREEN"##  - Grafana Admin password:           yes <hidden>                          "$COL_DEFAULT
    else
      echo -e $COL_GREEN"##  - Grafana Admin password:           "$COL_RED_BOLD"NO                     "$COL_DEFAULT
    fi
    echo -e $COL_GREEN"##  - Grafana Viewer username:          ${GRAFANA_VIEWER_USERNAME}              "$COL_DEFAULT
    if [ -n "${GRAFANA_VIEWER_PASSWORD}" ]; then
      echo -e $COL_GREEN"##  - Grafana Viewer password:          yes <hidden>                          "$COL_DEFAULT
    else
      echo -e $COL_GREEN"##  - Grafana Viewer password:          "$COL_RED_BOLD"NO                     "$COL_DEFAULT
    fi
    echo -e $COL_GREEN"##  - Grafana multicluster support:     ${GRAFANA_ENABLE_MULTICLUSTER_SUPPORT}  "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Grafana OAuth strict roles:       ${GRAFANA_OAUTH_ROLE_STRICT}            "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Grafana CPU resources:            Request: ${GRAFANA_CPU_REQUEST} / Limit: ${GRAFANA_CPU_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Grafana Memory resources:         Request: ${GRAFANA_MEM_REQUEST} / Limit: ${GRAFANA_MEM_LIMIT} "$COL_DEFAULT
    if [ "${PROMETHEUS_PERSISTENCE}" = "true" ]; then
      echo -e $COL_GREEN"##  - Grafana remote datasource:        ${GRAFANA_REMOTE_DATASOURCE_NAME}     "$COL_DEFAULT
    fi
  fi

  if [ "${EVENTS_STORE_TYPE}" == "elasticsearch" ]; then
    echo -e $COL_GREEN"##  - Install Event-Exporter:           true                                  "$COL_DEFAULT
    echo -e $COL_GREEN"##    - Event-Exporter HA:              ${EVENTS_EXPORTER_HA}                    "$COL_DEFAULT
    echo -e $COL_GREEN"##    - Event-Exporter HA Replicas:     ${EVENTS_EXPORTER_HA_REPLICAS}           "$COL_DEFAULT
    echo -e $COL_GREEN"##    - Event store type:               ${EVENTS_STORE_TYPE}                  "$COL_DEFAULT
    echo -e $COL_GREEN"##    - Elasticsearch URL:              ${EVENTS_ELASTICSEARCH_URL}           "$COL_DEFAULT
    if [[ "${EVENTS_ELASTICSEARCH_USERNAME}" != "" ]]; then
      echo -e $COL_GREEN"##    - Elasticsearch Username:         ${EVENTS_ELASTICSEARCH_USERNAME}      "$COL_DEFAULT
    fi
    if [ -n "${EVENTS_ELASTICSEARCH_PASSWORD}" ]; then
      echo -e $COL_GREEN"##    - Elasticsearch password:         yes <hidden>                          "$COL_DEFAULT
    else
      echo -e $COL_GREEN"##    - Elasticsearch password:         "$COL_RED_BOLD"NO                     "$COL_DEFAULT
    fi
    echo -e $COL_GREEN"##    - Elasticsearch Index Prefix:     ${EVENTS_ELASTICSEARCH_INDEX_PREFIX}  "$COL_DEFAULT
  fi

  echo -e $COL_GREEN"##  - Install Elasticsearch:            ${INSTALL_ELASTICSEARCH}              "$COL_DEFAULT
  if [ "${INSTALL_ELASTICSEARCH}" == "true" ]; then
    echo -e $COL_GREEN"##  - Elasticsearch replicas:           ${ELASTICSEARCH_REPLICAS}             "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Elasticsearch volume size:        ${ELASTICSEARCH_VOLUME_SIZE}          "$COL_DEFAULT
  fi
  echo -e $COL_GREEN"##  - Install Kibana:                   ${INSTALL_KIBANA}                     "$COL_DEFAULT
  if [ "${INSTALL_KIBANA}" == "true" ]; then
    echo -e $COL_GREEN"##  - Kibana Elasticsearch URL:         ${KIBANA_ELASTICSEARCH_URL}           "$COL_DEFAULT
    if [[ "${KIBANA_ELASTICSEARCH_USERNAME}" != "" ]]; then
      echo -e $COL_GREEN"##  - Kibana Elasticsearch Username:    ${KIBANA_ELASTICSEARCH_USERNAME}      "$COL_DEFAULT
    fi
  fi

  echo -e $COL_GREEN"##  - Install Filebeat:                 ${INSTALL_FILEBEAT}                   "$COL_DEFAULT
  if [ "${INSTALL_FILEBEAT}" == "true" ]; then
    echo -e $COL_GREEN"##  - Filebeat Elasticsearch URL:       ${FILEBEAT_ELASTICSEARCH_URL}         "$COL_DEFAULT
    if [[ "${FILEBEAT_ELASTICSEARCH_USERNAME}" != "" ]]; then
      echo -e $COL_GREEN"##  - Filebeat Elasticsearch Username:  ${FILEBEAT_ELASTICSEARCH_USERNAME}  "$COL_DEFAULT
    fi
    echo -e $COL_GREEN"##  - Filebeat Index Pattern:           ${FILEBEAT_INDEX_PATTERN}             "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Filebeat CPU resources:           Request: ${FILEBEAT_CPU_REQUEST} / Limit: ${FILEBEAT_CPU_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Filebeat Memory resources:        Request: ${FILEBEAT_MEM_REQUEST} / Limit: ${FILEBEAT_MEM_LIMIT} "$COL_DEFAULT
  fi

  echo -e $COL_GREEN"##  - Install MinIO:                    ${INSTALL_MINIO}                      "$COL_DEFAULT
  if [ "${INSTALL_MINIO}" == "true" ]; then
    echo -e $COL_GREEN"##  - MinIO Access Key:                 ${MINIO_ACCESS_KEY}                 "$COL_DEFAULT
  fi

  echo -e $COL_GREEN"##  - Install Etcd-Backup:              ${INSTALL_ETCD_BACKUP}                "$COL_DEFAULT
  if [ "${INSTALL_ETCD_BACKUP}" == "true" ]; then
    echo -e $COL_GREEN"##  - Etcd-Backup Endpoint:             ${ETCD_BACKUP_ETCD_ENDPOINT}        "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Etcd-Backup Schedule:             ${ETCD_BACKUP_SCHEDULE}             "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Etcd-Backup Delta period:         ${ETCD_BACKUP_DELTA_PERIOD}         "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Etcd-Backup S3 Endpoint:          ${ETCD_BACKUP_S3_ENDPOINT}          "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Etcd-Backup S3 Region:            ${ETCD_BACKUP_S3_REGION}            "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Etcd-Backup S3 Access Key:        ${ETCD_BACKUP_S3_ACCESS_KEY}        "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Etcd-Backup S3 Bucket:            ${ETCD_BACKUP_S3_BUCKET}            "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Etcd-Backup Storage prefix:       ${ETCD_BACKUP_STORAGE_PREFIX}       "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Etcd-Backup Trusted CAs:          ${ETCD_BACKUP_TRUSTED_CA_CONFIGMAP} "$COL_DEFAULT
  fi

  echo -e $COL_GREEN"##  - Install Descheduler:              ${INSTALL_DESCHEDULER}                "$COL_DEFAULT
  if [ "${INSTALL_DESCHEDULER}" == "true" ]; then
    echo -e $COL_GREEN"##  - Descheduler Interval:             ${DESCHEDULER_INTERVAL}         "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Descheduler Low Node Utilization: ${DESCHEDULER_LOW_ENABLED}                "$COL_DEFAULT
    if [ "${DESCHEDULER_LOW_ENABLED}" == "true" ]; then
      echo -e $COL_GREEN"##  - Descheduler CPU Utilization:      Thresholds: ${DESCHEDULER_LOW_THRESHOLD_CPU} / ${DESCHEDULER_LOW_TARGET_CPU} "$COL_DEFAULT
      echo -e $COL_GREEN"##  - Descheduler Memory Utilization:   Thresholds: ${DESCHEDULER_LOW_THRESHOLD_MEM} / ${DESCHEDULER_LOW_TARGET_MEM} "$COL_DEFAULT
      echo -e $COL_GREEN"##  - Descheduler Pods Utilization:     Thresholds: ${DESCHEDULER_LOW_THRESHOLD_POD} / ${DESCHEDULER_LOW_TARGET_POD} "$COL_DEFAULT
    fi
    echo -e $COL_GREEN"##  - Descheduler Spread Constraints:   ${DESCHEDULER_SPREAD_ENABLED}                "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Descheduler CPU resources:        Request: ${DESCHEDULER_CPU_REQUEST} / Limit: ${DESCHEDULER_CPU_LIMIT} "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Descheduler Memory resources:     Request: ${DESCHEDULER_MEM_REQUEST} / Limit: ${DESCHEDULER_MEM_LIMIT} "$COL_DEFAULT
  fi

  echo -e $COL_GREEN"##  - Install CertManager:              ${INSTALL_CERT_MANAGER}                "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Install CertManager Webhook OVH:  ${INSTALL_CERT_MANAGER_WEBHOOK_OVH}    "$COL_DEFAULT
  if [ "${INSTALL_CERT_MANAGER_WEBHOOK_OVH}" == "true" ]; then
    if [  -n "${CERT_MANAGER_WEBHOOK_OVH_CREDENTIALS_BASE64}" ]; then
      echo -e $COL_GREEN"##    - OVH credentials:                yes (base64)                                         "$COL_DEFAULT
    else
      echo -e $COL_GREEN"##    - OVH credentials:                no                                                   "$COL_DEFAULT
    fi
    echo -e $COL_GREEN"##    - Email:              ${CERT_MANAGER_WEBHOOK_OVH_EMAIL}        "$COL_DEFAULT
    if [ "${CERT_MANAGER_WEBHOOK_OVH_ISSUER_LETSENCRYPT_ENABLED}" == "true" ]; then
      echo -e $COL_GREEN"##    - LetsEncrypt use staging:        ${CERT_MANAGER_WEBHOOK_OVH_ISSUER_LETSENCRYPT_USE_STAGING}  "$COL_DEFAULT
    fi
    if [ "${CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_ENABLED}" == "true" ]; then
      if [ -z "$CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_EABKEYHMAC" ]; then
        echo -e $COL_GREEN"##    - ZeroSSL EAB Key HMAC:           No                                                   "$COL_DEFAULT
      else
        echo -e $COL_GREEN"##    - ZeroSSL EAB Key HMAC:           Yes                                                  "$COL_DEFAULT
      fi
    fi
  fi

  echo -e $COL_GREEN"##  - Install ClusterAPI:               ${INSTALL_CLUSTERAPI}                "$COL_DEFAULT
  echo -e $COL_GREEN"##    - Openstack provider:             ${INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_OPENSTACK}                "$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                            "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Cloud Controllers:                                                      "$COL_DEFAULT
  echo -e $COL_GREEN"##    - Install OpenStack C.C.: ${INSTALL_OPENSTACK_CLOUD_CONTROLLER}                "$COL_DEFAULT
  if [ "${INSTALL_OPENSTACK_CLOUD_CONTROLLER}" == "true" ]; then
    echo -e $COL_GREEN"##      - OpenStack clouds.yaml file : ${OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_FILE}                "$COL_DEFAULT
    echo -e $COL_GREEN"##      - OpenStack clouds.yaml key  : ${OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_KEY}                 "$COL_DEFAULT
  fi
  echo -e $COL_GREEN"##                                                                            "$COL_DEFAULT

  if [ "${PKI_BACKUP}" == "true" ]; then
    echo -e $COL_GREEN"##                                                                            "$COL_DEFAULT
    echo -e $COL_GREEN"## Cluster PKI Credentials backup:                                            "$COL_DEFAULT
    echo -e $COL_GREEN"##                                                                            "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Cluster PKI Backup S3 Endpoint:    ${PKI_BACKUP_S3_ENDPOINT}          "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Cluster PKI Backup S3 Region:      ${PKI_BACKUP_S3_REGION}            "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Cluster PKI Backup S3 Access Key:  ${PKI_BACKUP_S3_ACCESS_KEY}        "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Cluster PKI Backup S3 Bucket:      ${PKI_BACKUP_S3_BUCKET}            "$COL_DEFAULT
    echo -e $COL_GREEN"##  - Cluster PKI Backup Storage prefix: ${PKI_BACKUP_STORAGE_PREFIX}       "$COL_DEFAULT
  fi
  echo -e $COL_GREEN"##                                                                            "$COL_DEFAULT
fi
if [ -n "${DOCKER_REGISTRY_MIRRORS}" ]; then
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"## Docker registry mirrors: ${DOCKER_REGISTRY_MIRRORS[*]}                       "$COL_DEFAULT
fi
if [ -n "${DOCKERHUB_USERNAME}" ]; then
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"##  - DockerHub username:  ${DOCKERHUB_USERNAME}                                "$COL_DEFAULT
  if [ -n "${DOCKERHUB_PASSWORD}" ]; then
    echo -e $COL_GREEN"##  - DockerHub password:  yes <hidden>                                          "$COL_DEFAULT
  else
    echo -e $COL_GREEN"##  - DockerHub password:  "$COL_RED_BOLD"NO (credentials won't be used!)        "$COL_DEFAULT
  fi
fi
if [ "${PRELOAD_DOCKER_IMAGES}" = "true" ]; then
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"## Preload cluster docker images: ${PRELOAD_DOCKER_IMAGES}                      "$COL_DEFAULT
fi
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
if [ -n "${CUSTOM_DIR}" ]; then
  echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Custom installations from: ${CUSTOM_DIR}                                  "$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
fi
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo
read -rsp $'--> Press any key to begin installation...\n' -n1 key
echo


## ################################################################################
## ##            INSTALL 'MOREUTILS' PACKAGE ON LOCAL MACHINE                    ##
## ################################################################################
## echo  -e $COL_GREEN" ************************************************************"$COL_DEFAULT
## echo  -e $COL_GREEN" *  Install 'moreutils' package for log tracing timestamps..."$COL_DEFAULT
## echo  -e $COL_GREEN" ************************************************************"$COL_DEFAULT
## sudo apt-get update -q && sudo apt-get install moreutils -y -q

# Prepare directory for storing remote installations output
LOGS_DIR="${LOCAL_WORKDIR}/logs_installer_${TIMESTAMP}"
mkdir -p "${LOGS_DIR}"


################################################################################
##          PRE-FLIGHT CHECKS: ALL NECESSARY FILE ARE ACCESSIBLE              ##
################################################################################
echo  -e $COL_GREEN" *******************************************************·"$COL_DEFAULT
echo  -e $COL_GREEN" *  Pre-Flight check: Access to all installation files..."$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************·"$COL_DEFAULT

echo  -e $COL_BLUE"* Check all installation files and directories are accessible"$COL_DEFAULT

NOT_ACCESSIBLE="$(find ${LOCAL_WORKDIR} \( -type d -not -executable \) -o \( -type f -not -readable \) 2> /dev/null)"
if [ "${NOT_ACCESSIBLE}" != "" ]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: Some required files or directories are not accessible. Check their permissions (recommended) or run the script as root."$COL_DEFAULT
  echo
  echo -e $COL_RED_BOLD"${NOT_ACCESSIBLE}"$COL_DEFAULT
  echo
  exit -1
fi

# If AWS DNS provider configuration directory is set, check it exists
if [[ -n "${MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR}" ]]; then
  # Check directory exists
  if [[ ! -d "${MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR}" ]]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: Route53 config directory (${MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR}) not found."$COL_DEFAULT
    echo
    exit -1
  fi
  # Check access to all files inside it
  NOT_ACCESSIBLE="$(find ${MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR} \( -type d -not -executable \) -o \( -type f -not -readable \) 2> /dev/null)"
  if [ "${NOT_ACCESSIBLE}" != "" ]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: Some files in Route53 config directory (${MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR}) can't be read."$COL_DEFAULT
    echo
    echo -e $COL_RED_BOLD"${NOT_ACCESSIBLE}"$COL_DEFAULT
    echo
    exit -1
  fi
fi

# If OVH DNS provider configuration file is set, check it exists
if [[ -n "${MANAGED_DNS_OVH_CONFIG_FILE}" ]]; then
  # Check file exists
  if [[ ! -f "${MANAGED_DNS_OVH_CONFIG_FILE}" ]]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: OVH DNS config file (${MANAGED_DNS_OVH_CONFIG_FILE}) not found."$COL_DEFAULT
    echo
    exit -1
  fi
fi

# If Custom scripts directory is set, check it exists
if [[ -n "${CUSTOM_DIR}" ]]; then
  # Check directory exists
  if [[ ! -d "${CUSTOM_DIR}" ]]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: Custom scripts directory (${CUSTOM_DIR}) not found."$COL_DEFAULT
    echo
    exit -1
  fi
  # Check access to all files inside it
  NOT_ACCESSIBLE="$(find ${CUSTOM_DIR} \( -type d -not -executable \) -o \( -type f -not -readable \) 2> /dev/null)"
  if [ "${NOT_ACCESSIBLE}" != "" ]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: Some files in Custom scripts directory (${CUSTOM_DIR}) can't be read."$COL_DEFAULT
    echo
    echo -e $COL_RED_BOLD"${NOT_ACCESSIBLE}"$COL_DEFAULT
    echo
    exit -1
  fi
fi


# If Reference Domain certificate directory is set, check it exists
if [[ -n "${REFERENCE_DOMAIN_CERT_DIR}" ]]; then
  # Check directory exists
  if [[ ! -d "${REFERENCE_DOMAIN_CERT_DIR}" ]]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: Reference Domain certificate directory (${REFERENCE_DOMAIN_CERT_DIR}) not found."$COL_DEFAULT
    echo
    exit -1
  fi
  # Check access to all files inside it
  NOT_ACCESSIBLE="$(find ${REFERENCE_DOMAIN_CERT_DIR} \( -type d -not -executable \) -o \( -type f -not -readable \) 2> /dev/null)"
  if [ "${NOT_ACCESSIBLE}" != "" ]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: Some files in Reference Domain certificate directory (${REFERENCE_DOMAIN_CERT_DIR}) can't be read."$COL_DEFAULT
    echo
    echo -e $COL_RED_BOLD"${NOT_ACCESSIBLE}"$COL_DEFAULT
    echo
    exit -1
  fi
else
  # Currently, it is mandatory to provide reference domain certificates since
  # the platform only support HTTPS.
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: Reference Domain certificate directory is not configured (mandatory)."$COL_DEFAULT
  echo
  exit -1
fi

# If Admission uses clientcert authorization
if [ "${ADMISSION_AUTHENTICATION_TYPE}" == "clientcertificate" ]; then
  # If Admission  certificate directory is set, check it exists
  if [[ -n "${ADMISSION_SERVER_CERT_DIR}" ]]; then
    # Check directory exists
    if [[ ! -d "${ADMISSION_SERVER_CERT_DIR}" ]]; then
      echo
      echo -e $COL_RED_BOLD"Pre-flight error: Admission certificate directory (${ADMISSION_SERVER_CERT_DIR}) not found."$COL_DEFAULT
      echo
      exit -1
    fi
    # Check access to all files inside it
    NOT_ACCESSIBLE="$(find ${ADMISSION_SERVER_CERT_DIR} \( -type d -not -executable \) -o \( -type f -not -readable \) 2> /dev/null)"
    if [ "${NOT_ACCESSIBLE}" != "" ]; then
      echo
      echo -e $COL_RED_BOLD"Pre-flight error: Some files in Admission certificate directory (${ADMISSION_SERVER_CERT_DIR}) can't be read."$COL_DEFAULT
      echo
      echo -e $COL_RED_BOLD"${NOT_ACCESSIBLE}"$COL_DEFAULT
      echo
      exit -1
    fi
  else
    # Currently, it is mandatory to provide admission certificates
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: Admission certificate directory is not configured (mandatory)."$COL_DEFAULT
    echo
    exit -1
  fi
fi

# If RootCA file is set, check it exists
if [[ -n "${CLUSTER_PKI_ROOT_CA_FILE}" ]]; then
  # Check file exists
  if [[ ! -f "${CLUSTER_PKI_ROOT_CA_FILE}" ]]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: RootCA file (${CLUSTER_PKI_ROOT_CA_FILE}) not found."$COL_DEFAULT
    echo
    exit -1
  fi
else
  # Currently, it is mandatory to provide RootCA
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: RootCA is not configured (mandatory)."$COL_DEFAULT
  echo
  exit -1
fi

# If CA for Monitoring endpoints file is set, check it exists
if [[ -n "${MONITORING_CLIENTCERT_TRUSTED_CA_FILE}" ]]; then
  # Check file exists
  if [[ ! -f "${MONITORING_CLIENTCERT_TRUSTED_CA_FILE}" ]]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: Monitoring CA file (${MONITORING_CLIENTCERT_TRUSTED_CA_FILE}) not found."$COL_DEFAULT
    echo
    exit -1
  fi
fi

# If Monitoring persistence security is enabled, check the necessary certificate files exist
if [ "${PROMETHEUS_PERSISTENCE}" = "true" ] && [ "${PROMETHEUS_REMOTE_AUTH_TYPE}" = "clientcertificate" ]; then
  # Check monitoring remote client certificate files exist
  if [[ ! -f "${PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_CERT_FILE}" ]]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: Monitoring remote client certificate file (${PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_CERT_FILE}) not found."$COL_DEFAULT
    echo
    exit -1
  fi
  if [[ ! -f "${PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_KEY_FILE}" ]]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: Monitoring remote client certificate key file (${PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_KEY_FILE}) not found."$COL_DEFAULT
    echo
    exit -1
  fi
fi

echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo

echo  -e $COL_BLUE"* Check Reference Domain certificates"$COL_DEFAULT

# If Reference Domain certificate dir is set, check it exists and contains
# a valid wildcard certificate for Reference Domain.
WILDCARD_REFERENCE_DOMAIN="*.${REFERENCE_DOMAIN}"

# For now we require a fixed naming for wildcard certificates
WILDCARD_CERT_CRT_FILE="${REFERENCE_DOMAIN_CERT_DIR}/wildcard.${REFERENCE_DOMAIN}.crt"
WILDCARD_CERT_KEY_FILE="${REFERENCE_DOMAIN_CERT_DIR}/wildcard.${REFERENCE_DOMAIN}.key"

# Check wildcard certificate CRT file exists
if [[ ! -f "${WILDCARD_CERT_CRT_FILE}" ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: Reference domain wildcard certificate crt file (${WILDCARD_CERT_CRT_FILE}) not found."$COL_DEFAULT
  echo
  echo -e $COL_RED"Make sure you have the following files in ${REFERENCE_DOMAIN_CERT_DIR} :"$COL_DEFAULT
  echo -e $COL_RED"- ${WILDCARD_CERT_CRT_FILE}"$COL_DEFAULT
  echo -e $COL_RED"- ${WILDCARD_CERT_KEY_FILE}"$COL_DEFAULT
  echo
  exit -1
fi
echo "File ${WILDCARD_CERT_CRT_FILE} exists."
# Check wildcard certificate KEY file exists
if [[ ! -f "${WILDCARD_CERT_KEY_FILE}" ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: Reference domain wildcard certificate crt file (${WILDCARD_CERT_KEY_FILE}) not found."$COL_DEFAULT
  echo
  echo -e $COL_RED"Make sure you have the following files in ${REFERENCE_DOMAIN_CERT_DIR} :"$COL_DEFAULT
  echo -e $COL_RED"- ${WILDCARD_CERT_CRT_FILE}"$COL_DEFAULT
  echo -e $COL_RED"- ${WILDCARD_CERT_KEY_FILE}"$COL_DEFAULT
  echo
  exit -1
fi
echo "File ${WILDCARD_CERT_KEY_FILE} exists."
# Check certificate corresponds to the expected domain
echo "Checking if ${WILDCARD_REFERENCE_DOMAIN} is included in certificate domains for ${WILDCARD_CERT_CRT_FILE}..."
# Get list of all domains included in the certificate
RAW_SAN_DATA="$(openssl x509 -noout -text -in ${WILDCARD_CERT_CRT_FILE} | grep DNS)"
# echo "RAW_SAN_DATA: \"${RAW_SAN_DATA}\""
CERTIFICATE_DOMAINS=$(echo "${RAW_SAN_DATA}" | grep -oP '(?<=DNS:|IP Address:)[^,]+')
# echo "Certificate SAN domains: \"${CERTIFICATE_DOMAINS}\""

# Look for the expected domain in the list
MATCH="false"
for DOM in ${CERTIFICATE_DOMAINS[@]}; do
  # echo "Dominio: \"$DOM\""
  if [ "${DOM}" == "${WILDCARD_REFERENCE_DOMAIN}" ]; then
    MATCH="true"
    break
  fi
done
if [ "${MATCH}" != "true" ]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: provided certificate is not valid for ${WILDCARD_REFERENCE_DOMAIN}."$COL_DEFAULT
  echo
  echo -e $COL_RED"Certificate is valid for the following domains:"$COL_DEFAULT
  echo
  for DOM in ${CERTIFICATE_DOMAINS[@]}; do
    echo -e $COL_RED"- $DOM"$COL_DEFAULT
  done
  echo
  exit -1
fi

# Check certificate validity
echo
echo "Checking validity of certificate for ${WILDCARD_CERT_CRT_FILE}..."
# Check if certificate is expired
openssl x509 -enddate -noout -in ${WILDCARD_CERT_CRT_FILE} -checkend 0
if [ "$?" != "0" ]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: provided certificate is expired."$COL_DEFAULT
  echo
  exit -1
fi

echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo

# If Admission uses clientcert authorization
if [ "${ADMISSION_AUTHENTICATION_TYPE}" == "clientcertificate" ]; then

  echo  -e $COL_BLUE"* Check Admission certificates"$COL_DEFAULT

  # For now we require a fixed naming certificate file
  ADMISSION_CERT_CRT_FILE="${ADMISSION_SERVER_CERT_DIR}/cert.crt"
  ADMISSION_CERT_KEY_FILE="${ADMISSION_SERVER_CERT_DIR}/cert.key"

  # Check Admission certificate CRT file exists
  if [[ ! -f "${ADMISSION_CERT_CRT_FILE}" ]]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: Admission certificate crt file (${ADMISSION_CERT_CRT_FILE}) not found."$COL_DEFAULT
    echo
    echo -e $COL_RED"Make sure you have the following files in ${ADMISSION_SERVER_CERT_DIR} :"$COL_DEFAULT
    echo -e $COL_RED"- ${ADMISSION_CERT_CRT_FILE}"$COL_DEFAULT
    echo -e $COL_RED"- ${ADMISSION_CERT_KEY_FILE}"$COL_DEFAULT
    echo
    exit -1
  fi
  echo "File ${ADMISSION_CERT_CRT_FILE} exists."
  # Check wildcard certificate KEY file exists
  if [[ ! -f "${ADMISSION_CERT_KEY_FILE}" ]]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: Admission certificate crt file (${ADMISSION_CERT_KEY_FILE}) not found."$COL_DEFAULT
    echo
    echo -e $COL_RED"Make sure you have the following files in ${ADMISSION_SERVER_CERT_DIR} :"$COL_DEFAULT
    echo -e $COL_RED"- ${ADMISSION_CERT_CRT_FILE}"$COL_DEFAULT
    echo -e $COL_RED"- ${ADMISSION_CERT_KEY_FILE}"$COL_DEFAULT
    echo
    exit -1
  fi
  echo "File ${ADMISSION_CERT_KEY_FILE} exists."

  # Check certificate corresponds to the expected domain
  ADMISSION_DOMAIN="admission-${RELEASE_NAME}.${REFERENCE_DOMAIN}"
  echo "Checking if ${ADMISSION_DOMAIN} is included in certificate domains for ${ADMISSION_CERT_CRT_FILE}..."
  # Get list of all domains included in the certificate
  RAW_SAN_DATA="$(openssl x509 -noout -text -in ${ADMISSION_CERT_CRT_FILE} | grep DNS)"
  CERTIFICATE_DOMAINS=$(echo "${RAW_SAN_DATA}" | grep -oP '(?<=DNS:|IP Address:)[^,]+')
  # Look for the expected domain in the list
  MATCH="false"
  for DOM in ${CERTIFICATE_DOMAINS[@]}; do
    if [ "${DOM}" == "${ADMISSION_DOMAIN}" ]; then
      MATCH="true"
      break
    fi
  done
  if [ "${MATCH}" != "true" ]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: provided certificate is not valid for ${ADMISSION_DOMAIN}."$COL_DEFAULT
    echo
    echo -e $COL_RED"Certificate is valid for the following domains:"$COL_DEFAULT
    echo
    for DOM in ${CERTIFICATE_DOMAINS[@]}; do
      echo -e $COL_RED"- $DOM"$COL_DEFAULT
    done
    echo
    exit -1
  fi

  # Check certificate validity
  echo
  echo "Checking validity of certificate for ${ADMISSION_CERT_CRT_FILE}..."
  # Check if certificate is expired
  openssl x509 -enddate -noout -in ${ADMISSION_CERT_CRT_FILE} -checkend 0
  if [ "$?" != "0" ]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: provided certificate is expired."$COL_DEFAULT
    echo
    exit -1
  fi

  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi

echo  -e $COL_BLUE"* Check RootCA"$COL_DEFAULT

# Check certificate validity
echo
echo "Checking validity of RootCA ${CLUSTER_PKI_ROOT_CA_FILE}..."
# Check if certificate is expired
openssl x509 -enddate -noout -in ${CLUSTER_PKI_ROOT_CA_FILE} -checkend 0
if [ "$?" != "0" ]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: provided certificate is expired."$COL_DEFAULT
  echo
  exit -1
fi

if [[ -n "${MONITORING_CLIENTCERT_TRUSTED_CA_FILE}" ]]; then
  # Check certificate validity
  echo
  echo "Checking validity of Monitoring CA ${MONITORING_CLIENTCERT_TRUSTED_CA_FILE}..."
  # Check if certificate is expired
  openssl x509 -enddate -noout -in ${MONITORING_CLIENTCERT_TRUSTED_CA_FILE} -checkend 0
  if [ "$?" != "0" ]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: provided certificate is expired."$COL_DEFAULT
    echo
    exit -1
  fi
fi

echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo

# If DockerHub credentials are provided, verify they are valid
if [[ -n "${DOCKERHUB_USERNAME}" && -n "${DOCKERHUB_PASSWORD}" ]]; then
  echo  -e $COL_BLUE"* Validate DockerHub credentials"$COL_DEFAULT

  if ! curl -f --user "${DOCKERHUB_USERNAME}:${DOCKERHUB_PASSWORD}" "https://auth.docker.io/token" 2>/dev/null 1>/dev/null ; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: invalid DockerHub credentials."$COL_DEFAULT
    echo
    echo -e $COL_RED_BOLD"You can test them by running: curl -u <yourusername>:<yourpassword> https://auth.docker.io/token"$COL_DEFAULT
    echo
    exit -1
  else
    echo
    echo -e $COL_GREEN"Passed."$COL_DEFAULT
    echo
  fi
fi


################################################################################
##                PRE-FLIGHT CHECKS: DNS DOMAINS (IF UNMANAGED DNS)           ##
################################################################################

source "${COMMON_DIR}/preflight-checks/platform-dns-domain.sh"


################################################################################
##                PRE-FLIGHT CHECKS: SSH ACCESS TARGET HOSTS                  ##
################################################################################
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" *  Pre-Flight check: SSH access to all machines..."$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT

ALL_IPS=("${SUPER_MASTER_IP}" "${MASTERS_IPS[@]}" "${WORKERS_IPS[@]}")


echo  -e $COL_BLUE"* Remove hosts fingerprints from known hosts list"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ssh-keygen -R ${IP} >/dev/null 2>&1
done
echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo


echo  -e $COL_BLUE"* Check SSH connectivity and refresh hosts fingerprints"$COL_DEFAULT

UNREACHABLE=()
for IP in ${ALL_IPS[@]}; do
  ssh-keyscan ${IP} 2>&1 | grep -vE "^$|route|getaddrinfo" > /dev/null
  [ $? != 0 ] && UNREACHABLE+=(${IP})
done
if [[ "${#UNREACHABLE[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: SSH connectivity problem with the following machines:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${UNREACHABLE[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


echo  -e $COL_BLUE"* Add hosts SSH fingerprints"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ssh-keyscan -H ${IP} >> ${HOME}/.ssh/known_hosts 2>/dev/null
done
echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo


echo  -e $COL_BLUE"* Check user can connect via SSH"$COL_DEFAULT

FAILCONNECT=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
for IP in ${ALL_IPS[@]}; do
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} uptime 2> /dev/null)"
  [ "${RES}" == "" ] && FAILCONNECT+=(${IP})
done
if [[ "${#FAILCONNECT[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: Couldn't connect via SSH to the folowing machines:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAILCONNECT[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


################################################################################
##        PRE-FLIGHT CHECKS: HOSTS USE A SUPPORTED OS VERSION                 ##
################################################################################
echo  -e $COL_BLUE"* Check node OS version"$COL_DEFAULT

# Currently only one OS version is supported per distribution
SUPPORTED_OS="${OS_FLAVOUR} ${OS_VERSION}"
echo
echo "Supported OS: ${SUPPORTED_OS}"
echo

FAIL_OS=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
COMMAND_GET_OS="echo \"\$(lsb_release -is) \$(lsb_release -rs)\""
for IP in ${ALL_IPS[@]}; do
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \(${COMMAND_GET_OS}\))"
  # echo "RES= ${RES} --> ${RES,,}   (expected: ${SUPPORTED_OS,,})"

  # Lowercase comparison --> lowercase conversion in bash: ${variable,,}
  [ "${RES,,}" != "${SUPPORTED_OS,,}" ] && FAIL_OS+=("${IP}:${RES}")
done
if [[ "${#FAIL_OS[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: The following machines failed OS version (${SUPPORTED_OS}) check:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAIL_OS[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


################################################################################
##      PRE-FLIGHT CHECKS: HOSTS USE A SUPPORTED KERNEL VERSION               ##
################################################################################
echo  -e $COL_BLUE"* Check node kernel version"$COL_DEFAULT

# Currently only one kernel version is supported per distribution (if set)
echo
if [ -n "${KERNEL_VERSION}" ]; then
  echo "Supported kernel version: ${KERNEL_VERSION}"

  FAIL_KERNEL=()
  CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
  COMMAND_GET_KERNEL="uname -r"
  for IP in ${ALL_IPS[@]}; do
    RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \(${COMMAND_GET_KERNEL}\))"
    [[ "${RES}" =~ ^${KERNEL_VERSION}.* ]] || FAIL_KERNEL+=("${IP} (${RES})  ")
  done
  if [[ "${#FAIL_KERNEL[@]}" -ne 0 ]]; then
    # Allow operator to continue even if the check fails
    # echo
    # echo -e $COL_RED_BOLD"Pre-flight error: The following machines failed kernel version (${KERNEL_VERSION}) check:"$COL_DEFAULT
    # echo -e $COL_RED_BOLD"                  ${FAIL_KERNEL[*]}"$COL_DEFAULT
    # echo
    # exit -1
    echo
    echo -e $COL_YELLOW_BOLD"Pre-flight warning: The following machines failed kernel version (${KERNEL_VERSION}) check:"$COL_DEFAULT
    echo -e $COL_YELLOW_BOLD"                  ${FAIL_KERNEL[*]}"$COL_DEFAULT
    echo
    echo -e $COL_YELLOW_BOLD"You can proceed with installation with a different kernel than the recommended at your own risk."$COL_DEFAULT
    read -rsp $'--> Press any key to accept the risk and continue, or press CTRL-C to exit...\n' -n1 key
    echo
  else
    echo
    echo -e $COL_GREEN"Passed."$COL_DEFAULT
    echo
  fi
else
  echo "No specific kernel version required. Skipped kernel version check."
fi



################################################################################
##        PRE-FLIGHT CHECKS: HOSTS DON'T USE SWAP                             ##
################################################################################
echo  -e $COL_BLUE"* Check node swap is disabled"$COL_DEFAULT

FAIL_SWAP=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
COMMAND_GET_SWAP="sudo swapon --show"
for IP in ${ALL_IPS[@]}; do
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \(${COMMAND_GET_SWAP}\))"
  [ "${RES}" != "" ] && FAIL_SWAP+=("${IP}")
done
if [[ "${#FAIL_SWAP[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: The following machines have swap enabled:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAIL_SWAP[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


################################################################################
##        PRE-FLIGHT CHECKS: MASTER NODES HAVE ENOUGH RAM                     ##
################################################################################
if [ -n "${MASTER_MIN_MEMORY_KB}" ]; then
  echo  -e $COL_BLUE"* Check Master Nodes have enough memory (more than ${MASTER_MIN_MEMORY_KB} KB)"$COL_DEFAULT

  FAIL_MEM=()
  CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
  COMMAND_GET_MEM="free -m | awk '/^Mem:/{print \$2}'"
  for IP in ${INITIAL_MASTERS_IPS[@]}; do
    RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \(${COMMAND_GET_MEM}\))"
    [ "${RES}" -gt "${MASTER_MIN_MEMORY_KB}" ] || FAIL_MEM+=("${IP}")
  done
  if [[ "${#FAIL_MEM[@]}" -ne 0 ]]; then
    echo
    echo -e $COL_RED_BOLD"Pre-flight error: The following machines don't meet the memory requirements:"$COL_DEFAULT
    echo -e $COL_RED_BOLD"                  ${FAIL_MEM[*]}"$COL_DEFAULT
    echo
    exit -1
  else
    echo
    echo -e $COL_GREEN"Passed."$COL_DEFAULT
    echo
  fi
fi


################################################################################
##        PRE-FLIGHT CHECKS: HOSTS INTERNET ACCESS AND DNS RESOLUTION         ##
################################################################################
echo  -e $COL_BLUE"* Check node internet access and DNS"$COL_DEFAULT

FAIL_INTERNET=()
FAIL_DNS=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
TEST_TARGET_IP="8.8.8.8"
TEST_TARGET_DOMAIN="google.com"
COMMAND_TEST_IP="if nc -zw2 ${TEST_TARGET_IP} 443; then echo OK; else echo ERROR; fi"
COMMAND_TEST_DOMAIN="if nc -zw2 ${TEST_TARGET_DOMAIN} 443; then echo OK; else echo ERROR; fi"
for IP in ${ALL_IPS[@]}; do
  # RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \( hostname \))"
  # echo "RES= ${RES}"
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \(${COMMAND_TEST_IP}\))"
  # echo "RES= ${RES}"
  [ "${RES}" != "OK" ] && FAIL_INTERNET+=(${IP})
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} \(${COMMAND_TEST_DOMAIN}\))"
  # echo "RES= ${RES}"
  [ "${RES}" != "OK" ] && FAIL_DNS+=(${IP})
done
if [[ "${#FAIL_INTERNET[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: The following machines failed Internet access check:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAIL_INTERNET[*]}"$COL_DEFAULT
  echo
fi
if [[ "${#FAIL_DNS[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: The following machines failed DNS resolution check:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAIL_DNS[*]}"$COL_DEFAULT
  echo
fi
if [[ "${#FAIL_DNS[@]}" -ne 0 || "${#FAIL_INTERNET[@]}" -ne 0 ]]; then
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


# Create .kumori directory in all remote machines
echo  -e $COL_BLUE"* Creating .kumori directory in remote machines"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} mkdir -p .kumori &
done
wait

# Deactivate login welcome info
echo  -e $COL_BLUE"* Deactivating login welcome info in remote machines"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} touch .hushlogin &
done
wait

################################################################################
##                        START REMOTE INSTALLATION                           ##
################################################################################
IP="${SUPER_MASTER_IP}"
OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" *  Processing PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
. ${LOCAL_WORKDIR}/control-install-master-primary.sh \
  |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
  |& tee -a ${OUTPUT_FILE} \
  |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &

# Primary master's Kubelet must be up before secondary masters and nodes can
# join the cluster.
wait
echo
echo  -e $COL_GREEN" *****************************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" * Super-Master installation finished. Prepare secondary masters..."$COL_DEFAULT
echo  -e $COL_GREEN" *****************************************************************"$COL_DEFAULT
echo

for MASTER_IP in ${MASTERS_IPS[@]}; do
  IP="${MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/master_${IP}.log"
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" * Processing MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-install-master-secondary.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}MASTER ${IP}$COL_DEFAULT]") /" &
done

wait

# Master cluster joins are not parallelized to avoid Etcd overload
FIRST="true"
for MASTER_IP in ${MASTERS_IPS[@]}; do
  if [[ "${FIRST}" == "true" ]]; then
    FIRST="false"
  else
    echo
    echo  -e $COL_BLUE"* Waiting one minute for previous master join to stabilize..."$COL_DEFAULT
    echo
    sleep 60s
  fi
  IP="${MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/master_${IP}.log"
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" * Joining MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-join-master-secondary.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}MASTER ${IP}$COL_DEFAULT]") /"
done

echo
echo  -e $COL_GREEN" *****************************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" * Secondary masters installation finished. Prepare workers..."$COL_DEFAULT
echo  -e $COL_GREEN" *****************************************************************"$COL_DEFAULT
echo

for WORKER_IP in ${WORKERS_IPS[@]}; do
  IP="${WORKER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/worker_${IP}.log"
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" * Processing WORKER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-install-workers.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}WORKER ${IP}$COL_DEFAULT]") /" &
done

# Wait for any previous spawn processes to finish (for all master and worker
# machines to be ready)
wait
echo
echo  -e $COL_GREEN" ****************************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" * All parallel instalation processes finished. Install addons..."$COL_DEFAULT
echo  -e $COL_GREEN" ****************************************************************"$COL_DEFAULT
echo

IP="${SUPER_MASTER_IP}"
OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
if [ "${USE_ETCDBACKUP}" = "true" ]; then
  echo  -e $COL_GREEN" ****************************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" *  Addons will not be installed (using a restored etcd backup)"$COL_DEFAULT
  echo  -e $COL_GREEN" ****************************************************************"$COL_DEFAULT
else
  echo  -e $COL_GREEN" ****************************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" *  Installing addons in PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" ****************************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-install-addons.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /"
fi

wait


echo
IP="${SUPER_MASTER_IP}"
OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
if [ "${PKI_BACKUP}" = "true" ]; then
  if [ -f "${LOCAL_WORKDIR}/control-backup-pki.sh" ]; then
    echo  -e $COL_GREEN" ****************************************************************"$COL_DEFAULT
    echo  -e $COL_GREEN" *  Backing up Cluster PKI certificates                          "$COL_DEFAULT
    echo  -e $COL_GREEN" ****************************************************************"$COL_DEFAULT
    . ${LOCAL_WORKDIR}/control-backup-pki.sh \
      |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
      |& tee -a ${OUTPUT_FILE} \
      |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /"
  else
    echo  -e $COL_GREEN" *  Skipped Cluster PKI certificates backup (not supported)."$COL_DEFAULT
  fi
fi
echo

# Reactivate login welcome info
echo  -e $COL_BLUE"* Reactivating login welcome info in remote machines"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} rm -f .hushlogin &
done
wait

echo
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                   CLUSTER INSTALLATION COMPLETED                           ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## CLUSTER TOPOLOGY:                                                            "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Masters IPs        : ${INITIAL_MASTERS_IPS[*]}                            "$COL_DEFAULT
echo -e $COL_GREEN"##  - Workers IPs        : ${WORKERS_IPS[*]}                                    "$COL_DEFAULT
echo -e $COL_GREEN"##  - Ingress nodes IPs  : ${INGRESS_NODES_IPS[*]}                              "$COL_DEFAULT
if [ -n "${STORAGE_NODES_IPS}" ]; then
  echo -e $COL_GREEN"##  - Storage Nodes IPs  : ${STORAGE_NODES_IPS[*]}                              "$COL_DEFAULT
fi
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo
