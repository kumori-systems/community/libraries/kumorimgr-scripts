#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

echo
echo -e $COL_BLUE"* Uploading files to remote machine ${IP}..."$COL_DEFAULT
scp -i ${SSH_KEY} -r ${LOCAL_WORKDIR} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}
scp -i ${SSH_KEY} -r ${COMMON_DIR}/* ${SSH_USER}@${IP}:${REMOTE_WORKDIR}
[[ -n "${CUSTOM_DIR}" ]] && scp -i ${SSH_KEY} -r ${CUSTOM_DIR} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/custom
echo -e $COL_BLUE"* Upload finished."$COL_DEFAULT
echo

# ${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} 'chmod +x installer/*.sh'

echo
echo -e $COL_BLUE"* Executing remote installer - PHASE 1..."$COL_DEFAULT
${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} << EOF
 cd ${REMOTE_WORKDIR}
 echo >> variables.sh
 echo "NODE_IP=\"$IP\"" >> variables.sh
 chmod +x *.sh
 ./masters_0_install-base.sh
 ./masters_9_keepalived-enable.sh
EOF
echo -e $COL_BLUE"* Installer PHASE 1 finished."$COL_DEFAULT
echo

echo
echo -e $COL_BLUE"* Determining if a reboot is required after PHASE 1..."$COL_DEFAULT
CMD_CHEK_REBOOT="[ -f /var/run/reboot-required ] && echo 'REBOOT REQUIRED'"
RES="$(ssh -i ${SSH_KEY} ${SSH_USER}@${IP} \(${CMD_CHEK_REBOOT}\))"
if [ "${RES}" == "REBOOT REQUIRED" ]; then
  REBOOT_REQUIRED="true"
  echo "Reboot is required."
else
  REBOOT_REQUIRED="false"
  echo "Reboot is not required."
fi

if [ "${REBOOT_REQUIRED}" == "true" ]; then
  echo
  echo -e $COL_BLUE"* Rebooting remote machine..."$COL_DEFAULT
  # Do not indent!
  ${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} << EOF
{ sleep 5; sudo shutdown -r 0; } >/dev/null &
EOF
  echo
  echo -e $COL_BLUE"* Waiting for remote machine to reboot..."$COL_DEFAULT
  sleep 30s
  while ! timeout -k 5 10 ${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} uptime; do
    echo "Still unavailable..."
    sleep 5
  done
  echo -e $COL_BLUE"* Remote machine is back online, wait a minute for everything to be up..."$COL_DEFAULT
  sleep 30s
fi

