#!/bin/bash

# It should be noted that no credentials are required. With AWS, the node
# itself can access the API (via IAM roles)

kubectl apply -f ./manifests
