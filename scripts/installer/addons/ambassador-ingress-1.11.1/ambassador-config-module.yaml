# ------------------------------------------------------------------------------
#
# Ambassador module defines system-wide configuration.
#
# For now:
# - gzip compression module:
#   https://www.getambassador.io/docs/latest/topics/running/gzip/#the-gzip-api
#   https://www.envoyproxy.io/docs/envoy/latest/configuration/http/http_filters/compressor_filter#config-http-filters-compressor
# - Retry on Gateway errors
# - Disable Envoy circuit breakers
#
# ------------------------------------------------------------------------------
apiVersion: getambassador.io/v2
kind: Module
metadata:
  name: ambassador  # Mandatory: name=ambassador
  namespace: {{INGRESS_NAMESPACE}}
spec:
  config:
    # Add support for HTTP gzip compression headers
    gzip:
      enabled: true # Default configuration (can be refined)
    # Add retries when internal requests to backends fail
    # For now we just configure one single retry limited to gateway errors (for
    # getting rid of sparse 503 errors)
    retry_policy:
      retry_on: gateway-error
      num_retries: 1
    # Disable Envoy circuit breakers. This is the documented way to disable it.
    # Source: https://www.envoyproxy.io/docs/envoy/latest/faq/load_balancing/disable_circuit_breaking
    circuit_breakers:
    - max_connections: 1000000000
      max_pending_requests: 1000000000
      max_requests: 1000000000
      max_retries: 1000000000
      priority: default
    - max_connections: 1000000000
      max_pending_requests: 1000000000
      max_requests: 1000000000
      max_retries: 1000000000
      priority: high
    # Disable exposing '/ambassador/v0/diag' API
    # NOTE: Ambassador 1.11.1 exposes it also in the ingress port so it's
    # publicly accessible unless disabled (v0.86.1 didn't)
    diagnostics:
      enabled: false
    # Set the name Envoy sets as 'server_name' response header (so we can
    # differentiate it from balancer Envoy responses)
    server_name: ambassador-envoy
    use_proxy_proto: {{USE_PROXY_PROTOCOL}}
    # The following LUA script removes the port from the 'authority' header.
    # Ambassador 1.x does not support Host headers that include non-standard ports
    # and rejects the requests as 404.
    lua_scripts: |
      function envoy_on_request(request_handle)
        local authority = string.lower(request_handle:headers():get(":authority"))
        if(string.find(authority, ":") ~= nil)
        then
          local authority_index = string.find(authority, ":")
          local stripped_authority = string.sub(authority, 1, authority_index - 1)
          request_handle:headers():replace(":authority", stripped_authority)
        else
          request_handle:headers():replace(":authority", authority)
        end
      end

