# ------------------------------------------------------------------------------
# DaemonSet
# One Ambassador pod is deployed in each node, opening the following pod/host ports:
# - 8443/443 (https listener)
# - 8877/8080 (Ambassador admin API)
# - Configured TCP ports
# Resource limits/requests should be adjusted.
# ------------------------------------------------------------------------------
# - Taken from: https://www.getambassador.io/yaml/ambassador/ambassador-rbac.yaml
# - Changes by Kumori:
#   - Type changed from 'deployment' to 'daemonset' (and remove replicas)
#   - Replaced pod affinity with node selector (only ingress nodes)
#   - Added 'namespace: kumori' where necessary
#   - Added some configuration variables not set in the original:
#     - AMBASSADOR_SINGLE_NAMESPACE
#     - AMBASSADOR_DRAIN_TIME
#     - AMBASSADOR_FAST_RECONFIGURE
#     - AES_LOG_LEVEL
#   - Set our custom port list
#   - Increased limits
#   - Added label 'type: ingress'
#   - Set 'dnsPolicy: ClusterFirstWithHostNet'
#   - Set 'updateStrategy: RollingUpdate'
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: ambassador
  namespace: {{INGRESS_NAMESPACE}}
spec:
  selector:
    matchLabels:
      service: ambassador
  template:
    metadata:
      annotations:
        consul.hashicorp.com/connect-inject: 'false'
        sidecar.istio.io/inject: 'false'
      labels:
        service: ambassador
        app.kubernetes.io/managed-by: getambassador.io
        type: ingress
    spec:
      containers:
      - name: ambassador
        image: docker.io/datawire/ambassador:{{AMBASSADOR_VERSION}}
        env:
        - name: AMBASSADOR_SINGLE_NAMESPACE
        - name: AMBASSADOR_DRAIN_TIME
          value: "600"
        - name: AMBASSADOR_FAST_RECONFIGURE
          value: "true"
        - name: AES_LOG_LEVEL
          value: info
        - name: AMBASSADOR_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: HOST_IP
          valueFrom:
            fieldRef:
              fieldPath: status.hostIP
        ports:
        - name: http
          containerPort: 8080
          hostPort: 80
        - name: https
          containerPort: 8443
          hostPort: {{INGRESS_INTERNAL_PORT}}
        - name: admin
          containerPort: 8877
          hostPort: 8080
        livenessProbe:
          httpGet:
            path: /ambassador/v0/check_alive
            port: 8877
          initialDelaySeconds: 30
          periodSeconds: 3
        readinessProbe:
          httpGet:
            path: /ambassador/v0/check_ready
            port: 8877
          initialDelaySeconds: 30
          periodSeconds: 3
        resources:
          limits:
            cpu: {{AMBASSADOR_CPU_LIMIT}}
            memory: {{AMBASSADOR_MEM_LIMIT}}
          requests:
            cpu: {{AMBASSADOR_CPU_REQUEST}}
            memory: {{AMBASSADOR_MEM_REQUEST}}
        securityContext:
          allowPrivilegeEscalation: false
        volumeMounts:
        - mountPath: /tmp/ambassador-pod-info
          name: ambassador-pod-info
      dnsPolicy: ClusterFirstWithHostNet
      nodeSelector:
        kumori/ingress: "true"
      restartPolicy: Always
      securityContext:
        runAsUser: 8888
      serviceAccountName: ambassador
      terminationGracePeriodSeconds: 30
      volumes:
      - name: ambassador-pod-info
        downwardAPI:
          items:
          - fieldRef:
              fieldPath: metadata.labels
            path: labels
  updateStrategy:
    type: RollingUpdate
