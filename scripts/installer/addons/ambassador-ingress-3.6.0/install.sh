#!/bin/bash

# Build OutOfService full docker image name and crete credentials secret if necessary
if [ -z "${OUTOFSERVICE_IMAGE}" ]; then
  # If no custom image is provided, use default Kumori OutOfService image
  OUTOFSERVICE_FULL_DOCKER_IMAGE="${KUMORI_IMAGES_REGISTRY}/${KUMORI_OUTOFSERVICE_IMAGE}:v${KUMORI_OUTOFSERVICE_VERSION}"
  OUTOFSERVICE_USERNAME=""
else
  if [ -n "${OUTOFSERVICE_REGISTRY}" ]; then
    OUTOFSERVICE_FULL_DOCKER_IMAGE="${OUTOFSERVICE_REGISTRY}/${OUTOFSERVICE_IMAGE}"
  else
    OUTOFSERVICE_FULL_DOCKER_IMAGE="${OUTOFSERVICE_IMAGE}"
  fi
fi

# Prepare OutOfService deployment manifest
sed -i \
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" \
  -e "s?{{OUTOFSERVICE_IMAGE}}?${OUTOFSERVICE_FULL_DOCKER_IMAGE}?g" \
  outofservice.yaml

# Prepare apiext template
APIEXTFILE="ambassador-apiext.yaml"
cp ${APIEXTFILE}_template ${APIEXTFILE}
CALCULATED_AMBASSADOR_APIEXT_REPLICAS="1"
if [ "${AMBASSADOR_APIEXT_HA}" = "true" ]; then
  CALCULATED_AMBASSADOR_APIEXT_REPLICAS=${AMBASSADOR_APIEXT_REPLICAS}
fi
sed -i \
  -e "s?{{AMBASSADOR_APIEXT_REPLICAS}}?${CALCULATED_AMBASSADOR_APIEXT_REPLICAS}?g" \
  -e "s?{{AMBASSADOR_APIEXT_CPU_REQUEST}}?${AMBASSADOR_APIEXT_CPU_REQUEST}?g" \
  -e "s?{{AMBASSADOR_APIEXT_CPU_LIMIT}}?${AMBASSADOR_APIEXT_CPU_LIMIT}?g" \
  -e "s?{{AMBASSADOR_APIEXT_MEM_REQUEST}}?${AMBASSADOR_APIEXT_MEM_REQUEST}?g" \
  -e "s?{{AMBASSADOR_APIEXT_MEM_LIMIT}}?${AMBASSADOR_APIEXT_MEM_LIMIT}?g" \
  ${APIEXTFILE}

# Daemonset template. It depends on the IPFeature and AuthService features are
# enabled or not.
# The ambassador DaemonSet YAML file is created from templates to start with a
# clean file, since it is modified with YQ/JQ to add elements.
if [ "${INSTALL_IPFILTERING}" == "true" ] || [ "${INSTALL_AUTHSERVICE}" == "true" ]; then

  rm -f ambassador-daemonset-hostport-advanced.yaml
  cp ambassador-daemonset-hostport-advanced.yaml_template ambassador-daemonset-hostport-advanced.yaml

  if [ "${INSTALL_IPFILTERING}" == "true" ]; then
    IPFILTERING_CONTAINER=$(cat ipfiltering_container.yaml_part)
    IPFILTERING_CONFIGMAP=$(cat ipfiltering_configmap.yaml_part)
    sed -i \
      -e "/{{IPFILTERING_CONTAINER_PLACEHOLDER}}/r ipfiltering_container.yaml_part" -e "s///" \
      -e "/{{IPFILTERING_CONFIGMAP_PLACEHOLDER}}/r ipfiltering_configmap.yaml_part" -e "s///" \
      ambassador-daemonset-hostport-advanced.yaml
  else
    # Remove placeholders
    sed -i \
      -e "/^.*{{IPFILTERING_CONTAINER_PLACEHOLDER}}.*$/d" \
      -e "/^.*{{IPFILTERING_CONFIGMAP_PLACEHOLDER}}.*$/d" \
      ambassador-daemonset-hostport-advanced.yaml
  fi

  if [ "${INSTALL_AUTHSERVICE}" == "true" ]; then
    sed -i \
      -e "/{{AUTHSERVICE_CONTAINER_PLACEHOLDER}}/r authservice_container.yaml_part" -e "s///" \
      ambassador-daemonset-hostport-advanced.yaml
  else
    # Remove placeholders
    sed -i -e "/^.*{{AUTHSERVICE_CONTAINER_PLACEHOLDER}}.*$/d" ambassador-daemonset-hostport-advanced.yaml
  fi

  DAEMONSETFILE="ambassador-daemonset-hostport-advanced.yaml"

else
  rm -f ambassador-daemonset-hostport.yaml
  cp ambassador-daemonset-hostport.yaml_template ambassador-daemonset-hostport.yaml
  DAEMONSETFILE="ambassador-daemonset-hostport.yaml"
fi

# Ambassador module must be configured with
# (1) If ADMISSION_SERVER_CLIENT_CERT_VALIDATE_WITH_TRUSTED_CA == true and
#     INGRESS_CERTHEADER_FORMAT=XFCC, then Ambassador must clean and set the
#     x-forwarded-for-client header
# (2) If ADMISSION_SERVER_CLIENT_CERT_VALIDATE_WITH_TRUSTED_CA == false or
#     INGRESS_CERTHEADER_FORMAT=PEM, then Ambassador must just clent the
#     x-forwarded-for-client header
AMBASSADOR_XFCC_DETAILS="SANITIZE"
AMBASSADOR_SET_DETAILS_PLACEHOLDER=""
if [ "${INGRESS_CERTHEADER_FORMAT}" == "XFCC" ] || \
   [ "ADMISSION_SERVER_CLIENT_CERT_VALIDATE_WITH_TRUSTED_CA" == true ]; then
  AMBASSADOR_XFCC_DETAILS="SANITIZE_SET"
  AMBASSADOR_SET_DETAILS_PLACEHOLDER="set_current_client_cert_details: { cert: true, chain: true }"
fi

sed -i \
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" \
  -e "s?{{AMBASSADOR_XFCC_DETAILS}}?${AMBASSADOR_XFCC_DETAILS}?g" \
  -e "s?{{AMBASSADOR_SET_DETAILS_PLACEHOLDER}}?${AMBASSADOR_SET_DETAILS_PLACEHOLDER}?g" \
  ambassador-config-module.yaml
sed -i \
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" \
  -e "s?{{AMBASSADOR_VERSION}}?${AMBASSADOR_VERSION}?g" \
  -e "s?{{INGRESS_INTERNAL_PORT}}?${INGRESS_INTERNAL_PORT}?g" \
  -e "s?{{AMBASSADOR_CPU_REQUEST}}?${AMBASSADOR_CPU_REQUEST}?g" \
  -e "s?{{AMBASSADOR_CPU_LIMIT}}?${AMBASSADOR_CPU_LIMIT}?g" \
  -e "s?{{AMBASSADOR_MEM_REQUEST}}?${AMBASSADOR_MEM_REQUEST}?g" \
  -e "s?{{AMBASSADOR_MEM_LIMIT}}?${AMBASSADOR_MEM_LIMIT}?g" \
  ${DAEMONSETFILE}
sed -i -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" ambassador-rbac.yaml
sed -i -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" ambassador-service.yaml
sed -i -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" ingress-namespace.yaml

# If IPFiltering or Authservice is enabled, then access to Kumori docker images
# must be configured
if [ "${INSTALL_IPFILTERING}" == "true" ] || [ "${INSTALL_AUTHSERVICE}" == "true" ]; then
  # Determine the appropriate image pull Secret property for Controller manifests
  KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET="kumori-enterprise-registry-credentials"
  if [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ] && [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
    CONTROLLERS_IMAGE_PULL_SECRETS="[ { name: ${KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET} } ]"
  else
    CONTROLLERS_IMAGE_PULL_SECRETS="[]"
  fi
fi

# If IPFiltering is enabled, replace configuration variables
if [ "${INSTALL_IPFILTERING}" == "true" ]; then
  sed -i \
    -e "s?{{KUMORI_IPFILTERING_VERSION}}?${KUMORI_IPFILTERING_VERSION}?g" \
    -e "s?{{IPFILTERING_CPU_REQUEST}}?${IPFILTERING_CPU_REQUEST}?g" \
    -e "s?{{IPFILTERING_CPU_LIMIT}}?${IPFILTERING_CPU_LIMIT}?g" \
    -e "s?{{IPFILTERING_MEM_REQUEST}}?${IPFILTERING_MEM_REQUEST}?g" \
    -e "s?{{IPFILTERING_MEM_LIMIT}}?${IPFILTERING_MEM_LIMIT}?g" \
    -e "s?{{KUMORI_IMAGES_REGISTRY}}?${KUMORI_IMAGES_REGISTRY}?g" \
    -e "s?{{CONTROLLERS_IMAGE_PULL_SECRETS}}?${CONTROLLERS_IMAGE_PULL_SECRETS}?g" \
    ${DAEMONSETFILE}
  sed -i \
    -e "s?{{IPFILTERING_REJECTIFFAILURE}}?${IPFILTERING_REJECTIFFAILURE}?g"\
    -e "s?{{IPFILTERING_TIMEOUTMSEC}}?${IPFILTERING_TIMEOUTMSEC}?g"\
    -e "s?{{IPFILTERING_LOGLEVEL}}?${IPFILTERING_LOGLEVEL}?g"\
    -e "s?{{IPFILTERING_MAXLISTSIZE}}?${IPFILTERING_MAXLISTSIZE}?g"\
    ambassador-ipfiltering-stuff.yaml
fi

# If AuthService is enabled, replace configuration variables
if [ "${INSTALL_AUTHSERVICE}" == "true" ]; then
  sed -i \
    -e "s?{{KUMORI_AUTHSERVICE_VERSION}}?${KUMORI_AUTHSERVICE_VERSION}?g" \
    -e "s?{{AUTHSERVICE_CPU_REQUEST}}?${AUTHSERVICE_CPU_REQUEST}?g" \
    -e "s?{{AUTHSERVICE_CPU_LIMIT}}?${AUTHSERVICE_CPU_LIMIT}?g" \
    -e "s?{{AUTHSERVICE_MEM_REQUEST}}?${AUTHSERVICE_MEM_REQUEST}?g" \
    -e "s?{{AUTHSERVICE_MEM_LIMIT}}?${AUTHSERVICE_MEM_LIMIT}?g" \
    -e "s?{{KUMORI_IMAGES_REGISTRY}}?${KUMORI_IMAGES_REGISTRY}?g" \
    -e "s?{{CONTROLLERS_IMAGE_PULL_SECRETS}}?${CONTROLLERS_IMAGE_PULL_SECRETS}?g" \
    ${DAEMONSETFILE}
fi

# Create ingress namespace
kubectl apply -f ./ingress-namespace.yaml

# If username and password are set, create a docker credentials Secret
# and add an imagePullSecret to the deployment manifest
if [ -n "${OUTOFSERVICE_USERNAME}" ] && [ -n "${OUTOFSERVICE_PASSWORD}" ]; then
  kubectl -n kumori create secret docker-registry outofservice-docker-credentials \
    --docker-server="${OUTOFSERVICE_REGISTRY}" \
    --docker-username="${OUTOFSERVICE_USERNAME}" \
    --docker-password="${OUTOFSERVICE_PASSWORD}"

  # Add imagePullSecret to deployment manifest
  sed -i \
    -e "s?{{IMAGE_PULL_SECRET}}?- name: outofservice-docker-credentials?g" \
    outofservice.yaml
elif [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ] && [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
  # Add imagePullSecret to deployment manifest with Kumori controllers credentials secret
  KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET="kumori-enterprise-registry-credentials"
  sed -i \
    -e "s?{{IMAGE_PULL_SECRET}}?- name: ${KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET}?g" \
    outofservice.yaml
else
  # Remove imagePullSecret placeholder from deployment manifest and set it to []
  sed -i \
    -e "s|{{IMAGE_PULL_SECRET}}||g" \
    -e "s|imagePullSecrets:[[:space:]]*$|imagePullSecrets: []|g" \
    outofservice.yaml
fi

echo
echo "OUTOFSERVICE DEPLOYMENT MANIFEST:"
cat outofservice.yaml
echo


# Configure Ingress TCP ports (Ambassador "listens" on internal ports)
if [ -n "${INGRESS_TCP_PORTS}" ]; then
  # Add custom extra ports to listen to in Ambassador configuration
  TMP_FILE="ambassador-daemonset-hostport_TMP.yaml"

  echo "Adding custom ports to Ambassador..."

  # for PORT in ${INGRESS_TCP_PORTS[@]}; do
  for i in "${!INGRESS_TCP_PORTS[@]}"; do
    INTERNAL_PORT="${INGRESS_TCP_INTERNAL_PORTS[$i]}"
    EXTERNAL_PORT="${INGRESS_TCP_PORTS[$i]}"
    cat ${DAEMONSETFILE} \
      | yq r -j - \
      | jq ".spec.template.spec.containers[0].ports += [ { name: \"tcp${EXTERNAL_PORT}\", containerPort: ${INTERNAL_PORT} , hostPort: ${INTERNAL_PORT} } ]" \
      | yq r --prettyPrint - \
      > ${TMP_FILE}
    mv ${TMP_FILE} ${DAEMONSETFILE}
  done
  echo
  echo "Ambassador configuration with custom ports:"
  cat "$DAEMONSETFILE"
  echo
fi

# Create Ambassador CRDs
kubectl apply -f ./ambassador-crd.yaml

# Deploy de apiext version conversor and wait it is available
kubectl apply -f ./${APIEXTFILE}
sleep 30 # Just to wait for PODs to exist (probably not ready)
echo "Waiting emissary-apiext available at $(date) ..."
kubectl wait --timeout=600s --for=condition=Ready pods -l app.kubernetes.io/instance=emissary-apiext -n emissary-system
echo "Emissary-apiext is available at $(date)"

# Create Ambassador RBAC
kubectl apply -f ./ambassador-rbac.yaml

# Wait for Ambassador Module CRD to exist
echo
echo "Waiting for CRD 'modules.getambassador.io' to exist..."
until [ "$(kubectl get crd | grep 'modules.getambassador.io')" != "" ]; do
  date
  echo "  Not yet, wait a little bit more..."
  sleep 1
done
echo "CRD 'modules.getambassador.io' exists!"
echo

# Create Ambassador system-wide configuration
kubectl apply -f ./ambassador-config-module.yaml

# Create Ambassador admin Service
kubectl apply -f ./ambassador-service.yaml

# Create Ambassador resolvers
sed -i \
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" \
  ./ambassador-resolvers.yaml
kubectl apply -f ./ambassador-resolvers.yaml

# Create Ambassador DaemonSet (with or without IPFiltering/Authservice) and wait
# it is available.
# TBD: "kubectl wait" is not useful for daemonsets. If we want to wait it is
# available, a function waiting all its pods must be defined. But... do we really
# want to wait until Ambassador is up and running properly?
kubectl apply -f ./${DAEMONSETFILE}
# kubectl wait --timeout=600s --for=condition=available daemonset -lproduct=aes

# For each port enabled in the daemonset, a Listener CR must be created.
# - A 8443-listener is always opened
# - TBD: A 8080-listener should be opened? (to be done when working on issue
#        https://gitlab.com/kumori/project-management/-/issues/1083)
#   The port is indicated in INGRESS_INTERNAL_HTTP_PORT, and it is used in
#   the kuinbound configuration. But, with the new Ambassador v3.x, a listener
#   should be created. To be investigated ...
# - More tcp-listeneres (related to TCPPorts) are not required: when a tcpmapping
#   is created, Ambassador creates an ad-hoc listener in the Envoy configuration
#   (but not a ambassador listener CRD).
#
# We must take into account that the proxy protocol must be enabled when it is
# behind a balancer (for example: our Envoy balancer)
#
# NOTE:
# There is an issue in the new parameter "protocol" in listener resources, so
# the old "protocolStack" parameter is used.
# See:
# - scripts/installer/addons/ambassador-ingress-3.4.1/ambassador-listener.yaml_template
# - https://github.com/emissary-ingress/emissary/issues/4153,
# - https://github.com/emissary-ingress/emissary/issues/3979
#
if [ "${INGRESS_INTERNAL_BALANCING}" == "true" ]; then
  #HTTPS_LISTENER_PROTOCOL="HTTPSPROXY"
  HTTPS_LISTENER_PROTOCOL_STACK="[PROXY,TLS, HTTP, TCP]"

else
  #HTTPS_LISTENER_PROTOCOL="HTTPS"
  HTTPS_LISTENER_PROTOCOL_STACK="[TLS, HTTP, TCP]"
fi
rm -rf listeners
mkdir listeners
echo "Creating listeners"
cp ambassador-listener.yaml_template listeners/ambassador-listener-8443.yaml
sed -i \
  -e "s?{{PORT}}?8443?g"\
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g"\
  -e "s?{{LISTENER_PROTOCOL_STACK}}?${HTTPS_LISTENER_PROTOCOL_STACK}?g"\
  -e "s?{{LISTENER_TYPE}}?HTTPS?g"\
  -e "s?{{XFF_TRUSTED_HOPS}}?${AMBASSADOR_XFF_TRUSTED_HOPS}?g"\
  listeners/ambassador-listener-8443.yaml
echo
echo "Listener HTTPS"
cat listeners/ambassador-listener-8443.yaml
echo
kubectl apply -f ./listeners

# Create objects related to IP Filtering
if [ "${INSTALL_IPFILTERING}" == "true" ]; then
  kubectl apply -f ./ambassador-ipfiltering-stuff.yaml
fi

# Create objects related to AuthService
if [ "${INSTALL_AUTHSERVICE}" == "true" ]; then
  kubectl apply -f ./ambassador-authservice-stuff.yaml
fi

# Create Kumori OutOfService deployment and service
# OutOfService is a classic "Error 404 service unavailable" responder
kubectl apply -f ./outofservice.yaml

# Create a host just to access Emissary diagnostic web
# TBD: https://gitlab.com/kumori/project-management/-/issues/1315
CLUSTER_CORE_DOMAIN="cluster.core"
KUKUCERT_NAME="wildcard-${REFERENCE_DOMAIN//./-}"
KUKUCERT_SECRET_NAME="${CLUSTER_CORE_DOMAIN//./-}-cert-${KUKUCERT_NAME}-cert"
sed -i -e "s?{{KUKUCERT_SECRET_NAME}}?${KUKUCERT_SECRET_NAME}?g" ./diag-host.yaml
sed -i -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" ./diag-host.yaml
sed -i -e "s?{{REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" ./diag-host.yaml
sed -i -e "s?{{KUINBOUND_MINTLSVERSION}}?${KUINBOUND_MINTLSVERSION}?g" ./diag-host.yaml
sed -i -e "s?{{KUINBOUND_MAXTLSVERSION}}?${KUINBOUND_MAXTLSVERSION}?g" ./diag-host.yaml
sed -i -e "s?{{KUINBOUND_CIPHERSUITETLS}}?${KUINBOUND_CIPHERSUITETLS}?g" ./diag-host.yaml
sed -i -e "s?{{KUINBOUND_ECDHCURVESTLS}}?${KUINBOUND_ECDHCURVESTLS}?g" ./diag-host.yaml
kubectl apply -f ./diag-host.yaml


# If Ingress automatic Load-Balancer is used, create an LB Service and register
# the LB public IP in DNS
if [ "${INGRESS_AUTO_BALANCING}" = "true" ]; then
  echo
  echo "Creating Ingress LoadBalancer service..."
  echo

  # For Openstack, LoadBalancer service must include an annotation for creating
  # the LB in the same Network as the instances.
  if [ "${KUMORI_IAAS}" = "openstack" ]; then
    # Retrieve the Network ID where the LoadBalancer must be created
    LB_NETWORK_ID=$(sudo cloud-init query ds | jq -r .network_json.networks[0].network_id)

    echo "Detected Openstack Network ID: ${LB_NETWORK_ID}"
    echo

    INGRESS_LB_SVC_FILE="ingress-lb-service.yaml"
    INGRESS_LB_SVC_TEMPLATE_FILE="ingress-lb-service_openstack_template.yaml"
    cp ${INGRESS_LB_SVC_TEMPLATE_FILE} ${INGRESS_LB_SVC_FILE}
    sed -i \
      -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" \
      -e "s?{{NETWORK_ID}}?${LB_NETWORK_ID}?g" \
      ${INGRESS_LB_SVC_FILE}
  # For AWS, LoadBalancer service must include an annotation indicating the
  # type of balancer (we want "NLB")
  elif [ "${KUMORI_IAAS}" = "aws" ]; then
    INGRESS_LB_SVC_FILE="ingress-lb-service-aws.yaml"
    INGRESS_LB_SVC_TEMPLATE_FILE="ingress-lb-service_aws_template.yaml"
    cp ${INGRESS_LB_SVC_TEMPLATE_FILE} ${INGRESS_LB_SVC_FILE}
    sed -i -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" ${INGRESS_LB_SVC_FILE}
  else
    INGRESS_LB_SVC_FILE="ingress-lb-service.yaml"
    INGRESS_LB_SVC_TEMPLATE_FILE="ingress-lb-service_template.yaml"
    cp ${INGRESS_LB_SVC_TEMPLATE_FILE} ${INGRESS_LB_SVC_FILE}
    sed -i -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" ${INGRESS_LB_SVC_FILE}
  fi

  # Configure the appropriate port in the LB service
  if [ -n "${INGRESS_TCP_PORTS}" ]; then

    # Add custom extra ports to listen to in Ambassador configuration
    INGRESS_LB_SVC_TMP_FILE="ingress-lb-service_TMP.yaml"

    echo "Adding custom ports to Ingress LB Service..."

    # for PORT in ${INGRESS_TCP_PORTS[@]}; do
    for i in "${!INGRESS_TCP_PORTS[@]}"; do
      INTERNAL_PORT="${INGRESS_TCP_INTERNAL_PORTS[$i]}"
      EXTERNAL_PORT="${INGRESS_TCP_PORTS[$i]}"
      cat ${INGRESS_LB_SVC_FILE} \
        | yq r -j - \
        | jq ".spec.ports += [ { name: \"tcp${EXTERNAL_PORT}\", port: ${EXTERNAL_PORT} , targetPort: ${INTERNAL_PORT}, protocol: \"TCP\" } ]" \
        | yq r --prettyPrint - \
        > ${INGRESS_LB_SVC_TMP_FILE}
      mv ${INGRESS_LB_SVC_TMP_FILE} ${INGRESS_LB_SVC_FILE}
    done
  fi

  echo
  echo "Ingress LB Service manifest to be applied:"
  cat "${INGRESS_LB_SVC_FILE}"
  echo

  # Create the Ingress Load-Balancer Service
  kubectl apply -f ${INGRESS_LB_SVC_FILE}

  # Wait for the Ingress Load-Balancer Service to have the LB public IP
  INGRESS_LB_SERVICE_NAME="ingress-lb-service-${RELEASE_NAME}"
  echo "Waiting for Ingress LoadBalancer to be created..."
  until kubectl -n kumori get svc ${INGRESS_LB_SERVICE_NAME} --output=jsonpath='{.status.loadBalancer}' | grep "ingress"
  do
    echo "  Still waiting..."
    sleep 5
  done
  echo "  Ingress LoadBalancer created!"


  if [ "${KUMORI_IAAS}" = "openstack" ]; then
    INGRESS_LB_IP=$( kubectl -n kumori get svc ${INGRESS_LB_SERVICE_NAME} --output=jsonpath='{.status.loadBalancer.ingress[0].ip}' )
    INGRESS_DNS_TYPE="A"
    # INGRESS_DNS_TTL="300" -- setted in variables.sh
  elif [ "${KUMORI_IAAS}" = "aws" ]; then
    INGRESS_LB_IP=$( kubectl -n kumori get svc ${INGRESS_LB_SERVICE_NAME} --output=jsonpath='{.status.loadBalancer.ingress[0].hostname}' )
    # It is mandatory to add a "dot" when a CNAME record is created
    INGRESS_LB_IP=${INGRESS_LB_IP}.
    INGRESS_DNS_TYPE="CNAME"
    INGRESS_DNS_TTL="0"
  else
    echo
    echo "INGRESS LB IP: IAAS-NOT-SUPPORTED ${KUMORI_IAAS}"
    echo
  fi

  echo
  echo "INGRESS LB : ${INGRESS_DNS_TYPE} / ${INGRESS_LB_IP}"
  echo

  # Register the Ingress LB IP in DNS
  if [ "${MANAGED_DNS}" = "true" ] && [ "${INGRESS_REGISTER_DOMAIN}" = "true" ]; then
    echo
    echo "Registering Ingress DNS record..."
    echo

    if [ "${MANAGED_DNS_PROVIDER}" = "route53" ]; then
      AWS_CONFIG_DIR="${SCRIPT_DIR}/awsConfiguration"

      # Ingress DNS record template (DO NOT TABULATE! - USES EOF)
DNS_ADD_INGRESS_DOMAIN=$(cat <<EOF
{
  "Comment": "Create Ingress main record",
  "Changes": [
    {
      "Action": "UPSERT",
      "ResourceRecordSet": {
        "Name": "${INGRESS_DOMAIN}",
        "Type": "${INGRESS_DNS_TYPE}",
        "TTL": ${INGRESS_DNS_TTL},
        "ResourceRecords": [ { "Value": "${INGRESS_LB_IP}"} ]
      }
    }
  ]
}
EOF
)

      # Define AWS base command based on Docker image
      AWS_CLI="docker run --rm --env AWS_PAGER= -v ${AWS_CONFIG_DIR}:/root/.aws ${MANAGED_DNS_ROUTE53_CLI_IMAGE}"

      # Find out HostedZone ID from reference domain
      echo "Running command: ${AWS_CLI} route53 list-hosted-zones"
      RESULT=$(${AWS_CLI} route53 list-hosted-zones --output json)
      EXIT_CODE="$?"
      if [ "${EXIT_CODE}" != "0" ]; then
        echo
        echo -e $COL_RED_BOLD"Error getting DNS HostedZones. This will require manual corrective actions."
        echo
        exit 1
      fi

      echo
      echo "Current available HostedZones:"
      echo "${RESULT}"
      echo

      HOSTEDZONE_ID=$(echo ${RESULT} | jq -cr ".HostedZones[] | select(.Name==\"$REFERENCE_DOMAIN.\") | .Id")
      if [[ ${HOSTEDZONE_ID} != /hostedzone/* ]]; then
        echo
        echo -e $COL_RED_BOLD"Error getting HostedZone ID for ${REFERENCE_DOMAIN}. This will require manual corrective actions."
        echo
        exit 1
      fi

      echo "HostedZone domain: ${REFERENCE_DOMAIN}"
      echo "HostedZone ID: ${HOSTEDZONE_ID}"
      echo


      # Create a new Ingress DNS record
      echo
      echo -e $COL_BLUE"* Creating DNS entry for Ingress..."$COL_DEFAULT
      echo
      echo "Creating the following DNS record in AWS Route53:"
      echo
      echo
      echo " - Domain : ${INGRESS_DOMAIN}"
      echo "   Type   : ${INGRESS_DNS_TYPE}"
      echo "   Value  : ${INGRESS_LB_IP}"
      echo "   TTL    : ${INGRESS_DNS_TTL}"

      # Run command to create new DNS record set
      echo "Running command: ${AWS_CLI} route53 change-resource-record-sets \
        --hosted-zone-id ${HOSTEDZONE_ID} \
        --change-batch ${DNS_ADD_INGRESS_DOMAIN}"

      ${AWS_CLI} route53 change-resource-record-sets \
        --hosted-zone-id "${HOSTEDZONE_ID}" \
        --change-batch "${DNS_ADD_INGRESS_DOMAIN}"

      EXIT_CODE="$?"
      if [ "${EXIT_CODE}" == "0" ]; then
        echo
        echo "Domain created successfully."
        echo
      else
        echo
        echo -e $COL_RED_BOLD"An error ocurred when creating Ingress domain. This will require manual corrective actions."$COL_DEFAULT
        echo
        exit 1
      fi
    elif [ "${MANAGED_DNS_PROVIDER}" = "ovh" ]; then
      # Extract OVH credentials from credentials file
      echo "Extracting credentials from OVH configuration file..."
      OVH_CREDENTIALS_FILE="${SCRIPT_DIR}/ovhConfiguration/ovh-dns-credentials.ini"

      OVH_ENDPOINT="$(grep "dns_ovh_endpoint" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
      OVH_APPLICATION_KEY="$(grep "dns_ovh_application_key" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
      OVH_APPLICATION_SECRET="$(grep "dns_ovh_application_secret" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
      OVH_CONSUMER_KEY="$(grep "dns_ovh_consumer_key" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"

      echo " - Extracted OVH_ENDPOINT           : ${OVH_ENDPOINT}"
      echo " - Extracted OVH_APPLICATION_KEY    : ${OVH_APPLICATION_KEY}"
      echo " - Extracted OVH_APPLICATION_SECRET : <hidden>  (length: ${#OVH_APPLICATION_SECRET})"
      echo " - Extracted OVH_CONSUMER_KEY       : <hidden>  (length: ${#OVH_CONSUMER_KEY})"

      # Define OVH API helper base command based on Docker image
      OVH_CLI="docker run --rm \
        --name kovhcli \
        --env OVH_ENDPOINT="${OVH_ENDPOINT}" \
        --env OVH_APPLICATION_KEY="${OVH_APPLICATION_KEY}" \
        --env OVH_APPLICATION_SECRET="${OVH_APPLICATION_SECRET}" \
        --env OVH_CONSUMER_KEY="${OVH_CONSUMER_KEY}" \
        ${MANAGED_DNS_OVH_CLI_IMAGE}"

      # Validate Reference Domain and DNS Zone
      RESULT=$(${OVH_CLI} check-refdomain ${REFERENCE_DOMAIN})
      EXIT_CODE="$?"
      if [ "${EXIT_CODE}" != "0" ]; then
        echo
        echo -e $COL_RED_BOLD"Error checking Reference Domain against DNS Zones. This will require manual corrective actions."
        echo
        exit 1
      fi

      echo
      echo -e $COL_BLUE"* Creating DNS entry for Ingress..."$COL_DEFAULT
      echo
      echo "Creating the following DNS record in OVH:"
      echo
      echo
      echo " - Domain : ${INGRESS_DOMAIN}"
      echo "   Type   : ${INGRESS_DNS_TYPE}"
      echo "   Value  : ${INGRESS_LB_IP}"
      echo "   TTL    : ${INGRESS_DNS_TTL}"

      RESULT=$(${OVH_CLI} create-refdomain-record ${REFERENCE_DOMAIN} ingress-${RELEASE_NAME} ${INGRESS_DNS_TYPE} ${INGRESS_LB_IP} ${INGRESS_DNS_TTL})
      EXIT_CODE="$?"
      if [ "${EXIT_CODE}" == "0" ]; then
        echo
        echo "Domain created successfully."
        echo
      else
        echo
        echo -e $COL_RED_BOLD"An error ocurred when creating Ingress domain. This will require manual corrective actions."$COL_DEFAULT
        echo
        exit 1
      fi
    else
      echo
      echo -e $COL_YELLOW"DNS Provider ${MANAGED_DNS_PROVIDER} not supported. Skipping DNS configuration."$COL_DEFAULT
      echo
    fi
  else
    echo
    echo "Skipped Ingress DNS registration."
    echo
  fi
fi
