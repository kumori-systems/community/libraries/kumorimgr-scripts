# Kumori notes:
# This content is part of https://app.getambassador.io/yaml/emissary/3.6.0/emissary-emissaryns.yaml
# Ambassador module defines system-wide configuration.
#
# For now:
# - Retry on Gateway errors
# - Disable Envoy circuit breakers
#
# ------------------------------------------------------------------------------
apiVersion: getambassador.io/v3alpha1
kind: Module
metadata:
  name: ambassador  # Mandatory: name=ambassador
  namespace: {{INGRESS_NAMESPACE}}
  labels:
    app.kubernetes.io/component: emissary-ingress-ratelimit
    app.kubernetes.io/instance: emissary-ingress
    app.kubernetes.io/managed-by: getambassador.io
    app.kubernetes.io/name: emissary-ingress
    app.kubernetes.io/part-of: emissary-ingress
spec:
  config:

    # Add support for HTTP gzip compression headers
    # https://www.getambassador.io/docs/edge-stack/3.4/topics/running/gzip
    gzip:
      enabled: true # Default configuration (can be refined)

    # Add retries when internal requests to backends fail
    # For now we just configure one single retry limited to gateway errors (for
    # getting rid of sparse 503 errors)
    retry_policy:
      retry_on: gateway-error
      num_retries: 1

    # Disable Envoy circuit breakers. This is the documented way to disable it.
    # Source: https://www.envoyproxy.io/docs/envoy/latest/faq/load_balancing/disable_circuit_breaking
    circuit_breakers:
    - max_connections: 1000000000
      max_pending_requests: 1000000000
      max_requests: 1000000000
      max_retries: 1000000000
      priority: default
    - max_connections: 1000000000
      max_pending_requests: 1000000000
      max_requests: 1000000000
      max_retries: 1000000000
      priority: high

    # Disable exposing '/ambassador/v0/diag' API
    # Change both proporties to "true" to enable it
    diagnostics:
      enabled: false
      allow_non_local: false

    # Log format: it is the same as the default log format used by Envoy, but
    # adding the TLS version and cipher
    envoy_log_format: ACCESS [%START_TIME%] "%REQ(:METHOD)% %REQ(X-ENVOY-ORIGINAL-PATH?:PATH)% %PROTOCOL%" %RESPONSE_CODE% %RESPONSE_FLAGS% %BYTES_RECEIVED% %BYTES_SENT% %DURATION% %RESP(X-ENVOY-UPSTREAM-SERVICE-TIME)% "%REQ(X-FORWARDED-FOR)%" "%REQ(USER-AGENT)%" "%REQ(X-REQUEST-ID)%" "%REQ(:AUTHORITY)%" "%UPSTREAM_HOST%" %DOWNSTREAM_TLS_VERSION% %DOWNSTREAM_TLS_CIPHER%

    # Set the name Envoy sets as 'server_name' response header (so we can
    # differentiate it from balancer Envoy responses)
    server_name: ambassador-envoy

    # Proxy protocol must be enabled when it is behind a balancer (for example
    # an Envoy balancer) that uses it
    # NOTE: this parameter is no longer supported, as it is now part of the
    # Listener resource (and can be set per-AmbassadorListener rather than globally).
    # use_proxy_proto: {{USE_PROXY_PROTOCOL}}

    # The following LUA script removes the port from the 'authority' header.
    # Ambassador 1.x does not support Host headers that include non-standard ports
    # and rejects the requests as 404.
    # It is not enough to use the "strip_matching_host_port: true" property,
    # because the port is removed only if that port number matches the port
    # number of Envoy's listener.
    lua_scripts: |
      function envoy_on_request(request_handle)
        local authority = string.lower(request_handle:headers():get(":authority"))
        if(string.find(authority, ":") ~= nil)
        then
          local authority_index = string.find(authority, ":")
          local stripped_authority = string.sub(authority, 1, authority_index - 1)
          request_handle:headers():replace(":authority", stripped_authority)
        else
          request_handle:headers():replace(":authority", authority)
        end
      end

    # Tells Emissary-ingress to assume that unqualified services are in the
    # same namespace as Emissary-ingress
    use_ambassador_namespace_for_service_resolution: true

    # By default, the Ambassador module must be configured to just clean the
    # x-forwarded-for-client header. But, if "XFFC" format is configured, then
    # Ambassador module must be configured to clean and set the header (with the
    # client certificate + full-cert-chain)
    forward_client_cert_details: {{AMBASSADOR_XFCC_DETAILS}}
    {{AMBASSADOR_SET_DETAILS_PLACEHOLDER}}


    # Soft limit for an upstream service's read and write buffer limits
    # Its default value is 1MB.
    # Kumori is affected? (for example, large manifest.json content)
    # buffer_limit_bytes: 1048576
