#!/bin/bash

# Retrieve Kumori CRDs from client-go repository
echo "Retrieving Kumori CRDs from repository..."
TMP_DIR=`mktemp -d`
CLIENTGO_TMP_DIR="${TMP_DIR}/client-go"
CLIENTGO_REPOSITORY="https://gitlab.com/kumori-systems/community/libraries/client-go.git"
CLONED="true"
git clone \
  -b "v${KUMORI_CRD_VERSION}" \
  --single-branch \
  ${CLIENTGO_REPOSITORY} \
  ${CLIENTGO_TMP_DIR} || CLONED="false"

##################################################################
## TEMPORARY FIX (DEV411 BRANCH NOT PUBLISHED IN COMMUNITY YET) ##
##################################################################
if [ "${CLONED}" = "false" ]; then
  echo -e $COL_YELLOW"Git clone failed, trying to find client-go files locally..."$COL_DEFAULT

  CLIENTGO_SOURCE_DIR="/home/ubuntu/client-go"
  if [ -d "${CLIENTGO_SOURCE_DIR}" ]; then
    echo "Using local files from ${CLIENTGO_SOURCE_DIR}."
    mkdir -p ${CLIENTGO_TMP_DIR}
    cp -r ${CLIENTGO_SOURCE_DIR}/* ${CLIENTGO_TMP_DIR}/
  else
    echo "Couldn't find local files in ${CLIENTGO_SOURCE_DIR}."
  fi
fi
##################################################################
## END OF TEMPORARY FIX                                         ##
##################################################################



if [[ ! -d "${CLIENTGO_TMP_DIR}/artifacts/crd" ]]; then
  echo
  echo -e $COL_RED_BOLD"Error fetching Kumori CRDs from: ${CLIENTGO_REPOSITORY}."$COL_DEFAULT
  echo
else
  rm -rf crd && mkdir -p crd
  cp -r ${CLIENTGO_TMP_DIR}/artifacts/crd/* crd
  rm -rf ${TMP_DIR}
  echo "Retrieved Kumori CRDs from repository."
fi

echo
echo "Installing Kumori CRDs..."
# This is a workaround since 'kubectl apply -f crd/*.yaml' is not valid
# and the directories may contain other files than yaml.
find crd -name "*.yaml" -exec kubectl apply -f {} \;
