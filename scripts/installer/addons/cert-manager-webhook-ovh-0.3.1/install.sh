#!/bin/bash

# Name to use for the webhook in ApiServer
WEBHOOK_API_GROUP_NAME="axebow.kumori"

################################################################################
## Configure and deploy the official Cert-Manager Webhook OVH manifests       ##
################################################################################
# Generate ExternalDNS deployment manifest configured for OVH DNS
WEBHOOK_OVH_MANIFEST_TEMPLATE_FILE="cert-manager-webhook-ovh-manifests.yaml_template"
WEBHOOK_OVH_MANIFEST_FILE="cert-manager-webhook-ovh-manifests.yaml"

sed \
  -e "s?{{WEBHOOK_API_GROUP_NAME}}?${WEBHOOK_API_GROUP_NAME}?g" \
  ${WEBHOOK_OVH_MANIFEST_TEMPLATE_FILE} \
  > ${WEBHOOK_OVH_MANIFEST_FILE}

echo
echo "Creating CertManager Webhook OVH objects...."
echo

kubectl apply -f ${WEBHOOK_OVH_MANIFEST_FILE}

################################################################################
## Extract OVH DNS credentials info                                           ##
################################################################################

# The OVH credentials file is received as a base64-encoded string. Create a
# temporary file.
TMP_OVH_CREDENTIALS_FILE="ovh-dns-credentials.ini"
echo ${CERT_MANAGER_WEBHOOK_OVH_CREDENTIALS_BASE64} \
  | base64 --decode \
  > ${TMP_OVH_CREDENTIALS_FILE}

# Extract OVH credentials from credentials file
echo
echo "Extracting credentials from OVH configuration file..."
echo

OVH_ENDPOINT="$(grep "dns_ovh_endpoint" ${TMP_OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
OVH_APPLICATION_KEY="$(grep "dns_ovh_application_key" ${TMP_OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
OVH_APPLICATION_SECRET="$(grep "dns_ovh_application_secret" ${TMP_OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
OVH_CONSUMER_KEY="$(grep "dns_ovh_consumer_key" ${TMP_OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"

echo " - Extracted OVH_ENDPOINT           : ${OVH_ENDPOINT}"
echo " - Extracted OVH_APPLICATION_KEY    : ${OVH_APPLICATION_KEY}"
echo " - Extracted OVH_APPLICATION_SECRET : <hidden>  (length: ${#OVH_APPLICATION_SECRET})"
echo " - Extracted OVH_CONSUMER_KEY       : <hidden>  (length: ${#OVH_CONSUMER_KEY})"


################################################################################
## Create a secret containinig the OVH credendials application secret         ##
################################################################################

ISSUER_NAMESPACE="kumori"
OVH_DNS_APPLICATION_SECRET_NAME="ovh-dns-credentials"

echo
echo "Creating secret with OVH credendials application secret..."
echo

kubectl -n ${ISSUER_NAMESPACE} create secret generic ${OVH_DNS_APPLICATION_SECRET_NAME} \
  --from-literal=applicationSecret=${OVH_APPLICATION_SECRET}

################################################################################
## Create RBAC for the OVH Webhook to access the OVH credentials secret       ##
################################################################################

OVH_CREDENTIALS_RBAC_TEMPLATE_FILE="ovh-credentials-rbac.yaml_template"
OVH_CREDENTIALS_RBAC_FILE="ovh-credentials-rbac.yaml"

sed \
  -e "s?{{ISSUER_NAMESPACE}}?${ISSUER_NAMESPACE}?g" \
  -e "s?{{OVH_DNS_APPLICATION_SECRET_NAME}}?${OVH_DNS_APPLICATION_SECRET_NAME}?g" \
  ${OVH_CREDENTIALS_RBAC_TEMPLATE_FILE} \
  > ${OVH_CREDENTIALS_RBAC_FILE}

echo
echo "Creating RBAC for the OVH Webhook to access the OVH credentials secret..."
echo

kubectl apply -f ${OVH_CREDENTIALS_RBAC_FILE}

################################################################################
## Create a Let's Encrypt Issuer with OVH DNS challenges in the namespace     ##
################################################################################

if [ "${CERT_MANAGER_WEBHOOK_OVH_ISSUER_LETSENCRYPT_ENABLED}" == "true" ]; then

  LETSENCRYPT_SERVER="https://acme-v02.api.letsencrypt.org/directory"
  if [ "${CERT_MANAGER_WEBHOOK_OVH_ISSUER_LETSENCRYPT_USE_STAGING}" = "true" ]; then
    LETSENCRYPT_SERVER="https://acme-staging-v02.api.letsencrypt.org/directory"
  fi

  ISSUER_MANIFEST_TEMPLATE_FILE="issuer-letsencrypt-ovh.yaml_template"
  ISSUER_MANIFEST_FILE="issuer-letsencrypt-ovh.yaml"

  sed \
    -e "s?{{ISSUER_NAMESPACE}}?${ISSUER_NAMESPACE}?g" \
    -e "s?{{LETSENCRYPT_SERVER}}?${LETSENCRYPT_SERVER}?g" \
    -e "s?{{LETSENCRYPT_EMAIL}}?${CERT_MANAGER_WEBHOOK_OVH_EMAIL}?g" \
    -e "s?{{WEBHOOK_API_GROUP_NAME}}?${WEBHOOK_API_GROUP_NAME}?g" \
    -e "s?{{OVH_ENDPOINT}}?${OVH_ENDPOINT}?g" \
    -e "s?{{OVH_APPLICATION_KEY}}?${OVH_APPLICATION_KEY}?g" \
    -e "s?{{OVH_DNS_APPLICATION_SECRET_NAME}}?${OVH_DNS_APPLICATION_SECRET_NAME}?g" \
    -e "s?{{OVH_CONSUMER_KEY}}?${OVH_CONSUMER_KEY}?g" \
    ${ISSUER_MANIFEST_TEMPLATE_FILE} \
    > ${ISSUER_MANIFEST_FILE}

  echo
  echo "Creating Let's Encrypt issuer with OVH DNS challenges..."
  echo

  kubectl apply -f ${ISSUER_MANIFEST_FILE}

fi

################################################################################
## Create a ZeroSSL Issuer with OVH DNS challenges in the namespace           ##
## A kube/secret containing ZeroSSL credentials is required                   ##
################################################################################

if [ "${CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_ENABLED}" == "true" ]; then

  echo
  echo "Creating secret with ZeroSSL credentials..."
  echo

  # Create a kube/secret containing ZeroSSL credentials.
  # CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_EABKEYHMAC contains the "External
  # Account Binding Key HMAC" (Hash-based Message Authentication Code), obtained
  # from the ZeroSSL  website
  ZEROSSL_EABSECRET="zerossl-eabsecret"
  kubectl -n ${ISSUER_NAMESPACE} create secret generic ${ZEROSSL_EABSECRET} \
    --from-literal=secret="${CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_EABKEYHMAC}"

  ZEROSSL_SERVER="https://acme.zerossl.com/v2/DV90"
  ISSUER_MANIFEST_TEMPLATE_FILE="issuer-zerossl-ovh.yaml_template"
  ISSUER_MANIFEST_FILE="issuer-zerossl-ovh.yaml"

  sed \
    -e "s?{{ISSUER_NAMESPACE}}?${ISSUER_NAMESPACE}?g" \
    -e "s?{{ZEROSSL_SERVER}}?${ZEROSSL_SERVER}?g" \
    -e "s?{{ZEROSSL_EMAIL}}?${CERT_MANAGER_WEBHOOK_OVH_EMAIL}?g" \
    -e "s?{{ZEROSSL_KEY_ID}}?${CERT_MANAGER_WEBHOOK_OVH_ISSUER_ZEROSSL_KEY_ID}?g" \
    -e "s?{{ZEROSSL_EABSECRET}}?${ZEROSSL_EABSECRET}?g" \
    -e "s?{{WEBHOOK_API_GROUP_NAME}}?${WEBHOOK_API_GROUP_NAME}?g" \
    -e "s?{{OVH_ENDPOINT}}?${OVH_ENDPOINT}?g" \
    -e "s?{{OVH_APPLICATION_KEY}}?${OVH_APPLICATION_KEY}?g" \
    -e "s?{{OVH_DNS_APPLICATION_SECRET_NAME}}?${OVH_DNS_APPLICATION_SECRET_NAME}?g" \
    -e "s?{{OVH_CONSUMER_KEY}}?${OVH_CONSUMER_KEY}?g" \
    ${ISSUER_MANIFEST_TEMPLATE_FILE} \
    > ${ISSUER_MANIFEST_FILE}

  echo
  echo "Creating ZeroSSL issuer with OVH DNS challenges..."
  echo

  kubectl apply -f ${ISSUER_MANIFEST_FILE}

fi