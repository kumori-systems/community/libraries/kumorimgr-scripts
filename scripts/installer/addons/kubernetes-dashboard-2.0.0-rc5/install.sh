#!/bin/bash

sed -i -e "s?__K8S_DASHBOARD_VERSION__?$K8S_DASHBOARD_VERSION?g" *.yaml
sed -i -e "s?__RELEASE_NAME__?${RELEASE_NAME}?g" *.yaml
sed -i -e "s?__REFERENCE_DOMAIN__?${REFERENCE_DOMAIN}?g" *.yaml

kubectl apply -f k8s-dashboard.yaml
kubectl apply -f k8s-dashboard-ingress.yaml
