#!/bin/bash

echo "Installing CertManager v${CERT_MANAGER_VERSION} ..."
kubectl apply -f cert-manager.yaml


# Wait for all pods to be up and Ready
echo
echo "Waiting for CertManager Pods to be ready... ($(date))"
kubectl -n cert-manager wait \
  pods -l app.kubernetes.io/instance=cert-manager \
  --timeout=600s \
  --for=condition=Ready
echo "CertManager Pods are ready!  ($(date))"
echo
