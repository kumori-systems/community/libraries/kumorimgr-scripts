apiVersion: v1
data:
  config.yaml: |-
    resourceRules:
      "cpu":
        "containerLabel": "container"
        "containerQuery": |
          sum by (<<.GroupBy>>) (
            irate (
                container_cpu_usage_seconds_total{<<.LabelMatchers>>,container!="",pod!=""}[120s]
            )
          )
        "nodeQuery": |
          sum by (<<.GroupBy>>) (
            1 - irate(
              node_cpu_seconds_total{mode="idle"}[60s]
            )
            * on(namespace, pod) group_left(node) (
              node_namespace_pod:kube_pod_info:{<<.LabelMatchers>>}
            )
          )
          or sum by (<<.GroupBy>>) (
            1 - irate(
              windows_cpu_time_total{mode="idle", job="windows-exporter",<<.LabelMatchers>>}[4m]
            )
          )
        "resources":
          "overrides":
            "namespace":
              "resource": "namespace"
            "node":
              "resource": "node"
            "pod":
              "resource": "pod"
      "memory":
        "containerLabel": "container"
        "containerQuery": |
          sum by (<<.GroupBy>>) (
            container_memory_working_set_bytes{<<.LabelMatchers>>,container!="",pod!=""}
          )
        "nodeQuery": |
          sum by (<<.GroupBy>>) (
            node_memory_MemTotal_bytes{job="node-exporter",<<.LabelMatchers>>}
            -
            node_memory_MemAvailable_bytes{job="node-exporter",<<.LabelMatchers>>}
          )
          or sum by (<<.GroupBy>>) (
            windows_cs_physical_memory_bytes{job="windows-exporter",<<.LabelMatchers>>}
            -
            windows_memory_available_bytes{job="windows-exporter",<<.LabelMatchers>>}
          )
        "resources":
          "overrides":
            "instance":
              "resource": "node"
            "namespace":
              "resource": "namespace"
            "pod":
              "resource": "pod"
      "window": "5m"
    # --------------------------------------------------------------------------
    # Custom rules (custom metrics of Kubernetes objects) added by kumori
    # --------------------------------------------------------------------------
    rules:
    - seriesQuery: 'kubelet_volume_stats_capacity_bytes{}'
      seriesFilters: []
      resources:
        overrides:
          namespace:
            resource: namespace
          persistentvolumeclaim:
            resource: persistentvolumeclaim
      name:
        matches:
        as:
      metricsQuery: '<<.Series>>{<<.LabelMatchers>>}'
    - seriesQuery: 'kubelet_volume_stats_available_bytes{}'
      seriesFilters: []
      resources:
        overrides:
          namespace:
            resource: namespace
          persistentvolumeclaim:
            resource: persistentvolumeclaim
      name:
        matches:
        as:
      metricsQuery: '<<.Series>>{<<.LabelMatchers>>}'
    - seriesQuery: 'kubelet_volume_stats_used_bytes{}'
      seriesFilters: []
      resources:
        overrides:
          namespace:
            resource: namespace
          persistentvolumeclaim:
            resource: persistentvolumeclaim
      name:
        matches:
        as:
      metricsQuery: '<<.Series>>{<<.LabelMatchers>>}'
    # --------------------------------------------------------------------------
    # External rules (custom metrics of non-Kubernetes objects) added by Kumori
    # --------------------------------------------------------------------------
    externalRules:
    - seriesQuery: 'envoy_cluster_upstream_rq_total{namespace!="",envoy_cluster_name!=""}'
      resources:
      name:
        matches: "^(.*)_total"
        as: "${1}_per_second"
      metricsQuery: 'sum(rate(<<.Series>>{<<.LabelMatchers>>}[5m])) by (<<.GroupBy>>)'
    - seriesQuery: 'envoy_cluster_upstream_rq_active{namespace!="",envoy_cluster_name!=""}'
      resources:
      metricsQuery: 'sum(avg_over_time(<<.Series>>{<<.LabelMatchers>>}[5m])) by (<<.GroupBy>>)'

kind: ConfigMap
metadata:
  labels:
    app.kubernetes.io/component: metrics-adapter
    app.kubernetes.io/name: prometheus-adapter
    app.kubernetes.io/part-of: kube-prometheus
    app.kubernetes.io/version: 0.10.0
  name: adapter-config
  namespace: monitoring
