#!/bin/bash

# Delete kube-prometheus directory if it exists
rm -rf kube-prometheus

git clone -b "${KUBE_PROMETHEUS_VERSION}" https://github.com/coreos/kube-prometheus \
  || git clone https://github.com/coreos/kube-prometheus && cd kube-prometheus && git checkout "${KUBE_PROMETHEUS_VERSION}" && cd ..

echo

# Create a namespace for monitoring elements
kubectl create namespace monitoring

# If prometheus remote write is enabled and configured to use client certificate
# create a Kubernetes Secret containing the Client Certificate.
PROMETHEUS_PERSISTENCE_CLIENCERT_SECRET_NAME="monitoring-client-creds"
if [ "${PROMETHEUS_PERSISTENCE}" = "true" ] && [ "${PROMETHEUS_REMOTE_AUTH_TYPE}" = "clientcertificate" ]; then

  PROMETHEUS_PERSISTENCE_WITH_CLIENCERT="true"

  echo
  echo "Creating Monitoring remote client certificate secret"
  echo

  # Create a Kubernetes secret with the remote access client certificate files
  kubectl -n monitoring create secret tls ${PROMETHEUS_PERSISTENCE_CLIENCERT_SECRET_NAME} \
    --cert=${ADDON_FIXTURES_MONITORING_CLIENTCERT_CERT} \
    --key=${ADDON_FIXTURES_MONITORING_CLIENTCERT_KEY}
else
  PROMETHEUS_PERSISTENCE_WITH_CLIENCERT="false"
fi

# If remote/write is enabled, add a Grafana Datasource pointing to the remote
if [ "${PROMETHEUS_PERSISTENCE}" = "true" ]; then
  if [ "${PROMETHEUS_PERSISTENCE_WITH_CLIENCERT}" = "true" ]; then
    # Add Grafana Datasource for the remote (with client certificate authentication)
    sed \
      -e "s?{{PROMETHEUS_REMOTE_READ_URL}}?${PROMETHEUS_REMOTE_READ_URL}?g" \
      -e "s?{{GRAFANA_REMOTE_DATASOURCE_NAME}}?${GRAFANA_REMOTE_DATASOURCE_NAME}?g" \
      grafana/datasources_with_persistence_clientcert.yaml_template \
      > grafana/datasources_with_persistence_clientcert.yaml
    cp grafana/datasources_with_persistence_clientcert.yaml kube-prometheus/manifests/grafana-dashboardDatasources.yaml
  else
    # Add Grafana Datasource for the remote (with no authentication)
    sed \
      -e "s?{{PROMETHEUS_REMOTE_READ_URL}}?${PROMETHEUS_REMOTE_READ_URL}?g" \
      -e "s?{{GRAFANA_REMOTE_DATASOURCE_NAME}}?${GRAFANA_REMOTE_DATASOURCE_NAME}?g" \
      grafana/datasources_with_persistence.yaml_template \
      > grafana/datasources_with_persistence.yaml
    cp grafana/datasources_with_persistence.yaml kube-prometheus/manifests/grafana-dashboardDatasources.yaml
  fi
else
  cp grafana/datasources.yaml kube-prometheus/manifests/grafana-dashboardDatasources.yaml
fi

# CREATE CLIENT CERTIFICATE AND KUBERNETES SECRET FOR GATHERING ETCD METRICS
if [ -d "/etc/kubernetes/pki/etcd" ]; then
  cd etcd-monitoring

  # Set some additional variables for configuring Etcd metrics gathering (secure HTTPS)
  WORKDIR="$(pwd)"
  CSR_FILE="${WORKDIR}/etcd-metrics-client.csr"
  CRT_FILE="${WORKDIR}/etcd-metrics-client.crt"
  KEY_FILE="${WORKDIR}/etcd-metrics-client.key"
  ETCD_CA_CRT_FILE="/etc/kubernetes/pki/etcd/ca.crt"
  ETCD_CA_KEY_FILE="/etc/kubernetes/pki/etcd/ca.key"
  LOCAL_ETCD_CA_CRT_FILE="${WORKDIR}/etcd-ca.crt"
  LOCAL_ETCD_CA_KEY_FILE="${WORKDIR}/etcd-ca.key"
  ETCD_CERTS_SECRET_NAME="etcd-metrics-client-certs"

  # Initialize OpenSSL stuff for creating and signing certificates in local dir
  touch index.txt
  echo "unique_subject = yes/no" > index.txt.attr
  echo '01' > serial

  echo
  echo "Creating CSR..."
  echo
  # Certificate values (C, O, ST, CN, ...) are taken from openssl.cnf condifguration file
  openssl req \
    -batch \
    -config openssl.cnf \
    -new -nodes \
    -out "${CSR_FILE}" \
    -keyout "${KEY_FILE}"

  echo
  echo "Signing CSR and creating certificates..."
  echo
  # Make a local copy of Etcd CA files for signing the certificate
  sudo cp ${ETCD_CA_CRT_FILE} ${LOCAL_ETCD_CA_CRT_FILE}
  sudo cp ${ETCD_CA_KEY_FILE} ${LOCAL_ETCD_CA_KEY_FILE}
  sudo chown ${USER}:${USER} ${LOCAL_ETCD_CA_CRT_FILE}
  sudo chown ${USER}:${USER} ${LOCAL_ETCD_CA_KEY_FILE}

  openssl ca \
    -batch \
    -config openssl.cnf \
    -extensions etcd_client \
    -cert "${LOCAL_ETCD_CA_CRT_FILE}" \
    -keyfile "${LOCAL_ETCD_CA_KEY_FILE}" \
    -outdir "${WORKDIR}" \
    -out "${CRT_FILE}" \
    -infiles "${CSR_FILE}"

  echo
  echo "Creating Kubernetes secret for new certs..."
  echo
  # Needs sudo because Etcd CA certs are owned by root
  kubectl create secret generic ${ETCD_CERTS_SECRET_NAME} \
    -n monitoring \
    --from-file=etcd-ca.crt="${LOCAL_ETCD_CA_CRT_FILE}" \
    --from-file=etcd-metrics-client.crt="${CRT_FILE}" \
    --from-file=etcd-metrics-client.key="${KEY_FILE}"

  # Delete local copy of Etcd CA files
  rm ${LOCAL_ETCD_CA_CRT_FILE} ${LOCAL_ETCD_CA_KEY_FILE}

  echo
  echo "Done."
  echo
  cd ..
else
  echo "Not in a Master node. Skipping Etcd monitoring configuration (certificate)."
  ETCD_CERTS_SECRET_NAME=""
fi

echo
echo "Creating Kubernetes secret for Grafana admin user..."
echo
# If master credentials are not provided use default
GRAFANA_ADMIN_USERNAME="${GRAFANA_ADMIN_USERNAME:-admin}"
GRAFANA_ADMIN_PASSWORD="${GRAFANA_ADMIN_PASSWORD:-admin}"
ENCODED_ADMIN_USERNAME="$( echo -n ${GRAFANA_ADMIN_USERNAME} | base64 )"
ENCODED_ADMIN_PASSWORD="$( echo -n ${GRAFANA_ADMIN_PASSWORD} | base64 )"
sed -i \
  -e "s?{{ENCODED_ADMIN_USERNAME}}?${ENCODED_ADMIN_USERNAME}?g" \
  -e "s?{{ENCODED_ADMIN_PASSWORD}}?${ENCODED_ADMIN_PASSWORD}?g" \
  grafana/grafana-credentials.yaml

kubectl apply -f grafana/grafana-credentials.yaml
echo
echo "Done."
echo

echo
echo "Creating Kubernetes secret for Grafana viewer user..."
echo
# If master credentials are not provided use default
GRAFANA_VIEWER_USERNAME="${GRAFANA_VIEWER_USERNAME:-admin}"
GRAFANA_VIEWER_PASSWORD="${GRAFANA_VIEWER_PASSWORD:-admin}"
ENCODED_VIEWER_USERNAME="$( echo -n ${GRAFANA_VIEWER_USERNAME} | base64 )"
ENCODED_VIEWER_PASSWORD="$( echo -n ${GRAFANA_VIEWER_PASSWORD} | base64 )"
sed -i \
  -e "s?{{ENCODED_VIEWER_USERNAME}}?${ENCODED_VIEWER_USERNAME}?g" \
  -e "s?{{ENCODED_VIEWER_PASSWORD}}?${ENCODED_VIEWER_PASSWORD}?g" \
  grafana/grafana-viewer.yaml

kubectl apply -f grafana/grafana-viewer.yaml
echo
echo "Done."
echo

echo
echo "Installing grafana shell operator..."
echo
sed -i \
  -e "s?{{KUMORI_IMAGES_REGISTRY}}?${KUMORI_IMAGES_REGISTRY}?g" \
  -e "s?{{KUMORI_IMAGES_PULL_POLICY}}?${KUMORI_IMAGES_PULL_POLICY}?g" \
  -e "s?{{CONTROLLERS_IMAGE_PULL_SECRETS}}?${CONTROLLERS_IMAGE_PULL_SECRETS}?g" \
  grafana/grafana-shell-operator.yaml
kubectl apply -f grafana/grafana-shell-operator.yaml
echo
echo "Done."
echo

# ------------------------------------------------------------------------------
# Create a KukuCA for the Monitoring CA (if necessary)
# ------------------------------------------------------------------------------
if [ "${MONITORING_REQUIRE_CLIENTCERT}" = "true" ]; then
  echo
  echo "Creating a new KukuCA for the Monitoring CA..."

  # Some variables related to certs and CAs
  #
  # Define a default Kumori domain for cluster core KukuElements
  CLUSTER_CORE_DOMAIN="cluster.core"

  # KukuCA for the Monitoring CA: internal name, object name and underlying secret
  MONITORING_CA_KUKUCA_NAME="monitoringca"
  MONITORING_CA_KUKUCA_FULL_NAME="${CLUSTER_CORE_DOMAIN//./-}-ca-${MONITORING_CA_KUKUCA_NAME}"
  MONITORING_CA_KUKUCA_SECRET_NAME="${MONITORING_CA_KUKUCA_FULL_NAME}-ca"


  # Define files path used for creating KukuCert manifest
  MONITORING_CA_CERT_CRT_FILE=$ADDON_FIXTURES_MONITORING_CA
  MONITORING_CA_KUKUCA_KUMORI_MANIFEST_TEMPLATE="ingress/kukuca/kukuca-manifest.json_template"
  MONITORING_CA_KUKUCA_KUMORI_MANIFEST="ingress/kukuca/monitoring-ca-kukuca-manifest.json"
  MONITORING_CA_KUKUCA_YAML_TEMPLATE="ingress/kukuca/kukuca.yaml_template"
  MONITORING_CA_KUKUCA_YAML="ingress/kukuca/monitoring-ca-kukuca.yaml"

  # KukuCa owner (always admin)
  MONITORING_CA_KUKUCA_OWNER="admin"

  # KukuCa public setting (cuurently not public)
  MONITORING_CA_KUKUCA_IS_PUBLIC="false"

  # Base64 encode the certificate file into variables
  MONITORING_CA_ENCODED_CRT=$(cat "${MONITORING_CA_CERT_CRT_FILE}" | base64 -w 0)

  # Create a temporary KukuCa Kumori manifest (JSON) from a template
  sed \
    -e "s?{{KUKUCA_DOMAIN}}?${CLUSTER_CORE_DOMAIN}?g" \
    -e "s?{{KUKUCA_NAME}}?${MONITORING_CA_KUKUCA_NAME}?g" \
    -e "s?{{KUKUCA_OWNER}}?${MONITORING_CA_KUKUCA_OWNER}?g" \
    -e "s?{{KUKUCA_FULL_NAME}}?${MONITORING_CA_KUKUCA_FULL_NAME}?g" \
    -e "s?{{ENCODED_CRT}}?${MONITORING_CA_ENCODED_CRT}?g" \
    -e "s?{{IS_PUBLIC}}?${MONITORING_CA_KUKUCA_IS_PUBLIC}?g" \
    ${MONITORING_CA_KUKUCA_KUMORI_MANIFEST_TEMPLATE} \
    > ${MONITORING_CA_KUKUCA_KUMORI_MANIFEST}

  # Base64 encode the KukuCert Kumori manifest
  MONITORING_CA_ENCODED_KUMORI_MANIFEST="$(cat ${MONITORING_CA_KUKUCA_KUMORI_MANIFEST} | gzip | base64 -w0)"

  # Calculate hashed values for labels using FNV1-32
  # Check if docker is available
  if [ -x "$(command -v docker)" ]; then
    echo "Detected docker installed, calculate KukuCa labels hashes with docker image..."
    HASHER="docker run --rm kumori/fnv1-32-hash:v1.0.0 fnv132 -s"
    # NOTE: the 'cut -c 3-' is for removing the leading '0x' in the Hex response
    HASHED_MONITORING_CA_KUKUCA_OWNER=$(${HASHER} ${MONITORING_CA_KUKUCA_OWNER} | cut -c 3-)
    HASHED_MONITORING_CA_KUKUCA_DOMAIN=$(${HASHER} ${CLUSTER_CORE_DOMAIN} | cut -c 3-)
    HASHED_MONITORING_CA_KUKUCA_NAME=$(${HASHER} ${MONITORING_CA_KUKUCA_NAME} | cut -c 3-)
  elif [ -x "$(command -v fnv-32-1-hasher)" ]; then
    echo "Detected fnv32-1-hasher installed, use it to calculate KukuCa labels hashes..."
    HASHER="fnv-32-1-hasher"
    HASHED_MONITORING_CA_KUKUCA_OWNER=$(${HASHER} ${MONITORING_CA_KUKUCA_OWNER})
    HASHED_MONITORING_CA_KUKUCA_DOMAIN=$(${HASHER} ${CLUSTER_CORE_DOMAIN})
    HASHED_MONITORING_CA_KUKUCA_NAME=$(${HASHER} ${MONITORING_CA_KUKUCA_NAME})
  else
    echo -e $COL_RED"ERROR: Unable to find a FNV-32-1 hasher (neither docker nor fnv-32-1-hasher are available)."$COL_DEFAULT
    HASHED_MONITORING_CA_KUKUCA_OWNER="\"\""
    HASHED_MONITORING_CA_KUKUCA_DOMAIN="\"\""
    HASHED_MONITORING_CA_KUKUCA_NAME="\"\""
  fi

  # Create a KukuCA manifest (YAML) from a template
  sed \
    -e "s?{{ENCODED_KUMORI_MANIFEST}}?${MONITORING_CA_ENCODED_KUMORI_MANIFEST}?g" \
    -e "s?{{HASHED_KUKUCA_OWNER}}?${HASHED_MONITORING_CA_KUKUCA_OWNER}?g" \
    -e "s?{{HASHED_KUKUCA_DOMAIN}}?${HASHED_MONITORING_CA_KUKUCA_DOMAIN}?g" \
    -e "s?{{HASHED_KUKUCA_NAME}}?${HASHED_MONITORING_CA_KUKUCA_NAME}?g" \
    -e "s?{{KUKUCA_OWNER}}?${MONITORING_CA_KUKUCA_OWNER}?g" \
    -e "s?{{KUKUCA_DOMAIN}}?${CLUSTER_CORE_DOMAIN}?g" \
    -e "s?{{KUKUCA_NAME}}?${MONITORING_CA_KUKUCA_NAME}?g" \
    -e "s?{{KUKUCA_FULL_NAME}}?${MONITORING_CA_KUKUCA_FULL_NAME}?g" \
    -e "s?{{ENCODED_CRT}}?${MONITORING_CA_ENCODED_CRT}?g" \
    -e "s?{{MANIFEST_SHA}}?${MONITORING_CA_MANIFEST_SHA}?g" \
    -e "s?{{IS_PUBLIC}}?${MONITORING_CA_KUKUCA_IS_PUBLIC}?g" \
    ${MONITORING_CA_KUKUCA_YAML_TEMPLATE} \
    > ${MONITORING_CA_KUKUCA_YAML}

  # Apply the KukuCA manifest to the cluster
  kubectl apply -f ${MONITORING_CA_KUKUCA_YAML}
fi

# Add our custom manifests:
# - adapter-external-apiService.yaml (ticket 1535)
# - adapter-custom-apiService.yaml (kv3 ticket 640)
#   Kube-prometheus only creates an APIService object to allow Prometheus-Adapter
#   to aggregate the metric.k8s.io api. But we want to allow Prometheus-Adapter
#   to aggregate the external.k8s.io.api and custom.k8s.io.api too.
# - external-metrics-cluster-role.yaml (ticket 1535)
#   ClusterRole object providing access to external.metrics.k8s.io api
# - custom-metrics-cluster-role.yaml (kv3 ticket 640)
#   ClusterRole object providing access to custom.metrics.k8s.io api
# - hpa-external-metrics-cluster-role-binding.yaml (ticket 1535)
#   The HPA service account is binded to the previous cluster role
# - prometheusAdapter-configMap.yaml
#   It isnt a new file; the original is
#   https://github.com/coreos/kube-prometheus/blob/master/manifests/prometheusAdapter-configMap.yaml
#   ... but we added to it external and custom rules (for example: http requests per second)
cp custom-manifests/adapter-custom-apiService.yaml kube-prometheus/manifests/adapter-custom-apiService.yaml
cp custom-manifests/adapter-external-apiService.yaml kube-prometheus/manifests/adapter-external-apiService.yaml
cp custom-manifests/custom-metrics-cluster-role.yaml kube-prometheus/manifests/custom-metrics-cluster-role.yaml
cp custom-manifests/external-metrics-cluster-role.yaml kube-prometheus/manifests/external-metrics-cluster-role.yaml
cp custom-manifests/hpa-external-metrics-cluster-role-binding.yaml kube-prometheus/manifests/hpa-external-metrics-clouster-role-binding.yaml
cp custom-manifests/prometheusAdapter-configMap.yaml kube-prometheus/manifests/prometheusAdapter-configMap.yaml


# Replace official PrometheusRules definition files with custom:
#
# - Remove completely files where only alerts are defined (alerts are managed by Kumori KuAlarm)
rm -f kube-prometheus/manifests/alertmanager-prometheusRule.yaml
rm -f kube-prometheus/manifests/kubeStateMetrics-prometheusRule.yaml
rm -f kube-prometheus/manifests/prometheus-prometheusRule.yaml
rm -f kube-prometheus/manifests/prometheusOperator-prometheusRule.yaml
# - In files where alerts and record rules are defined, replace with new files
#   where only record rules are defined
rm -f kube-prometheus/nodeExporter-prometheusRule.yaml
rm -f kube-prometheus/kubernetesControlPlane-prometheusRule.yaml
rm -f kube-prometheus/grafana-prometheusRule.yaml
rm -f kube-prometheus/kubePrometheus-prometheusRule.yaml
cp custom-manifests/rules/nodeExporter-prometheusRule.yaml kube-prometheus/manifests/nodeExporter-prometheusRule.yaml
cp custom-manifests/rules/kubernetesControlPlane-prometheusRule.yaml kube-prometheus/manifests/kubernetesControlPlane-prometheusRule.yaml
cp custom-manifests/rules/grafana-prometheusRule.yaml kube-prometheus/manifests/grafana-prometheusRule.yaml
cp custom-manifests/rules/kubePrometheus-prometheusRule.yaml kube-prometheus/manifests/kubePrometheus-prometheusRule.yaml
# - Add specific kumori rules (used in custom alerts)
cp custom-manifests/rules/kumori-prometheusRule.yaml kube-prometheus/manifests/kumori-prometheusRule.yaml

# Use the alertmanager configuration derived from the CUE configuration (created
# by kumorimgr)
if [ "${INSTALL_KUALARM}" == "true" ]; then
  cp ${ADDON_FIXTURES_DIR_KUMORI}/alarms/alarmmanager.yaml kube-prometheus/manifests/alertmanager-secret.yaml
fi

# Overwrite kube-prometheus version with custom version with multicluster support
# configured (enable/disable cluster selector in dashboards)
if [ "${GRAFANA_ENABLE_MULTICLUSTER_SUPPORT}" = "true" ]; then
  # Value "0" means selector active
  sed \
    -e "s?___CLUSTER_SELECTOR_HIDDEN_VALUE___?0?g" \
    grafana/grafana-dashboardDefinitions.yaml_template \
    > kube-prometheus/manifests/grafana-dashboardDefinitions.yaml

  sed -i \
    -e "s?___CLUSTER_SELECTOR_HIDDEN_VALUE___?0?g" \
    grafana/dashboards/*.yaml
else
  # Value "2" means selector hidden
  sed \
    -e "s?___CLUSTER_SELECTOR_HIDDEN_VALUE___?2?g" \
    grafana/grafana-dashboardDefinitions.yaml_template \
    > kube-prometheus/manifests/grafana-dashboardDefinitions.yaml

  sed -i \
    -e "s?___CLUSTER_SELECTOR_HIDDEN_VALUE___?2?g" \
    grafana/dashboards/*.yaml
fi


# Dashboards added by Kumori (including a provisional generic dashboard for alarms)
cp -r grafana/dashboards kube-prometheus/manifests/

### # TWEAK SOME CONFIGURATION PARAMETERS (to avoid using JSONNET)
### patch kube-prometheus/manifests/grafana-deployment.yaml < grafana/grafana-deployment.patch

# Replace grafana deployment manifest with one tweaked by Kumori
rm -f kube-prometheus/manifests/grafana-deployment.yaml
cp custom-manifests/grafana-deployment.yaml kube-prometheus/manifests/grafana-deployment.yaml

# Replace resources requests and limits values in Grafana deployment manifest
# Keycloak OAuth endpoints (cluster-internal and external)
# KEYCLOAK_OAUTH_ENDPOINT_BASE_INTERNAL="http://keycloak-http.keycloak:8080/auth/realms/KumoriCluster/protocol/openid-connect"
# KEYCLOAK_OAUTH_ENDPOINT_BASE_EXTERNAL="https://keycloak-${RELEASE_NAME}.${REFERENCE_DOMAIN}/auth/realms/KumoriCluster/protocol/openid-connect"
# # Keycloak OAuth Auth endpoint. MUST be an external URL since it is used from browsers.
# KEYCLOAK_OAUTH_ENDPOINT_AUTH="${KEYCLOAK_OAUTH_ENDPOINT_BASE_EXTERNAL}/auth"
# # Keycloak OAuth Token endpoint. Can be a cluster-internal URL.
# KEYCLOAK_OAUTH_ENDPOINT_TOKEN="${KEYCLOAK_OAUTH_ENDPOINT_BASE_INTERNAL}/token"
# # Keycloak OAuth API endpoint. Can be a cluster-internal URL.
# KEYCLOAK_OAUTH_ENDPOINT_API="${KEYCLOAK_OAUTH_ENDPOINT_BASE_INTERNAL}/userinfo"


# If prometheus remote authentication with client certificate is not enabled,
# remove all lines from custom Grafana deployment file related to remote with
# client certificate authentication.
# These lines are marked with a specific comment at the end.
if [ "${PROMETHEUS_PERSISTENCE_WITH_CLIENCERT}" = "false" ]; then
  sed -i \
    -e '/# REMOVE IF NO CLIENCERT FOR REMOTE/d' \
    kube-prometheus/manifests/grafana-deployment.yaml
fi

sed -i \
  -e "s?{{GRAFANA_CPU_REQUEST}}?${GRAFANA_CPU_REQUEST}?g" \
  -e "s?{{GRAFANA_CPU_LIMIT}}?${GRAFANA_CPU_LIMIT}?g" \
  -e "s?{{GRAFANA_MEM_REQUEST}}?${GRAFANA_MEM_REQUEST}?g" \
  -e "s?{{GRAFANA_MEM_LIMIT}}?${GRAFANA_MEM_LIMIT}?g" \
  -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" \
  -e "s?{{REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" \
  -e "s?{{REMOTE_CLIENTCERT_SECRET_NAME}}?${PROMETHEUS_PERSISTENCE_CLIENCERT_SECRET_NAME}?g" \
  kube-prometheus/manifests/grafana-deployment.yaml

cd kube-prometheus

# ADAPTING MANIFESTS/PROMETHEUS-PROMETHEUS.YAML FILE

# Number of prometheus replicas
PROMETHEUS_REPLICAS="1"
if [ "${PROMETHEUS_HA}" = "true" ]; then
  PROMETHEUS_REPLICAS=${PROMETHEUS_HA_REPLICAS}
fi

# If only one replica of Prometheus, don't create a PodDisruptionBudget
if [ "${PROMETHEUS_REPLICAS}" = "1" ]; then
  rm -f manifests/prometheus-podDisruptionBudget.yaml
fi

# Configure Prometheus deployment (using the Prometheus CRD used by
# prometheus-operator) with :
# 1.- the number of replicas
# 2.- a secret with credentials for gathering Etcd metrics
# 3.- a retention time (when to remove old data from local storage)
# 4.- a podAntiAffinity to force to Prometheus to be alocated in different nodes
# 5.- a remoteWrite, if Prometheus persistence is used
#
# Changes 1,2 and 3: Convert YAML manifest to JSON (for easier manipulation),
# manipulate the JSON and convert JSON manifest back to YAML
if [ -n "${ETCD_CERTS_SECRET_NAME}" ]; then
  cat manifests/prometheus-prometheus.yaml \
    | yq r -j - \
    | jq ".spec.replicas = ${PROMETHEUS_REPLICAS}" \
    | jq ".spec.secrets += [ \"${ETCD_CERTS_SECRET_NAME}\" ]" \
    | jq ".spec.retention +=  \"${PROMETHEUS_RETENTION_TIME}\"" \
    | jq ".spec.resources.requests.memory = \"${PROMETHEUS_MEM_REQUEST}\"" \
    | jq ".spec.resources.requests.cpu = \"${PROMETHEUS_CPU_REQUEST}\"" \
    | jq ".spec.resources.limits.memory = \"${PROMETHEUS_MEM_LIMIT}\"" \
    | jq ".spec.resources.limits.cpu = \"${PROMETHEUS_CPU_LIMIT}\"" \
    | yq r --prettyPrint - \
    > manifests/prometheus-prometheus_TMP.yaml
else
  cat manifests/prometheus-prometheus.yaml \
    | yq r -j - \
    | jq ".spec.replicas = ${PROMETHEUS_REPLICAS}" \
    | jq ".spec.retention +=  \"${PROMETHEUS_RETENTION_TIME}\"" \
    | jq ".spec.resources.requests.memory = \"${PROMETHEUS_MEM_REQUEST}\"" \
    | jq ".spec.resources.requests.cpu = \"${PROMETHEUS_CPU_REQUEST}\"" \
    | jq ".spec.resources.limits.memory = \"${PROMETHEUS_MEM_LIMIT}\"" \
    | jq ".spec.resources.limits.cpu = \"${PROMETHEUS_CPU_LIMIT}\"" \
    | yq r --prettyPrint - \
    > manifests/prometheus-prometheus_TMP.yaml
fi
#
# Changes 4 and 5: add partial YAML content to the file
cat ../custom-manifests/prometheus-prometheus-affinity.yaml.part >> manifests/prometheus-prometheus_TMP.yaml
if [ "${PROMETHEUS_PERSISTENCE}" = "true" ]; then
  # Remove 'externalLabels' property from configuration
  yq d -i manifests/prometheus-prometheus_TMP.yaml 'spec.externalLabels'

  if [ "${PROMETHEUS_PERSISTENCE_WITH_CLIENCERT}" = "true" ]; then
    # Add persistence with client certificate partial YAML config
    cat ../custom-manifests/prometheus-prometheus-remotewrite-with-clientcert.yaml.part >> manifests/prometheus-prometheus_TMP.yaml
    sed -i \
      -e "s?{{PROMETHEUS_REMOTE_WRITE_URL}}?${PROMETHEUS_REMOTE_WRITE_URL}?g" \
      -e "s?{{CLUSTER_NAME}}?${RELEASE_NAME}?g" \
      -e "s?{{REMOTE_CLIENTCERT_SECRET_NAME}}?${PROMETHEUS_PERSISTENCE_CLIENCERT_SECRET_NAME}?g" \
      manifests/prometheus-prometheus_TMP.yaml
  else
    # Add persistence partial YAML config
    cat ../custom-manifests/prometheus-prometheus-remotewrite.yaml.part >> manifests/prometheus-prometheus_TMP.yaml
    sed -i \
      -e "s?{{PROMETHEUS_REMOTE_WRITE_URL}}?${PROMETHEUS_REMOTE_WRITE_URL}?g" \
      -e "s?{{CLUSTER_NAME}}?${RELEASE_NAME}?g" \
      manifests/prometheus-prometheus_TMP.yaml
  fi
fi

mv manifests/prometheus-prometheus_TMP.yaml manifests/prometheus-prometheus.yaml

echo "Prometheus manifest to be applied:"
cat manifests/prometheus-prometheus.yaml
echo

# ADAPTING MANIFESTS/PROMETHEUSOPERATOR-DEPLOYMENT.YAML FILE

if [ -n "${PROMETHEUS_CONFIG_RELOADER_MEM_REQUEST}" ]; then
  cat manifests/prometheusOperator-deployment.yaml \
  | yq r -j - \
  | jq ".spec.template.spec.containers[0].args[.spec.template.spec.containers[0].args| length] |= . + \"--config-reloader-memory-request=${PROMETHEUS_CONFIG_RELOADER_MEM_REQUEST}\"" \
  | yq r --prettyPrint - \
  > manifests/prometheusOperator-deployment_TMP.yaml
  mv manifests/prometheusOperator-deployment_TMP.yaml manifests/prometheusOperator-deployment.yaml
fi

if [ -n "${PROMETHEUS_CONFIG_RELOADER_CPU_REQUEST}" ]; then
  cat manifests/prometheusOperator-deployment.yaml \
  | yq r -j - \
  | jq ".spec.template.spec.containers[0].args[.spec.template.spec.containers[0].args| length] |= . + \"--config-reloader-cpu-request=${PROMETHEUS_CONFIG_RELOADER_CPU_REQUEST}\"" \
  | yq r --prettyPrint - \
  > manifests/prometheusOperator-deployment_TMP.yaml
  mv manifests/prometheusOperator-deployment_TMP.yaml manifests/prometheusOperator-deployment.yaml
fi

if [ -n "${PROMETHEUS_CONFIG_RELOADER_MEM_LIMIT}" ]; then
  cat manifests/prometheusOperator-deployment.yaml \
  | yq r -j - \
  | jq ".spec.template.spec.containers[0].args[.spec.template.spec.containers[0].args| length] |= . + \"--config-reloader-memory-limit=${PROMETHEUS_CONFIG_RELOADER_MEM_LIMIT}\"" \
  | yq r --prettyPrint - \
  > manifests/prometheusOperator-deployment_TMP.yaml
  mv manifests/prometheusOperator-deployment_TMP.yaml manifests/prometheusOperator-deployment.yaml
fi

if [ -n "${PROMETHEUS_CONFIG_RELOADER_CPU_LIMIT}" ]; then
  cat manifests/prometheusOperator-deployment.yaml \
  | yq r -j - \
  | jq ".spec.template.spec.containers[0].args[.spec.template.spec.containers[0].args| length] |= . + \"--config-reloader-cpu-limit=${PROMETHEUS_CONFIG_RELOADER_CPU_LIMIT}\"" \
  | yq r --prettyPrint - \
  > manifests/prometheusOperator-deployment_TMP.yaml
  mv manifests/prometheusOperator-deployment_TMP.yaml manifests/prometheusOperator-deployment.yaml
fi

echo "Prometheus operator manifest to be applied:"
cat manifests/prometheusOperator-deployment.yaml
echo

# ADAPTING MANIFESTS/ALERTMANAGER-ALERTMANAGER.YAML FILE

# Number of alertmanager replicas
ALERTMANAGER_REPLICAS="1"
if [ "${ALERTMANAGER_HA}" = "true" ]; then
  ALERTMANAGER_REPLICAS=${ALERTMANAGER_HA_REPLICAS}
fi

# If only one replica of AlertManager, don't create a PodDisruptionBudget
if [ "${ALERTMANAGER_REPLICAS}" = "1" ]; then
  rm -f manifests/alertmanager-podDisruptionBudget.yaml
fi

# Configure AlertManager deployment (using the Prometheus CRD used by
# prometheus-operator) with :
# 1.- the number of replicas
# 2.- a podAntiAffinity to force to AlertManager to be alocated in different nodes
#
# Change 1: Convert YAML manifest to JSON (for easier manipulation),
# manipulate the JSON and convert JSON manifest back to YAML
cat manifests/alertmanager-alertmanager.yaml \
  | yq r -j - \
  | jq ".spec.replicas = ${ALERTMANAGER_REPLICAS}" \
  | jq ".spec.resources.requests.memory = \"${ALERTMANAGER_MEM_REQUEST}\"" \
  | jq ".spec.resources.requests.cpu = \"${ALERTMANAGER_CPU_REQUEST}\"" \
  | jq ".spec.resources.limits.memory = \"${ALERTMANAGER_MEM_LIMIT}\"" \
  | jq ".spec.resources.limits.cpu = \"${ALERTMANAGER_CPU_LIMIT}\"" \
  | yq r --prettyPrint - \
  > manifests/alertmanager-alertmanager_TMP.yaml
#
# Change 2: add partial YAML content to the file
cat ../custom-manifests/alertmanager-alertmanager-affinity.yaml.part >> manifests/alertmanager-alertmanager_TMP.yaml

mv manifests/alertmanager-alertmanager_TMP.yaml manifests/alertmanager-alertmanager.yaml

echo "AlertManager manifest to be applied:"
cat manifests/alertmanager-alertmanager.yaml
echo

# ADAPTING MANIFESTS/GRAFANA-DEPLOYMENT.YAML FILE

# Number of grafana replicas
GRAFANA_REPLICAS="1"
if [ "${GRAFANA_HA}" = "true" ]; then
  GRAFANA_REPLICAS=${GRAFANA_HA_REPLICAS}
fi

# Grafana deployment rolling update (hardcoded value: not configurable)
GRAFANA_ROLLINGUPDATE="{
  \"rollingUpdate\": {
    \"maxSurge\": \"50%\",
    \"maxUnavailable\": \"50%\"
  },
  \"type\": \"RollingUpdate\"
}"

# Configure Grafana deployment with :
# 1.- a podAntiAffinity to force to Grafana to be alocated in different nodes
# 2.- the number of replicas
# 3.- rollingupdate with: maxSurge: 50%, maxUnavailable: 50%
#
# Change 1: add partial YAML content to the file
cp manifests/grafana-deployment.yaml manifests/grafana-deployment_TMP_1.yaml
cat ../custom-manifests/grafana-deployment-affinity.yaml.part >> manifests/grafana-deployment_TMP_1.yaml
#
# Change 2 and 3: Convert YAML manifest to JSON (for easier manipulation),
# manipulate the JSON and convert JSON manifest back to YAML
cat manifests/grafana-deployment_TMP_1.yaml \
  | yq r -j - \
  | jq ".spec.replicas = ${GRAFANA_REPLICAS}" \
  | jq ".spec.strategy = ${GRAFANA_ROLLINGUPDATE}" \
  | yq r --prettyPrint - \
  > manifests/grafana-deployment_TMP_2.yaml

rm manifests/grafana-deployment_TMP_1.yaml
mv manifests/grafana-deployment_TMP_2.yaml manifests/grafana-deployment.yaml

echo "Grafana manifest to be applied:"
cat manifests/grafana-deployment.yaml
echo

# ADAPTING MANIFESTS/KUBESTATEMETRICS-DEPLOYMENT IN KUBE PROMETHEUS (TICKET 1024)
# - include tolerations to avoid executing kube-state-metrics in non-ready or unreachable nodes (previously applied as a patch in kube-state-metrics-deployment-tolerations.yaml.part)
# - added --metric-labels-allowlist to generate metrics from pods, deployments and statefulsets labels (previously as a patch applied using yq and jq)
# - added --metric-annotations-allowlist to generate metrics from pods, deployments and statefulsets annotations (new in 2.2.0)
#
cp ../custom-manifests/kubeStateMetrics-deployment.yaml manifests/kubeStateMetrics-deployment.yaml
echo "Kube-state-metrics manifest to be applied:"
cat manifests/kubeStateMetrics-deployment.yaml
echo

# CONFIGURING NODE-EXPORTER PORT
# Change default NodeExporter port from 9100 to a custom one
NODE_EXPORTER_CUSTOM_PORT="10100"
sed -i \
  -e "s?9100?${NODE_EXPORTER_CUSTOM_PORT}?g" \
  manifests/nodeExporter-daemonset.yaml
sed -i \
  -e "s?9100?${NODE_EXPORTER_CUSTOM_PORT}?g" \
  manifests/nodeExporter-service.yaml
sed -i \
  -e "s?9100?${NODE_EXPORTER_CUSTOM_PORT}?g" \
  manifests/nodeExporter-networkPolicy.yaml

# Kube-prometheus creates networkpolicies, but it breaks Ingress accessing (to
# grafana/prometheus/alermanager webs) and user pods accessing prometheus
# For now, the networkpolicies are removed, but we will reconsider use it
# (related ticket: 1372)
rm manifests/alertmanager-networkPolicy.yaml
rm manifests/blackboxExporter-networkPolicy.yaml
rm manifests/grafana-networkPolicy.yaml
rm manifests/kubeStateMetrics-networkPolicy.yaml
rm manifests/nodeExporter-networkPolicy.yaml
rm manifests/prometheus-networkPolicy.yaml
rm manifests/prometheusAdapter-networkPolicy.yaml
rm manifests/prometheusOperator-networkPolicy.yaml

# CRDS

# Create the namespace and CRDs, and then wait for them to be available before
# creating the remaining resources
# Note that due to some CRD size we are using kubectl server-side apply feature
# which is generally available since kubernetes 1.22.
# If you are using previous kubernetes versions this feature may not be available
# and you would need to use kubectl create instead.
kubectl apply --server-side -f manifests/setup
kubectl wait \
	--for condition=Established \
	--all CustomResourceDefinition \
	--namespace=monitoring
kubectl create -f manifests/dashboards
kubectl create -f manifests/

# Ingress to access prometheus/grafana/alertmanager
cd ..
sed -i -e "s?__RELEASE_NAME__?${RELEASE_NAME}?g" ingress/*.yaml
sed -i -e "s?__REFERENCE_DOMAIN__?${REFERENCE_DOMAIN}?g" ingress/*.yaml
sed -i -e "s?__KUKUCERT_SECRET_NAME__?${KUKUCERT_SECRET_NAME}?g" ingress/*.yaml
sed -i -e "s?__KUINBOUND_CIPHERSUITETLS__?${KUINBOUND_CIPHERSUITETLS}?g" ingress/*.yaml
sed -i -e "s?__KUINBOUND_ECDHCURVESTLS__?${KUINBOUND_ECDHCURVESTLS}?g" ingress/*.yaml
sed -i -e "s?__KUINBOUND_MINTLSVERSION__?${KUINBOUND_MINTLSVERSION}?g" ingress/*.yaml
sed -i -e "s?__KUINBOUND_MAXTLSVERSION__?${KUINBOUND_MAXTLSVERSION}?g" ingress/*.yaml
sed -i -e "s?__MONITORING_CLIENT_CERT_REQUIRED__?${MONITORING_REQUIRE_CLIENTCERT}?g" ingress/*.yaml
if [ "${MONITORING_REQUIRE_CLIENTCERT}" = "true" ]; then
  sed -i -e "s?__MONITORING_INBOUND_CA_SECRET__?${MONITORING_CA_KUKUCA_SECRET_NAME}?g" ingress/*.yaml
else
  # Leave CA secret empty
  sed -i -e "s?__MONITORING_INBOUND_CA_SECRET__??g" ingress/*.yaml
fi

# If IPFiltering feature is enabled and a list of enabled IP/CIDR has been
# defined for Prom/Graf/AlertMan, add the AllowedIP section to the
# Prom/Graf/AlertMan mapping
add_allowed_ip_to_mapping $INSTALL_IPFILTERING ingress/prometheus-ingress.yaml $PROMETHEUS_ALLOWEDIP
add_allowed_ip_to_mapping $INSTALL_IPFILTERING ingress/grafana-ingress.yaml $GRAFANA_ALLOWEDIP
add_allowed_ip_to_mapping $INSTALL_IPFILTERING ingress/alertmanager-ingress.yaml $ALERTMANAGER_ALLOWEDIP

kubectl apply -f ingress/
kubectl create -f rbac/

# Prepare ServiceMonitors for installed addons
# . prepare_servicemonitor.sh
# Ambassador
sed -i -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" servicemonitor/ambassador-servicemonitor.yaml


# Prepare internal balancers ServiceMonitors (ApiServer and Ingress)
# These require a manuallist of endpoints (Node IPs)
INTERNAL_BALANCER_SERVICEMONITOR_TEMPLATE="servicemonitor/envoy-internal-balancer-servicemonitor.yaml_template"

if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ] ; then
  INGRESS_BALANCER_SERVICEMONITOR="servicemonitor/ingress-internal-balancer-servicemonitor.yaml"

  sed -e "s?{{INTERNAL_BALANCER_NAME}}?ingress?g" \
    ${INTERNAL_BALANCER_SERVICEMONITOR_TEMPLATE} \
    > ${INGRESS_BALANCER_SERVICEMONITOR}

  EMPTY_LIST="true"
  for INGRESS_NODE_IP in ${INGRESS_NODES_IPS[@]}; do
    echo "IP: ${INGRESS_NODE_IP}"
    EMPTY_LIST="false"
    # Duplicate the NODE_ADDRESS_ITEM template line and replace the IP in one of the copies
    sed -i -e "s/^\(\s*\)- {{NODE_ADDRESS_ITEM}}\s*$/\1- ip: ${INGRESS_NODE_IP}\n&/g" ${INGRESS_BALANCER_SERVICEMONITOR}
  done

  # If address list is empty, set it to []
  if [[ "${EMPTY_LIST}" == "true" ]]; then
    sed -i -e "s/^\(\s*\)- addresses:\s*$/- addresses: []/g" ${INGRESS_BALANCER_SERVICEMONITOR}
  fi

  # Remove NODE_ADDRESS_ITEM  template line, if any left
  sed -i -e "/^.*{{NODE_ADDRESS_ITEM}}.*$/d" ${INGRESS_BALANCER_SERVICEMONITOR}

  cat ${INGRESS_BALANCER_SERVICEMONITOR}
fi

if [ "${APISERVER_INTERNAL_BALANCING}" = "true" ] ; then
  APISERVER_BALANCER_SERVICEMONITOR="servicemonitor/apiserver-internal-balancer-servicemonitor.yaml"

  sed -e "s?{{INTERNAL_BALANCER_NAME}}?apiserver?g" \
    ${INTERNAL_BALANCER_SERVICEMONITOR_TEMPLATE} \
    > ${APISERVER_BALANCER_SERVICEMONITOR}

  EMPTY_LIST="true"
  for MASTER_NODE_IP in ${MASTERS_IPS[@]}; do
    echo "IP: ${MASTER_NODE_IP}"
    EMPTY_LIST="false"
    # Duplicate the NODE_ADDRESS_ITEM template line and replace the IP in one of the copies
    sed -i -e "s/^\(\s*\)- {{NODE_ADDRESS_ITEM}}\s*$/\1- ip: ${MASTER_NODE_IP}\n&/g" ${APISERVER_BALANCER_SERVICEMONITOR}
  done

  # If address list is empty, set it to []
  if [[ "${EMPTY_LIST}" == "true" ]]; then
    sed -i -e "s/^\(\s*\)- addresses:\s*$/- addresses: []/g" ${APISERVER_BALANCER_SERVICEMONITOR}
  fi

  # Remove NODE_ADDRESS_ITEM  template line, if any left
  sed -i -e "/^.*{{NODE_ADDRESS_ITEM}}.*$/d" ${APISERVER_BALANCER_SERVICEMONITOR}

  cat ${APISERVER_BALANCER_SERVICEMONITOR}
fi


# Create all configured ServiceMonitors and PodMonitors
kubectl create -f servicemonitor/
kubectl create -f podmonitor/

# Specific problem (ticket1373):
# K8s 1.24 has removed the Docker plugin from cAdvisor. So while you can use
# cri-dockerd (Docker by Mirantis) to adjust the container runtime, kubelet can
# no longer retrieve Docker container information such as image, pod, container
# labels, etc. through cAdvisor."
# The solution is deploy a daemonset running cadvisor in all nodes, and instruct
# prometheus to scrap its metrics
if [ "${INSTALL_CADVISOR}" == "true" ]; then
  DAEMONSETFILE="cadvisor/cadvisor.yaml"
  rm -f ${DAEMONSETFILE}
  cp ${DAEMONSETFILE}_template ${DAEMONSETFILE}
  sed -i \
    -e "s?{{CADVISOR_VERSION}}?${CADVISOR_VERSION}?g" \
    -e "s?{{CADVISOR_CPU_REQUEST}}?${CADVISOR_CPU_REQUEST}?g" \
    -e "s?{{CADVISOR_CPU_LIMIT}}?${CADVISOR_CPU_LIMIT}?g" \
    -e "s?{{CADVISOR_MEM_REQUEST}}?${CADVISOR_MEM_REQUEST}?g" \
    -e "s?{{CADVISOR_MEM_LIMIT}}?${CADVISOR_MEM_LIMIT}?g" \
  ${DAEMONSETFILE}
  kubectl apply -f ${DAEMONSETFILE}
fi