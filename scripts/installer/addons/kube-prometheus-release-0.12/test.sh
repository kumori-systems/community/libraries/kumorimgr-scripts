#!/bin/bash

APISERVER_INTERNAL_BALANCING="true"

INGRESS_NODES_IPS=(
  "10.0.0.1"
  "10.0.0.2"
  "10.0.0.3"
)
MASTERS_IPS=(
  "10.0.0.1"
  "10.0.0.2"
  "10.0.0.3"
)
# APISERVER_NODES_IPS=()
MASTERS_IPS=()

# Prepare internal balancers ServiceMonitors (ApiServer and Ingress)
# These require a manuallist of endpoints (Node IPs)
INTERNAL_BALANCER_SERVICEMONITOR_TEMPLATE="servicemonitor/envoy-internal-balancer-servicemonitor.yaml_template"

if [ "${APISERVER_INTERNAL_BALANCING}" = "true" ] ; then
  APISERVER_BALANCER_SERVICEMONITOR="servicemonitor/apiserver-internal-balancer-servicemonitor.yaml"

  sed -e "s?{{INTERNAL_BALANCER_NAME}}?apiserver?g" \
    ${INTERNAL_BALANCER_SERVICEMONITOR_TEMPLATE} \
    > ${APISERVER_BALANCER_SERVICEMONITOR}

  EMPTY_LIST="true"
  for MASTER_NODE_IP in ${MASTERS_IPS[@]}; do
    echo "IP: ${MASTER_NODE_IP}"
    EMPTY_LIST="false"
    # Duplicate the NODE_ADDRESS_ITEM template line and replace the IP in one of the copies
    sed -i -e "s/^\(\s*\)- {{NODE_ADDRESS_ITEM}}\s*$/\1- ${MASTER_NODE_IP}\n&/g" ${APISERVER_BALANCER_SERVICEMONITOR}
  done

  # If address list is empty, set it to []
  if [[ "${EMPTY_LIST}" == "true" ]]; then
    sed -i -e "s/^\(\s*\)- addresses:\s*$/- addresses: []/g" ${APISERVER_BALANCER_SERVICEMONITOR}
  fi

  # Remove NODE_ADDRESS_ITEM  template line, if any left
  sed -i -e "/^.*{{NODE_ADDRESS_ITEM}}.*$/d" ${APISERVER_BALANCER_SERVICEMONITOR}

  cat ${APISERVER_BALANCER_SERVICEMONITOR}
fi

