#!/bin/bash

# curl https://docs.projectcalico.org/${CALICO_VERSION}/manifests/calico.yaml -O
curl https://docs.projectcalico.org/archive/${CALICO_VERSION}/manifests/calico.yaml -O

## ticket411 - CALICO ALREADY INCLUDES WHAT WE USED TO PATCH (bandwidth=true)
# patch calico.yaml < enable_bandwidth_plugin.patch

## ticket411 - CALICO v3.18 AUTOMATICALLY DETECTS THE POD CIDR USED WITH KUBEADM
# sed -i -e "s?192.168.0.0/16?${PODS_NETWORK_CIDR}?g" calico.yaml

# PREPARE CALICO DAEMONSET CONFIGURATION
# Extract calico-node DaemonSet manifest from the full YAML file
# (it's the document number 22 of the file)
yq read --doc 22 calico.yaml > calico-node-daemonset.yaml

# Delete the manifest we just extracted from the full file
yq delete --inplace --doc 22 calico.yaml "*"
# Deletion leaves a line with {} that kubectl doesn't like. Remove it.
sed -i -e "/^.*{}.*$/d" calico.yaml

# Configure custom ports to free the TCP Ingress port range 9000-9999
# Add custom FELIX_HEALTHPORT configuration (9099 --> 10099)
cat calico-node-daemonset.yaml \
  | yq r -j - \
  | jq ".spec.template.spec.containers[0].env += [ { name: \"FELIX_HEALTHPORT\", value: \"10099\" } ]" \
  | yq r --prettyPrint - \
  > calico-node-daemonset_TMP.yaml
mv calico-node-daemonset_TMP.yaml calico-node-daemonset.yaml

kubectl apply -f calico.yaml
kubectl apply -f calico-node-daemonset.yaml

until kubectl get felixconfiguration default ; do
  echo "$(date +"%d/%m/%Y %H:%M:%S") Waiting for Calico 'default felixconfiguration' to be ready..."
  sleep 5
  echo ""
done
kubectl get felixconfiguration default -o yaml > felixconfiguration.yaml
echo "  prometheusMetricsEnabled: true" >> felixconfiguration.yaml
# Configure custom ports to free the TCP Ingress port range 9000-9999
# Replace prometheus metrics port (9091 --> 10091)
echo "  prometheusMetricsPort: 10091" >> felixconfiguration.yaml
kubectl apply -f felixconfiguration.yaml
kubectl apply -f calico-node-service.yaml


