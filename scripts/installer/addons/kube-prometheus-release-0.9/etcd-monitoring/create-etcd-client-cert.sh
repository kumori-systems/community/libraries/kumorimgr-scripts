#!/bin/bash

SCRIPT_DIR="$(dirname $(readlink -f $0))"

CLIENT_CERT_C="ES"
CLIENT_CERT_ST="Valencia"
CLIENT_CERT_L="Valencia"
CLIENT_CERT_O="Kumori Systems SL"
CLIENT_CERT_CN="etcd-metrics-client"

CA_PATH="/etc/kubernetes/pki/etcd"

WORKDIR="${SCRIPT_DIR}"

CSR_FILE="${WORKDIR}/etcd-metrics-client.csr"
CRT_FILE="${WORKDIR}/etcd-metrics-client.crt"
KEY_FILE="${WORKDIR}/etcd-metrics-client.key"

ETCD_CA_CRT_FILE="/etc/kubernetes/pki/etcd/ca.crt"
ETCD_CA_KEY_FILE="/etc/kubernetes/pki/etcd/ca.key"
LOCAL_ETCD_CA_CRT_FILE="${WORKDIR}/etcd-ca.crt"
LOCAL_ETCD_CA_KEY_FILE="${WORKDIR}/etcd-ca.key"

touch index.txt
echo "unique_subject = yes/no" > index.txt.attr
echo '01' > serial

echo
echo "Creating CSR..."
echo
# openssl req -config openssl.cnf -new -nodes -keyout private/etcd-client.key -out etcd-client.csr
openssl req \
  -config openssl.cnf \
  -new -nodes \
  -out "${CSR_FILE}" \
  -keyout "${KEY_FILE}" \
  -subj "/C=${CLIENT_CERT_C}/ST=${CLIENT_CERT_ST}/L=${CLIENT_CERT_L}/O=${CLIENT_CERT_O}/CN=${CLIENT_CERT_CN}"

echo
echo "Signing CSR and creating certificates..."
echo

sudo cp ${ETCD_CA_CRT_FILE} ${LOCAL_ETCD_CA_CRT_FILE}
sudo cp ${ETCD_CA_KEY_FILE} ${LOCAL_ETCD_CA_KEY_FILE}
sudo chown ${USER}:${USER} ${LOCAL_ETCD_CA_CRT_FILE}
sudo chown ${USER}:${USER} ${LOCAL_ETCD_CA_KEY_FILE}

# openssl ca -config openssl.cnf -extensions etcd_client -keyfile private/ca.key -cert certs/ca.crt -out certs/etcd-client.crt -infiles etcd-client.csr
openssl ca \
  -batch \
  -config openssl.cnf \
  -extensions etcd_client \
  -cert "${LOCAL_ETCD_CA_CRT_FILE}" \
  -keyfile "${LOCAL_ETCD_CA_KEY_FILE}" \
  -outdir "${WORKDIR}" \
  -out "${CRT_FILE}" \
  -infiles "${CSR_FILE}"

sudo chown $USER:$USER ${CRT_FILE}
echo
echo "Done."
echo

echo
echo "Creating Kubernetes secret for new certs..."
echo

ETCD_CERTS_SECRET_NAME="etcd-metrics-client-certs"

# Needs sudo because Etcd CA certs are owned by root
sudo kubectl create secret generic ${ETCD_CERTS_SECRET_NAME} \
  -n monitoring \
  --from-file=etcd-ca.crt="${LOCAL_ETCD_CA_CRT_FILE}" \
  --from-file=etcd-metrics-client.crt="${CRT_FILE}" \
  --from-file=etcd-metrics-client.key="${KEY_FILE}"

rm ${LOCAL_ETCD_CA_CRT_FILE} ${LOCAL_ETCD_CA_KEY_FILE}

