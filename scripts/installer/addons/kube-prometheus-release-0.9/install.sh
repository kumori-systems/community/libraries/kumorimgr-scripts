#!/bin/bash

# Delete kube-prometheus directory if it exists
rm -rf kube-prometheus

git clone -b "${KUBE_PROMETHEUS_VERSION}" https://github.com/coreos/kube-prometheus \
  || git clone https://github.com/coreos/kube-prometheus && cd kube-prometheus && git checkout "${KUBE_PROMETHEUS_VERSION}" && cd ..


if [ "${PROMETHEUS_PERSISTENCE}" = "true" ]; then
  sed -i -e "s?__PROMETHEUS_REMOTE_READ_URL__?${PROMETHEUS_REMOTE_READ_URL}?g" grafana/datasources_with_persistence.yaml
  cp grafana/datasources_with_persistence.yaml kube-prometheus/manifests/grafana-dashboardDatasources.yaml
else
  cp grafana/datasources.yaml kube-prometheus/manifests/grafana-dashboardDatasources.yaml
fi

# Create a namespace for monitoring elements
kubectl create namespace monitoring

# CREATE CLIENT CERTIFICATE AND KUBERNETES SECRET FOR GATHERING ETCD METRICS
if [ -d "/etc/kubernetes/pki/etcd" ]; then
  cd etcd-monitoring

  # Set some additional variables for configuring Etcd metrics gathering (secure HTTPS)
  WORKDIR="$(pwd)"
  CSR_FILE="${WORKDIR}/etcd-metrics-client.csr"
  CRT_FILE="${WORKDIR}/etcd-metrics-client.crt"
  KEY_FILE="${WORKDIR}/etcd-metrics-client.key"
  ETCD_CA_CRT_FILE="/etc/kubernetes/pki/etcd/ca.crt"
  ETCD_CA_KEY_FILE="/etc/kubernetes/pki/etcd/ca.key"
  LOCAL_ETCD_CA_CRT_FILE="${WORKDIR}/etcd-ca.crt"
  LOCAL_ETCD_CA_KEY_FILE="${WORKDIR}/etcd-ca.key"
  ETCD_CERTS_SECRET_NAME="etcd-metrics-client-certs"

  # Initialize OpenSSL stuff for creating and signing certificates in local dir
  touch index.txt
  echo "unique_subject = yes/no" > index.txt.attr
  echo '01' > serial

  echo
  echo "Creating CSR..."
  echo
  # Certificate values (C, O, ST, CN, ...) are taken from openssl.cnf condifguration file
  openssl req \
    -batch \
    -config openssl.cnf \
    -new -nodes \
    -out "${CSR_FILE}" \
    -keyout "${KEY_FILE}"

  echo
  echo "Signing CSR and creating certificates..."
  echo
  # Make a local copy of Etcd CA files for signing the certificate
  sudo cp ${ETCD_CA_CRT_FILE} ${LOCAL_ETCD_CA_CRT_FILE}
  sudo cp ${ETCD_CA_KEY_FILE} ${LOCAL_ETCD_CA_KEY_FILE}
  sudo chown ${USER}:${USER} ${LOCAL_ETCD_CA_CRT_FILE}
  sudo chown ${USER}:${USER} ${LOCAL_ETCD_CA_KEY_FILE}

  openssl ca \
    -batch \
    -config openssl.cnf \
    -extensions etcd_client \
    -cert "${LOCAL_ETCD_CA_CRT_FILE}" \
    -keyfile "${LOCAL_ETCD_CA_KEY_FILE}" \
    -outdir "${WORKDIR}" \
    -out "${CRT_FILE}" \
    -infiles "${CSR_FILE}"

  echo
  echo "Creating Kubernetes secret for new certs..."
  echo
  # Needs sudo because Etcd CA certs are owned by root
  kubectl create secret generic ${ETCD_CERTS_SECRET_NAME} \
    -n monitoring \
    --from-file=etcd-ca.crt="${LOCAL_ETCD_CA_CRT_FILE}" \
    --from-file=etcd-metrics-client.crt="${CRT_FILE}" \
    --from-file=etcd-metrics-client.key="${KEY_FILE}"

  # Delete local copy of Etcd CA files
  rm ${LOCAL_ETCD_CA_CRT_FILE} ${LOCAL_ETCD_CA_KEY_FILE}

  echo
  echo "Done."
  echo
  cd ..
else
  echo "Not in a Master node. Skipping Etcd monitoring configuration (certificate)."
  ETCD_CERTS_SECRET_NAME=""
fi

echo
echo "Creating Kubernetes secret for Grafana admin user..."
echo
# If master credentials are not provided use default
GRAFANA_ADMIN_USERNAME="${GRAFANA_ADMIN_USERNAME:-admin}"
GRAFANA_ADMIN_PASSWORD="${GRAFANA_ADMIN_PASSWORD:-admin}"
ENCODED_ADMIN_USERNAME="$( echo -n ${GRAFANA_ADMIN_USERNAME} | base64 )"
ENCODED_ADMIN_PASSWORD="$( echo -n ${GRAFANA_ADMIN_PASSWORD} | base64 )"
sed -i \
  -e "s?{{ENCODED_ADMIN_USERNAME}}?${ENCODED_ADMIN_USERNAME}?g" \
  -e "s?{{ENCODED_ADMIN_PASSWORD}}?${ENCODED_ADMIN_PASSWORD}?g" \
  grafana/grafana-credentials.yaml

kubectl apply -f grafana/grafana-credentials.yaml
echo
echo "Done."
echo

# Add our custom manifests:
# - adapter-external-apiService.yaml (ticket 1535)
# - adapter-custom-apiService.yaml (kv3 ticket 640)
#   Kube-prometheus only creates an APIService object to allow Prometheus-Adapter
#   to aggregate the metric.k8s.io api. But we want to allow Prometheus-Adapter
#   to aggregate the external.k8s.io.api and custom.k8s.io.api too.
# - external-metrics-cluster-role.yaml (ticket 1535)
#   ClusterRole object providing access to external.metrics.k8s.io api
# - custom-metrics-cluster-role.yaml (kv3 ticket 640)
#   ClusterRole object providing access to custom.metrics.k8s.io api
# - hpa-external-metrics-clouster-role-binding.yaml (ticket 1535)
#   The HPA service account is binded to the previous cluster role
# - prometheus-adapter-configMap.yaml
#   It isnt a new file; the original is
#   https://github.com/coreos/kube-prometheus/blob/master/manifests/prometheus-adapter-configMap.yaml
#   ... but we added to it external and custom rules (for example: http requests per second)
cp custom-manifests/adapter-external-apiService.yaml kube-prometheus/manifests/adapter-external-apiService.yaml
cp custom-manifests/external-metrics-cluster-role.yaml kube-prometheus/manifests/external-metrics-cluster-role.yaml
cp custom-manifests/adapter-custom-apiService.yaml kube-prometheus/manifests/adapter-custom-apiService.yaml
cp custom-manifests/custom-metrics-cluster-role.yaml kube-prometheus/manifests/custom-metrics-cluster-role.yaml
cp custom-manifests/hpa-external-metrics-clouster-role-binding.yaml kube-prometheus/manifests/hpa-external-metrics-clouster-role-binding.yaml
cp custom-manifests/prometheus-adapter-configMap.yaml kube-prometheus/manifests/prometheus-adapter-configMap.yaml


# Replace official PrometheusRules definition files with custom:
# - Removed alerts from every file (alerts are managed by Kumori KuAlarm)
rm -f kube-prometheus/manifests/alertmanager-prometheusRule.yaml
rm -f kube-prometheus/manifests/kube-prometheus-prometheusRule.yaml
rm -f kube-prometheus/manifests/kubernetes-prometheusRule.yaml
rm -f kube-prometheus/manifests/kube-state-metrics-prometheusRule.yaml
rm -f kube-prometheus/manifests/node-exporter-prometheusRule.yaml
rm -f kube-prometheus/manifests/prometheus-operator-prometheusRule.yaml
rm -f kube-prometheus/manifests/prometheus-prometheusRule.yaml
# Only three file contain record rules (the rest only contained alerts)
cp custom-manifests/rules/kube-prometheus-prometheusRule.yaml kube-prometheus/manifests/kube-prometheus-prometheusRule.yaml
cp custom-manifests/rules/kubernetes-prometheusRule.yaml kube-prometheus/manifests/kubernetes-prometheusRule.yaml
cp custom-manifests/rules/node-exporter-prometheusRule.yaml kube-prometheus/manifests/node-exporter-prometheusRule.yaml
cp custom-manifests/rules/kumori-prometheusRule.yaml kube-prometheus/manifests/kumori-prometheusRule.yaml

# PROVISIONAL: alertmanager configuration, for now, is the secret used by
# AlertManager addon. But we want the AlertManager configuration to be consumed
# by the KuAlarm controller.
if [ "${INSTALL_KUALARM}" == "true" ]; then
  cp ${ADDON_FIXTURES_DIR_KUMORI}/alarms/alarmmanager.yaml kube-prometheus/manifests/alertmanager-secret.yaml
fi

# PROVISIONAL: For now we use a generic dashboard for alarms
cp -r grafana/dashboards kube-prometheus/manifests/

### # TWEAK SOME CONFIGURATION PARAMETERS (to avoid using JSONNET)
### patch kube-prometheus/manifests/grafana-deployment.yaml < grafana/grafana-deployment.patch

# Replace grafana deployment manifest with one tweaked by Kumori
rm -f kube-prometheus/manifests/grafana-deployment.yaml
cp custom-manifests/grafana-deployment.yaml kube-prometheus/manifests/grafana-deployment.yaml


# Replace resources requests and limits values in Grafana deployment manifest
# Keycloak OAuth endpoints (cluster-internal and external)
KEYCLOAK_OAUTH_ENDPOINT_BASE_INTERNAL="http://keycloak-http.keycloak:8080/auth/realms/KumoriCluster/protocol/openid-connect"
KEYCLOAK_OAUTH_ENDPOINT_BASE_EXTERNAL="https://keycloak-${RELEASE_NAME}.${REFERENCE_DOMAIN}/auth/realms/KumoriCluster/protocol/openid-connect"
# Keycloak OAuth Auth endpoint. MUST be an external URL since it is used from browsers.
KEYCLOAK_OAUTH_ENDPOINT_AUTH="${KEYCLOAK_OAUTH_ENDPOINT_BASE_EXTERNAL}/auth"
# Keycloak OAuth Token endpoint. Can be a cluster-internal URL.
KEYCLOAK_OAUTH_ENDPOINT_TOKEN="${KEYCLOAK_OAUTH_ENDPOINT_BASE_INTERNAL}/token"
# Keycloak OAuth API endpoint. Can be a cluster-internal URL.
KEYCLOAK_OAUTH_ENDPOINT_API="${KEYCLOAK_OAUTH_ENDPOINT_BASE_INTERNAL}/userinfo"

sed -i \
  -e "s?{{GRAFANA_CPU_REQUEST}}?${GRAFANA_CPU_REQUEST}?g" \
  -e "s?{{GRAFANA_CPU_LIMIT}}?${GRAFANA_CPU_LIMIT}?g" \
  -e "s?{{GRAFANA_MEM_REQUEST}}?${GRAFANA_MEM_REQUEST}?g" \
  -e "s?{{GRAFANA_MEM_LIMIT}}?${GRAFANA_MEM_LIMIT}?g" \
  -e "s?{{OAUTH_ENDPOINT_AUTH}}?${KEYCLOAK_OAUTH_ENDPOINT_AUTH}?g" \
  -e "s?{{OAUTH_ENDPOINT_TOKEN}}?${KEYCLOAK_OAUTH_ENDPOINT_TOKEN}?g" \
  -e "s?{{OAUTH_ENDPOINT_API}}?${KEYCLOAK_OAUTH_ENDPOINT_API}?g" \
  -e "s?{{OAUTH_ROLE_STRICT}}?${GRAFANA_OAUTH_ROLE_STRICT}?g" \
  -e "s?{{OAUTH_CLIENT_SECRET}}?${KEYCLOAK_GRAFANA_CLIENT_SECRET}?g" \
  -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" \
  -e "s?{{REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" \
  kube-prometheus/manifests/grafana-deployment.yaml

cd kube-prometheus

# ADAPTING MANIFESTS/PROMETHEUS-PROMETHEUS.YAML FILE

# Number of prometheus replicas
PROMETHEUS_REPLICAS="1"
if [ "${PROMETHEUS_HA}" = "true" ]; then
  PROMETHEUS_REPLICAS=${PROMETHEUS_HA_REPLICAS}
fi

# If only one replica of Prometheus, don't create a PodDisruptionBudget
if [ "${PROMETHEUS_REPLICAS}" = "1" ]; then
  rm -f manifests/prometheus-podDisruptionBudget.yaml
fi

# Configure Prometheus deployment (using the Prometheus CRD used by
# prometheus-operator) with :
# 1.- the number of replicas
# 2.- a secret with credentials for gathering Etcd metrics
# 3.- a retention time (when to remove old data from local storage)
# 4.- a podAntiAffinity to force to Prometheus to be alocated in different nodes
# 5.- a remoteWrite, if Prometheus persistence is used
#
# Changes 1,2 and 3: Convert YAML manifest to JSON (for easier manipulation),
# manipulate the JSON and convert JSON manifest back to YAML
if [ -n "${ETCD_CERTS_SECRET_NAME}" ]; then
  cat manifests/prometheus-prometheus.yaml \
    | yq r -j - \
    | jq ".spec.replicas = ${PROMETHEUS_REPLICAS}" \
    | jq ".spec.secrets += [ \"${ETCD_CERTS_SECRET_NAME}\" ]" \
    | jq ".spec.retention +=  \"${PROMETHEUS_RETENTION_TIME}\"" \
    | jq ".spec.resources.requests.memory = \"${PROMETHEUS_MEM_REQUEST}\"" \
    | jq ".spec.resources.requests.cpu = \"${PROMETHEUS_CPU_REQUEST}\"" \
    | jq ".spec.resources.limits.memory = \"${PROMETHEUS_MEM_LIMIT}\"" \
    | jq ".spec.resources.limits.cpu = \"${PROMETHEUS_CPU_LIMIT}\"" \
    | yq r --prettyPrint - \
    > manifests/prometheus-prometheus_TMP.yaml
else
  cat manifests/prometheus-prometheus.yaml \
    | yq r -j - \
    | jq ".spec.replicas = ${PROMETHEUS_REPLICAS}" \
    | jq ".spec.retention +=  \"${PROMETHEUS_RETENTION_TIME}\"" \
    | jq ".spec.resources.requests.memory = \"${PROMETHEUS_MEM_REQUEST}\"" \
    | jq ".spec.resources.requests.cpu = \"${PROMETHEUS_CPU_REQUEST}\"" \
    | jq ".spec.resources.limits.memory = \"${PROMETHEUS_MEM_LIMIT}\"" \
    | jq ".spec.resources.limits.cpu = \"${PROMETHEUS_CPU_LIMIT}\"" \
    | yq r --prettyPrint - \
    > manifests/prometheus-prometheus_TMP.yaml
fi
#
# Changes 4 and 5: add partial YAML content to the file
cat ../custom-manifests/prometheus-prometheus-affinity.yaml.part >> manifests/prometheus-prometheus_TMP.yaml
if [ "${PROMETHEUS_PERSISTENCE}" = "true" ]; then
  cat ../custom-manifests/prometheus-prometheus-remotewrite.yaml.part >> manifests/prometheus-prometheus_TMP.yaml
  sed -i \
    -e "s?{{PROMETHEUS_REMOTE_READ_URL}}?${PROMETHEUS_REMOTE_READ_URL}?g" \
    -e "s?{{PROMETHEUS_REMOTE_WRITE_URL}}?${PROMETHEUS_REMOTE_WRITE_URL}?g" \
    manifests/prometheus-prometheus_TMP.yaml
fi

mv manifests/prometheus-prometheus_TMP.yaml manifests/prometheus-prometheus.yaml

echo "Prometheus manifest to be applied:"
cat manifests/prometheus-prometheus.yaml
echo

# ADAPTING MANIFESTS/ALERTMANAGER-ALERTMANAGER.YAML FILE

# Number of alertmanager replicas
ALERTMANAGER_REPLICAS="1"
if [ "${ALERTMANAGER_HA}" = "true" ]; then
  ALERTMANAGER_REPLICAS=${ALERTMANAGER_HA_REPLICAS}
fi

# If only one replica of AlertManager, don't create a PodDisruptionBudget
if [ "${ALERTMANAGER_REPLICAS}" = "1" ]; then
  rm -f manifests/alertmanager-podDisruptionBudget.yaml
fi

# Configure AlertManager deployment (using the Prometheus CRD used by
# prometheus-operator) with :
# 1.- the number of replicas
# 2.- a podAntiAffinity to force to AlertManager to be alocated in different nodes
#
# Change 1: Convert YAML manifest to JSON (for easier manipulation),
# manipulate the JSON and convert JSON manifest back to YAML
cat manifests/alertmanager-alertmanager.yaml \
  | yq r -j - \
  | jq ".spec.replicas = ${ALERTMANAGER_REPLICAS}" \
  | jq ".spec.resources.requests.memory = \"${ALERTMANAGER_MEM_REQUEST}\"" \
  | jq ".spec.resources.requests.cpu = \"${ALERTMANAGER_CPU_REQUEST}\"" \
  | jq ".spec.resources.limits.memory = \"${ALERTMANAGER_MEM_LIMIT}\"" \
  | jq ".spec.resources.limits.cpu = \"${ALERTMANAGER_CPU_LIMIT}\"" \
  | yq r --prettyPrint - \
  > manifests/alertmanager-alertmanager_TMP.yaml
#
# Change 2: add partial YAML content to the file
cat ../custom-manifests/alertmanager-alertmanager-affinity.yaml.part >> manifests/alertmanager-alertmanager_TMP.yaml

mv manifests/alertmanager-alertmanager_TMP.yaml manifests/alertmanager-alertmanager.yaml

echo "AlertManager manifest to be applied:"
cat manifests/alertmanager-alertmanager.yaml
echo

# ADAPTING MANIFESTS/GRAFANA-DEPLOYMENT.YAML FILE

# Number of grafana replicas
GRAFANA_REPLICAS="1"
if [ "${GRAFANA_HA}" = "true" ]; then
  GRAFANA_REPLICAS=${GRAFANA_HA_REPLICAS}
fi

# Grafana deployment rolling update (hardcoded value: not configurable)
GRAFANA_ROLLINGUPDATE="{
  \"rollingUpdate\": {
    \"maxSurge\": \"50%\",
    \"maxUnavailable\": \"50%\"
  },
  \"type\": \"RollingUpdate\"
}"

# Configure Grafana deployment with :
# 1.- a podAntiAffinity to force to Grafana to be alocated in different nodes
# 2.- the number of replicas
# 3.- rollingupdate with: maxSurge: 50%, maxUnavailable: 50%
#
# Change 1: add partial YAML content to the file
cp manifests/grafana-deployment.yaml manifests/grafana-deployment_TMP_1.yaml
cat ../custom-manifests/grafana-deployment-affinity.yaml.part >> manifests/grafana-deployment_TMP_1.yaml
#
# Change 2 and 3: Convert YAML manifest to JSON (for easier manipulation),
# manipulate the JSON and convert JSON manifest back to YAML
cat manifests/grafana-deployment_TMP_1.yaml \
  | yq r -j - \
  | jq ".spec.replicas = ${GRAFANA_REPLICAS}" \
  | jq ".spec.strategy = ${GRAFANA_ROLLINGUPDATE}" \
  | yq r --prettyPrint - \
  > manifests/grafana-deployment_TMP_2.yaml

rm manifests/grafana-deployment_TMP_1.yaml
mv manifests/grafana-deployment_TMP_2.yaml manifests/grafana-deployment.yaml

echo "Grafana manifest to be applied:"
cat manifests/grafana-deployment.yaml
echo

# ADAPTING MANIFESTS/KUBE-STATE-METRICS MANIFESTS IN KUBE PROMETHEUS

# FROM TICKET 1024
# Overwrite custom kube-state-metrics manifests with the one included in custom-manifests. The following changes
# has been applied to those manifests:
#
# * All
#   - change app.kubernetes.io/version label value to 2.2.0
#
# * kube-state-metrics-deployment.yaml
#   - include tolerations to avoid executing kube-state-metrics in non-ready or unreachable nodes (previously applied as a patch in kube-state-metrics-deployment-tolerations.yaml.part)
#   - added --metric-labels-allowlist to generate metrics from pods, deployments and statefulsets labels (previously as a patch applied using yq and jq)
#   - added --metric-annotations-allowlist to generate metrics from pods, deployments and statefulsets annotations (new in 2.2.0)
#   - change image version to k8s.gcr.io/kube-state-metrics/kube-state-metrics:v2.2.0 (first version including metrics containing kubernetes objects annotations)
#   - change app.kubernetes.io/version label value to 2.2.0 also in selector labels and template labels
#   
cp ../custom-manifests/kube-state-metrics-clusterRoleBinding.yaml manifests/kube-state-metrics-clusterRoleBinding.yaml
cp ../custom-manifests/kube-state-metrics-clusterRole.yaml        manifests/kube-state-metrics-clusterRole.yaml
cp ../custom-manifests/kube-state-metrics-deployment.yaml         manifests/kube-state-metrics-deployment.yaml
cp ../custom-manifests/kube-state-metrics-prometheusRule.yaml     manifests/kube-state-metrics-prometheusRule.yaml
cp ../custom-manifests/kube-state-metrics-serviceAccount.yaml     manifests/kube-state-metrics-serviceAccount.yaml
cp ../custom-manifests/kube-state-metrics-serviceMonitor.yaml     manifests/kube-state-metrics-serviceMonitor.yaml
cp ../custom-manifests/kube-state-metrics-service.yaml            manifests/kube-state-metrics-service.yaml


echo "Kube-state-metrics manifest to be applied:"
cat manifests/kube-state-metrics-deployment.yaml
echo


# CONFIGURING NODE-EXPORTER PORT
# Change default NodeExporter port from 9100 to a custom one
NODE_EXPORTER_CUSTOM_PORT="10100"
sed -i \
  -e "s?9100?${NODE_EXPORTER_CUSTOM_PORT}?g" \
  manifests/node-exporter-daemonset.yaml
sed -i \
  -e "s?9100?${NODE_EXPORTER_CUSTOM_PORT}?g" \
  manifests/node-exporter-service.yaml

# NAMESPACE AND CRDS

# Create the namespace and CRDs, and then wait for them to be availble before creating the remaining resources
kubectl create -f manifests/setup
kubectl create -f manifests/dashboards

# Replace this (prints nasty errors) with a cleaner wait
# until kubectl get servicemonitors --all-namespaces ; do date; sleep 1; echo ""; done
echo
echo "Waiting for ServiceMonitor CRD to be applied..."
until [ "$(kubectl get crd | grep servicemonitors)" != "" ]; do
  date
  echo "  Not yet, wait a little bit more..."
  sleep 1
done
echo "ServiceMonitor CRD already created!"
echo

kubectl create -f manifests/
cd ..
sed -i -e "s?__RELEASE_NAME__?${RELEASE_NAME}?g" ingress/*.yaml
sed -i -e "s?__REFERENCE_DOMAIN__?${REFERENCE_DOMAIN}?g" ingress/*.yaml

# If IPFiltering feature is enabled and a list of enabled IP/CIDR has been
# defined for Prom/Graf/AlertMan, add the AllowedIP section to the 
# Prom/Graf/AlertMan mapping
add_allowed_ip_to_mapping $INSTALL_IPFILTERING ingress/prometheus-ingress.yaml $PROMETHEUS_ALLOWEDIP
add_allowed_ip_to_mapping $INSTALL_IPFILTERING ingress/grafana-ingress.yaml $GRAFANA_ALLOWEDIP
add_allowed_ip_to_mapping $INSTALL_IPFILTERING ingress/alertmanager-ingress.yaml $ALERTMANAGER_ALLOWEDIP

kubectl apply -f ingress/
kubectl create -f rbac/

# Prepare ServiceMonitors for installed addons
# . prepare_servicemonitor.sh
# Ambassador
sed -i -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" servicemonitor/ambassador-servicemonitor.yaml

# Victoria Metrics
if [ "${PROMETHEUS_PERSISTENCE}" = "true" ]; then
  victoria_metrics_ip=$(echo ${PROMETHEUS_REMOTE_READ_URL} | cut -d'/' -f3 | cut -d':' -f1)
  sed -i -e "s?__VICTORIA_METRICS_IP__?${VICTORIA_METRICS_IP}?g" servicemonitor/victoria-metrics-servicemonitor.yaml
else
  rm -f servicemonitor/victoria-metrics-servicemonitor.yaml
fi

# Prepare internal balancers ServiceMonitors (ApiServer and Ingress)
# These require a manuallist of endpoints (Node IPs)
INTERNAL_BALANCER_SERVICEMONITOR_TEMPLATE="servicemonitor/envoy-internal-balancer-servicemonitor.yaml_template"

if [ "${INGRESS_INTERNAL_BALANCING}" = "true" ] ; then
  INGRESS_BALANCER_SERVICEMONITOR="servicemonitor/ingress-internal-balancer-servicemonitor.yaml"

  sed -e "s?{{INTERNAL_BALANCER_NAME}}?ingress?g" \
    ${INTERNAL_BALANCER_SERVICEMONITOR_TEMPLATE} \
    > ${INGRESS_BALANCER_SERVICEMONITOR}

  EMPTY_LIST="true"
  for INGRESS_NODE_IP in ${INGRESS_NODES_IPS[@]}; do
    echo "IP: ${INGRESS_NODE_IP}"
    EMPTY_LIST="false"
    # Duplicate the NODE_ADDRESS_ITEM template line and replace the IP in one of the copies
    sed -i -e "s/^\(\s*\)- {{NODE_ADDRESS_ITEM}}\s*$/\1- ip: ${INGRESS_NODE_IP}\n&/g" ${INGRESS_BALANCER_SERVICEMONITOR}
  done

  # If address list is empty, set it to []
  if [[ "${EMPTY_LIST}" == "true" ]]; then
    sed -i -e "s/^\(\s*\)- addresses:\s*$/- addresses: []/g" ${INGRESS_BALANCER_SERVICEMONITOR}
  fi

  # Remove NODE_ADDRESS_ITEM  template line, if any left
  sed -i -e "/^.*{{NODE_ADDRESS_ITEM}}.*$/d" ${INGRESS_BALANCER_SERVICEMONITOR}

  cat ${INGRESS_BALANCER_SERVICEMONITOR}
fi

if [ "${APISERVER_INTERNAL_BALANCING}" = "true" ] ; then
  APISERVER_BALANCER_SERVICEMONITOR="servicemonitor/apiserver-internal-balancer-servicemonitor.yaml"

  sed -e "s?{{INTERNAL_BALANCER_NAME}}?apiserver?g" \
    ${INTERNAL_BALANCER_SERVICEMONITOR_TEMPLATE} \
    > ${APISERVER_BALANCER_SERVICEMONITOR}

  EMPTY_LIST="true"
  for MASTER_NODE_IP in ${MASTERS_IPS[@]}; do
    echo "IP: ${MASTER_NODE_IP}"
    EMPTY_LIST="false"
    # Duplicate the NODE_ADDRESS_ITEM template line and replace the IP in one of the copies
    sed -i -e "s/^\(\s*\)- {{NODE_ADDRESS_ITEM}}\s*$/\1- ip: ${MASTER_NODE_IP}\n&/g" ${APISERVER_BALANCER_SERVICEMONITOR}
  done

  # If address list is empty, set it to []
  if [[ "${EMPTY_LIST}" == "true" ]]; then
    sed -i -e "s/^\(\s*\)- addresses:\s*$/- addresses: []/g" ${APISERVER_BALANCER_SERVICEMONITOR}
  fi

  # Remove NODE_ADDRESS_ITEM  template line, if any left
  sed -i -e "/^.*{{NODE_ADDRESS_ITEM}}.*$/d" ${APISERVER_BALANCER_SERVICEMONITOR}

  cat ${APISERVER_BALANCER_SERVICEMONITOR}
fi


# Create all configured ServiceMonitors
kubectl create -f servicemonitor/
