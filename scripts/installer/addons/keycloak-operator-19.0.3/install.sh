#!/bin/bash

# Prepare YAML files with correct configuration
sed -i \
  -e "s?{{KEYCLOAK_ADMISSION_CLIENT_SECRET}}?${KEYCLOAK_ADMISSION_CLIENT_SECRET}?g" \
  -e "s?{{KEYCLOAK_GRAFANA_CLIENT_SECRET}}?${KEYCLOAK_GRAFANA_CLIENT_SECRET}?g" \
  -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" \
  -e "s?{{REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" \
  -e "s?{{ADMISSION_ADMIN_PASSWORD}}?${ADMISSION_ADMIN_PASSWORD}?g" \
  -e "s?{{ADMISSION_DEVEL_PASSWORD}}?${ADMISSION_DEVEL_PASSWORD}?g" \
  -e "s?{{GRAFANA_VIEWER_USERNAME}}?${GRAFANA_VIEWER_USERNAME}?g" \
  -e "s?{{GRAFANA_VIEWER_PASSWORD}}?${GRAFANA_VIEWER_PASSWORD}?g" \
  -e "s?{{KUKUCERT_SECRET_NAME}}?${KUKUCERT_SECRET_NAME}?g" \
  -e "s?{{KUINBOUND_MINTLSVERSION}}?${KUINBOUND_MINTLSVERSION}?g"  \
  -e "s?{{KUINBOUND_MAXTLSVERSION}}?${KUINBOUND_MAXTLSVERSION}?g"  \
  -e "s?{{KUINBOUND_CIPHERSUITETLS}}?${KUINBOUND_CIPHERSUITETLS}?g"  \
  -e "s?{{KUINBOUND_ECDHCURVESTLS}}?${KUINBOUND_ECDHCURVESTLS}?g"  \
  *.yaml


# Clone Keycloak Operator repository
rm -rf keycloak-operator
git clone https://github.com/keycloak/keycloak-operator
cd keycloak-operator
git checkout ${KEYCLOAK_VERSION}
cd ..

# Create Keycloak namespace
kubectl apply -f namespace-keycloak.yaml

# Configure Ingress ASAP to minimize DNS propagation times
echo
echo "Configuring Ingress for Keycloak..."
echo
# If IPFiltering feature is enabled and a list of enabled IP/CIDR has been
# defined for keycloak, add the AllowedIP section to the keycloak mapping
add_allowed_ip_to_mapping $INSTALL_IPFILTERING ingress.yaml $KEYCLOAK_ALLOWEDIP 
kubectl apply -f ingress.yaml

# Configure RBAC for allowing monitoring by Prometheus in 'keycloak' namespace
echo
echo "Configuring RBAC for allowing monitoring by Prometheus in namespace..."
echo
kubectl apply -f keycloak-monitoring-rbac.yaml

echo
echo "Creating Keycloak Operator CRDs and RBAC..."
echo
# Create Keycloak Operator CRDs
kubectl apply -f keycloak-operator/deploy/crds/

# Create Keycloak Operator RBAC
kubectl -n keycloak apply -f keycloak-operator/deploy/role.yaml
kubectl -n keycloak apply -f keycloak-operator/deploy/role_binding.yaml
kubectl -n keycloak apply -f keycloak-operator/deploy/service_account.yaml


# Deploy the Keycloak Operator
echo
echo "Deploying Keycloak Operator"
echo
# Customize Operator manifest
# - Set fixed Keycloak images versions
# - Set Operator Pod resource requests and limits
ORIGINAL_YAML="keycloak-operator/deploy/operator.yaml"
CUSTOML_YAML="operator.yaml"

# Set a fixed specific of the Keycloak Operator image
KEYCLOAK_OPERATOR_IMAGE="quay.io/keycloak/keycloak-operator:${KEYCLOAK_VERSION}-legacy"
# Set a fixed specific of the Keycloak image
KEYCLOAK_IMAGE="quay.io/keycloak/keycloak:${KEYCLOAK_VERSION}-legacy"
# Set a fixed specific of the Keycloak Init Container image
KEYCLOAK_INIT_IMAGE="quay.io/keycloak/keycloak-init-container:${KEYCLOAK_VERSION}-legacy"


# Configuration of Keycloak-Operator deployment
cat ${ORIGINAL_YAML} \
  | yq r -j - \
  | jq ".spec.template.spec.containers[0].env += [ { name: \"RELATED_IMAGE_KEYCLOAK\", value: \"${KEYCLOAK_IMAGE}\" } ]" \
  | jq ".spec.template.spec.containers[0].env += [ { name: \"RELATED_IMAGE_KEYCLOAK_INIT_CONTAINER\", value: \"${KEYCLOAK_INIT_IMAGE}\" } ]" \
  | jq ".spec.template.spec.containers[0].image = \"${KEYCLOAK_OPERATOR_IMAGE}\"" \
  | jq '.spec.template.spec.containers[0].resources = {requests:{cpu:"150m",memory:"100Mi"},limits:{cpu:"250m",memory:"100Mi"}}' \
  | yq r --prettyPrint - \
  > ${CUSTOML_YAML}

# Deploy Keycloak Operator
kubectl -n keycloak apply -f ${CUSTOML_YAML}

echo "Waiting for Keycloak Operator to be ready..."
while true ; do
  kubectl -n keycloak get pods -l name=keycloak-operator
  IS_READY="$(
    kubectl -n keycloak get pods \
      -l name=keycloak-operator \
      -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}'
  )"
  if [[ "${IS_READY}" == "True" ]]; then
    echo
    echo "Keycloak Operator is ready!"
    echo
    break
  else
    echo
    echo "Waiting for Keycloak Operator to be ready..."
    echo
    sleep 5
  fi
done


# Create a specially named secret (<credential-<keycloak_CR_name>) with
# the custom master credentials.
echo
echo "Creating 'credential-keycloak' Secret with custom credentials."
echo
ENCODED_USERNAME="$( echo -n ${KEYCLOAK_ADMIN_USERNAME} | base64 )"
ENCODED_PASSWORD="$( echo -n ${KEYCLOAK_ADMIN_PASSWORD} | base64 )"
sed -i \
  -e "s?{{ENCODED_USERNAME}}?${ENCODED_USERNAME}?g" \
  -e "s?{{ENCODED_PASSWORD}}?${ENCODED_PASSWORD}?g" \
  keycloak-master-credentials.yaml
kubectl apply -f keycloak-master-credentials.yaml


# Configure and deploy a Keycloak instance
echo
echo "Creating a Keycloak deployment resource"
echo
sed -i \
  -e "s?{{KEYCLOAK_MEM_REQUEST}}?${KEYCLOAK_MEM_REQUEST}?g" \
  -e "s?{{KEYCLOAK_CPU_REQUEST}}?${KEYCLOAK_CPU_REQUEST}?g" \
  -e "s?{{KEYCLOAK_MEM_LIMIT}}?${KEYCLOAK_MEM_LIMIT}?g" \
  -e "s?{{KEYCLOAK_CPU_LIMIT}}?${KEYCLOAK_CPU_LIMIT}?g" \
  keycloak.yaml
kubectl -n keycloak apply -f keycloak.yaml
sleep 10

echo "Waiting for Keycloak Pod to be ready..."
while true ; do
  kubectl -n keycloak get pods keycloak-0
  IS_READY="$(
    kubectl -n keycloak get pods keycloak-0  \
      -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}'
  )"
  if [[ "${IS_READY}" == "True" ]]; then
    echo
    echo "Keycloak Pod is ready!"
    echo
    break
  else
    echo
    echo "Waiting for Keycloak Pod to be ready..."
    echo
    sleep 5
  fi
done


### # Update automatically created credentials secret with custom username and password
### echo
### echo "Waiting for Secret 'credential-keycloak' to be created by operator (some error traces are normal)."
### echo
### while true ; do
###   kubectl -n keycloak get secret credential-keycloak && break
###   sleep 5
### done
### 
### echo
### echo "Detected Secret 'credential-keycloak'. Patching it with custom credentials."
### echo
### ENCODED_USERNAME="$( echo -n ${KEYCLOAK_ADMIN_USERNAME} | base64 )"
### ENCODED_PASSWORD="$( echo -n ${KEYCLOAK_ADMIN_PASSWORD} | base64 )"
### 
### kubectl -n keycloak patch secrets credential-keycloak \
###   --type='json' \
###   -p='[{"op": "replace", "path":"/data/ADMIN_USERNAME", "value":"'${ENCODED_USERNAME}'"}, {"op": "replace", "path":"/data/ADMIN_PASSWORD", "value":"'${ENCODED_PASSWORD}'"}]'
### 
### sleep 5

echo
echo "Configuring Keycloak realm, client and users..."
echo

kubectl -n keycloak apply -f realm-kumoricluster.yaml
echo "Waiting for KumoriCluster Realm to be ready..."
while true ; do
  STATUS_READY="$(kubectl -n keycloak get keycloakrealms kumori-cluster -ojson | jq -r .status.ready)"
  STATUS_PHASE="$(kubectl -n keycloak get keycloakrealms kumori-cluster -ojson | jq -r .status.phase)"
  STATUS_MESSAGE="$(kubectl -n keycloak get keycloakrealms kumori-cluster -ojson | jq -r .status.message)"
  echo "Current status: { phase: \"${STATUS_PHASE}\", message: \"${STATUS_MESSAGE}\", ready: \"${STATUS_READY}\" }"
  # if [[ "${STATUS_PHASE}" == "reconciled" && "${STATUS_MESSAGE}" == "" ]]  ; then
  if [[ "${STATUS_READY}" == "true" ]]  ; then
    echo
    echo "KumoriCluster Realm is ready!"
    echo
    break
  else
    echo
    echo "Waiting for KumoriCluster Realm to be ready..."
    echo
    sleep 5
  fi
done
sleep 5


kubectl -n keycloak apply -f client-admission.yaml
echo "Waiting for Admission Keycloak client to be ready..."
while true ; do
  STATUS_READY="$(kubectl -n keycloak get keycloakclients admission -ojson | jq -r .status.ready)"
  STATUS_PHASE="$(kubectl -n keycloak get keycloakclients admission -ojson | jq -r .status.phase)"
  STATUS_MESSAGE="$(kubectl -n keycloak get keycloakclients admission -ojson | jq -r .status.message)"
  echo "Current status: { phase: \"${STATUS_PHASE}\", message: \"${STATUS_MESSAGE}\" }"
  if [[ "${STATUS_READY}" == "true" || ( "${STATUS_PHASE}" == "reconciled" && "${STATUS_MESSAGE}" == "" ) ]]  ; then
    echo
    echo "Admission Keycloak client is ready!"
    echo
    break
  else
    echo
    echo "Waiting for Admission Keycloak client to be ready..."
    echo
    sleep 5
  fi
done
sleep 5


kubectl -n keycloak apply -f client-grafana.yaml
echo "Waiting for Grafana Keycloak client to be ready..."
while true ; do
  STATUS_READY="$(kubectl -n keycloak get keycloakclients grafana -ojson | jq -r .status.ready)"
  STATUS_PHASE="$(kubectl -n keycloak get keycloakclients grafana -ojson | jq -r .status.phase)"
  STATUS_MESSAGE="$(kubectl -n keycloak get keycloakclients grafana -ojson | jq -r .status.message)"
  echo "Current status: { phase: \"${STATUS_PHASE}\", message: \"${STATUS_MESSAGE}\" }"
  if [[ "${STATUS_READY}" == "true" || ( "${STATUS_PHASE}" == "reconciled" && "${STATUS_MESSAGE}" == "" ) ]]  ; then
    echo
    echo "Grafana Keycloak client is ready!"
    echo
    break
  else
    echo
    echo "Waiting for Grafana Keycloak client to be ready..."
    echo
    sleep 5
  fi
done
sleep 5


kubectl -n keycloak apply -f user-admin.yaml
sleep 5
kubectl -n keycloak apply -f user-devel.yaml
sleep 5
kubectl -n keycloak apply -f user-grafana-viewer.yaml

echo
echo "Done."
echo

