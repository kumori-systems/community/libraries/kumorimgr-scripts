#!/bin/bash


if [ "${MANAGED_DNS_PROVIDER}" = "route53" ]; then

  PROVIDER_TYPE="aws"

  # Extract AWS credentials from credentials file
  echo "Extracting credentials from AWS configuration file..."
  AWS_CREDENTIALS_FILE="${SCRIPT_DIR}/awsConfiguration/credentials"

  AWS_ACCESS_KEY_ID=$(grep "aws_access_key_id" ${AWS_CREDENTIALS_FILE}|cut -d'=' -f2 | tr -d "[:space:]")
  AWS_SECRET_ACCESS_KEY=$(grep "aws_secret_access_key" ${AWS_CREDENTIALS_FILE}|cut -d'=' -f2 | tr -d "[:space:]")

  echo " - Extracted AWS_ACCESS_KEY_ID     : ${AWS_ACCESS_KEY_ID}"
  echo " - Extracted AWS_SECRET_ACCESS_KEY : <hidden>  (length: ${#AWS_SECRET_ACCESS_KEY})"

  # Generate ExternalDNS deployment manifest configured for AWS Route53
  MANIFEST_TEMPLATE_FILE="external-dns_Route53_template.yaml"

  sed \
    -e "s?{{PROVIDER_TYPE}}?${PROVIDER_TYPE}?g" \
    -e "s?{{EXTERNALDNS_AWS_ACCESS_KEY}}?${AWS_ACCESS_KEY_ID}?g" \
    -e "s?{{EXTERNALDNS_AWS_SECRET_KEY}}?${AWS_SECRET_ACCESS_KEY}?g" \
    -e "s?{{EXTERNALDNS_VERSION}}?${EXTERNALDNS_VERSION}?g" \
    -e "s?{{EXTERNALDNS_OWNER_ID}}?${EXTERNALDNS_OWNER_ID}?g" \
    -e "s?{{EXTERNALDNS_INTERVAL}}?${EXTERNALDNS_INTERVAL}?g" \
    -e "s?{{EXTERNALDNS_ROUTE53_AWSBATCHCHANGESIZE}}?${EXTERNALDNS_ROUTE53_AWSBATCHCHANGESIZE}?g" \
    -e "s?{{EXTERNALDNS_ROUTE53_AWSBATCHCHANGEINTERVAL}}?${EXTERNALDNS_ROUTE53_AWSBATCHCHANGEINTERVAL}?g" \
    -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" \
    -e "s?{{REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" \
    -e "s?{{RANDOMS_REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" \
    ${MANIFEST_TEMPLATE_FILE} \
    > external-dns-deployment.yaml

elif [ "${MANAGED_DNS_PROVIDER}" = "ovh" ]; then

  PROVIDER_TYPE="ovh"

  # Extract OVH credentials from credentials file
  echo "Extracting credentials from OVH configuration file..."
  OVH_CREDENTIALS_FILE="${SCRIPT_DIR}/ovhConfiguration/ovh-dns-credentials.ini"

  OVH_API_RATE_LIMIT="20"  # Max request per second
  OVH_ENDPOINT="$(grep "dns_ovh_endpoint" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
  OVH_APPLICATION_KEY="$(grep "dns_ovh_application_key" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
  OVH_APPLICATION_SECRET="$(grep "dns_ovh_application_secret" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"
  OVH_CONSUMER_KEY="$(grep "dns_ovh_consumer_key" ${OVH_CREDENTIALS_FILE} | cut -d'=' -f2 | tr -d "[:space:]")"

  echo " - Extracted OVH_ENDPOINT           : ${OVH_ENDPOINT}"
  echo " - Extracted OVH_APPLICATION_KEY    : ${OVH_APPLICATION_KEY}"
  echo " - Extracted OVH_APPLICATION_SECRET : <hidden>  (length: ${#OVH_APPLICATION_SECRET})"
  echo " - Extracted OVH_CONSUMER_KEY       : <hidden>  (length: ${#OVH_CONSUMER_KEY})"

  # OVH has DNS zones that may not match with the reference domain.
  # We must determine the DNS Zone that corresponds to the Reference Domain to
  # configure it as the DNS zone filter in ExternalDNS.

  # Define OVH API helper base command based on Docker image
  OVH_CLI="docker run --rm \
    --name kovhcli \
    --env OVH_ENDPOINT="${OVH_ENDPOINT}" \
    --env OVH_APPLICATION_KEY="${OVH_APPLICATION_KEY}" \
    --env OVH_APPLICATION_SECRET="${OVH_APPLICATION_SECRET}" \
    --env OVH_CONSUMER_KEY="${OVH_CONSUMER_KEY}" \
    ${MANAGED_DNS_OVH_CLI_IMAGE}"

  # Validate Reference Domain and DNS Zone
  RESULT=$(${OVH_CLI} get-refdomain-zone ${REFERENCE_DOMAIN})
  EXIT_CODE="$?"
  if [ "${EXIT_CODE}" != "0" ]; then
    echo
    echo -e $COL_RED_BOLD"Error getting the DNS Zone associated to the Reference Domain. This will require manual corrective actions."
    echo
    OVH_MANAGED_ZONE="kumori-undetermined"   # Set to something that won't math any zone
  else
    OVH_MANAGED_ZONE="${RESULT}"
    echo "Reference Domain DNS Zone: ${OVH_MANAGED_ZONE}"
  fi

  # Generate ExternalDNS deployment manifest configured for OVH DNS
  MANIFEST_TEMPLATE_FILE="external-dns_OVH_template.yaml"

  # Prepare ExternalDNS manifests
  sed \
    -e "s?{{PROVIDER_TYPE}}?${PROVIDER_TYPE}?g" \
    -e "s?{{EXTERNALDNS_OVH_ENDPOINT}}?${OVH_ENDPOINT}?g" \
    -e "s?{{EXTERNALDNS_OVH_APPLICATION_KEY}}?${OVH_APPLICATION_KEY}?g" \
    -e "s?{{EXTERNALDNS_OVH_APPLICATION_SECRET}}?${OVH_APPLICATION_SECRET}?g" \
    -e "s?{{EXTERNALDNS_OVH_CONSUMER_KEY}}?${OVH_CONSUMER_KEY}?g" \
    -e "s?{{EXTERNALDNS_OVH_API_RATE_LIMIT}}?${OVH_API_RATE_LIMIT}?g" \
    -e "s?{{EXTERNALDNS_VERSION}}?${EXTERNALDNS_VERSION}?g" \
    -e "s?{{EXTERNALDNS_OWNER_ID}}?${EXTERNALDNS_OWNER_ID}?g" \
    -e "s?{{EXTERNALDNS_INTERVAL}}?${EXTERNALDNS_INTERVAL}?g" \
    -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" \
    -e "s?{{REFERENCE_DOMAIN}}?${OVH_MANAGED_ZONE}?g" \
    -e "s?{{RANDOMS_REFERENCE_DOMAIN}}?${OVH_MANAGED_ZONE}?g" \
    ${MANIFEST_TEMPLATE_FILE} \
    > external-dns-deployment.yaml

else
  echo
  echo -e $COL_YELLOW"DNS Provider ${MANAGED_DNS_PROVIDER} not supported. Skipping ExternalDNS installation."$COL_DEFAULT
  echo
  # Return to calling script without doing anything
  return
fi



# Create ExternalDNS objects
kubectl apply -f external-dns-RBAC.yaml
kubectl apply -f external-dns-deployment.yaml
