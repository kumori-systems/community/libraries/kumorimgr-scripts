#!/bin/bash

echo "Installing ClusterAPI Core v${CLUSTERAPI_VERSION} ..."
kubectl apply -f manifests/cluster-api-v${CLUSTERAPI_VERSION}.yaml

echo "Installing ClusterAPI Bootstrap provider: kubeadm v${CLUSTERAPI_VERSION} ..."
kubectl apply -f manifests/bootstrap-kubeadm-v${CLUSTERAPI_VERSION}.yaml

echo "Installing ClusterAPI ControlPlane provider: kubeadm v${CLUSTERAPI_VERSION} ..."
kubectl apply -f manifests/control-plane-kubeadm-v${CLUSTERAPI_VERSION}.yaml

if [ "${INSTALL_CLUSTERAPI_INFRASTRUCTURE_PROVIDER_OPENSTACK}" = "true" ]; then
  echo "Installing ClusterAPI Infrastructure provider: openstack v${CLUSTERAPI_VERSION} ..."
  kubectl apply -f manifests/infrastructure-openstack-v${CLUSTERAPI_OPENSTACK_PROVIDER_VERSION}.yaml
fi