# ------------------------------------------------------------------------------
# DaemonSet
# One Ambassador pod is deployed in each node, opening the following pod/host ports:
# - 8443/443 (https listener)
# - 8877/8080 (Ambassador admin API)
# - Configured TCP ports
# Resource limits/requests should be adjusted.
# ------------------------------------------------------------------------------
# - Taken from: https://github.com/emissary-ingress/emissary/blob/v1.13.10/manifests/ambassador/ambassador.yaml
# - Changes by Kumori:
#   - Moved deployment stuff to a separate YAML file (this one)
#   - Type changed from 'deployment' to 'daemonset' (and remove replicas)
#   - Replaced pod affinity with node selector (only ingress nodes)
#   - Added 'namespace: kumori' where necessary
#   - Added some configuration variables not set in the original:
#     - AMBASSADOR_SINGLE_NAMESPACE
#     - AMBASSADOR_DRAIN_TIME
#     - AMBASSADOR_FAST_RECONFIGURE
#     - AES_LOG_LEVEL
#   - Set our custom port list
#   - Increased limits
#   - Added label 'type: ingress'
#   - Set 'dnsPolicy: ClusterFirstWithHostNet'
#   - Set 'updateStrategy: RollingUpdate'
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: ambassador
  namespace: {{INGRESS_NAMESPACE}}
spec:
  selector:
    matchLabels:
      service: ambassador
  template:
    metadata:
      annotations:
        consul.hashicorp.com/connect-inject: 'false'
        sidecar.istio.io/inject: 'false'
      labels:
        service: ambassador
        app.kubernetes.io/managed-by: getambassador.io
        type: ingress
    spec:
      containers:
      - name: ambassador
        image: docker.io/datawire/ambassador:{{AMBASSADOR_VERSION}}
        imagePullPolicy: IfNotPresent
        env:
        - name: AMBASSADOR_SINGLE_NAMESPACE
        - name: AMBASSADOR_DRAIN_TIME
          value: "600"
        - name: AMBASSADOR_FAST_RECONFIGURE
          value: "true"
        - name: AES_LOG_LEVEL
          value: info
        # Disable Envoy-configuration rate-limiting based on memory pressure.
        # Defaults to "false".
        - name: AMBASSADOR_AMBEX_NO_RATELIMIT
          value: "true"
        # Number of Envoy-config snapshots saved. "0" to disable.
        # Defaults to "30"
        - name: AMBASSADOR_AMBEX_SNAPSHOT_COUNT
          value: "30"
        - name: AMBASSADOR_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: HOST_IP
          valueFrom:
            fieldRef:
              fieldPath: status.hostIP
        ports:
        - name: http
          containerPort: 8080
          hostPort: 80
        - name: https
          containerPort: 8443
          hostPort: {{INGRESS_INTERNAL_PORT}}
        - name: admin
          containerPort: 8877
          hostPort: 8080
        livenessProbe:
          httpGet:
            path: /ambassador/v0/check_alive
            port: admin
          failureThreshold: 3
          initialDelaySeconds: 30
          periodSeconds: 3
        readinessProbe:
          httpGet:
            path: /ambassador/v0/check_ready
            port: admin
          failureThreshold: 3
          initialDelaySeconds: 30
          periodSeconds: 3
        resources:
          limits:
            cpu: {{AMBASSADOR_CPU_LIMIT}}
            memory: {{AMBASSADOR_MEM_LIMIT}}
          requests:
            cpu: {{AMBASSADOR_CPU_REQUEST}}
            memory: {{AMBASSADOR_MEM_REQUEST}}
        securityContext:
          allowPrivilegeEscalation: false
        volumeMounts:
        - name: ambassador-pod-info
          mountPath: /tmp/ambassador-pod-info
          readOnly: true
      dnsPolicy: ClusterFirstWithHostNet
      nodeSelector:
        kumori/ingress: "true"
      restartPolicy: Always
      securityContext:
        runAsUser: 8888
      serviceAccountName: ambassador
      # This was set to 0 in the original YAMLs, but we were setting 30.
      # terminationGracePeriodSeconds: 30
      terminationGracePeriodSeconds: 0 
      volumes:
      - downwardAPI:
          items:
          - fieldRef:
              fieldPath: metadata.labels
            path: labels
        name: ambassador-pod-info
  updateStrategy:
    type: RollingUpdate
