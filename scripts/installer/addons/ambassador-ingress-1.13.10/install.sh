#!/bin/bash

# Build OutOfService full docker image name and crete credentials secret if necessary
if [ -z "${OUTOFSERVICE_IMAGE}" ]; then
  # If no custom image is provided, use default Kumori OutOfService image
  OUTOFSERVICE_FULL_DOCKER_IMAGE="${KUMORI_IMAGES_REGISTRY}/${KUMORI_OUTOFSERVICE_IMAGE}:v${KUMORI_OUTOFSERVICE_VERSION}"
  OUTOFSERVICE_USERNAME=""
else
  if [ -n "${OUTOFSERVICE_REGISTRY}" ]; then
    OUTOFSERVICE_FULL_DOCKER_IMAGE="${OUTOFSERVICE_REGISTRY}/${OUTOFSERVICE_IMAGE}"
  else
    OUTOFSERVICE_FULL_DOCKER_IMAGE="${OUTOFSERVICE_IMAGE}"
  fi
fi

# Prepare OutOfService deployment manifest
sed -i \
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" \
  -e "s?{{OUTOFSERVICE_IMAGE}}?${OUTOFSERVICE_FULL_DOCKER_IMAGE}?g" \
  outofservice.yaml

# Daemonset template. It depends on the IPFeature feature is enabled or not
# The ambassador DaemonSet YAML file is created from templates to start with a 
# clean file, since it is modified with YQ/JQ to add elements.
DAEMONSETFILE="ambassador-daemonset-hostport.yaml"
if [ "${INSTALL_IPFILTERING}" == "true" ]; then
  rm -f ambassador-daemonset-hostport-ipfiltering.yaml
  cp ambassador-daemonset-hostport-ipfiltering.yaml_template ambassador-daemonset-hostport-ipfiltering.yaml
  DAEMONSETFILE="ambassador-daemonset-hostport-ipfiltering.yaml"
else
  rm -f ambassador-daemonset-hostport.yaml
  cp ambassador-daemonset-hostport.yaml_template ambassador-daemonset-hostport.yaml
  DAEMONSETFILE="ambassador-daemonset-hostport.yaml"
fi

# Replace configuration variables in templates
if [ "${INGRESS_INTERNAL_BALANCING}" == "true" ]; then
  USE_PROXY_PROTOCOL="true"
else
  USE_PROXY_PROTOCOL="false"
fi
sed -i \
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" \
  -e "s?{{USE_PROXY_PROTOCOL}}?${USE_PROXY_PROTOCOL}?g" \
  ambassador-config-module.yaml
sed -i \
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" \
  -e "s?{{AMBASSADOR_VERSION}}?${AMBASSADOR_VERSION}?g" \
  -e "s?{{INGRESS_INTERNAL_PORT}}?${INGRESS_INTERNAL_PORT}?g" \
  -e "s?{{AMBASSADOR_CPU_REQUEST}}?${AMBASSADOR_CPU_REQUEST}?g" \
  -e "s?{{AMBASSADOR_CPU_LIMIT}}?${AMBASSADOR_CPU_LIMIT}?g" \
  -e "s?{{AMBASSADOR_MEM_REQUEST}}?${AMBASSADOR_MEM_REQUEST}?g" \
  -e "s?{{AMBASSADOR_MEM_LIMIT}}?${AMBASSADOR_MEM_LIMIT}?g" \
  ${DAEMONSETFILE}
sed -i -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" ambassador-rbac.yaml
sed -i -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" ambassador-service.yaml
sed -i -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" ingress-namespace.yaml

# If IPFiltering is enabled, replace configuration variables
if [ "${INSTALL_IPFILTERING}" == "true" ]; then
  # Determine the appropriate image pull Secret property for Controller manifests
  KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET="kumori-enterprise-registry-credentials"
  if [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ] && [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
    CONTROLLERS_IMAGE_PULL_SECRETS="[ { name: ${KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET} } ]"
  else
    CONTROLLERS_IMAGE_PULL_SECRETS="[]"
  fi

  sed -i \
    -e "s?{{KUMORI_IPFILTERING_VERSION}}?${KUMORI_IPFILTERING_VERSION}?g" \
    -e "s?{{IPFILTERING_CPU_REQUEST}}?${IPFILTERING_CPU_REQUEST}?g" \
    -e "s?{{IPFILTERING_CPU_LIMIT}}?${IPFILTERING_CPU_LIMIT}?g" \
    -e "s?{{IPFILTERING_MEM_REQUEST}}?${IPFILTERING_MEM_REQUEST}?g" \
    -e "s?{{IPFILTERING_MEM_LIMIT}}?${IPFILTERING_MEM_LIMIT}?g" \
    -e "s?{{KUMORI_IMAGES_REGISTRY}}?${KUMORI_IMAGES_REGISTRY}?g" \
    -e "s?{{CONTROLLERS_IMAGE_PULL_SECRETS}}?${CONTROLLERS_IMAGE_PULL_SECRETS}?g" \
    ${DAEMONSETFILE}

  sed -i \
    -e "s?{{IPFILTERING_REJECTIFFAILURE}}?${IPFILTERING_REJECTIFFAILURE}?g"\
    -e "s?{{IPFILTERING_TIMEOUTMSEC}}?${IPFILTERING_TIMEOUTMSEC}?g"\
    -e "s?{{IPFILTERING_LOGLEVEL}}?${IPFILTERING_LOGLEVEL}?g"\
    -e "s?{{IPFILTERING_MAXLISTSIZE}}?${IPFILTERING_MAXLISTSIZE}?g"\
    ambassador-ipfiltering-stuff.yaml
fi

# Create ingress namespace
kubectl apply -f ./ingress-namespace.yaml

# If username and password are set, create a docker credentials Secret
# and add an imagePullSecret to the deployment manifest
if [ -n "${OUTOFSERVICE_USERNAME}" ] && [ -n "${OUTOFSERVICE_PASSWORD}" ]; then
  kubectl -n kumori create secret docker-registry outofservice-docker-credentials \
    --docker-server="${OUTOFSERVICE_REGISTRY}" \
    --docker-username="${OUTOFSERVICE_USERNAME}" \
    --docker-password="${OUTOFSERVICE_PASSWORD}"

  # Add imagePullSecret to deployment manifest
  sed -i \
    -e "s?{{IMAGE_PULL_SECRET}}?- name: outofservice-docker-credentials?g" \
    outofservice.yaml
elif [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ] && [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
  # Add imagePullSecret to deployment manifest with Kumori controllers credentials secret
  KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET="kumori-enterprise-registry-credentials"
  sed -i \
    -e "s?{{IMAGE_PULL_SECRET}}?- name: ${KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET}?g" \
    outofservice.yaml
else
  # Remove imagePullSecret placeholder from deployment manifest and set it to []
  sed -i \
    -e "s|{{IMAGE_PULL_SECRET}}||g" \
    -e "s|imagePullSecrets:[[:space:]]*$|imagePullSecrets: []|g" \
    outofservice.yaml
fi

echo
echo "OUTOFSERVICE DEPLOYMENT MANIFEST:"
cat outofservice.yaml
echo


# Configure Ingress TCP ports (Ambassador "listens" on internal ports)
if [ -n "${INGRESS_TCP_PORTS}" ]; then
  # Add custom extra ports to listen to in Ambassador configuration
  TMP_FILE="ambassador-daemonset-hostport_TMP.yaml"

  echo "Adding custom ports to Ambassador..."

  # for PORT in ${INGRESS_TCP_PORTS[@]}; do
  for i in "${!INGRESS_TCP_PORTS[@]}"; do
    INTERNAL_PORT="${INGRESS_TCP_INTERNAL_PORTS[$i]}"
    EXTERNAL_PORT="${INGRESS_TCP_PORTS[$i]}"
    cat ${DAEMONSETFILE} \
      | yq r -j - \
      | jq ".spec.template.spec.containers[0].ports += [ { name: \"tcp${EXTERNAL_PORT}\", containerPort: ${INTERNAL_PORT} , hostPort: ${INTERNAL_PORT} } ]" \
      | yq r --prettyPrint - \
      > ${TMP_FILE}
    mv ${TMP_FILE} ${DAEMONSETFILE}
  done
  echo
  echo "Ambassador configuration with custom ports:"
  cat "$DAEMONSETFILE"
  echo
fi

# Create Ambassador CRDs
kubectl apply -f ./ambassador-crd.yaml
# Create Ambassador RBAC
kubectl apply -f ./ambassador-rbac.yaml

# Wait for Ambassador Module CRD to exist
echo
echo "Waiting for CRD 'modules.getambassador.io' to exist..."
until [ "$(kubectl get crd | grep 'modules.getambassador.io')" != "" ]; do
  date
  echo "  Not yet, wait a little bit more..."
  sleep 1
done
echo "CRD 'modules.getambassador.io' exists!"
echo

# Create Ambassador system-wide configuration
kubectl apply -f ./ambassador-config-module.yaml
# Create Ambassador DaemonSet (with or without IPFiltering)
kubectl apply -f ./${DAEMONSETFILE}
# Create Ambassador admin Service
kubectl apply -f ./ambassador-service.yaml
# Create objects related to IP Filtering
if [ "${INSTALL_IPFILTERING}" == "true" ]; then
  kubectl apply -f ./ambassador-ipfiltering-stuff.yaml
fi

# Create Kumori OutOfService deployment and service
# OutOfService is a classic "Error 404 service unavailable" responder
kubectl apply -f ./outofservice.yaml
