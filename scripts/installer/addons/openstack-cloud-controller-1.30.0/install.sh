#!/bin/bash


# KUBECTL_CMD="kubectl --kubeconfig ${SCRIPT_DIR}/kubeconfig-capoclustertest"
KUBECTL_CMD="kubectl"

# Openstack tenant clouds.yaml file and key inside the file
OPENSTACK_CLOUDS_YAML_FILE="${SCRIPT_DIR}/openstack-cloud-credentials/clouds.yaml"
OPENSTACK_CLOUDS_YAML_KEY="${OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_KEY}"
echo "Preparing Cloud configuration secret..."
echo
echo "Contents of clouds.yaml file ${OPENSTACK_CLOUDS_YAML_FILE} :"
cat ${OPENSTACK_CLOUDS_YAML_FILE}
echo
echo "Looking for key: ${OPENSTACK_CLOUDS_YAML_KEY}"
echo

# Temporary file. Name MUST be 'cloud.conf' (expected by Cloud Controller)
CLOUD_CONF_FILE="cloud.conf"


# Generate cloud.conf file from Openstack clouds.yaml file
./capo-create-cloud-conf.sh ${OPENSTACK_CLOUDS_YAML_FILE} ${OPENSTACK_CLOUDS_YAML_KEY} \
  > ${CLOUD_CONF_FILE}

# Create Kubernetes secrets containing the cloud.conf file
# echo "${KUBECTL_CMD} \
#   create secret \
#   -n kube-system \
#   generic \
#   cloud-config \
#   --from-file=${CLOUD_CONF_FILE}"

${KUBECTL_CMD} \
  create secret \
  -n kube-system \
  generic \
  cloud-config \
  --from-file=${CLOUD_CONF_FILE}

# Delete temporary file
rm ${CLOUD_CONF_FILE}

# Deploy Openstack Cloud Controller (external)
${KUBECTL_CMD} apply -f cloud-controller-manager-role-bindings.yaml
${KUBECTL_CMD} apply -f cloud-controller-manager-roles.yaml
${KUBECTL_CMD} apply -f openstack-cloud-controller-manager-ds.yaml
