VERSION: v1.30.0

Download links:

wget https://raw.githubusercontent.com/kubernetes/cloud-provider-openstack/v1.30.0/manifests/controller-manager/openstack-cloud-controller-manager-ds.yaml
wget https://raw.githubusercontent.com/kubernetes/cloud-provider-openstack/v1.30.0/manifests/controller-manager/cloud-controller-manager-roles.yaml
wget https://raw.githubusercontent.com/kubernetes/cloud-provider-openstack/v1.30.0/manifests/controller-manager/cloud-controller-manager-role-bindings.yaml
