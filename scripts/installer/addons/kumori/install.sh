#!/bin/bash

# Create a Secret for the Kumori controllers docker registry credentials
KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET="kumori-enterprise-registry-credentials"
if [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ] && [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
  echo "Creating controller registry credentials secret for user ${KUMORI_IMAGES_REGISTRY_USERNAME} in registry ${KUMORI_IMAGES_REGISTRY}..."
  kubectl -n kumori create secret docker-registry ${KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET} \
    --docker-server=${KUMORI_IMAGES_REGISTRY} \
    --docker-username=${KUMORI_IMAGES_REGISTRY_USERNAME} \
    --docker-password=${KUMORI_IMAGES_REGISTRY_PASSWORD}
else
  echo
  echo "No controller registry credentials provided. Skipped secret creation."
fi
echo


# Create a Cluster Configuration ConfigMap
cp cr/cluster-config-configMap.yaml_template cr/cluster-config-configMap.yaml

KUMORICTL_SUPPORTED_VERSION_LIST=$(IFS=,; echo "${KUMORICTL_SUPPORTED_VERSIONS[*]}")
KUMORIMGR_SUPPORTED_VERSION_LIST=$(IFS=,; echo "${KUMORIMGR_SUPPORTED_VERSIONS[*]}")
echo "Supported CLI versions:"
echo "- KumoriCtl : ${KUMORICTL_SUPPORTED_VERSION_LIST}"
echo "- KumoriMgr : ${KUMORIMGR_SUPPORTED_VERSION_LIST}"

sed \
  -e "s?{{CLUSTER_VERSION}}?${CLUSTER_VERSION}?g" \
  -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" \
  -e "s?{{REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" \
  -e "s?{{INGRESS_DOMAIN}}?${INGRESS_DOMAIN}?g" \
  -e "s?{{KUMORICTL_SUPPORTED_VERSIONS}}?${KUMORICTL_SUPPORTED_VERSION_LIST}?g" \
  -e "s?{{KUMORIMGR_SUPPORTED_VERSIONS}}?${KUMORIMGR_SUPPORTED_VERSION_LIST}?g" \
  -e "s?{{ADMISSION_AUTHENTICATION_TYPE}}?${ADMISSION_AUTHENTICATION_TYPE}?g" \
  cr/cluster-config-configMap.yaml_template \
  > cr/cluster-config-configMap.yaml

# Add volume types configuration
EMPTY_LIST="true"
if [[ "${#VOLUME_TYPE_NAMES[@]}" -gt 0  ]]; then
  EMPTY_LIST="false"
  # Remove "{}" from the volumeTypes line
  sed -i "s|volumeTypes:[[:space:]]*{}[[:space:]]*$|volumeTypes:|g" cr/cluster-config-configMap.yaml
fi

for i in "${!VOLUME_TYPE_NAMES[@]}"; do
    VT_NAME="${VOLUME_TYPE_NAMES[$i]}"
    VT_STORAGECLASS="${VOLUME_TYPE_STORAGECLASSES[$i]}"
    VT_PROPERTIES="${VOLUME_TYPE_PROPERTIES[$i]}"

    # Duplicate the peer template line and replace the IP in one of the copies
    sed -i -e "s/^\(\s*\){{VOLUME_TYPE_PLACEHOLDER}}\s*$/\1${VT_NAME}:\n&/g" cr/cluster-config-configMap.yaml
    sed -i -e "s/^\(\s*\){{VOLUME_TYPE_PLACEHOLDER}}\s*$/\1  storageClass: ${VT_STORAGECLASS}\n&/g" cr/cluster-config-configMap.yaml
    sed -i -e "s/^\(\s*\){{VOLUME_TYPE_PLACEHOLDER}}\s*$/\1  properties: ${VT_PROPERTIES}\n&/g" cr/cluster-config-configMap.yaml
done

# Remove placeholder line
sed -i -e "/^.*{{VOLUME_TYPE_PLACEHOLDER}}.*$/d" cr/cluster-config-configMap.yaml

echo
echo "Cluster cofiguration ConfigMap:"
echo
cat cr/cluster-config-configMap.yaml
echo
echo
kubectl apply -f cr/cluster-config-configMap.yaml
echo

# Determine the appropriate image pull Secret property for Controller manifests
if [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ] && [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
  CONTROLLERS_IMAGE_PULL_SECRETS="[ { name: ${KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET} } ]"
else
  CONTROLLERS_IMAGE_PULL_SECRETS="[]"
fi

# Cluster configuration ConfigMap data
CLUSTER_CONFIGMAP_NAME="cluster-config"
CLUSTER_CONFIGMAP_NAMESPACE="kumori"
CLUSTER_CONFIGMAP_KEY="cluster-config.yaml"

# Some variables related to certs and CAs
#
# Define a default Kumori domain for cluster core KukuElements
CLUSTER_CORE_DOMAIN="cluster.core"
#
# KukuCert for the reference domain: internal name, object name and underlying secret
KUKUCERT_NAME="wildcard-${REFERENCE_DOMAIN//./-}"
KUKUCERT_FULL_NAME="${CLUSTER_CORE_DOMAIN//./-}-cert-${KUKUCERT_NAME}"
KUKUCERT_SECRET_NAME="${KUKUCERT_FULL_NAME}-cert"
#
# KukuCert for the admission domain: internal name, object name and underlying secret
ADMISSION_KUKUCERT_NAME="admission-${REFERENCE_DOMAIN//./-}"
ADMISSION_KUKUCERT_FULL_NAME="${CLUSTER_CORE_DOMAIN//./-}-cert-${ADMISSION_KUKUCERT_NAME}"
ADMISSION_KUKUCERT_SECRET_NAME="${ADMISSION_KUKUCERT_FULL_NAME}-cert"
#
# KukuCA for the root-ca: internal name, object name and underlying secret
ROOTCA_KUKUCA_NAME="rootca"
ROOTCA_KUKUCA_FULL_NAME="${CLUSTER_CORE_DOMAIN//./-}-ca-${ROOTCA_KUKUCA_NAME}"
ROOTCA_KUKUCA_SECRET_NAME="${ROOTCA_KUKUCA_FULL_NAME}-ca"

if [ "${ADMISSION_AUTHENTICATION_TYPE}" == "clientcertificate" ]; then
  ADMISSION_INBOUND_CERT_SECRET=$ADMISSION_KUKUCERT_SECRET_NAME
  ADMISSION_INBOUND_CA_SECRET=$ROOTCA_KUKUCA_SECRET_NAME
else
  ADMISSION_INBOUND_CERT_SECRET=$KUKUCERT_SECRET_NAME
  ADMISSION_INBOUND_CA_SECRET=""
fi

# TBD: about how admission host is prepared: https://gitlab.com/kumori/project-management/-/issues/1315
sed -i -e "s?{{TOPOLOGY_CONTROLLER_VERSION}}?${KUMORI_TOPOLOGY_CONTROLLER_VERSION}?g" controllers/*.yaml
sed -i -e "s?{{KUCONTROLLER_VERSION}}?${KUMORI_KUCONTROLLER_VERSION}?g" controllers/*.yaml
sed -i -e "s?{{KUINBOUND_VERSION}}?${KUMORI_KUINBOUND_VERSION}?g" controllers/*.yaml
sed -i -e "s?{{KUALARM_VERSION}}?${KUMORI_KUALARM_VERSION}?g" controllers/*.yaml
sed -i -e "s?{{VOLUME_CONTROLLER_VERSION}}?${KUMORI_VOLUME_CONTROLLER_VERSION}?g" controllers/*.yaml
sed -i -e "s?{{KUMORI_ADMISSION_VERSION}}?${KUMORI_ADMISSION_VERSION}?g" controllers/*.yaml
sed -i -e "s?{{SOLUTION_CONTROLLER_VERSION}}?${KUMORI_SOLUTION_CONTROLLER_VERSION}?g" controllers/*.yaml
sed -i -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" controllers/*.yaml
sed -i -e "s?{{REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" controllers/*.yaml
sed -i -e "s?{{KUMORI_IMAGES_PULL_POLICY}}?${KUMORI_IMAGES_PULL_POLICY}?g" controllers/*.yaml
sed -i -e "s?{{KUMORI_IMAGES_REGISTRY}}?${KUMORI_IMAGES_REGISTRY}?g" controllers/*.yaml
sed -i -e "s?{{CONTROLLERS_IMAGE_PULL_SECRETS}}?${CONTROLLERS_IMAGE_PULL_SECRETS}?g" controllers/*.yaml
sed -i -e "s?{{KUKUCERT_FULL_NAME}}?${KUKUCERT_FULL_NAME}?g" controllers/*.yaml
sed -i -e "s?{{KUKUCERT_SECRET_NAME}}?${KUKUCERT_SECRET_NAME}?g" controllers/*.yaml
sed -i -e "s?{{ROOTCA_KUKUCA_FULL_NAME}}?${ROOTCA_KUKUCA_FULL_NAME}?g" controllers/*.yaml
sed -i -e "s?{{ADMISSION_KUKUCERT_FULL_NAME}}?${ADMISSION_KUKUCERT_FULL_NAME}?g" controllers/*.yaml
sed -i -e "s?{{ADMISSION_INBOUND_CERT_SECRET}}?${ADMISSION_INBOUND_CERT_SECRET}?g" controllers/*.yaml
sed -i -e "s?{{ADMISSION_INBOUND_CA_SECRET}}?${ADMISSION_INBOUND_CA_SECRET}?g" controllers/*.yaml
sed -i -e "s?{{ADMISSION_CLIENT_CERT_REQUIRED}}?${ADMISSION_CLIENT_CERT_REQUIRED}?g" controllers/*.yaml
sed -i -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" controllers/*.yaml
sed -i -e "s?{{REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" controllers/*.yaml
sed -i -e "s?{{KUINBOUND_MINTLSVERSION}}?${KUINBOUND_MINTLSVERSION}?g"  controllers/*.yaml
sed -i -e "s?{{KUINBOUND_MAXTLSVERSION}}?${KUINBOUND_MAXTLSVERSION}?g"  controllers/*.yaml
sed -i -e "s?{{KUINBOUND_CIPHERSUITETLS}}?${KUINBOUND_CIPHERSUITETLS}?g"  controllers/*.yaml
sed -i -e "s?{{KUINBOUND_ECDHCURVESTLS}}?${KUINBOUND_ECDHCURVESTLS}?g"  controllers/*.yaml
sed -i -e "s?{{CLUSTER_CONFIGMAP_NAME}}?${CLUSTER_CONFIGMAP_NAME}?g"  controllers/*.yaml
sed -i -e "s?{{CLUSTER_CONFIGMAP_NAMESPACE}}?${CLUSTER_CONFIGMAP_NAMESPACE}?g"  controllers/*.yaml
sed -i -e "s?{{CLUSTER_CONFIGMAP_KEY}}?${CLUSTER_CONFIGMAP_KEY}?g"  controllers/*.yaml

# Variable to determine if persistent storage is enabled or not in the cluster
# Storage is enabled if any volume type has been defined
DISABLE_PERSISTENT_STORAGE="true"
if [[ "${#VOLUME_TYPE_NAMES[@]}" -gt 0  ]]; then
  DISABLE_PERSISTENT_STORAGE="false"
fi

# Variable to determine if volatile volumes use a Storage provider
USE_PVC_FOR_VOLATILE="true"
if [ -z "${KUCONTROLLER_DEFAULT_VOLATILE_VOLUMES_TYPE}" ]; then
  USE_PVC_FOR_VOLATILE="false"
fi
# Variable to determine if KuController must use NodeAffinities (only for multi-node clusters)
ENABLE_NODE_AFFINITIES="true"
if [[ "${#WORKERS_IPS[@]}" -eq 0 ]]; then
  # No worker nodes defined, so it's a single node cluster
  ENABLE_NODE_AFFINITIES="false"
fi

# Ambassador internal HTTP port (for automatic redirection of HTTP requests to HTTPS)
INGRESS_INTERNAL_HTTP_PORT="8080"

# ------------------------------------------------------------------------------
# KuInbound specific configuration
# ------------------------------------------------------------------------------
sed -i \
  -e "s?{{KUINBOUND_MINTLSVERSION}}?${KUINBOUND_MINTLSVERSION}?g" \
  -e "s?{{KUINBOUND_MAXTLSVERSION}}?${KUINBOUND_MAXTLSVERSION}?g" \
  -e "s?{{KUINBOUND_CIPHERSUITETLS}}?${KUINBOUND_CIPHERSUITETLS}?g" \
  -e "s?{{KUINBOUND_ECDHCURVESTLS}}?${KUINBOUND_ECDHCURVESTLS}?g" \
  -e "s?{{INGRESS_INTERNAL_HTTP_PORT}}?${INGRESS_INTERNAL_HTTP_PORT}?g" \
  -e "s?{{KUINBOUND_HSTS}}?${KUINBOUND_HSTS}?g" \
  controllers/kuinbound.yaml

# ------------------------------------------------------------------------------
# KuController specific configuration
# ------------------------------------------------------------------------------

# Nodes labels used to spread instances among nodes. By default, instances are
# equally distributed among all nodes. If nodes are grouped among zones,
# topology.kubernetes.io/zone label is used instead
KUCONTROLLER_SPREAD_CONSTRAINTS_LABELS_SOFT="topology.kubernetes.io/zone"

# Indent the externalServicesAccess YAML configuration snippet to match the indentation
# in kucontroller.yaml
if [ -z "${KUCONTROLLER_EXTERNAL_SERVICES_ACCESS}" ]; then
  KUCONTROLLER_EXTERNAL_SERVICES_ACCESS_INDENT="{}"
else
  KUCONTROLLER_EXTERNAL_SERVICES_ACCESS_INDENT="\n$(echo -e "\n$KUCONTROLLER_EXTERNAL_SERVICES_ACCESS" | sed 's?^?      ?' | sed -e 's?[[:space:]]*$??')"
  KUCONTROLLER_EXTERNAL_SERVICES_ACCESS_INDENT="${KUCONTROLLER_EXTERNAL_SERVICES_ACCESS_INDENT//$'\n'/\\n}"
fi

sed -i \
  -e "s?{{DISABLE_PERSISTENT_STORAGE}}?${DISABLE_PERSISTENT_STORAGE}?g" \
  -e "s?{{USE_PVC_FOR_VOLATILE}}?${USE_PVC_FOR_VOLATILE}?g" \
  -e "s?{{KUCONTROLLER_DEFAULT_VOLATILE_VOLUMES_TYPE}}?${KUCONTROLLER_DEFAULT_VOLATILE_VOLUMES_TYPE}?g" \
  -e "s?{{DEFAULT_STARTUP_TIMEOUT}}?${KUCONTROLLER_DEFAULT_LIVENESS_TIMEOUT}?g" \
  -e "s?{{DEFAULT_LIVENESS_TIMEOUT}}?${KUCONTROLLER_DEFAULT_LIVENESS_TIMEOUT}?g" \
  -e "s?{{DEFAULT_READINESS_TIMEOUT}}?${KUCONTROLLER_DEFAULT_READINESS_TIMEOUT}?g" \
  -e "s?{{TSC_MAX_SKEW}}?${KUCONTROLLER_TSC_MAX_SKEW}?g" \
  -e "s?{{REVISION_HISTORY_LIMIT}}?${KUCONTROLLER_REVISION_HISTORY_LIMIT}?g" \
  -e "s?{{FORCE_REBOOT_ON_UPDATE}}?${KUCONTROLLER_FORCE_REBOOT_ON_UPDATE}?g" \
  -e "s?{{DEFAULT_DISK_REQUEST_SIZE}}?${KUCONTROLLER_DEFAULT_DISK_REQUEST_SIZE}?g" \
  -e "s?{{DEFAULT_DISK_REQUEST_UNIT}}?${KUCONTROLLER_DEFAULT_DISK_REQUEST_UNIT}?g" \
  -e "s?{{ENABLE_NODE_AFFINITIES}}?${ENABLE_NODE_AFFINITIES}?g" \
  -e "s?{{SPREAD_CONSTRAINTS_LABELS_SOFT}}?${KUCONTROLLER_SPREAD_CONSTRAINTS_LABELS_SOFT}?g" \
  -e "s?{{EXTERNAL_SERVICES_ACCESS}}?${KUCONTROLLER_EXTERNAL_SERVICES_ACCESS_INDENT}?g" \
  controllers/kucontroller.yaml

# ------------------------------------------------------------------------------
# Volume Controller specific configuration
# ------------------------------------------------------------------------------

# sed -i \
#   controllers/kuvolume.yaml

# ------------------------------------------------------------------------------
# Admission specific configuration
# ------------------------------------------------------------------------------

if [ "${INSTALL_DESCHEDULER}" = "true" ]; then
  RESCHEDULING_ENABLED="true"
  RESCHEDULER_TYPE="descheduler"
else
  RESCHEDULING_ENABLED="false"
  RESCHEDULER_TYPE=""
fi
if [ "${INSTALL_KUALARM}" == "true" ]; then
  ALARMS_ENABLED="true"
else
  ALARMS_ENABLED="false"
fi
# If several Docker registry mirrors are configured, used the first one
DOCKER_REGISTRY_MIRROR=""
if [ -n "${DOCKER_REGISTRY_MIRRORS}" ]; then
  DOCKER_REGISTRY_MIRROR="${DOCKER_REGISTRY_MIRRORS[0]}"
fi

sed -i \
  -e "s?{{KEYCLOAK_ADMISSION_CLIENT_SECRET}}?${KEYCLOAK_ADMISSION_CLIENT_SECRET}?g" \
  -e "s?{{DISABLE_PERSISTENT_STORAGE}}?${DISABLE_PERSISTENT_STORAGE}?g" \
  -e "s?{{EVENTS_STORE_TYPE}}?${EVENTS_STORE_TYPE}?g" \
  -e "s?{{EVENTS_ELASTICSEARCH_URL}}?${EVENTS_ELASTICSEARCH_URL}?g" \
  -e "s?{{EVENTS_ELASTICSEARCH_USERNAME}}?${EVENTS_ELASTICSEARCH_USERNAME}?g" \
  -e "s?{{EVENTS_ELASTICSEARCH_PASSWORD}}?${EVENTS_ELASTICSEARCH_PASSWORD}?g" \
  -e "s?{{EVENTS_ELASTICSEARCH_INDEX}}?${EVENTS_ELASTICSEARCH_INDEX_ADMISSION}?g" \
  -e "s?{{RESCHEDULING_ENABLED}}?${RESCHEDULING_ENABLED}?g" \
  -e "s?{{RESCHEDULER_TYPE}}?${RESCHEDULER_TYPE}?g" \
  -e "s?{{ALARMS_ENABLED}}?${ALARMS_ENABLED}?g" \
  -e "s?{{DOCKERHUB_USERNAME}}?${DOCKERHUB_USERNAME}?g" \
  -e "s?{{DOCKERHUB_PASSWORD}}?${DOCKERHUB_PASSWORD}?g" \
  -e "s?{{DOCKER_REGISTRY_MIRROR}}?${DOCKER_REGISTRY_MIRROR}?g" \
  -e "s?{{REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" \
  controllers/admission-server-config.json

# If ITI mirror is used, add its ITI-internal address to Admission Pods '/etc/hosts'.
# The reason is that ITI mirror requires special DNS resolution (ITI-internal).
if [ -n "${DOCKER_REGISTRY_MIRROR}" ]; then
  ITI_MIRROR_DOMAIN="docker-mirror.iti.upv.es"
  ITI_MIRROR_IP="10.149.16.56"

  # Detect if ITI mirror is used
  if [[ "${DOCKER_REGISTRY_MIRROR}" == *"${ITI_MIRROR_DOMAIN}" ]]; then
    HOST_ALIASES="hostAliases: [ { ip: \"${ITI_MIRROR_IP}\", hostnames: [ \"${ITI_MIRROR_DOMAIN}\" ] } ]"
    # Add a HostAliases section to Admission deployment manifest
    sed -i \
      -e "s?{{HOST_ALIASES_PLACEHOLDER}}?${HOST_ALIASES}?g" \
      controllers/admission.yaml
  else
    # Remove the HostAliases placeholder
    sed -i \
      -e "/^.*{{HOST_ALIASES_PLACEHOLDER}}.*$/d" \
      controllers/admission.yaml
  fi
else
  # Remove the HostAliases placeholder
  sed -i \
    -e "/^.*{{HOST_ALIASES_PLACEHOLDER}}.*$/d" \
    controllers/admission.yaml
fi

# Admission mapping must be configured with
# (1) If ADMISSION_SERVER_CLIENT_CERT_VALIDATE_WITH_TRUSTED_CA == true and
#     INGRESS_CERTHEADER_FORMAT=XFCC, then the mapping must NOT set the
#     x-forwarded-for-client header
# (2) If ADMISSION_SERVER_CLIENT_CERT_VALIDATE_WITH_TRUSTED_CA == false or
#     INGRESS_CERTHEADER_FORMAT=PEM, then the mapping must set the
#     x-forwarded-for-client header
if [ "${INGRESS_CERTHEADER_FORMAT}" == "XFCC" ] || \
   [ "ADMISSION_SERVER_CLIENT_CERT_VALIDATE_WITH_TRUSTED_CA" == true ]; then
  ADD_REQUEST_HEADERS=""
else
  ADD_REQUEST_HEADERS="add_request_headers: { x-forwarded-client-cert: { value: '%DOWNSTREAM_PEER_CERT%' } }"
fi
sed -i \
  -e "s?{{ADD_REQUEST_HEADERS_PLACEHOLDER}}?${ADD_REQUEST_HEADERS}?g" \
  controllers/admission.yaml

# If IPFiltering feature is enabled and a list of enabled IP/CIDR has been
# defined for admission, add the AllowedIP section to the admission mapping
# Note that this function (that uses "yq") MUST be executed after the
# ADD_REQUEST_HEADERS_PLACEHOLDER has been replaced
add_allowed_ip_to_mapping $INSTALL_IPFILTERING controllers/admission.yaml $ADMISSION_ALLOWEDIP

# Convert the INGRESS_TCP_PORT and INGRESS_TCP_INTERNAL_PORT arrays into comma-separated strings
INGRESS_TCP_PORTS_STR=""
INGRESS_TCP_INTERNAL_PORTS_STR=""
SEP=""
# Loop array indices
for i in "${!INGRESS_TCP_PORTS[@]}"; do
  INGRESS_TCP_PORTS_STR="${INGRESS_TCP_PORTS_STR}${SEP}${INGRESS_TCP_PORTS[$i]}"
  INGRESS_TCP_INTERNAL_PORTS_STR="${INGRESS_TCP_INTERNAL_PORTS_STR}${SEP}${INGRESS_TCP_INTERNAL_PORTS[$i]}"
  SEP=","
done
echo "Configuring Admission with"
echo "- External TCP Ports: $INGRESS_TCP_PORTS_STR"
echo "- Internal TCP Ports: $INGRESS_TCP_INTERNAL_PORTS_STR"

sed -i -e "s?{{INGRESS_TCP_PORTS}}?${INGRESS_TCP_PORTS_STR}?g" controllers/admission-server-config.json
sed -i -e "s?{{INGRESS_TCP_INTERNAL_PORTS}}?${INGRESS_TCP_INTERNAL_PORTS_STR}?g" controllers/admission-server-config.json


# Depending on Admission authentication type, add configuration to admission
# configuration
ADMISSION_AUTHENTICATION_SLUG=null

if [ "${ADMISSION_AUTHENTICATION_TYPE}" == "clientcertificate" ]; then

  if [ "${ADMISSION_SERVER_CLIENT_CERT_VALIDATE_WITH_TRUSTED_CA}" == "true" ]; then
    ROOTCA_CONTENT=$(cat "${ADDON_FIXTURES_KPKI_ROOTCA}")
  else
    ROOTCA_CONTENT=""
  fi
  sed -i \
    -e "s?{{REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" \
    -e "s?{{VALIDATE_WITH_TRUSTED_CA}}?${ADMISSION_SERVER_CLIENT_CERT_VALIDATE_WITH_TRUSTED_CA}?g" \
    controllers/admission-server-clientcertificate-config.json

  # Replace the ROOTCA implies jq to be used ...
  cat controllers/admission-server-clientcertificate-config.json | \
    jq --arg rcavar "${ROOTCA_CONTENT}" '.clientCertificateConfig.trustedCA |= $rcavar' > tmp.json
  mv tmp.json controllers/admission-server-clientcertificate-config.json

  ADMISSION_AUTHENTICATION_SLUG=$(cat controllers/admission-server-clientcertificate-config.json)
  echo "ClientCertificate Admission authentication configured."

elif [ "${ADMISSION_AUTHENTICATION_TYPE}" == "token" ]; then
  sed -i \
    -e "s?{{KEYCLOAK_ADMISSION_CLIENT_SECRET}}?${KEYCLOAK_ADMISSION_CLIENT_SECRET}?g" \
    controllers/admission-server-keycloak-config.json
  ADMISSION_AUTHENTICATION_SLUG=$(cat controllers/admission-server-keycloak-config.json)
  echo "Keycloak Admission authentication configured."
else
  echo "Disabled Admission authentication (Keycloak is not being installed)."
fi

cat controllers/admission-server-config.json \
  | jq ".admission.authentication = ${ADMISSION_AUTHENTICATION_SLUG}" \
  > controllers/admission-server-config_TMP.json
mv controllers/admission-server-config_TMP.json controllers/admission-server-config.json

# Create admission configuration as a secret (it contains sensitive credentials)
cd controllers
kubectl create secret generic admission-config-secret \
  --namespace kumori \
  --from-file=admission-server-config.json
cd ..

# ------------------------------------------------------------------------------
# Kumori Flex Volume Driver specific configuration
# ------------------------------------------------------------------------------

sed -i \
  -e "s?{{KUMORI_KUVOLUME_VERSION}}?${KUMORI_KUVOLUME_VERSION}?g" \
  -e "s?{{KUMORI_IMAGES_PULL_POLICY}}?${KUMORI_IMAGES_PULL_POLICY}?g" \
  -e "s?{{KUMORI_IMAGES_REGISTRY}}?${KUMORI_IMAGES_REGISTRY}?g" \
  -e "s?{{CONTROLLERS_IMAGE_PULL_SECRETS}}?${CONTROLLERS_IMAGE_PULL_SECRETS}?g" \
  cr/kumori-flex-driver-installer.yaml


# ------------------------------------------------------------------------------
# Controllers installation
# ------------------------------------------------------------------------------

# This is a workaround since 'kubectl apply -f cr/*.yaml' is not valid
# and the directories may contain other files than yaml.
# Alarms CR (alarm instances, alarm definitions, alarm implementations) are JSON.
find cr          -name "*.yaml" -exec kubectl apply -f {} \;
# Do not install KuAlarm if KubePrometheus is not installed

# Install Admission
kubectl apply -f controllers/admission.yaml
kubectl apply -f controllers/rbac-admission.yaml

# Install KuController
kubectl apply -f controllers/kucontroller.yaml
kubectl apply -f controllers/rbac-kucontroller.yaml

# Install KuInbound
kubectl apply -f controllers/kuinbound.yaml
kubectl apply -f controllers/rbac-kuinbound.yaml

# Install SolutionController
kubectl apply -f controllers/solution-controller.yaml
kubectl apply -f controllers/rbac-solution-controller.yaml

# Install TopologyController
kubectl apply -f controllers/rbac-topology-controller.yaml
kubectl apply -f controllers/topology-controller.yaml

if [ "${INSTALL_VOLUME_CONTROLLER}" == "true" ]; then
  # Install VolumeController
  kubectl apply -f controllers/kuvolume.yaml
  kubectl apply -f controllers/rbac-kuvolume.yaml
else
  echo "Skipped VolumeController installation (disabled)."
fi

if [ "${INSTALL_KUBE_PROMETHEUS}" == "true" ]; then
  if [ "${INSTALL_KUALARM}" == "true" ]; then
    # Install KuAlarm
    kubectl apply -f controllers/kualarm.yaml
    kubectl apply -f controllers/rbac-kualarm.yaml
  else
    echo "Skipped KuAlarm installation (disabled)."
  fi
else
  echo "Skipped KuAlarm installation (Prometheus not installed)."
fi


# ------------------------------------------------------------------------------
# Create a KukuCert for the reference domain
# ------------------------------------------------------------------------------
echo
echo "Creating a new KukuCert for the reference domain..."

# Define files path used for creating KukuCert manifest
REFDOMAIN_CERT_CRT_FILE="${ADDON_FIXTURES_DIR_CERTS}/wildcard.${REFERENCE_DOMAIN}.crt"
REFDOMAIN_CERT_KEY_FILE="${ADDON_FIXTURES_DIR_CERTS}/wildcard.${REFERENCE_DOMAIN}.key"
KUKUCERT_KUMORI_MANIFEST_TEMPLATE="cr/refdomain-kukucert-manifest.json_template"
KUKUCERT_KUMORI_MANIFEST="cr/refdomain-kukucert-manifest.json"
KUKUCERT_YAML_TEMPLATE="cr/refdomain-kukucert.yaml_template"
KUKUCERT_YAML="cr/refdomain-kukucert.yaml"

# KukuCert owner (always admin)
KUKUCERT_OWNER="admin"

# Base64 encode the certificate file into variables
ENCODED_CRT=$(cat "${REFDOMAIN_CERT_CRT_FILE}" | base64 -w 0)
ENCODED_KEY=$(cat "${REFDOMAIN_CERT_KEY_FILE}" | base64 -w 0)

# Create a temporary KukuCert Kumori manifest (JSON) from a template
sed \
  -e "s?{{KUKUCERT_DOMAIN}}?${CLUSTER_CORE_DOMAIN}?g" \
  -e "s?{{KUKUCERT_NAME}}?${KUKUCERT_NAME}?g" \
  -e "s?{{KUKUCERT_OWNER}}?${KUKUCERT_OWNER}?g" \
  -e "s?{{KUKUCERT_FULL_NAME}}?${KUKUCERT_FULL_NAME}?g" \
  -e "s?{{CERT_DOMAIN}}?*.${REFERENCE_DOMAIN}?g" \
  -e "s?{{ENCODED_CRT}}?${ENCODED_CRT}?g" \
  -e "s?{{ENCODED_KEY}}?${ENCODED_KEY}?g" \
  ${KUKUCERT_KUMORI_MANIFEST_TEMPLATE} \
  > ${KUKUCERT_KUMORI_MANIFEST}

# Base64 encode the KukuCert Kumori manifest
ENCODED_KUMORI_MANIFEST="$(cat ${KUKUCERT_KUMORI_MANIFEST} | gzip | base64 -w0)"

# Calculate hashed values for labels using FNV1-32
# Check if docker is available
if [ -x "$(command -v docker)" ]; then
  echo "Detected docker installed, calculate KukuCert labels hashes with docker image..."
  HASHER="docker run --rm kumori/fnv1-32-hash:v1.0.0 fnv132 -s"
  # NOTE: the 'cut -c 3-' is for removing the leading '0x' in the Hex response
  HASHED_KUKUCERT_OWNER=$(${HASHER} ${KUKUCERT_OWNER} | cut -c 3-)
  HASHED_KUKUCERT_DOMAIN=$(${HASHER} ${CLUSTER_CORE_DOMAIN} | cut -c 3-)
  HASHED_KUKUCERT_NAME=$(${HASHER} ${KUKUCERT_NAME} | cut -c 3-)
elif [ -x "$(command -v fnv-32-1-hasher)" ]; then
  echo "Detected fnv32-1-hasher installed, use it to calculate KukuCert labels hashes..."
  HASHER="fnv-32-1-hasher"
  HASHED_KUKUCERT_OWNER=$(${HASHER} ${KUKUCERT_OWNER})
  HASHED_KUKUCERT_DOMAIN=$(${HASHER} ${CLUSTER_CORE_DOMAIN})
  HASHED_KUKUCERT_NAME=$(${HASHER} ${KUKUCERT_NAME})
else
  echo -e $COL_RED"ERROR: Unable to find a FNV-32-1 hasher (neither docker nor fnv-32-1-hasher are available)."$COL_DEFAULT
  HASHED_KUKUCERT_OWNER="\"\""
  HASHED_KUKUCERT_DOMAIN="\"\""
  HASHED_KUKUCERT_NAME="\"\""
fi

# Create a KukuCert manifest (YAML) from a template
sed \
  -e "s?{{ENCODED_KUMORI_MANIFEST}}?${ENCODED_KUMORI_MANIFEST}?g" \
  -e "s?{{HASHED_KUKUCERT_OWNER}}?${HASHED_KUKUCERT_OWNER}?g" \
  -e "s?{{HASHED_KUKUCERT_DOMAIN}}?${HASHED_KUKUCERT_DOMAIN}?g" \
  -e "s?{{HASHED_KUKUCERT_NAME}}?${HASHED_KUKUCERT_NAME}?g" \
  -e "s?{{KUKUCERT_OWNER}}?${KUKUCERT_OWNER}?g" \
  -e "s?{{KUKUCERT_DOMAIN}}?${CLUSTER_CORE_DOMAIN}?g" \
  -e "s?{{KUKUCERT_NAME}}?${KUKUCERT_NAME}?g" \
  -e "s?{{KUKUCERT_FULL_NAME}}?${KUKUCERT_FULL_NAME}?g" \
  -e "s?{{CERT_DOMAIN}}?*.${REFERENCE_DOMAIN}?g" \
  -e "s?{{ENCODED_CRT}}?${ENCODED_CRT}?g" \
  -e "s?{{ENCODED_KEY}}?${ENCODED_KEY}?g" \
  ${KUKUCERT_YAML_TEMPLATE} \
  > ${KUKUCERT_YAML}

# Apply the KukuCert manifest to the cluster
kubectl apply -f ${KUKUCERT_YAML}

# ------------------------------------------------------------------------------
# Create a KukuCert for Admission (only if authentication type is "clientcertificate")
# ------------------------------------------------------------------------------

if [ "${ADMISSION_AUTHENTICATION_TYPE}" == "clientcertificate" ]; then

  echo
  echo "Creating a new KukuCert for Admission..."

  # Define files path used for creating KukuCert manifest
  ADMISSION_CERT_CRT_FILE="${ADDON_FIXTURES_KPKI_ADMISSION_SERVER_CERT_DIR}/cert.crt"
  ADMISSION_CERT_KEY_FILE="${ADDON_FIXTURES_KPKI_ADMISSION_SERVER_CERT_DIR}/cert.key"
  ADMISSION_KUKUCERT_KUMORI_MANIFEST_TEMPLATE="cr/admission-kukucert-manifest.json_template"
  ADMISSION_KUKUCERT_KUMORI_MANIFEST="cr/admission-kukucert-manifest.json"
  ADMISSION_KUKUCERT_YAML_TEMPLATE="cr/admission-kukucert.yaml_template"
  ADMISSION_KUKUCERT_YAML="cr/admission-kukucert.yaml"

  # Admission URL
  ADMISSION_CERT_DOMAIN="admission-${RELEASE_NAME}.${REFERENCE_DOMAIN}"

  # KukuCert Owner (always owner)
  ADMISSION_KUKUCERT_OWNER="admin"

  # Base64 encode the certificate file into variables
  ADMISSION_ENCODED_CRT=$(cat "${ADMISSION_CERT_CRT_FILE}" | base64 -w 0)
  ADMISSION_ENCODED_KEY=$(cat "${ADMISSION_CERT_KEY_FILE}" | base64 -w 0)

  # Create a temporary KukuCert Kumori manifest (JSON) from a template
  sed \
    -e "s?{{ADMISSION_KUKUCERT_DOMAIN}}?${CLUSTER_CORE_DOMAIN}?g" \
    -e "s?{{ADMISSION_KUKUCERT_NAME}}?${ADMISSION_KUKUCERT_NAME}?g" \
    -e "s?{{ADMISSION_KUKUCERT_OWNER}}?${ADMISSION_KUKUCERT_OWNER}?g" \
    -e "s?{{ADMISSION_KUKUCERT_FULL_NAME}}?${ADMISSION_KUKUCERT_FULL_NAME}?g" \
    -e "s?{{ADMISSION_CERT_DOMAIN}}?${ADMISSION_CERT_DOMAIN}?g" \
    -e "s?{{ADMISSION_ENCODED_CRT}}?${ADMISSION_ENCODED_CRT}?g" \
    -e "s?{{ADMISSION_ENCODED_KEY}}?${ADMISSION_ENCODED_KEY}?g" \
    ${ADMISSION_KUKUCERT_KUMORI_MANIFEST_TEMPLATE} \
    > ${ADMISSION_KUKUCERT_KUMORI_MANIFEST}

  # Base64 encode the KukuCert Kumori manifest
  ADMISSION_ENCODED_KUMORI_MANIFEST="$(cat ${ADMISSION_KUKUCERT_KUMORI_MANIFEST} | gzip | base64 -w0)"

  # Calculate hashed values for labels using FNV1-32
  # Check if docker is available
  if [ -x "$(command -v docker)" ]; then
    echo "Detected docker installed, calculate KukuCert labels hashes with docker image..."
    HASHER="docker run --rm kumori/fnv1-32-hash:v1.0.0 fnv132 -s"
    # NOTE: the 'cut -c 3-' is for removing the leading '0x' in the Hex response
    HASHED_ADMISSION_KUKUCERT_OWNER=$(${HASHER} ${ADMISSION_KUKUCERT_OWNER} | cut -c 3-)
    HASHED_ADMISSION_KUKUCERT_DOMAIN=$(${HASHER} ${CLUSTER_CORE_DOMAIN} | cut -c 3-)
    HASHED_ADMISSION_KUKUCERT_NAME=$(${HASHER} ${ADMISSION_KUKUCERT_NAME} | cut -c 3-)
  elif [ -x "$(command -v fnv-32-1-hasher)" ]; then
    echo "Detected fnv32-1-hasher installed, use it to calculate KukuCert labels hashes..."
    HASHER="fnv-32-1-hasher"
    HASHED_ADMISSION_KUKUCERT_OWNER=$(${HASHER} ${ADMISSION_KUKUCERT_OWNER})
    HASHED_ADMISSION_KUKUCERT_DOMAIN=$(${HASHER} ${CLUSTER_CORE_DOMAIN})
    HASHED_ADMISSION_KUKUCERT_NAME=$(${HASHER} ${ADMISSION_KUKUCERT_NAME})
  else
    echo -e $COL_RED"ERROR: Unable to find a FNV-32-1 hasher (neither docker nor fnv-32-1-hasher are available)."$COL_DEFAULT
    HASHED_ADMISSION_KUKUCERT_OWNER="\"\""
    HASHED_ADMISSION_KUKUCERT_DOMAIN="\"\""
    HASHED_ADMISSION_KUKUCERT_NAME="\"\""
  fi

  # Create a KukuCert manifest (YAML) from a template
  sed \
    -e "s?{{ADMISSION_ENCODED_KUMORI_MANIFEST}}?${ADMISSION_ENCODED_KUMORI_MANIFEST}?g" \
    -e "s?{{HASHED_ADMISSION_KUKUCERT_OWNER}}?${HASHED_ADMISSION_KUKUCERT_OWNER}?g" \
    -e "s?{{HASHED_ADMISSION_KUKUCERT_DOMAIN}}?${HASHED_ADMISSION_KUKUCERT_DOMAIN}?g" \
    -e "s?{{HASHED_ADMISSION_KUKUCERT_NAME}}?${HASHED_ADMISSION_KUKUCERT_NAME}?g" \
    -e "s?{{ADMISSION_KUKUCERT_OWNER}}?${ADMISSION_KUKUCERT_OWNER}?g" \
    -e "s?{{ADMISSION_KUKUCERT_DOMAIN}}?${CLUSTER_CORE_DOMAIN}?g" \
    -e "s?{{ADMISSION_KUKUCERT_NAME}}?${ADMISSION_KUKUCERT_NAME}?g" \
    -e "s?{{ADMISSION_KUKUCERT_FULL_NAME}}?${ADMISSION_KUKUCERT_FULL_NAME}?g" \
    -e "s?{{ADMISSION_CERT_DOMAIN}}?${ADMISSION_CERT_DOMAIN}?g" \
    -e "s?{{ADMISSION_ENCODED_CRT}}?${ADMISSION_ENCODED_CRT}?g" \
    -e "s?{{ADMISSION_ENCODED_KEY}}?${ADMISSION_ENCODED_KEY}?g" \
    ${ADMISSION_KUKUCERT_YAML_TEMPLATE} \
    > ${ADMISSION_KUKUCERT_YAML}

  # Apply the KukuCert manifest to the cluster
kubectl apply -f ${ADMISSION_KUKUCERT_YAML}

fi

# ------------------------------------------------------------------------------
# Create a KukuCA for the Root CA
# ------------------------------------------------------------------------------
echo
echo "Creating a new KukuCA for RootCA..."

# Define files path used for creating KukuCert manifest
ROOTCA_CERT_CRT_FILE=$ADDON_FIXTURES_KPKI_ROOTCA
ROOTCA_KUKUCA_KUMORI_MANIFEST_TEMPLATE="cr/rootca-kukuca-manifest.json_template"
ROOTCA_KUKUCA_KUMORI_MANIFEST="cr/rootca-kukuca-manifest.json"
ROOTCA_KUKUCA_YAML_TEMPLATE="cr/rootca-kukuca.yaml_template"
ROOTCA_KUKUCA_YAML="cr/rootca-kukuca.yaml"

# KukuCa owner (always admin)
ROOTCA_KUKUCA_OWNER="admin"

# Base64 encode the certificate file into variables
ROOTCA_ENCODED_CRT=$(cat "${ROOTCA_CERT_CRT_FILE}" | base64 -w 0)

# Create a temporary KukuCa Kumori manifest (JSON) from a template
sed \
  -e "s?{{ROOTCA_KUKUCA_DOMAIN}}?${CLUSTER_CORE_DOMAIN}?g" \
  -e "s?{{ROOTCA_KUKUCA_NAME}}?${ROOTCA_KUKUCA_NAME}?g" \
  -e "s?{{ROOTCA_KUKUCA_OWNER}}?${ROOTCA_KUKUCA_OWNER}?g" \
  -e "s?{{ROOTCA_KUKUCA_FULL_NAME}}?${ROOTCA_KUKUCA_FULL_NAME}?g" \
  -e "s?{{ROOTCA_ENCODED_CRT}}?${ROOTCA_ENCODED_CRT}?g" \
  ${ROOTCA_KUKUCA_KUMORI_MANIFEST_TEMPLATE} \
  > ${ROOTCA_KUKUCA_KUMORI_MANIFEST}

# Base64 encode the KukuCert Kumori manifest
ROOTCA_ENCODED_KUMORI_MANIFEST="$(cat ${ROOTCA_KUKUCA_KUMORI_MANIFEST} | gzip | base64 -w0)"

# Calculate hashed values for labels using FNV1-32
# Check if docker is available
if [ -x "$(command -v docker)" ]; then
  echo "Detected docker installed, calculate KukuCa labels hashes with docker image..."
  HASHER="docker run --rm kumori/fnv1-32-hash:v1.0.0 fnv132 -s"
  # NOTE: the 'cut -c 3-' is for removing the leading '0x' in the Hex response
  HASHED_ROOTCA_KUKUCA_OWNER=$(${HASHER} ${ROOTCA_KUKUCA_OWNER} | cut -c 3-)
  HASHED_ROOTCA_KUKUCA_DOMAIN=$(${HASHER} ${CLUSTER_CORE_DOMAIN} | cut -c 3-)
  HASHED_ROOTCA_KUKUCA_NAME=$(${HASHER} ${ROOTCA_KUKUCA_NAME} | cut -c 3-)
elif [ -x "$(command -v fnv-32-1-hasher)" ]; then
  echo "Detected fnv32-1-hasher installed, use it to calculate KukuCa labels hashes..."
  HASHER="fnv-32-1-hasher"
  HASHED_ROOTCA_KUKUCA_OWNER=$(${HASHER} ${ROOTCA_KUKUCA_OWNER})
  HASHED_ROOTCA_KUKUCA_DOMAIN=$(${HASHER} ${CLUSTER_CORE_DOMAIN})
  HASHED_ROOTCA_KUKUCA_NAME=$(${HASHER} ${ROOTCA_KUKUCA_NAME})
else
  echo -e $COL_RED"ERROR: Unable to find a FNV-32-1 hasher (neither docker nor fnv-32-1-hasher are available)."$COL_DEFAULT
  HASHED_ROOTCA_KUKUCA_OWNER="\"\""
  HASHED_ROOTCA_KUKUCA_DOMAIN="\"\""
  HASHED_ROOTCA_KUKUCA_NAME="\"\""
fi

# Create a KukuCert manifest (YAML) from a template
sed \
  -e "s?{{ROOTCA_ENCODED_KUMORI_MANIFEST}}?${ROOTCA_ENCODED_KUMORI_MANIFEST}?g" \
  -e "s?{{HASHED_ROOTCA_KUKUCA_OWNER}}?${HASHED_ROOTCA_KUKUCA_OWNER}?g" \
  -e "s?{{HASHED_ROOTCA_KUKUCA_DOMAIN}}?${HASHED_ROOTCA_KUKUCA_DOMAIN}?g" \
  -e "s?{{HASHED_ROOTCA_KUKUCA_NAME}}?${HASHED_ROOTCA_KUKUCA_NAME}?g" \
  -e "s?{{ROOTCA_KUKUCA_OWNER}}?${ROOTCA_KUKUCA_OWNER}?g" \
  -e "s?{{ROOTCA_KUKUCA_DOMAIN}}?${CLUSTER_CORE_DOMAIN}?g" \
  -e "s?{{ROOTCA_KUKUCA_NAME}}?${ROOTCA_KUKUCA_NAME}?g" \
  -e "s?{{ROOTCA_KUKUCA_FULL_NAME}}?${ROOTCA_KUKUCA_FULL_NAME}?g" \
  -e "s?{{ROOTCA_ENCODED_CRT}}?${ROOTCA_ENCODED_CRT}?g" \
  -e "s?{{ROOTCA_MANIFEST_SHA}}?${ROOTCA_MANIFEST_SHA}?g" \
  ${ROOTCA_KUKUCA_YAML_TEMPLATE} \
  > ${ROOTCA_KUKUCA_YAML}

# Apply the KukuCert manifest to the cluster
kubectl apply -f ${ROOTCA_KUKUCA_YAML}

# ------------------------------------------------------------------------------
# Create AlarmDefinitions (ONLY if alarm files are present and KuAlarm and
# Prometheus are installed).
# Be careful:
# 1) alarmdefinition must be created befor alarms, because kualarm-controller doesn't allow
#    create an alarm if the kualarmdefition doesnt exist
# 2) kualarm requires that prometehus is already installed
# ------------------------------------------------------------------------------

if [ "${INSTALL_KUBE_PROMETHEUS}" == "true" ] && [ "${INSTALL_KUALARM}" == "true" ]; then
  if [ -d "alarms/alarmdefinitions" ] || \
    [ -d "alarms/alarmimpls/prometheus" ] || \
    [ -d "alarms/alarms" ]
  then
    echo
    echo "Waiting kualarm-controller to be available"
    kubectl wait --for=condition=available --timeout=600s deployment/kualarm-controller-manager -n kumori
    echo "kualarm-controller deployment is in 'Available' state!"

    echo
    echo "Getting IP of a KuAlarm Pod..."
    KUALARM_POD_IP=""
    while [[ "${KUALARM_POD_IP}" == "" ]]; do
      KUALARM_POD_IP=$(kubectl -n kumori get pods --ignore-not-found=true -l app=kualarm -o jsonpath="{.items[0].status.podIP}")
      sleep 1
    done
    echo "Got KuAlarm Pod IP: ${KUALARM_POD_IP}"

    echo
    echo "Testing KuAlarm API (/healthz):"
    KUALARM_HEALTHZ=$(curl -s -S http://${KUALARM_POD_IP}:8085/healthz)
    if [ "${KUALARM_HEALTHZ}" != "OK" ]; then
      echo -e $COL_BOLD_RED"Request to KuAlarm /healthz did not return the expected result."$COL_DEFAULT
    else
      echo "KuAlarm /healthz returned OK."
    fi

    echo
    echo "Creating alarm definitions"
    find alarms/alarmdefinitions -name "*.json" \
      -exec curl -s -S -X POST -H "Content-Type: application/json" http://${KUALARM_POD_IP}:8085/kualarmdefinition -d @{} \;
    echo
    echo "Creating alarm implementations"
    find alarms/alarmimpls/prometheus -name "*.json" \
      -exec curl -s -S -X POST -H "Content-Type: application/json" http://${KUALARM_POD_IP}:8085/kualarmimplprometheus -d @{} \;
    echo
    echo "Creating alarm alarms"
    find alarms/alarms -name "*.json" \
      -exec curl -s -S -X POST -H "Content-Type: application/json" http://${KUALARM_POD_IP}:8085/kualarm -d @{} \;
    echo
  else
    echo
    echo "No Kumori alarm definitions found, skipping wait for KuAlarmController to be ready."
    echo
  fi
fi
