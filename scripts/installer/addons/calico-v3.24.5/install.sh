#!/bin/bash

# curl https://raw.githubusercontent.com/projectcalico/calico/v3.24.5/manifests/calico.yaml
curl https://raw.githubusercontent.com/projectcalico/calico/${CALICO_VERSION}/manifests/calico.yaml -O


# PREPARE CALICO DAEMONSET CONFIGURATION
# Extract calico-node DaemonSet manifest from the full YAML file
# (it's the document number 25 of the file)
yq read --doc 25 calico.yaml > calico-node-daemonset.yaml

# Delete the manifest we just extracted from the full file
yq delete --inplace --doc 25 calico.yaml "*"
# Deletion leaves a line with {} that kubectl doesn't like. Remove it.
sed -i -e "/^.*{}.*$/d" calico.yaml

# Configure custom ports to free the TCP Ingress port range 9000-9999
# Add custom FELIX_HEALTHPORT configuration (9099 --> 10099)
cat calico-node-daemonset.yaml \
  | yq r -j - \
  | jq ".spec.template.spec.containers[0].env += [ { name: \"FELIX_HEALTHPORT\", value: \"10099\" } ]" \
  | yq r --prettyPrint - \
  > calico-node-daemonset_TMP.yaml
mv calico-node-daemonset_TMP.yaml calico-node-daemonset.yaml

kubectl apply -f calico.yaml
kubectl apply -f calico-node-daemonset.yaml

until kubectl get felixconfiguration default ; do
  echo "$(date +"%d/%m/%Y %H:%M:%S") Waiting for Calico 'default felixconfiguration' to be ready..."
  sleep 5
  echo ""
done
kubectl get felixconfiguration default -o yaml > felixconfiguration.yaml
echo "  prometheusMetricsEnabled: true" >> felixconfiguration.yaml
# Configure custom ports to free the TCP Ingress port range 9000-9999
# Replace prometheus metrics port (9091 --> 10091)
echo "  prometheusMetricsPort: 10091" >> felixconfiguration.yaml
kubectl apply -f felixconfiguration.yaml
kubectl apply -f calico-node-service.yaml


