#!/bin/bash

if [ "${MANAGED_DNS_PROVIDER}" = "route53" ]; then
  # Extract AWS credentials from credentials file
  echo "Extracting credentials from AWS configuration file..."
  AWS_CREDENTIALS_FILE="${SCRIPT_DIR}/awsConfiguration/credentials"

  AWS_ACCESS_KEY_ID=$(grep "aws_access_key_id" ${AWS_CREDENTIALS_FILE}|cut -d'=' -f2 | tr -d "[:space:]")
  AWS_SECRET_ACCESS_KEY=$(grep "aws_secret_access_key" ${AWS_CREDENTIALS_FILE}|cut -d'=' -f2 | tr -d "[:space:]")

  echo " - Extracted AWS_ACCESS_KEY_ID     : ${AWS_ACCESS_KEY_ID}"
  echo " - Extracted AWS_SECRET_ACCESS_KEY : <hidden>  (length: ${#AWS_SECRET_ACCESS_KEY})"

  sed -i -e "s?{{EXTERNALDNS_VERSION}}?${EXTERNALDNS_VERSION}?g" external-dns.yaml
  sed -i -e "s?{{EXTERNALDNS_OWNER_ID}}?${EXTERNALDNS_OWNER_ID}?g" external-dns.yaml
  sed -i -e "s?{{PROVIDER_TYPE}}?aws?g" external-dns.yaml
  sed -i -e "s?{{EXTERNALDNS_AWS_ACCESS_KEY}}?${AWS_ACCESS_KEY_ID}?g" external-dns.yaml
  sed -i -e "s?{{EXTERNALDNS_AWS_SECRET_KEY}}?${AWS_SECRET_ACCESS_KEY}?g" external-dns.yaml
  sed -i -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" external-dns.yaml
  sed -i -e "s?{{REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" external-dns.yaml
  sed -i -e "s?{{RANDOMS_REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" external-dns.yaml

  kubectl apply -f external-dns.yaml
else
  echo
  echo -e $COL_YELLOW"DNS Provider ${MANAGED_DNS_PROVIDER} not supported. Skipping ExternalDNS installation."$COL_DEFAULT
  echo
fi
