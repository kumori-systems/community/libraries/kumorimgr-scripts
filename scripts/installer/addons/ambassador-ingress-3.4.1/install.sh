#!/bin/bash

# Build OutOfService full docker image name and crete credentials secret if necessary
if [ -z "${OUTOFSERVICE_IMAGE}" ]; then
  # If no custom image is provided, use default Kumori OutOfService image
  OUTOFSERVICE_FULL_DOCKER_IMAGE="${KUMORI_IMAGES_REGISTRY}/${KUMORI_OUTOFSERVICE_IMAGE}:v${KUMORI_OUTOFSERVICE_VERSION}"
  OUTOFSERVICE_USERNAME=""
else
  if [ -n "${OUTOFSERVICE_REGISTRY}" ]; then
    OUTOFSERVICE_FULL_DOCKER_IMAGE="${OUTOFSERVICE_REGISTRY}/${OUTOFSERVICE_IMAGE}"
  else
    OUTOFSERVICE_FULL_DOCKER_IMAGE="${OUTOFSERVICE_IMAGE}"
  fi
fi

# Prepare OutOfService deployment manifest
sed -i \
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" \
  -e "s?{{OUTOFSERVICE_IMAGE}}?${OUTOFSERVICE_FULL_DOCKER_IMAGE}?g" \
  outofservice.yaml

# Prepare apiext template
APIEXTFILE="ambassador-apiext.yaml"
cp ${APIEXTFILE}_template ${APIEXTFILE}
CALCULATED_AMBASSADOR_APIEXT_REPLICAS="1"
if [ "${AMBASSADOR_APIEXT_HA}" = "true" ]; then
  CALCULATED_AMBASSADOR_APIEXT_REPLICAS=${AMBASSADOR_APIEXT_REPLICAS}
fi
sed -i \
  -e "s?{{AMBASSADOR_APIEXT_REPLICAS}}?${CALCULATED_AMBASSADOR_APIEXT_REPLICAS}?g" \
  -e "s?{{AMBASSADOR_APIEXT_CPU_REQUEST}}?${AMBASSADOR_APIEXT_CPU_REQUEST}?g" \
  -e "s?{{AMBASSADOR_APIEXT_CPU_LIMIT}}?${AMBASSADOR_APIEXT_CPU_LIMIT}?g" \
  -e "s?{{AMBASSADOR_APIEXT_MEM_REQUEST}}?${AMBASSADOR_APIEXT_MEM_REQUEST}?g" \
  -e "s?{{AMBASSADOR_APIEXT_MEM_LIMIT}}?${AMBASSADOR_APIEXT_MEM_LIMIT}?g" \
  ${APIEXTFILE}

# Daemonset template. It depends on the IPFeature feature is enabled or not
# The ambassador DaemonSet YAML file is created from templates to start with a
# clean file, since it is modified with YQ/JQ to add elements.
DAEMONSETFILE="ambassador-daemonset-hostport.yaml"
if [ "${INSTALL_IPFILTERING}" == "true" ]; then
  rm -f ambassador-daemonset-hostport-ipfiltering.yaml
  cp ambassador-daemonset-hostport-ipfiltering.yaml_template ambassador-daemonset-hostport-ipfiltering.yaml
  DAEMONSETFILE="ambassador-daemonset-hostport-ipfiltering.yaml"
else
  rm -f ambassador-daemonset-hostport.yaml
  cp ambassador-daemonset-hostport.yaml_template ambassador-daemonset-hostport.yaml
  DAEMONSETFILE="ambassador-daemonset-hostport.yaml"
fi

sed -i \
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" \
  ambassador-config-module.yaml
sed -i \
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" \
  -e "s?{{AMBASSADOR_VERSION}}?${AMBASSADOR_VERSION}?g" \
  -e "s?{{INGRESS_INTERNAL_PORT}}?${INGRESS_INTERNAL_PORT}?g" \
  -e "s?{{AMBASSADOR_CPU_REQUEST}}?${AMBASSADOR_CPU_REQUEST}?g" \
  -e "s?{{AMBASSADOR_CPU_LIMIT}}?${AMBASSADOR_CPU_LIMIT}?g" \
  -e "s?{{AMBASSADOR_MEM_REQUEST}}?${AMBASSADOR_MEM_REQUEST}?g" \
  -e "s?{{AMBASSADOR_MEM_LIMIT}}?${AMBASSADOR_MEM_LIMIT}?g" \
  ${DAEMONSETFILE}
sed -i -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" ambassador-rbac.yaml
sed -i -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" ambassador-service.yaml
sed -i -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" ingress-namespace.yaml

# If IPFiltering is enabled, replace configuration variables
if [ "${INSTALL_IPFILTERING}" == "true" ]; then
  # Determine the appropriate image pull Secret property for Controller manifests
  KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET="kumori-enterprise-registry-credentials"
  if [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ] && [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
    CONTROLLERS_IMAGE_PULL_SECRETS="[ { name: ${KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET} } ]"
  else
    CONTROLLERS_IMAGE_PULL_SECRETS="[]"
  fi

  sed -i \
    -e "s?{{KUMORI_IPFILTERING_VERSION}}?${KUMORI_IPFILTERING_VERSION}?g" \
    -e "s?{{IPFILTERING_CPU_REQUEST}}?${IPFILTERING_CPU_REQUEST}?g" \
    -e "s?{{IPFILTERING_CPU_LIMIT}}?${IPFILTERING_CPU_LIMIT}?g" \
    -e "s?{{IPFILTERING_MEM_REQUEST}}?${IPFILTERING_MEM_REQUEST}?g" \
    -e "s?{{IPFILTERING_MEM_LIMIT}}?${IPFILTERING_MEM_LIMIT}?g" \
    -e "s?{{KUMORI_IMAGES_REGISTRY}}?${KUMORI_IMAGES_REGISTRY}?g" \
    -e "s?{{CONTROLLERS_IMAGE_PULL_SECRETS}}?${CONTROLLERS_IMAGE_PULL_SECRETS}?g" \
    ${DAEMONSETFILE}

  sed -i \
    -e "s?{{IPFILTERING_REJECTIFFAILURE}}?${IPFILTERING_REJECTIFFAILURE}?g"\
    -e "s?{{IPFILTERING_TIMEOUTMSEC}}?${IPFILTERING_TIMEOUTMSEC}?g"\
    -e "s?{{IPFILTERING_LOGLEVEL}}?${IPFILTERING_LOGLEVEL}?g"\
    -e "s?{{IPFILTERING_MAXLISTSIZE}}?${IPFILTERING_MAXLISTSIZE}?g"\
    ambassador-ipfiltering-stuff.yaml
fi

# Create ingress namespace
kubectl apply -f ./ingress-namespace.yaml

# If username and password are set, create a docker credentials Secret
# and add an imagePullSecret to the deployment manifest
if [ -n "${OUTOFSERVICE_USERNAME}" ] && [ -n "${OUTOFSERVICE_PASSWORD}" ]; then
  kubectl -n kumori create secret docker-registry outofservice-docker-credentials \
    --docker-server="${OUTOFSERVICE_REGISTRY}" \
    --docker-username="${OUTOFSERVICE_USERNAME}" \
    --docker-password="${OUTOFSERVICE_PASSWORD}"

  # Add imagePullSecret to deployment manifest
  sed -i \
    -e "s?{{IMAGE_PULL_SECRET}}?- name: outofservice-docker-credentials?g" \
    outofservice.yaml
elif [ -n "${KUMORI_IMAGES_REGISTRY_USERNAME}" ] && [ -n "${KUMORI_IMAGES_REGISTRY_PASSWORD}" ]; then
  # Add imagePullSecret to deployment manifest with Kumori controllers credentials secret
  KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET="kumori-enterprise-registry-credentials"
  sed -i \
    -e "s?{{IMAGE_PULL_SECRET}}?- name: ${KUMORI_IMAGES_REGISTRY_CREDENTIALS_SECRET}?g" \
    outofservice.yaml
else
  # Remove imagePullSecret placeholder from deployment manifest and set it to []
  sed -i \
    -e "s|{{IMAGE_PULL_SECRET}}||g" \
    -e "s|imagePullSecrets:[[:space:]]*$|imagePullSecrets: []|g" \
    outofservice.yaml
fi

echo
echo "OUTOFSERVICE DEPLOYMENT MANIFEST:"
cat outofservice.yaml
echo


# Configure Ingress TCP ports (Ambassador "listens" on internal ports)
if [ -n "${INGRESS_TCP_PORTS}" ]; then
  # Add custom extra ports to listen to in Ambassador configuration
  TMP_FILE="ambassador-daemonset-hostport_TMP.yaml"

  echo "Adding custom ports to Ambassador..."

  # for PORT in ${INGRESS_TCP_PORTS[@]}; do
  for i in "${!INGRESS_TCP_PORTS[@]}"; do
    INTERNAL_PORT="${INGRESS_TCP_INTERNAL_PORTS[$i]}"
    EXTERNAL_PORT="${INGRESS_TCP_PORTS[$i]}"
    cat ${DAEMONSETFILE} \
      | yq r -j - \
      | jq ".spec.template.spec.containers[0].ports += [ { name: \"tcp${EXTERNAL_PORT}\", containerPort: ${INTERNAL_PORT} , hostPort: ${INTERNAL_PORT} } ]" \
      | yq r --prettyPrint - \
      > ${TMP_FILE}
    mv ${TMP_FILE} ${DAEMONSETFILE}
  done
  echo
  echo "Ambassador configuration with custom ports:"
  cat "$DAEMONSETFILE"
  echo
fi

# Create Ambassador CRDs
kubectl apply -f ./ambassador-crd.yaml

# Deploy de apiext version conversor and wait it is available
kubectl apply -f ./${APIEXTFILE}
echo "Waiting emissary-apiext available at $(date) ..."
kubectl wait --timeout=600s --for=condition=Ready pods -l app.kubernetes.io/instance=emissary-apiext -n emissary-system
echo "Emissary-apiext is availableat $(date)"
# Temporary workaround ticket1370
echo "Waiting 2 minutes more, waiting emissary-apiext is really-really available (ticket1370) at $(date) ..."
sleep 120
echo "Waiting 2 minutes done at $(date)"

# Create Ambassador RBAC
kubectl apply -f ./ambassador-rbac.yaml

# Wait for Ambassador Module CRD to exist
echo
echo "Waiting for CRD 'modules.getambassador.io' to exist..."
until [ "$(kubectl get crd | grep 'modules.getambassador.io')" != "" ]; do
  date
  echo "  Not yet, wait a little bit more..."
  sleep 1
done
echo "CRD 'modules.getambassador.io' exists!"
echo

# Create Ambassador system-wide configuration
kubectl apply -f ./ambassador-config-module.yaml

# Create Ambassador admin Service
kubectl apply -f ./ambassador-service.yaml

# Create Ambassador resolvers
sed -i \
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g" \
  ./ambassador-resolvers.yaml
kubectl apply -f ./ambassador-resolvers.yaml

# Create Ambassador DaemonSet (with or without IPFiltering) and wait it is available
# TBD: "kubectl wait" is not useful for daemonsets. If we want to wait it is
# available, a function waiting all its pods must be defined. But... do we really
# want to wait until Ambassador is up and running properly?
kubectl apply -f ./${DAEMONSETFILE}
# kubectl wait --timeout=600s --for=condition=available daemonset -lproduct=aes

# For each port enabled in the daemonset, a Listener CR must be created.
# - A 8443-listener is always opened
# - TBD: A 8080-listener should be opened? (to be done when working on issue
#        https://gitlab.com/kumori/project-management/-/issues/1083)
#   The port is indicated in INGRESS_INTERNAL_HTTP_PORT, and it is used in
#   the kuinbound configuration. But, with the new Ambassador v3.x, a listener
#   should be created. To be investigated ...
# - More tcp-listeneres (related to TCPPorts) are not required: when a tcpmapping
#   is created, Ambassador creates an ad-hoc listener in the Envoy configuration
#   (but not a ambassador listener CRD).
#
# We must take into account that the proxy protocol must be enabled when it is
# behind a balancer (for example: our Envoy balancer)
#
# NOTE:
# There is an issue in the new parameter "protocol" in listener resources, so
# the old "protocolStack" parameter is used.
# See:
# - scripts/installer/addons/ambassador-ingress-3.4.1/ambassador-listener.yaml_template
# - https://github.com/emissary-ingress/emissary/issues/4153,
# - https://github.com/emissary-ingress/emissary/issues/3979
#
if [ "${INGRESS_INTERNAL_BALANCING}" == "true" ]; then
  #HTTPS_LISTENER_PROTOCOL="HTTPSPROXY"
  HTTPS_LISTENER_PROTOCOL_STACK="[PROXY,TLS, HTTP, TCP]"

else
  #HTTPS_LISTENER_PROTOCOL="HTTPS"
  HTTPS_LISTENER_PROTOCOL_STACK="[TLS, HTTP, TCP]"
fi
rm -rf listeners
mkdir listeners
echo "Creating listeners"
cp ambassador-listener.yaml_template listeners/ambassador-listener-8443.yaml
sed -i \
  -e "s?{{PORT}}?8443?g"\
  -e "s?{{INGRESS_NAMESPACE}}?${INGRESS_NAMESPACE}?g"\
  -e "s?{{LISTENER_PROTOCOL_STACK}}?${HTTPS_LISTENER_PROTOCOL_STACK}?g"\
  -e "s?{{LISTENER_TYPE}}?HTTPS?g"\
  listeners/ambassador-listener-8443.yaml
echo
echo "Listener HTTPS"
cat listeners/ambassador-listener-8443.yaml
echo
kubectl apply -f ./listeners

# Create objects related to IP Filtering
if [ "${INSTALL_IPFILTERING}" == "true" ]; then
  kubectl apply -f ./ambassador-ipfiltering-stuff.yaml
fi

# Create Kumori OutOfService deployment and service
# OutOfService is a classic "Error 404 service unavailable" responder
kubectl apply -f ./outofservice.yaml

# Create a host just to access Emissary diagnostic web
# TBD: https://gitlab.com/kumori/project-management/-/issues/1315
CLUSTER_CORE_DOMAIN="cluster.core"
KUKUCERT_NAME="wildcard-${REFERENCE_DOMAIN//./-}"
KUKUCERT_SECRET_NAME="${CLUSTER_CORE_DOMAIN//./-}-cert-${KUKUCERT_NAME}-cert"
sed -i -e "s?{{KUKUCERT_SECRET_NAME}}?${KUKUCERT_SECRET_NAME}?g" ./diag-host.yaml
sed -i -e "s?{{RELEASE_NAME}}?${RELEASE_NAME}?g" ./diag-host.yaml
sed -i -e "s?{{REFERENCE_DOMAIN}}?${REFERENCE_DOMAIN}?g" ./diag-host.yaml
sed -i -e "s?{{KUINBOUND_MINTLSVERSION}}?${KUINBOUND_MINTLSVERSION}?g" ./diag-host.yaml
sed -i -e "s?{{KUINBOUND_MAXTLSVERSION}}?${KUINBOUND_MAXTLSVERSION}?g" ./diag-host.yaml
sed -i -e "s?{{KUINBOUND_CIPHERSUITETLS}}?${KUINBOUND_CIPHERSUITETLS}?g" ./diag-host.yaml
sed -i -e "s?{{KUINBOUND_ECDHCURVESTLS}}?${KUINBOUND_ECDHCURVESTLS}?g" ./diag-host.yaml
kubectl apply -f ./diag-host.yaml
