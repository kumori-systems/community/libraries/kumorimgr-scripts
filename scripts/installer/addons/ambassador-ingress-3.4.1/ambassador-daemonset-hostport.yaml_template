# Kumori notes:
# This content is part of https://app.getambassador.io/yaml/emissary/3.4.1/emissary-emissaryns.yaml
# But we have adapted the "kube Deployment" to "kube Daemonset"
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  labels:
    app.kubernetes.io/instance: emissary-ingress
    app.kubernetes.io/managed-by: getambassador.io
    app.kubernetes.io/name: emissary-ingress
    app.kubernetes.io/part-of: emissary-ingress
    product: aes
  name: ambassador
  namespace: {{INGRESS_NAMESPACE}}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/instance: emissary-ingress
      app.kubernetes.io/name: emissary-ingress
  template:
    metadata:
      annotations:
        consul.hashicorp.com/connect-inject: "false"
        sidecar.istio.io/inject: "false"
      labels:
        app.kubernetes.io/instance: emissary-ingress
        app.kubernetes.io/managed-by: getambassador.io
        app.kubernetes.io/name: emissary-ingress
        app.kubernetes.io/part-of: emissary-ingress
        product: aes
        profile: main
        type: ingress
    spec:
      containers:
      - name: ambassador
        image: docker.io/emissaryingress/emissary:{{AMBASSADOR_VERSION}}
        env:
        - name: AMBASSADOR_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: HOST_IP
          valueFrom:
            fieldRef:
              fieldPath: status.hostIP
        - name: AMBASSADOR_DRAIN_TIME
          value: "600"
        - name: AMBASSADOR_FAST_RECONFIGURE
          value: "true"
        - name: AMBASSADOR_FORCE_SECRET_VALIDATION
          value: "true"
        ports:
        - name: http
          containerPort: 8080
          hostPort: 80
        - name: https
          containerPort: 8443
          hostPort: {{INGRESS_INTERNAL_PORT}}
        - name: admin
          containerPort: 8877
        livenessProbe:
          httpGet:
            path: /ambassador/v0/check_alive
            port: 8877
          initialDelaySeconds: 30
          periodSeconds: 3
        readinessProbe:
          httpGet:
            path: /ambassador/v0/check_ready
            port: 8877
          initialDelaySeconds: 30
          periodSeconds: 3
        resources:
          limits:
            cpu: {{AMBASSADOR_CPU_LIMIT}}
            memory: {{AMBASSADOR_MEM_LIMIT}}
          requests:
            cpu: {{AMBASSADOR_CPU_REQUEST}}
            memory: {{AMBASSADOR_MEM_REQUEST}}
        securityContext:
          allowPrivilegeEscalation: false
        volumeMounts:
        - mountPath: /tmp/ambassador-pod-info
          name: ambassador-pod-info
      dnsPolicy: ClusterFirstWithHostNet
      nodeSelector:
        kumori/ingress: "true"
      hostNetwork: false
      imagePullSecrets: []
      restartPolicy: Always
      securityContext:
        runAsUser: 8888
      serviceAccountName: emissary-ingress
      terminationGracePeriodSeconds: 30
      volumes:
      - downwardAPI:
          items:
          - fieldRef:
              fieldPath: metadata.labels
            path: labels
        name: ambassador-pod-info
  updateStrategy:
    type: RollingUpdate
