= CAPI Installation manifest generation

== ClusterAPI manifests

The recommended ClusterAPI installation procedure is to install ClusterAPI CLI
`clusterctl` and run the `init` command, that will take care to install all the
necessary material in the bootstrap cluster.

This method does not suit our need for including ClusterAPI installation in
Kumori clusters.

== Previous step: CertManager installation

ClusterAPI CLI `init` takes care of installing CertManager, but the `generate`
command does not have an option for creating the manifests required to install
CertManager, so we need to install it manually.

CertManager installation manifests can be retrieved from:

https://github.com/cert-manager/cert-manager/releases/download/v1.13.3/cert-manager.yaml



== ClusterAPI CLI installation

Just for reference, ClusterAPI CLI, `clusterctl` can be installed with the
following commands:

[%autofit]
----
#!/bin/bash

CLUSTERAPI_VERSION="1.8.4"
BIN_DIR="${HOME}/bin"

# Download and install ClusterAPI CLI (clusterctl)
wget -O clusterctl-${CLUSTERAPI_VERSION} https://github.com/kubernetes-sigs/cluster-api/releases/download/v${CLUSTERAPI_VERSION}/clusterctl-linux-amd64
chmod +x clusterctl-${CLUSTERAPI_VERSION}
mv clusterctl-${CLUSTERAPI_VERSION} ${BIN_DIR}
----


== `clusterctl generate` command

ClusterAPI CLI offer a `generate` command that will create the manifests to
install ClusterAPI Core stuff (CRDs, Controllers, etc.) as well as any selected
providers.

We will generate the manifests for the following elements and versions:

* *Core*: cluster-api v1.8.4
* *Bootstrap*: kubeadm v1.8.4
* *Control-plane*: kubeadm v1.8.4
* *Infrastructure*: openstack v0.10.5
* *Infrastructure*: aws v2.6.1

[%autofit]
----
clusterctl-1.8.4 generate provider --core cluster-api:v1.8.4 --write-to manifests-cluster-api-v1.8.4.yaml
clusterctl-1.8.4 generate provider --bootstrap kubeadm:v1.8.4 --write-to manifests-bootstrap-kubeadm-v1.8.4.yaml
clusterctl-1.8.4 generate provider --control-plane kubeadm:v1.8.4 --write-to manifests-control-plane-kubeadm-v1.8.4.yaml
clusterctl-1.8.4 generate provider --infrastructure openstack:v0.10.5 --write-to manifests-infrastructure-openstack-v0.10.5.yaml
clusterctl-1.8.4 generate provider --infrastructure aws:v2.6.1 --write-to manifests-infrastructure-aws-v2.6.1.yaml
----

NOTE: we change the manifests of the AWS provider with `ExternalResourceGC=true`, to allow the controller to remove
the Ingress LoadBalancer Service created by the AWS cloud controller.