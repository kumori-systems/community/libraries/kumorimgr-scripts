#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

echo
echo -e $COL_BLUE"* Uploading files to remote machine ${IP}..."$COL_DEFAULT
scp -i ${SSH_KEY} -r ${LOCAL_WORKDIR} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}
scp -i ${SSH_KEY} -r ${COMMON_DIR}/* ${SSH_USER}@${IP}:${REMOTE_WORKDIR}
[[ -n "${CUSTOM_DIR}" ]] && scp -i ${SSH_KEY} -r ${CUSTOM_DIR} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/custom
[[ -n "${REFERENCE_DOMAIN_CERT_DIR}" ]] && scp -i ${SSH_KEY} -r ${REFERENCE_DOMAIN_CERT_DIR} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/clusterCerts
ssh -i ${SSH_KEY} ${SSH_USER}@${IP} "mkdir -p ${REMOTE_WORKDIR}/kumoriPki/rootCA"
[[ -n "${CLUSTER_PKI_ROOT_CA_FILE}" ]] && scp -i ${SSH_KEY} -r ${CLUSTER_PKI_ROOT_CA_FILE} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/kumoriPki/rootCA
if [ "${MONITORING_REQUIRE_CLIENTCERT}" == "true" ]; then
  ssh -i ${SSH_KEY} ${SSH_USER}@${IP} "mkdir -p ${REMOTE_WORKDIR}/kumoriPki/monitoringCA"
  [[ -n "${MONITORING_CLIENTCERT_TRUSTED_CA_FILE}" ]] && scp -i ${SSH_KEY} -r ${MONITORING_CLIENTCERT_TRUSTED_CA_FILE} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/kumoriPki/monitoringCA
fi
if [ "${PROMETHEUS_REMOTE_AUTH_TYPE}" == "clientcertificate" ]; then
  ssh -i ${SSH_KEY} ${SSH_USER}@${IP} "mkdir -p ${REMOTE_WORKDIR}/kumoriPki/monitoringClientCert"
  [[ -n "${PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_CERT_FILE}" ]] && scp -i ${SSH_KEY} -r ${PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_CERT_FILE} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/kumoriPki/monitoringClientCert
  [[ -n "${PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_KEY_FILE}" ]] && scp -i ${SSH_KEY} -r ${PROMETHEUS_REMOTE_CLIENT_CERTIFICATE_KEY_FILE} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/kumoriPki/monitoringClientCert
fi
if [ "${ADMISSION_AUTHENTICATION_TYPE}" == "clientcertificate" ]; then
  ssh -i ${SSH_KEY} ${SSH_USER}@${IP} "mkdir -p ${REMOTE_WORKDIR}/kumoriPki/admission"
  scp -i ${SSH_KEY} -r ${ADMISSION_SERVER_CERT_DIR}/* ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/kumoriPki/admission
fi
echo -e $COL_BLUE"* Upload finished."$COL_DEFAULT
echo

# Upload AWS credentials only if necessary
if [ "${MANAGED_DNS}" = "true" ] && [ "${MANAGED_DNS_PROVIDER}" = "route53" ]; then
  echo
  echo -e $COL_BLUE"* Uploading AWS configuration for Ingress DNS registration..."$COL_DEFAULT
  scp -i ${SSH_KEY} -r ${MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/awsConfiguration
  echo -e $COL_BLUE"* Upload finished."$COL_DEFAULT
  echo
fi

# Upload OVH credentials only if necessary
if [ "${MANAGED_DNS}" = "true" ] && [ "${MANAGED_DNS_PROVIDER}" = "ovh" ]; then
  echo
  echo -e $COL_BLUE"* Uploading OVH configuration for Ingress DNS registration..."$COL_DEFAULT
  ssh -i ${SSH_KEY} ${SSH_USER}@${IP} "mkdir -p ${REMOTE_WORKDIR}/ovhConfiguration"
  scp -i ${SSH_KEY} ${MANAGED_DNS_OVH_CONFIG_FILE} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/ovhConfiguration/ovh-dns-credentials.ini
  echo -e $COL_BLUE"* Upload finished."$COL_DEFAULT
  echo
fi

# Upload Openstack cloud credentials only if necessary. File will be renamed to 'clouds.yaml'.
if [ "${INSTALL_OPENSTACK_CLOUD_CONTROLLER}" = "true" ]; then
  echo
  echo -e $COL_BLUE"* Uploading Openstack cloud credentials..."$COL_DEFAULT
  ssh -i ${SSH_KEY} ${SSH_USER}@${IP} "mkdir -p ${REMOTE_WORKDIR}/openstack-cloud-credentials"
  scp -i ${SSH_KEY} ${OPENSTACK_CLOUD_CONTROLLER_CLOUDS_YAML_FILE} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/openstack-cloud-credentials/clouds.yaml
  echo -e $COL_BLUE"* Upload finished."$COL_DEFAULT
  echo
fi

echo
echo -e $COL_BLUE"* Executing remote installer - PHASE 1..."$COL_DEFAULT
${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} << EOF
 cd ${REMOTE_WORKDIR}
 echo >> variables.sh
 echo "NODE_IP=\"$IP\"" >> variables.sh
 chmod +x *.sh
 ./masters_0_install-base.sh
 ./masters_9_keepalived-enable.sh
EOF
echo -e $COL_BLUE"* Installer PHASE 1 finished."$COL_DEFAULT
echo

echo
echo -e $COL_BLUE"* Determining if a reboot is required after PHASE 1..."$COL_DEFAULT
CMD_CHEK_REBOOT="[ -f /var/run/reboot-required ] && echo 'REBOOT REQUIRED'"
RES="$(ssh -i ${SSH_KEY} ${SSH_USER}@${IP} \(${CMD_CHEK_REBOOT}\))"
if [ "${RES}" == "REBOOT REQUIRED" ]; then
  REBOOT_REQUIRED="true"
  echo "Reboot is required."
else
  REBOOT_REQUIRED="false"
  echo "Reboot is not required."
fi

if [ "${REBOOT_REQUIRED}" == "true" ]; then
  echo
  echo -e $COL_BLUE"* Rebooting remote machine..."$COL_DEFAULT
  # Do not indent!
  ${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} << EOF
{ sleep 5; sudo shutdown -r 0; } >/dev/null &
EOF
  echo
  echo -e $COL_BLUE"* Waiting for remote machine to reboot..."$COL_DEFAULT
  sleep 30s
  while ! timeout -k 5 10 ${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} uptime; do
    echo "Still unavailable..."
    sleep 5
  done
  echo -e $COL_BLUE"* Remote machine is back online, wait a minute for everything to be up..."$COL_DEFAULT
  sleep 30s
fi

echo
echo -e $COL_BLUE"* Executing remote installer - PHASE 2..."$COL_DEFAULT
${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} << EOF
 cd ${REMOTE_WORKDIR}
 ./primary-master_1_init-cluster.sh
EOF
echo -e $COL_BLUE"* Installer PHASE 2 finished."$COL_DEFAULT
echo


echo
echo -e $COL_BLUE"* Retrieving cluster join scripts from remote machine..."$COL_DEFAULT
rm -rf ${JOIN_SCRIPTS_DIR} && mkdir -p ${JOIN_SCRIPTS_DIR}
scp -i ${SSH_KEY} -r ${SSH_USER}@${IP}:/tmp/join-*.sh ${JOIN_SCRIPTS_DIR}
echo -e $COL_BLUE"* Retrieved join scripts."$COL_DEFAULT
echo
