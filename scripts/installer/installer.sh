#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"

COMMON_DIR="${LOCAL_WORKDIR}/../common"

# Load install configuration variables from file
source "${COMMON_DIR}/defs-colors.sh"
source "${LOCAL_WORKDIR}/variables.sh"


################################################################
##         COMMUNITY-SPECIFIC CONFIGURATION CHECK             ##
################################################################
source "${COMMON_DIR}/preflight-checks/community-specific.sh"

# Call specific installer script for multi-node or single-node cluster
if [[ "${#MASTERS_IPS[@]}" -gt 1 || "${#WORKERS_IPS[@]}" -gt 0 ]]; then
  source ${LOCAL_WORKDIR}/multi-node-installer.sh
else
  source ${LOCAL_WORKDIR}/single-node-installer.sh
fi
