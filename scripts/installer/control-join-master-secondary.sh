#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

echo
echo -e $COL_BLUE"* Uploading cluster join scripts to remote machine..."$COL_DEFAULT
scp -i ${SSH_KEY} ${JOIN_SCRIPTS_DIR}/join-masters.sh ${SSH_USER}@${IP}:/tmp
echo -e $COL_BLUE"* Uploaded join scripts."$COL_DEFAULT
echo


echo
echo -e $COL_BLUE"* Executing remote installer - PHASE 2..."$COL_DEFAULT
${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} << EOF
 cd ${REMOTE_WORKDIR}
 ./masters_1_join-cluster.sh
EOF
echo -e $COL_BLUE"* Installer PHASE 2 finished."$COL_DEFAULT
echo



