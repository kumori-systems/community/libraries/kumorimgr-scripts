= Patch 1 for Kumori Cluster v0.2.x

Released on 11/01/2021

== Features

This patch adds full support for using TCP ports for Ingress in Kumori Cluster.

== Changes

The patching process will perform the following changes in the cluster:

* Install new Kumori CRDs resources:
  ** `tcpport` for managing TCP port resources reservations
  ** `tcpinbound` for deploying TCP Inbound services
* Grant permissions to Kumori Controllers for managing the new CRDs
* Modify Admission service configuration to include a list of the cluster TCP ports
* Update the following Kumori Controllers:
  ** Admission (to version 0.2.12)
  ** Kuinbound (to version 0.4.0)
  ** Kucontroller (to version 0.1.24)
  

== Installation process

1. Upload the patch tar file to a Master node in the cluster
2. Extract the files: `tar xzf <patch-file.tgz>`
3. Run `patch-installer.sh` script

