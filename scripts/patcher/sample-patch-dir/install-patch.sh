#!/bin/bash

SCRIPT_DIR="$(dirname $(readlink -f $0))"

PROVISION="${SCRIPT_DIR}/provision"

# To use values from the cluster configuration, uncomment the following line
# source "${SCRIPT_DIR}/../variables.sh"

# To use colors codes in echo uncomment the following line
# source "${SCRIPT_DIR}/../defs-colors.sh"


###############################################################################
##  ADD NEW KUMORI CRDs TO KUBERNETES                                        ##
###############################################################################
echo
echo "Adding new Kumori CRDs..."
kubectl apply -f "${PROVISION}/kuku-tcpinbound-crd.yaml"
kubectl apply -f "${PROVISION}/kuku-tcpport-crd.yaml"
echo "Done."
echo


###############################################################################
##  ADDING PERMISSIONS FOR MANAGING NEW CRDs TO KUMORI CONTROLLERS           ##
###############################################################################
echo
echo "Adding permissions to manage new CRDs to Kumori Controllers..."
kubectl apply -f "${PROVISION}/rbac-admission.yaml"
kubectl apply -f "${PROVISION}/rbac-kuinbound.yaml"
echo "Done."
echo


###############################################################################
##  UPDATE ADMISSION CONFIGURATION: ADD NEW 'tcpports' PROPERTY              ##
###############################################################################
ADMISSION_CONFIG_SECRET="admission-config-secret"

# Determine Ingress TCP ports configured in the cluster (could also be set in variables)
if [ -z "${INGRESS_TCP_PORTS}" ]; then
  echo
  echo "Determining Ingress TCP ports configured in the cluster..."
  PORT_LIST=$(kubectl -n kumori get daemonsets.apps ambassador -oyaml \
    | grep -E 'name: tcp[0-9]*$' \
    | sed -e 's/.*name:\s*tcp\([0-9]*\)$/\1/g')
  INGRESS_TCP_PORTS=( $PORT_LIST )
  echo "Current Ingress TCP ports: ${INGRESS_TCP_PORTS[*]}"
  echo
fi

echo
echo "Retrieving current Admission configuration..."
# Get current Admission configuration from Kubernetes secret
ADMISSION_CONFIG=$(kubectl -n kumori get secret ${ADMISSION_CONFIG_SECRET} -ojson \
  | jq -r '.data["admission-server-config.json"]' \
  | base64 --decode)

echo "Preparing new configuration..."
# Add a new 'tcpports' property in admission configuration with an empty array
NEW_ADMISSION_CONFIG=$(echo ${ADMISSION_CONFIG} | jq ".admission.tcpports = []")
# Add each Ingress TCP port to the new 'tcpports' array
for PORT in ${INGRESS_TCP_PORTS[@]}; do
  NEW_ADMISSION_CONFIG=$(echo ${NEW_ADMISSION_CONFIG} | jq ".admission.tcpports += [ ${PORT} ]")
done

echo
echo "Applying new configuration to Admission:"
echo "${NEW_ADMISSION_CONFIG}"
echo

# Patch Admission configuration secret with the new configuration (base64 encoded)
NEW_ENCODED_CONFIG=$(echo -n ${NEW_ADMISSION_CONFIG} | base64 -w 0)
kubectl -n kumori patch secret ${ADMISSION_CONFIG_SECRET} \
  -p "{\"data\":{\"admission-server-config.json\":\"${NEW_ENCODED_CONFIG}\"}}"

echo
echo "Done."
echo


###############################################################################
##  UPDATE KUMORI CONTROLLERS VERSIONS                                       ##
###############################################################################

# Determine Kumori image registry in use in the cluster (could also be set in variables)
if [ -z "${KUMORI_IMAGES_REGISTRY}" ]; then
  echo
  echo "Determining Kumori image registry in use in the cluster..."
  KUMORI_IMAGES_REGISTRY=$(kubectl -n kumori get deployment admission -oyaml | grep -E 'image:\s*.*/admission:v' | sed -e 's|.*image:\s*\(.*\)/admission:v[.0-9]*$|\1|g')
  echo "Using Kumori image registry: ${KUMORI_IMAGES_REGISTRY}"
  echo
fi

echo
echo "Updating Kumori Controllers versions..."
kubectl -n kumori set image deployment/admission admission=${KUMORI_IMAGES_REGISTRY}/admission:v0.2.12
kubectl -n kumori set image deployment/kuinbound kuinbound=${KUMORI_IMAGES_REGISTRY}/kuinbound:v0.4.0
kubectl -n kumori set image deployment/kucontroller kucontroller=${KUMORI_IMAGES_REGISTRY}/kucontroller:v0.1.24

echo
echo "Done."
echo
