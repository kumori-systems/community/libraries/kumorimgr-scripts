#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"

COMMON_DIR="${LOCAL_WORKDIR}/../common"

# Load install configuration variables from file
source "${COMMON_DIR}/defs-colors.sh"
source "${LOCAL_WORKDIR}/variables.sh"


# Timestamp label to use in files, traces, etc.
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

SSH_COMMAND="ssh -o ServerAliveInterval=15 -o ServerAliveCountMax=4"

REMOTE_WORKDIR=".kumori/Kumori_patcher_${TIMESTAMP}"


################################################################################
##  VALIDATE NECESSARY CONFIGURATION HAS BEEN PROVIDED                        ##
################################################################################
# Validate that a list of MASTER nodes has been provided
if [[ "${#MASTERS_IPS[@]}" -eq 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - MASTERS_IPS can't be empty."$COL_DEFAULT
  echo
  exit -1
fi

# Validate a directory for the patch has been set
if [[ -z "${PATCH_DIR}" ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - No patch directory specified."$COL_DEFAULT
  echo
  exit -1
fi

# Validate the patch directory contains the expected install script
if [[ ! -f "${PATCH_DIR}/install-patch.sh" ]]; then
  echo
  echo -e $COL_RED_BOLD"WRONG CONFIGURATION:"$COL_DEFAULT
  echo -e $COL_RED_BOLD" - Patch doesn't have an 'install-patch.sh' script (required)."$COL_DEFAULT
  echo
  exit -1
fi

# TODO:
# For patches, we might check the directory contains the expected files, for example:
#  - a "manifest" file (patch-info.txt) woth patch detail we can print before applying it (for confirmation)


################################################################################
##  SELECT A MASTER NODE TO PERFORM THE PATCH INSTALLATION FROM               ##
################################################################################
# We just select the first master on the list
MASTER_IP="${MASTERS_IPS[0]}"



echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                      CLUSTER PATCH INSTALLER                               ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## PATCH DIRECTORY : ${PATCH_DIR}                                               "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## MASTER IP (for patch installation): ${MASTER_IP}                             "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
if [[ -f "${PATCH_DIR}/patch-info.txt" ]]; then
  echo -e $COL_GREEN"## PATCH INFORMATION:                                                           "$COL_DEFAULT
  cat "${PATCH_DIR}/patch-info.txt"
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
else
  echo -e $COL_GREEN"## PATCH CONTAINS NO INFORMATION FILE.                                          "$COL_DEFAULT
fi
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT


# Prepare directory for storing remote installations output
LOGS_DIR="${LOCAL_WORKDIR}/logs_patcher_${TIMESTAMP}"
mkdir -p "${LOGS_DIR}"


################################################################################
##                PRE-FLIGHT CHECKS: SSH ACCESS TARGET HOSTS                  ##
################################################################################
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" *  Pre-Flight check: SSH access to Master machine...   "$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT

echo  -e $COL_BLUE"* Check SSH connectivity and refresh hosts fingerprints"$COL_DEFAULT
UNREACHABLE=()
ssh-keyscan ${MASTER_IP} 2>&1 | grep -vE "^$|route|getaddrinfo" > /dev/null
[ $? != 0 ] && UNREACHABLE+=(${MASTER_IP})
if [[ "${#UNREACHABLE[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: SSH connectivity problem with the following machines:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${UNREACHABLE[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


echo  -e $COL_BLUE"* Add hosts SSH fingerprints"$COL_DEFAULT
ssh-keygen -R ${MASTER_IP} >/dev/null 2>&1
ssh-keyscan -H ${MASTER_IP} >> ${HOME}/.ssh/known_hosts 2>/dev/null
echo
echo -e $COL_GREEN"Passed."$COL_DEFAULT
echo


echo  -e $COL_BLUE"* Check user can connect via SSH"$COL_DEFAULT
FAILCONNECT=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${MASTER_IP} uptime 2> /dev/null)"
[ "${RES}" == "" ] && FAILCONNECT+=(${MASTER_IP})
if [[ "${#FAILCONNECT[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED_BOLD"Pre-flight error: Couldn't connect via SSH to the folowing machines:"$COL_DEFAULT
  echo -e $COL_RED_BOLD"                  ${FAILCONNECT[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo -e $COL_GREEN"Passed."$COL_DEFAULT
  echo
fi


################################################################################
##                     START REMOTE PATCH INSTALLATION                        ##
################################################################################
OUTPUT_FILE="${LOGS_DIR}/master_${MASTER_IP}.log"
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" *  Running patch in MASTER node in ${MASTER_IP}...     "$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
. ${LOCAL_WORKDIR}/control-apply-patch.sh \
  |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
  |& tee -a ${OUTPUT_FILE} \
  |& sed "s/^/$(printf "[${BACKGROUND_COLORS[0]}MASTER ${MASTER_IP}$COL_DEFAULT]") /"

echo
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                      CLUSTER PATCHING COMPLETED                            ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo

