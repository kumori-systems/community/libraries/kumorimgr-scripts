#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

echo
echo -e $COL_BLUE"* Uploading files to remote machine ${IP}..."$COL_DEFAULT
scp -i ${SSH_KEY} -r ${LOCAL_WORKDIR} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}
scp -i ${SSH_KEY} -r ${COMMON_DIR}/* ${SSH_USER}@${IP}:${REMOTE_WORKDIR}
[[ -n "${CUSTOM_DIR}" ]] && scp -i ${SSH_KEY} -r ${CUSTOM_DIR} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/custom
echo -e $COL_BLUE"* Upload finished."$COL_DEFAULT
echo

# Upload AWS credentials only if necessary
if [ "${MANAGED_DNS}" = "true" ] && [ "${MANAGED_DNS_PROVIDER}" = "route53" ]; then
  echo
  echo -e $COL_BLUE"* Uploading AWS configuration for Ingress DNS registration..."$COL_DEFAULT
  scp -i ${SSH_KEY} -r ${MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR} ${SSH_USER}@${IP}:${REMOTE_WORKDIR}/awsConfiguration
  echo -e $COL_BLUE"* Upload finished."$COL_DEFAULT
  echo
fi

echo
echo -e $COL_BLUE"* Executing remote node clean-up commands..."$COL_DEFAULT
${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} << EOF
 cd ${REMOTE_WORKDIR}
 chmod +x *.sh
 ./masters_3_clean-hard.sh  mockValue
 cd -
 { sleep 5; sudo shutdown -r 0; } >/dev/null &
EOF
echo
echo -e $COL_BLUE"* Cluster node clean-up finished."$COL_DEFAULT
echo

echo -e $COL_BLUE"* Waiting for remote machine to reboot..."$COL_DEFAULT
sleep 30s
# while ! ping -c 1 ${IP} &>/dev/null; do :; done
while ! timeout -k 5 10 ${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} uptime; do
  echo "Still unavailable..."
  sleep 5
done
echo -e $COL_BLUE"* Remote machine is back online. "$COL_DEFAULT

echo
echo -e $COL_BLUE"* Machine is clean and ready to be reused."$COL_DEFAULT
echo
