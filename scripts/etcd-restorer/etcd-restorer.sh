#!/bin/bash

################################################################
##   MOCK ETCD-RESTORE SCRIPT FOR COMMUNITY VERSION           ##
################################################################

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"

COMMON_DIR="${LOCAL_WORKDIR}/../common"

# Load install configuration variables from file
source "${COMMON_DIR}/defs-colors.sh"
source "${LOCAL_WORKDIR}/variables.sh"

echo
echo -e $COL_RED_BOLD"Restore command is not supported in Community version."$COL_DEFAULT
echo
exit -1
