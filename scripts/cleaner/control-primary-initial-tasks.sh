#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

echo
echo -e $COL_BLUE"* Downloading kubeConfig from remote machine ${IP}..."$COL_DEFAULT
scp -i ${SSH_KEY} ${SSH_USER}@${IP}:.kube/config ${LOCAL_WORKDIR}/kubeConfig
echo -e $COL_BLUE"* Download finished."$COL_DEFAULT
echo

echo
echo -e $COL_BLUE"* Executing remote Kubernetes namespaces deletion..."$COL_DEFAULT
${SSH_COMMAND} -i ${SSH_KEY} ${SSH_USER}@${IP} << EOF
 # kubectl delete namespace kumori
 # kubectl delete namespace monitoring
 # kubectl delete namespace minio
 # kubectl delete namespace keycloak
 # kubectl delete namespace kubernetes-dashboard
EOF
echo
echo -e $COL_BLUE"* Kubernetes namespaces deletion finished."$COL_DEFAULT
echo


# For reference:  HOW TO DEAL WITH NAMESPACE DELETIONS THAT GET STUCK
#
# The reason they get stuck is typically some trouble with finalizers, that
# get too long to finish or end up in some race condition.
#
# The trick is to remove the finalizers by editing the Namespace definition
# and saving it (although it might not be possible) or sending it directly to
# the APIServer with (in JSON format):
#
#   curl -X PUT \
#     -H "Content-Type: application/json" \
#     --data-binary @temp.json \
#     http://<api-server-url>/api/v1/namespaces/<namespace>/finalize
#
# An example one-liner:
#
#   kubectl get namespace "$NAMESPACE" -o json \
#     | jq 'del(.spec.finalizers[] | select("kubernetes"))' \
#     | curl -s -k \
#       -H "Content-Type: application/json" \
#       -X PUT \
#       -o /dev/null \
#       --data-binary @- \
#       http://<api-server-url>/api/v1/namespaces/$NAMESPACE/finalize \
#     && echo "Killed namespace: $NAMESPACE"
