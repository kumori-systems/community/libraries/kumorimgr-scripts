#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################

LOCAL_WORKDIR="$(dirname $(readlink -f $0))"

COMMON_DIR="${LOCAL_WORKDIR}/../common"

# Load install configuration variables from file
source "${COMMON_DIR}/defs-colors.sh"
source "${LOCAL_WORKDIR}/variables.sh"


echo -e $COL_RED"################################################################################"$COL_DEFAULT
echo -e $COL_RED"##                                  WARNING                                   ##"$COL_DEFAULT
echo -e $COL_RED"################################################################################"$COL_DEFAULT
echo
echo -e $COL_RED"This script is a Work In Progress and must not be used at the moment. Use clean-hard scripts instead."$COL_DEFAULT
echo
exit -1


# Timestamp label to use in files, traces, etc.
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

SSH_COMMAND="ssh -o ServerAliveInterval=15 -o ServerAliveCountMax=4"

REMOTE_WORKDIR=".kumori/Kumori_cleaner_${TIMESTAMP}"

################################################################################
##  PREPARE ASSOCIATION OF EACH MACHINE IP WITH A COLOR FOR CLEARER LOGGING   ##
################################################################################
declare -A IP_COLORS

i=0
if [[ -n "${SUPER_MASTER_IP}" ]]; then
  IP_COLORS[$SUPER_MASTER_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
fi

for MASTER_IP in ${MASTERS_IPS[@]}; do
  IP_COLORS[$MASTER_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done
i=1
for WORKERS_IP in ${WORKERS_IPS[@]}; do
  IP_COLORS[$WORKERS_IP]=${BACKGROUND_COLORS[$i]}
  ((i++))
done


echo -e $COL_YELLOW"################################################################################"$COL_DEFAULT
echo -e $COL_YELLOW"##                  CLUSTER CLEANER (WILL RESET ALL NODES)                    ##"$COL_DEFAULT
echo -e $COL_YELLOW"################################################################################"$COL_DEFAULT
echo -e $COL_YELLOW"##                                                                              "$COL_DEFAULT
echo -e $COL_YELLOW"## NODES TO CLEAN:                                                              "$COL_DEFAULT
echo -e $COL_YELLOW"##                                                                              "$COL_DEFAULT
echo -e $COL_YELLOW"##  - Primary master IP : ${SUPER_MASTER_IP}                                    "$COL_DEFAULT
echo -e $COL_YELLOW"##  - Masters IPs       : ${MASTERS_IPS[*]}                                     "$COL_DEFAULT
echo -e $COL_YELLOW"##  - Workers IPs       : ${WORKERS_IPS[*]}                                     "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                               "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                               "$COL_DEFAULT
if [ "${MANAGED_DNS}" = "true" ]; then
  echo -e $COL_GREEN"## CLUSTER DNS CONFIGURATION:                                                   "$COL_DEFAULT
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Reference domain : ${REFERENCE_DOMAIN}                                    "$COL_DEFAULT
  echo -e $COL_GREEN"##  - ApiServer domain : ${APISERVER_DOMAIN}                                    "$COL_DEFAULT
  echo -e $COL_GREEN"##  - Ingress domain   : ${INGRESS_DOMAIN}                                    "$COL_DEFAULT
  echo -e $COL_GREEN"##  - DNS Provider     : ${MANAGED_DNS_PROVIDER}                                  "$COL_DEFAULT
  if [ "${MANAGED_DNS_PROVIDER}" = "route53" ]; then
    echo -e $COL_GREEN"##  - AWS config dir : ${MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR}                    "$COL_DEFAULT
    echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
  fi
  echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
fi
echo -e $COL_GREEN"#################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                               "$COL_DEFAULT
echo -e $COL_GREEN"##  - Custom installations from:          ${CUSTOM_DIR}                          "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                               "$COL_DEFAULT
echo -e $COL_GREEN"#################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"#################################################################################"$COL_DEFAULT
echo
echo -e $COL_YELLOW"**  THIS WILL DESTROY ALL DATA IN SELECTED NODES  **"$COL_DEFAULT
echo
read -rsp $'--> Press any key to begin complete nodes clean-up...\n' -n1 key
echo
echo
echo -e $COL_RED_BOLD"################################################################################"$COL_DEFAULT
echo -e $COL_RED_BOLD"##       ARE YOU REALLY SURE YOU WANT TO COMPLETELY RESET THE NODES?          ##"$COL_DEFAULT
echo -e $COL_RED_BOLD"################################################################################"$COL_DEFAULT
echo
read -rsp $'--> Press any key to confirm data destruction...\n' -n1 key
echo


## ################################################################################
## ##            INSTALL 'MOREUTILS' PACKAGE ON LOCAL MACHINE                    ##
## ################################################################################
## echo  -e $COL_GREEN" ************************************************************"$COL_DEFAULT
## echo  -e $COL_GREEN" *  Install 'moreutils' package for log tracing timestamps..."$COL_DEFAULT
## echo  -e $COL_GREEN" ************************************************************"$COL_DEFAULT
## sudo apt-get update -q && sudo apt-get install moreutils -y -q

# Prepare directory for storing remote installations output
LOGS_DIR="${LOCAL_WORKDIR}/logs_cleaner_${TIMESTAMP}"
mkdir -p "${LOGS_DIR}"


################################################################################
##          PRE-FLIGHT CHECKS: ALL NECESSARY FILE ARE ACCESSIBLE              ##
################################################################################
echo  -e $COL_GREEN" *******************************************************·"$COL_DEFAULT
echo  -e $COL_GREEN" *  Pre-Flight check: Access to all installation files..."$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************·"$COL_DEFAULT

NOT_ACCESSIBLE="$(find ${LOCAL_WORKDIR} \( -type d -not -executable \) -o \( -type f -not -readable \) 2> /dev/null)"
if [ "${NOT_ACCESSIBLE}" != "" ]; then
  echo
  echo -e $COL_RED"Pre-flight error: Some required files or directories are not accessible. Check their permissions (recommended) or run the script as root."$COL_DEFAULT
  echo
  echo -e $COL_RED"${NOT_ACCESSIBLE}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo "Done."
  echo
fi

################################################################################
##                PRE-FLIGHT CHECKS: SSH ACCESS TARGET HOSTS                  ##
################################################################################
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" *  Pre-Flight check: SSH access to all machines...     "$COL_DEFAULT
echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT

ALL_IPS=("${SUPER_MASTER_IP}" "${MASTERS_IPS[@]}" "${WORKERS_IPS[@]}")


echo  -e $COL_BLUE"* Remove hosts fingerprints from known hosts list"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ssh-keygen -R ${IP} >/dev/null 2>&1
done
echo
echo "Done."
echo


echo  -e $COL_BLUE"* Check SSH connectivity and refresh hosts fingerprints"$COL_DEFAULT

UNREACHABLE=()
for IP in ${ALL_IPS[@]}; do
  ssh-keyscan ${IP} 2>&1 | grep -vE "^$|route|getaddrinfo" > /dev/null
  [ $? != 0 ] && UNREACHABLE+=(${IP})
done
if [[ "${#UNREACHABLE[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED"Pre-flight error: SSH connectivity problem with the following machines:"$COL_DEFAULT
  echo -e $COL_RED"                  ${UNREACHABLE[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo "Done."
  echo
fi


echo  -e $COL_BLUE"* Add hosts SSH fingerprints"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ssh-keyscan -H ${IP} >> ${HOME}/.ssh/known_hosts 2>/dev/null
done
echo
echo "Done."
echo


echo  -e $COL_BLUE"* Check user can connect via SSH"$COL_DEFAULT

FAILCONNECT=()
CONN_FLAGS="-o ConnectTimeout=10 -o ConnectionAttempts=1"
for IP in ${ALL_IPS[@]}; do
  RES="$(${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} uptime 2> /dev/null)"
  [ "${RES}" == "" ] && FAILCONNECT+=(${IP})
done
if [[ "${#FAILCONNECT[@]}" -ne 0 ]]; then
  echo
  echo -e $COL_RED"Pre-flight error: Couldn't connect via SSH to the folowing machines:"$COL_DEFAULT
  echo -e $COL_RED"                  ${FAILCONNECT[*]}"$COL_DEFAULT
  echo
  exit -1
else
  echo
  echo "Done."
  echo
fi

# Create .kumori directory in all remote machines
echo  -e $COL_BLUE"* Creating .kumori directory in remote machines"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} mkdir -p .kumori &
done
wait

# Deactivate login welcome info
echo  -e $COL_BLUE"* Deactivating login welcome info in remote machines"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} touch .hushlogin &
done
wait

################################################################################
##                        START REMOTE INSTALLATION                           ##
################################################################################

# Remove all addons (or most of them) so node drainings don't take too long.
# Also, retrieve kubeConfig file from Super-Master machine.
# The necessary kubectl commands can't be run from workers, since their kubectls are not
# configured. We will configure then for the removal procedure.
IP="${SUPER_MASTER_IP}"
OUTPUT_FILE="${LOGS_DIR}/supermaster_${IP}.log"
echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
echo  -e $COL_GREEN" *  Retrieving kubeConfig from PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
echo  -e $COL_GREEN" *************************************************************"$COL_DEFAULT
. ${LOCAL_WORKDIR}/control-primary-initial-tasks.sh \
  |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
  |& tee -a ${OUTPUT_FILE} \
  |& sed "s/^/$(printf "[${IP_COLORS[$IP]}SUPER-MASTER ${IP}$COL_DEFAULT]") /" &

  wait

for WORKER_IP in ${WORKERS_IPS[@]}; do
  IP="${WORKER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/clean_worker_${IP}.log"
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" * Cleaning-up WORKER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-clean-worker.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}WORKER ${IP}$COL_DEFAULT]") /" &
done

wait

for MASTER_IP in ${MASTERS_IPS[@]}; do
  IP="${MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/clean_master_${IP}.log"
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" * Cleaning-up MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-clean-master-secondary.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}MASTER ${IP}$COL_DEFAULT]") /" &
done

wait

# In this script receives parameter "exclude-supermaster", then supermaster
# node must not be cleaned
if [[ -z "$1" || $1 != 'exclude-supermaster' ]]; then
  IP="${SUPER_MASTER_IP}"
  OUTPUT_FILE="${LOGS_DIR}/clean_supermaster_${IP}.log"
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  echo  -e $COL_GREEN" * Cleaning-up PRIMARY MASTER node in ${IP}..."$COL_DEFAULT
  echo  -e $COL_GREEN" *******************************************************"$COL_DEFAULT
  . ${LOCAL_WORKDIR}/control-clean-master-primary.sh \
    |& stdbuf -oL gawk '{ print strftime("[%d-%m-%Y %H:%M:%S]"), $0 }' \
    |& tee -a ${OUTPUT_FILE} \
    |& sed "s/^/$(printf "[${IP_COLORS[$IP]}WORKER ${IP}$COL_DEFAULT]") /" &

  wait
fi


# Reactivate login welcome info
echo  -e $COL_BLUE"* Reactivating login welcome info in remote machines"$COL_DEFAULT

for IP in ${ALL_IPS[@]}; do
  ${SSH_COMMAND} ${CONN_FLAGS} -i ${SSH_KEY} ${SSH_USER}@${IP} rm -f .hushlogin &
done
wait

echo
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                       NODE CLEANUP COMPLETED                               ##"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"## THE FOLLOWING NODES WERE CLEANED:                                            "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"##  - Primary Masters IP   : ${SUPER_MASTER_IP}                                 "$COL_DEFAULT
echo -e $COL_GREEN"##  - Masters IPs          : ${MASTERS_IPS[*]}                                  "$COL_DEFAULT
echo -e $COL_GREEN"##  - Workers IPs          : ${WORKERS_IPS[*]}                                  "$COL_DEFAULT
echo -e $COL_GREEN"##                                                                              "$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo -e $COL_GREEN"################################################################################"$COL_DEFAULT
echo
